open Data

val interp : print:bool -> check:bool -> ?env:env -> (env_flag list * string) list -> unit
val ocaml_stdlib : print:bool -> check:bool -> unit
val ocaml_compiler : print:bool -> check:bool -> unit
