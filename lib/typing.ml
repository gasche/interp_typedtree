open Asttypes

module T = Typedtree
module P = Parsetree
module E = Env

exception Type_error

let fake_type = Ctype.none

let fake_path () = Path.Pident(Ident.create_local "")

let rec typing (parsed: P.structure) (typed: T.implementation) =
  match parsed with
  | item::items ->
    typing items (type_item (E.empty) item typed)
  | [] -> typed

and type_item (env: E.t) (structure: P.structure_item) (typed: T.implementation) =
  let pstr: P.structure_item_desc = structure.pstr_desc in
  make_item env structure typed (type_item_desc env pstr)

and type_item_desc (env: E.t) (pstr: P.structure_item_desc) =
  match pstr with
  | Pstr_eval(expr, att) -> T.Tstr_eval(type_expr env expr, att)
  | Pstr_value(flag, bindings) -> T.Tstr_value(flag, type_bindings env bindings)
  | Pstr_type(flag, decls) -> T.Tstr_type(flag, List.map (type_declaration env) decls)
  | Pstr_exception(except) -> T.Tstr_exception(type_exception env except)
  | Pstr_module(modl_binds) -> T.Tstr_module(type_module_binding env modl_binds)
  | Pstr_recmodule(modl_binds) -> T.Tstr_recmodule(List.map (type_module_binding env) modl_binds)
  | Pstr_modtype(modl_decl) -> T.Tstr_modtype(type_module_type_declaration env modl_decl)
  | Pstr_open(decl) -> T.Tstr_open(type_open_declaration env decl)
  | Pstr_class(l_decl) -> T.Tstr_class(List.map (fun decl -> (type_class_declaration env decl, [""])) l_decl)
  | Pstr_class_type(l_decl) -> T.Tstr_class_type(List.map (fun decl -> (Ident.create_local "", {txt = ""; loc = Location.none}, type_class_type_declaration env decl)) l_decl)
  | Pstr_include(decl) -> T.Tstr_include(type_include_declaration env decl)
  | Pstr_attribute(att) -> T.Tstr_attribute(att)
  | Pstr_extension _ -> failwith "Pstr_extension yet to be included in the hack"
  | Pstr_primitive(descr) -> T.Tstr_primitive(type_value_description env descr)
  | Pstr_typext _ -> failwith "Pstr_typext yet to be included in the hack"

and make_item (env: E.t) (structure: P.structure_item) (typed: T.implementation) (desc: T.structure_item_desc): T.implementation =
    let str_item: T.structure_item = {
      str_desc = desc;
      str_loc = structure.pstr_loc;
      str_env = env;
    } in
    let str: T.structure = {
      str_items = typed.structure.str_items@[str_item];
      str_type = [];
      str_final_env = env;
    } in
    let implem: T.implementation = {
      structure = str;
      coercion = Tcoerce_none;
      signature = [];
    } in implem

and type_expr (env: E.t) (expr: P.expression): T.expression =
  let desc: P.expression_desc = expr.pexp_desc in
  match desc with
  | Pexp_constraint(_, _)
  | Pexp_coerce(_, _, _)
  | Pexp_poly(_, _)
  | Pexp_newtype(_, _) -> make_expr_extra env (type_exp_extra env desc)
  | _ -> make_expr env expr (type_expr_desc env desc)

and type_expr_desc (env: E.t) (desc: P.expression_desc) : T.expression_desc =
  match desc with
  | Pexp_ident(name) -> T.Texp_ident(fake_path (), name, types_expr_description ())
  | Pexp_constant(x) -> T.Texp_constant(type_constant x)
  | Pexp_let(flag, bindings, expr) -> T.Texp_let(flag, type_bindings env bindings, type_expr env expr)
  | Pexp_fun(lbl, guard, pat, expr) ->
    let func = T.Texp_function( 
      { arg_label = lbl;
        param = Ident.create_local "f";
        cases = type_case env expr guard pat;
        partial = T.Total;
      } )
    in
    func
  | Pexp_function(cases) ->
    let func = T.Texp_function( 
      { arg_label = Nolabel;
        param = Ident.create_local "";
        cases = type_cases env cases;
        partial = T.Total;
      } )
    in
    func
  | Pexp_apply(func, args) -> T.Texp_apply(type_expr env func, type_args env args)
  | Pexp_match(expr, cases) ->
    begin
      let computations =
        let rec type_computation (env: E.t) (cases: P.case list): T.computation T.case list =
          match cases with
          | [] -> []
          | case::cases ->
            let lhs = T.as_computation_pattern (type_pattern env case.pc_lhs) in
            let case_: T.computation T.case =
            {
              c_lhs = lhs;
              c_guard = type_expr_option env case.pc_guard;
              c_rhs = type_expr env case.pc_rhs;
            }
            in case_::(type_computation env cases)
        in type_computation env cases
      in
      T.Texp_match(type_expr env expr, computations, Total)
    end
  | Pexp_try(expr, cases) -> T.Texp_try(type_expr env expr, type_cases env cases)
  | Pexp_tuple(exprs) ->
    let typed_exprs = List.map (type_expr env) exprs in
    (T.Texp_tuple(typed_exprs))
  | Pexp_construct(id, expr_) ->
    let l_expr =
      match expr_ with
      | None -> []
      | Some(e) -> [type_expr env e]
    in
    (T.Texp_construct(id, make_constructor id, l_expr))
  | Pexp_variant(lbl, expr_) -> T.Texp_variant(lbl, type_expr_option env expr_)
  | Pexp_record(ids, expr_) ->
    let f (id, expr) = types_label id, T.Overridden (id, type_expr env expr) in
    let record: T.expression_desc =
    Texp_record(
    {
      fields = Array.of_list (List.map f ids);
      representation = Types.Record_regular;
      extended_expression = type_expr_option env expr_;
    })
    in record
  | Pexp_field(expr_, id) -> (T.Texp_field(type_expr env expr_, id, types_label id))
  | Pexp_setfield(expr_, id, s_expr) -> (T.Texp_setfield(type_expr env expr_, id, types_label id, type_expr env s_expr))
  | Pexp_sequence(expr1, expr2) -> (T.Texp_sequence(type_expr env expr1, type_expr env expr2))
  | Pexp_array(exprs) -> (T.Texp_array(List.map (type_expr env) exprs))
  | Pexp_ifthenelse(if_expr, then_expr, else_expr) -> (T.Texp_ifthenelse(type_expr env if_expr, type_expr env then_expr, type_expr_option env else_expr))
  | Pexp_while(cond, body) -> (T.Texp_while(type_expr env cond, type_expr env body))
  | Pexp_for(pat, init, final, flag, expr) -> (T.Texp_for(Ident.create_local "for", pat, type_expr env init, type_expr env final, flag, type_expr env expr))
  | Pexp_send(expr, lbl) ->
    let msg: T.meth = Tmeth_name(lbl.txt) in
    T.Texp_send(type_expr env expr, msg, None)
  | Pexp_setinstvar(lbl, expr_) ->
    let path = fake_path () in
    (T.Texp_setinstvar(path, path, lbl, type_expr env expr_))
  | Pexp_override(l) -> type_override env desc l
  | Pexp_letmodule(lbl, modl, expr_) -> (T.Texp_letmodule(None, lbl, Mp_present, type_module env modl, type_expr env expr_))
  | Pexp_letexception(ext_construct, expr_) -> (T.Texp_letexception(type_extension_constructor env ext_construct, type_expr env expr_))
  | Pexp_assert(expr) -> (T.Texp_assert(type_expr env expr))
  | Pexp_lazy(expr) -> (T.Texp_lazy(type_expr env expr))
  | Pexp_object(cs) -> (T.Texp_object(type_class_structure env cs, []))
  | Pexp_pack(modl_expr) -> (T.Texp_pack(type_module env modl_expr))
  | Pexp_open(decl, expr_) -> T.Texp_open(type_open_declaration env decl, type_expr env expr_)
  | Pexp_unreachable -> T.Texp_unreachable
  | Pexp_new(id) -> (T.Texp_new(fake_path (), id, types_class_declaration_none ()))
  | Pexp_extension((lbl, _)) -> (T.Texp_extension_constructor({txt = Lident(lbl.txt); loc = lbl.loc}, fake_path ()))
  | Pexp_letop(ltp) ->
    begin
    let lhs: T.pattern =
      {
        pat_desc = Tpat_any;
        pat_loc = Location.none;
        pat_extra = [];
        pat_type = fake_type;
        pat_env = env;
        pat_attributes = ltp.body.pexp_attributes;
      }
    in
    let body: 'a T.case =
      {
        c_lhs = lhs;
        c_guard = None;
        c_rhs = type_expr env ltp.body;
      }
    in
    T.Texp_letop{
      let_ = type_binding_op env ltp.let_;
      ands = List.map (type_binding_op env) ltp.ands;
      param = Ident.create_local "";
      body = body;
      partial = T.Total;
    }
    end
  | _ -> failwith "New expression to add\n"


and type_exp_extra (env: E.t) (extra: P.expression_desc): P.expression * T.exp_extra =
  match extra with
  | Pexp_constraint(expr, core) -> expr, T.Texp_constraint(type_core env core)
  | Pexp_coerce(expr, core_option, core) -> expr, T.Texp_coerce(type_core_option env core_option, type_core env core)
  | Pexp_poly(expr, core_option) -> expr, T.Texp_poly(type_core_option env core_option)
  | Pexp_newtype(lbl, expr) -> expr, T.Texp_newtype(lbl.txt)
  | _ -> failwith "New expression to add\n"

and make_expr (env: E.t) (expr: P.expression) (desc: T.expression_desc): T.expression =
  { 
    exp_desc = desc;
    exp_loc = expr.pexp_loc;
    exp_extra = [];
    exp_type = fake_type;
    exp_env = env;
    exp_attributes = expr.pexp_attributes;
  }

and make_expr_extra (env: E.t) ((expr, extra): P.expression * T.exp_extra): T.expression =
  { 
    exp_desc = (type_expr env expr).exp_desc;
    exp_loc = expr.pexp_loc;
    exp_extra = [(extra, expr.pexp_loc, expr.pexp_attributes)];
    exp_type = fake_type;
    exp_env = env;
    exp_attributes = expr.pexp_attributes;
  }

and type_expr_option (env: E.t) (expr: P.expression option): T.expression option =
  match expr with
  | None -> None
  | Some e -> Some (type_expr env e)

and type_constant (x: P.constant): constant =
  match x with
  | Pconst_integer(s, None) ->
      let n: int = int_of_string s in Const_int(n)
  | Pconst_integer(s, Some('l')) ->
      let n: int32 = Int32.of_string s in Const_int32(n)
  | Pconst_integer(s, Some('L')) ->
      let n: int64 = Int64.of_string s in Const_int64(n)
  | Pconst_integer(s, Some('n')) ->
      let n: nativeint = Nativeint.of_string s in Const_nativeint(n)
  | Pconst_integer(_, Some(c)) ->
      Printf.ksprintf invalid_arg "There is no '%c' literal suffix" c
  | Pconst_char(c) -> Const_char(c)
  | Pconst_string(s, l, opt) -> Const_string(s, l, opt)
  | Pconst_float(s, None) -> Const_float(s)
  | Pconst_float(_, Some(c)) ->
      Printf.ksprintf invalid_arg "There is no '%c' literal suffix" c

and type_bindings (env: E.t) (bindings: P.value_binding list): T.value_binding list =
  match bindings with
  | [] -> []
  | bind::bindings ->
    let typed_bind: T.value_binding = {
      vb_pat = type_pattern env bind.pvb_pat;
      vb_expr = type_expr env bind.pvb_expr;
      vb_attributes = bind.pvb_attributes;
      vb_loc = bind.pvb_loc;
    }
    in typed_bind::(type_bindings env bindings)

and type_pattern (env: E.t) (pat: P.pattern): T.pattern =
  let desc: P.pattern_desc = pat.ppat_desc in
  match desc with
  | Ppat_constraint(pat_, core) ->
      make_pattern_extra env pat pat_ (T.Tpat_constraint(type_core env core))
  | Ppat_type(_) -> failwith "Unsupported Ppat_type"
  | Ppat_unpack(_) -> failwith "Unsupported Ppat_unpack"
  | Ppat_open(id, pat_) ->
      make_pattern_extra env pat pat_ (T.Tpat_open(fake_path (), id, env))
  | Ppat_exception(pat_) -> type_pattern env pat_
  | _-> type_pattern_value env pat

and type_pattern_value (env: E.t) (pat: P.pattern): T.pattern =
  let rec type_pattern_desc (env: E.t) (desc: P.pattern_desc): 'a T.pattern_desc =
    match desc with
    | Ppat_any -> T.Tpat_any
    | Ppat_var(lbl) -> T.Tpat_var(Ident.create_local lbl.txt, lbl)
    | Ppat_alias(pat, lbl) -> T.Tpat_alias(type_pattern env pat, Ident.create_local lbl.txt, lbl)
    | Ppat_constant(x) -> T.Tpat_constant(type_constant x)
    | Ppat_interval(x, y) ->
      begin
        let next = function
          | P.Pconst_char(c) ->
              let n: int = int_of_char c in
              let next_ = n + 1 in
              P.Pconst_char(char_of_int next_)
          | _ -> failwith "Unsupported interval pattern\n"
        in
        if (x = y) then
          T.Tpat_constant(type_constant x)
        else
          let desc1 = type_pattern_desc env (Ppat_constant(x)) in
          let pat1 = make_pattern env pat desc1 in
          let desc2 = type_pattern_desc env (Ppat_interval(next x, y)) in
          let pat2 = make_pattern env pat desc2 in
          T.Tpat_or(pat1, pat2, None)
      end
    | Ppat_tuple(l_pats) -> T.Tpat_tuple(List.map (type_pattern env) l_pats)
    | Ppat_construct(id, l_pat) ->
      begin
        let nl_pat =
          match l_pat with
          | None -> []
          | Some(_, pat) -> [type_pattern env pat]
        in
        T.Tpat_construct(id, make_constructor id, nl_pat, None)
      end
    | Ppat_variant(lbl, pat_option) ->
      begin
        let n_pat =
          match pat_option with
          | None -> None
          | Some(pat) -> Some(type_pattern env pat)
        in
        T.Tpat_variant(lbl, n_pat, types_row_desc ())
      end
    | Ppat_record(l_pats, flag) -> (T.Tpat_record(List.map (fun (id, pat) -> (id, types_label_pat id pat, type_pattern env pat)) l_pats, flag))
    | Ppat_array(l_pats) -> (T.Tpat_array(List.map (type_pattern env) l_pats))
    | Ppat_or(pat1, pat2) -> (T.Tpat_or(type_pattern env pat1, type_pattern env pat2, None))
    | Ppat_lazy(pat) -> (T.Tpat_lazy(type_pattern env pat))
    | Ppat_extension(_) -> failwith "Ppat_extension has no equivalent\n"
    | _ -> failwith "New pattern to add"
  in make_pattern env pat (type_pattern_desc env pat.ppat_desc)

and make_pattern (env: E.t) (pat: P.pattern) (desc: 'a T.pattern_desc): T.pattern =
  {
    pat_desc = desc;
    pat_loc = pat.ppat_loc;
    pat_extra = [];
    pat_type = fake_type;
    pat_env = env;
    pat_attributes = pat.ppat_attributes;
  }

and make_pattern_extra (env: E.t) (pat: P.pattern) (pat_: P.pattern) (extra: T.pat_extra): T.pattern =
    {
      pat_desc = (type_pattern env pat_).pat_desc;
      pat_loc = pat.ppat_loc;
      pat_extra = [(extra, pat.ppat_loc, pat.ppat_attributes)];
      pat_type = fake_type;
      pat_env = env;
      pat_attributes = pat.ppat_attributes;
    }

and make_constructor (id: Longident.t loc): Types.constructor_description =
  { cstr_name = Longident.last id.txt;
    cstr_normal = 0;
    cstr_res = fake_type;
    cstr_existentials = [];
    cstr_args = [];
    cstr_arity = 10;
    cstr_tag = Cstr_constant(0);
    cstr_consts = 0;
    cstr_nonconsts = 0;
    cstr_generalized = false;
    cstr_private = Public;
    cstr_loc = id.loc;
    cstr_attributes = [];
    cstr_inlined = None;
    cstr_uid = Types.Uid.mk ~current_unit:"";
   }

and type_cases (env: E.t) (cases: P.case list): 'a T.case list =
  match cases with
  | [] -> []
  | case::cases ->
    let typed_case: 'a T.case =
    {
      c_lhs = type_pattern env case.pc_lhs;
      c_guard = type_expr_option env case.pc_guard;
      c_rhs = type_expr env case.pc_rhs;
    }
    in typed_case::(type_cases env cases)

and type_case (env: E.t) (expr: P.expression) (guard: P.expression option) (pat: P.pattern): 'a T.case list =
  let pat_desc: 'a T.pattern_desc = (type_pattern env pat).pat_desc
  in
  let gen_pat: 'a T.pattern_desc T.pattern_data =
  { pat_desc = pat_desc;
    pat_loc = expr.pexp_loc;
    pat_extra = [];
    pat_type = fake_type;
    pat_env = env;
    pat_attributes = expr.pexp_attributes;
  }
  in
  [
  { c_lhs = gen_pat;
    c_guard = type_expr_option env guard;
    c_rhs = type_expr env expr;
  }
  ]

and type_args (env: E.t) (args: (arg_label * P.expression) list): (arg_label * T.expression option) list =
  match args with
  | [] -> []
  | (lbl, expr)::args ->
    let typed_expr: T.expression option = Some(type_expr env expr) in
    (lbl, typed_expr)::(type_args env args)

and type_override (env: E.t) (expr: P.expression_desc) (l: (label loc * P.expression) list) =
  let rec aux (env: E.t) (expr: P.expression_desc) (l: (label loc * P.expression) list) (t_l: (Path.t * label loc * T.expression) list): T.expression_desc =
    let path = fake_path () in
    match l with
    | [] -> T.Texp_override(path, t_l)
    | (lbl, expr_)::t -> aux env expr t ((path, lbl, (type_expr env expr_))::t_l)
  in aux env expr l []

and type_module (env: E.t) (modl: P.module_expr): T.module_expr =
  {
  mod_desc = type_module_desc env modl.pmod_desc;
  mod_loc = modl.pmod_loc;
  mod_type = Mty_ident(fake_path ());
  mod_env = env;
  mod_attributes = modl.pmod_attributes;
  }

and type_module_desc (env: E.t) (modl: P.module_expr_desc): T.module_expr_desc =
  match modl with
  | Pmod_ident(id) -> Tmod_ident(fake_path () ,id)
  | Pmod_structure(structure) -> Tmod_structure(type_structure structure)
  | Pmod_functor(param, expr) -> Tmod_functor(type_functor env param, type_module env expr)
  | Pmod_apply(expr1, expr2) -> Tmod_apply(type_module env expr1, type_module env expr2, Tcoerce_none)
  | Pmod_constraint(expr, modl_type) -> Tmod_constraint(type_module env expr, types_module_type env modl_type, Tmodtype_implicit, Tcoerce_none)
  | Pmod_unpack(expr) -> Tmod_unpack(type_expr env expr, Mty_ident(fake_path ()))
  | Pmod_extension _ -> failwith "Pmod_extension has no equivalent"

and type_functor (env: E.t) (param: P.functor_parameter): T.functor_parameter =
  match param with
  | Unit -> Unit
  | Named(lbl, mod_type) -> Named(None, lbl, type_module_type env mod_type)

and type_module_type (env: E.t) (modl_type: P.module_type): T.module_type =
  {
    mty_desc = type_module_type_desc env modl_type.pmty_desc;
    mty_type = Mty_ident(fake_path ());
    mty_env = env;
    mty_loc = modl_type.pmty_loc;
    mty_attributes = modl_type.pmty_attributes;
  }

and types_module_type (env: E.t) (modl_type: P.module_type): Types.module_type =
  let desc: P.module_type_desc = modl_type.pmty_desc in
  match desc with
  | Pmty_ident(_) -> Types.Mty_ident(fake_path ())
  | Pmty_signature(_) -> Types.Mty_signature([])
  | Pmty_functor(_, modl_type) -> Types.Mty_functor(Unit, types_module_type env modl_type)
  | Pmty_alias(_) -> Types.Mty_alias(fake_path ())
  | Pmty_with(_, _) -> failwith "Pmty_with has no equivalent"
  | Pmty_typeof(_) -> failwith "Pmty_typeof has no equivalent"
  | Pmty_extension(_) -> failwith "Pmty_extension has no equivalent"

and type_module_type_desc (env: E.t) (modl_type: P.module_type_desc): T.module_type_desc =
  match modl_type with
  | Pmty_ident(id) -> Tmty_ident(fake_path (), id)
  | Pmty_signature(s) -> Tmty_signature(type_signature env s)
  | Pmty_functor(param, expr) -> Tmty_functor(type_functor env param, type_module_type env expr)
  | Pmty_with(modl, l_const) -> Tmty_with(type_module_type env modl, type_constraints env l_const)
  | Pmty_typeof(expr) -> Tmty_typeof(type_module env expr)
  | Pmty_alias(id) -> Tmty_alias(fake_path (), id)
  | Pmty_extension(_) -> failwith "Pmty_extension has no equivalent"

and type_signature (env: E.t) (s: P.signature): T.signature =
  { sig_items = List.map (type_signature_item env) s;
    sig_type = [];
    sig_final_env = env;
  }

and type_signature_item (env: E.t) (s: P.signature_item): T.signature_item =
  let desc: P.signature_item_desc = s.psig_desc in
  match desc with
  | Psig_value(descr) -> make_sid env s (T.Tsig_value(type_value_description env descr))
  | Psig_type(flag, l_decl) -> make_sid env s (T.Tsig_type(flag, List.map (type_declaration env) l_decl))
  | Psig_typesubst(l_decl) -> make_sid env s (T.Tsig_typesubst(List.map (type_declaration env) l_decl))
  | Psig_typext(extension) -> make_sid env s (T.Tsig_typext(type_extension env extension))
  | Psig_exception(excep) -> make_sid env s (T.Tsig_exception(type_exception env excep))
  | Psig_module(decl) -> make_sid env s (T.Tsig_module(type_module_declaration env decl))
  | Psig_modsubst(substitution) -> make_sid env s (T.Tsig_modsubst(type_module_substitution  substitution))
  | Psig_recmodule(l_decl) -> make_sid env s (T.Tsig_recmodule(List.map (type_module_declaration env) l_decl))
  | Psig_modtype(decl) -> make_sid env s (T.Tsig_modtype(type_module_type_declaration env decl))
  | Psig_modtypesubst(decl) -> make_sid env s (T.Tsig_modtypesubst(type_module_type_declaration env decl))
  | Psig_open(descr) -> make_sid env s (T.Tsig_open(type_open_description env descr))
  | Psig_include(descr) -> make_sid env s (T.Tsig_include(type_include_description env descr))
  | Psig_class(l_descr) -> make_sid env s (T.Tsig_class(List.map (type_class_description env) l_descr))
  | Psig_class_type(l_decl) -> make_sid env s (T.Tsig_class_type(List.map (type_class_type_declaration env) l_decl))
  | Psig_attribute(att) -> make_sid env s (T.Tsig_attribute(att))
  | Psig_extension(_) -> failwith "Psig_extension has no equivalent"

and make_sid (env: E.t) (s: P.signature_item) (item: T.signature_item_desc): T.signature_item =
  {
    sig_desc = item;
    sig_env = env;
    sig_loc = s.psig_loc;
  }

and type_constraints (env: E.t) (l_const: P.with_constraint list): (Path.t * Longident.t loc * T.with_constraint) list =
  match l_const with
  | [] -> []
  | const::constraints ->
    (type_with_constraint env const)::(type_constraints env constraints)

and type_with_constraint (env: E.t) (const: P.with_constraint): (Path.t * Longident.t loc * T.with_constraint) =
  match const with
  | Pwith_type(ib, declaration) -> (fake_path (), ib, Twith_type(type_declaration env declaration))
  | Pwith_module(id1, id2) -> (fake_path (), id1, Twith_module(fake_path (), id2))
  | Pwith_modtype(id, module_type) -> (fake_path (), id, Twith_modtype(type_module_type env module_type))
  | Pwith_modtypesubst(id, module_type) -> (fake_path (), id, Twith_modtypesubst(type_module_type env module_type))
  | Pwith_typesubst(id, declaration) -> (fake_path (), id, Twith_typesubst(type_declaration env declaration))
  | Pwith_modsubst(id1, id2) -> (fake_path (), id1, Twith_modsubst(fake_path (), id2))

and type_declaration (env: E.t) (decl: P.type_declaration): T.type_declaration =
  {
    typ_id = Ident.create_local "";
    typ_name = decl.ptype_name;
    typ_params = List.map (fun (core, (v, i)) -> (type_core env core, (v, i))) decl.ptype_params;
    typ_type = types_declaration decl;
    typ_cstrs = List.map (fun (core1, core2, loc) -> (type_core env core1, type_core env core2, loc)) decl.ptype_cstrs;
    typ_kind = type_kind env decl.ptype_kind;
    typ_private = decl.ptype_private;
    typ_manifest = type_core_option env decl.ptype_manifest;
    typ_loc = decl.ptype_loc;
    typ_attributes = decl.ptype_attributes;
  }

and type_value_description (env: E.t) (value: P.value_description): T.value_description =
  {
    val_id = Ident.create_local "";
    val_name = value.pval_name;
    val_desc = type_core env value.pval_type;
    val_val = types_value_description value;
    val_prim = value.pval_prim;
    val_loc = value.pval_loc;
    val_attributes = value.pval_attributes;
  }

and type_kind (env: E.t) (kind: P.type_kind): T.type_kind =
  match kind with
  | Ptype_abstract -> Ttype_abstract
  | Ptype_variant(l_const) -> Ttype_variant(List.map (type_constructor_declaration env) l_const)
  | Ptype_record(l_decl) -> Ttype_record(List.map (type_label_declaration env) l_decl)
  | Ptype_open -> Ttype_open

and type_label_declaration (env: E.t) (decl: P.label_declaration): T.label_declaration =
  {
    ld_id = Ident.create_local "";
    ld_name = decl.pld_name;
    ld_mutable = decl.pld_mutable;
    ld_type = type_core env decl.pld_type;
    ld_loc = decl.pld_loc;
    ld_attributes = decl.pld_attributes;
  }

and type_core (env: E.t) (pcore: P.core_type): T.core_type =
  {
    ctyp_desc = type_core_desc env pcore.ptyp_desc;
    ctyp_type = fake_type;
    ctyp_env = env;
    ctyp_loc = pcore.ptyp_loc;
    ctyp_attributes = pcore.ptyp_attributes;
  }

and type_core_option (env: E.t) (pcore: P.core_type option): T.core_type option =
  match pcore with
  | None -> None
  | Some(pcore) -> Some(type_core env pcore)

and type_core_desc (env: E.t) (core_desc: P.core_type_desc): T.core_type_desc =
  match core_desc with
  | Ptyp_any -> Ttyp_any
  | Ptyp_var(lbl) -> Ttyp_var(lbl)
  | Ptyp_arrow(arg_labe, core1, core2) -> Ttyp_arrow(arg_labe, type_core env core1, type_core env core2)
  | Ptyp_tuple(l_cores) -> Ttyp_tuple(List.map (type_core env) l_cores)
  | Ptyp_constr(id, l_cores) -> Ttyp_constr(fake_path (), id, List.map (type_core env) l_cores)
  | Ptyp_object(l_ogj, flag) -> Ttyp_object(List.map (type_object_field env) l_ogj, flag)
  | Ptyp_class(id, l_cores) -> Ttyp_class(fake_path (), id, List.map (type_core env) l_cores)
  | Ptyp_alias(core, lbl) -> Ttyp_alias(type_core env core, lbl)
  | Ptyp_variant(l_field, flag, l_lbl) -> Ttyp_variant(List.map (type_row_field env) l_field, flag, l_lbl)
  | Ptyp_poly(l_lbl, core_t) -> Ttyp_poly(List.map (fun lbl -> lbl.txt) l_lbl, type_core env core_t)
  | Ptyp_package(package) -> Ttyp_package(type_package env package)
  | Ptyp_extension(_, _) -> failwith "Ptyp_extension has no equivalent"

and type_package (env: E.t) ((id, l_packages): P.package_type): T.package_type =
  {
    pack_path = fake_path ();
    pack_fields = List.map (fun (id, core) -> (id, type_core env core)) l_packages;
    pack_type = Mty_ident(fake_path ());
    pack_txt = id;
  }

and type_row_field (env: E.t) (field: P.row_field): T.row_field =
  {
    rf_desc = type_row_field_desc env field.prf_desc;
    rf_loc = field.prf_loc;
    rf_attributes = field.prf_attributes;
  }

and type_row_field_desc (env: E.t) (field_desc: P.row_field_desc): T.row_field_desc =
  match field_desc with
  | Rtag(lbl, b, l_cores) -> Ttag(lbl, b, List.map (type_core env) l_cores)
  | Rinherit(core) -> Tinherit(type_core env core)

and type_object_field (env: E.t) (field: P.object_field): T.object_field =
  {
    of_desc = type_object_field_desc env field.pof_desc;
    of_loc = field.pof_loc;
    of_attributes = field.pof_attributes;
  }

and type_object_field_desc (env: E.t) (field_desc: P.object_field_desc): T.object_field_desc =
  match field_desc with
  | Otag(lbl, core) -> OTtag(lbl, type_core env core)
  | Oinherit(core) -> OTinherit(type_core env core)

and type_constructor_declaration (env: E.t) (decl: P.constructor_declaration): T.constructor_declaration =
  {
    cd_id = Ident.create_local "";
    cd_name = decl.pcd_name;
    cd_args = type_constructor_args env decl.pcd_args;
    cd_res = type_core_option env decl.pcd_res;
    cd_loc = decl.pcd_loc;
    cd_attributes = decl.pcd_attributes;
  }

and type_constructor_args (env: E.t) (args: P.constructor_arguments) : T.constructor_arguments =
  match args with
  | Pcstr_tuple(l_cores) -> Cstr_tuple(List.map (type_core env) l_cores)
  | Pcstr_record(l_decl) -> Cstr_record(List.map (type_label_declaration env) l_decl)

and type_extension (env: E.t) (ext: P.type_extension): T.type_extension =
  {
    tyext_path = fake_path ();
    tyext_txt = ext.ptyext_path;
    tyext_params = List.map (fun (core, (v, i)) -> (type_core env core, (v, i))) ext.ptyext_params;
    tyext_constructors = List.map (type_extension_constructor env) ext.ptyext_constructors;
    tyext_private = ext.ptyext_private;
    tyext_loc = ext.ptyext_loc;
    tyext_attributes = ext.ptyext_attributes;
  }

and type_extension_constructor (env: E.t) (decl: P.extension_constructor): T.extension_constructor =
  {
    ext_id = Ident.create_local "";
    ext_name = decl.pext_name;
    ext_type = types_extension_constructor decl;
    ext_kind = type_constructor_kind env decl.pext_kind;
    ext_loc = decl.pext_loc;
    ext_attributes = decl.pext_attributes;
  }

and type_constructor_kind (env: E.t) (kind: P.extension_constructor_kind): T.extension_constructor_kind =
  match kind with
  | Pext_decl(args, core_option) -> Text_decl(type_constructor_args env args, type_core_option env core_option)
  | Pext_rebind(id) -> Text_rebind(fake_path (), id)

and type_exception (env: E.t) (ex: P.type_exception): T.type_exception =
  {
    tyexn_constructor = type_extension_constructor env ex.ptyexn_constructor;
    tyexn_loc = ex.ptyexn_loc;
    tyexn_attributes = ex.ptyexn_attributes;
  }

and type_module_declaration (env: E.t) (decl: P.module_declaration): T.module_declaration =
  {
    md_id = None;
    md_name = decl.pmd_name;
    md_presence = Mp_present;
    md_type = type_module_type env decl.pmd_type;
    md_attributes = decl.pmd_attributes;
    md_loc = decl.pmd_loc;
  }

and type_module_substitution (subst: P.module_substitution): T.module_substitution =
  {
  ms_id = Ident.create_local "";
  ms_name = subst.pms_name;
  ms_manifest = fake_path ();
  ms_txt = subst.pms_manifest;
  ms_attributes = subst.pms_attributes;
  ms_loc = subst.pms_loc;
  }

and type_module_type_declaration (env: E.t) (decl: P.module_type_declaration): T.module_type_declaration =
  {
    mtd_id = Ident.create_local "";
    mtd_name = decl.pmtd_name;
    mtd_type =
    begin
    match decl.pmtd_type with
    | None -> None
    | Some(mdl) -> Some(type_module_type env mdl)
    end;
    mtd_attributes = decl.pmtd_attributes;
    mtd_loc = decl.pmtd_loc;
  }

and type_open_description (env: E.t) (desc: P.open_description): T.open_description =
  let aux (env: E.t) (infos: Longident.t loc P.open_infos): (Path.t * Longident.t loc) T.open_infos =
    {
      open_expr = (fake_path (), infos.popen_expr);
      open_bound_items = [];
      open_override = infos.popen_override;
      open_env = env;
      open_loc = infos.popen_loc;
      open_attributes = infos.popen_attributes;
    }
  in aux env desc

and type_include_description (env: E.t) (descr: P.include_description): T.include_description =
  let aux (env: E.t) (modl_type: P.module_type P.include_infos): T.module_type T.include_infos =
    {
      incl_mod = type_module_type env modl_type.pincl_mod;
      incl_type = [];
      incl_loc = modl_type.pincl_loc;
      incl_attributes = modl_type.pincl_attributes;
    }
  in aux env descr

and type_class_description (env: E.t) (desc: P.class_description): T.class_description =
  let aux (env: E.t) (class_type: P.class_type P.class_infos): T.class_type T.class_infos =
    {
      ci_virt = class_type.pci_virt;
      ci_params  = List.map (fun (core, (v, i)) -> (type_core env core, (v, i))) class_type.pci_params;
      ci_id_name = class_type.pci_name;
      ci_id_class = Ident.create_local "";
      ci_id_class_type = Ident.create_local "";
      ci_id_object = Ident.create_local "";
      ci_id_typehash = Ident.create_local "";
      ci_expr = type_class_type env class_type.pci_expr;
      ci_decl = types_class_declaration class_type;
      ci_type_decl = types_class_type_declaration class_type;
      ci_loc = class_type.pci_loc;
      ci_attributes = class_type.pci_attributes;
    }
  in aux env desc

and type_class_type (env: E.t) (class_type: P.class_type): T.class_type =
  {
    cltyp_desc = type_class_type_desc env class_type.pcty_desc;
    cltyp_type = Cty_signature(types_class_signature ());
    cltyp_env = env;
    cltyp_loc = class_type.pcty_loc;
    cltyp_attributes = class_type.pcty_attributes;
  }

and type_class_type_desc (env: E.t) (desc: P.class_type_desc): T.class_type_desc =
  match desc with
  | Pcty_constr(id, l_cores) -> Tcty_constr(fake_path (), id, List.map (fun (core) -> type_core env core) l_cores)
  | Pcty_signature(cs) -> Tcty_signature(type_class_signature env cs)
  | Pcty_arrow(lbl, core, ct) -> Tcty_arrow(lbl, type_core env core, type_class_type env ct)
  | Pcty_open(open_descr, ct) -> Tcty_open(type_open_description env open_descr, type_class_type env ct)
  | Pcty_extension(_) -> failwith "Pcty_extension has no equivalent in T.class_type_desc"

and type_class_signature (env: E.t) (cs: P.class_signature): T.class_signature =
  {
    csig_self = type_core env cs.pcsig_self;
    csig_fields = List.map (type_class_type_field env) cs.pcsig_fields;
    csig_type = types_class_signature ();
  }

and type_class_type_field (env: E.t) (fld: P.class_type_field): T.class_type_field =
  {
    ctf_desc = type_class_type_field_desc env fld.pctf_desc;
    ctf_loc = fld.pctf_loc;
    ctf_attributes = fld.pctf_attributes;
  }

and type_class_type_field_desc (env: E.t) (desc: P.class_type_field_desc): T.class_type_field_desc =
  match desc with
  | Pctf_inherit(ct) -> Tctf_inherit(type_class_type env ct)
  | Pctf_val(lbl, m_flag, v_flag, core) -> Tctf_val(lbl.txt, m_flag, v_flag, type_core env core)
  | Pctf_method(lbl, p_flag, v_flag, core) -> Tctf_method(lbl.txt, p_flag, v_flag, type_core env core)
  | Pctf_constraint(core1, core2) -> Tctf_constraint(type_core env core1, type_core env core2)
  | Pctf_attribute(att) -> Tctf_attribute(att)
  | Pctf_extension(_) -> failwith "Pctf_extension has no equivalent in T.class_type_field_desc"

and type_class_type_declaration (env: E.t) (decl: P.class_type_declaration): T.class_type_declaration =
  let aux (env: E.t) (class_type: P.class_type P.class_infos): T.class_type T.class_infos =
    {
      ci_virt = class_type.pci_virt;
      ci_params  = List.map (fun (core, (v, i)) -> (type_core env core, (v, i))) class_type.pci_params;
      ci_id_name = class_type.pci_name;
      ci_id_class = Ident.create_local "";
      ci_id_class_type = Ident.create_local "";
      ci_id_object = Ident.create_local "";
      ci_id_typehash = Ident.create_local "";
      ci_expr = type_class_type env class_type.pci_expr;
      ci_decl = types_class_declaration class_type;
      ci_type_decl = types_class_type_declaration class_type;
      ci_loc = class_type.pci_loc;
      ci_attributes = class_type.pci_attributes;
    }
  in aux env decl

and type_class_structure (env: E.t) (structure: P.class_structure): T.class_structure =
  {
    cstr_self = type_pattern env structure.pcstr_self;
    cstr_fields = List.map (type_class_field env) structure.pcstr_fields;
    cstr_type = types_class_signature ();
    cstr_meths = Types.Meths.empty;
  }

and type_class_field (env: E.t) (fld: P.class_field): T.class_field =
  {
    cf_desc = type_class_field_desc env fld.pcf_desc;
    cf_loc = fld.pcf_loc;
    cf_attributes = fld.pcf_attributes;
  }

and type_class_field_desc (env: E.t) (desc: P.class_field_desc): T.class_field_desc =
  match desc with
  | Pcf_inherit(flag, ce, lbl) ->
    begin
    let l =
      match lbl with
      | None -> None
      | Some(lbl) -> Some(lbl.txt)
    in
    Tcf_inherit(flag, type_class_expr env ce, l, [], [])
    end
  | Pcf_val(lbl, m_flag, cfk) -> Tcf_val(lbl, m_flag, Ident.create_local "", type_class_field_kind env cfk, true)
  | Pcf_method(lbl, p_flag, cfk) -> Tcf_method(lbl, p_flag, type_class_field_kind env cfk)
  | Pcf_constraint(core1, core2) -> Tcf_constraint(type_core env core1, type_core env core2)
  | Pcf_initializer(expr) -> Tcf_initializer(type_expr env expr)
  | Pcf_attribute(att) -> Tcf_attribute(att)
  | Pcf_extension(_) -> failwith "Pcf_extension has no equivalent in T.class_field_desc"

and type_class_expr (env: E.t) (expr: P.class_expr): T.class_expr =
  {
    cl_desc = type_class_expr_desc env expr.pcl_desc;
    cl_loc = expr.pcl_loc;
    cl_type = Cty_signature(types_class_signature ());
    cl_env = env;
    cl_attributes = expr.pcl_attributes;
  }

and type_class_expr_desc (env: E.t) (desc: P.class_expr_desc): T.class_expr_desc =
  match desc with
  | Pcl_constr(id, l_cores) -> Tcl_ident(fake_path (), id, List.map (fun (core) -> type_core env core) l_cores)
  | Pcl_structure(cs) -> Tcl_structure(type_class_structure env cs)
  | Pcl_fun(lbl, expr_option, pat, ce) ->
    begin
    let l_expr =
      match expr_option with
      | None -> []
      | Some(expr) -> [(Ident.create_local "", type_expr env expr)]
      in
    Tcl_fun(lbl, type_pattern env pat, l_expr, type_class_expr env ce, Total)
    end
  | Pcl_apply(ce, l_expr) -> Tcl_apply(type_class_expr env ce, List.map (fun (arg, expr) -> (arg, Some(type_expr env expr))) l_expr)
  | Pcl_let(flag, bindings, ce) -> Tcl_let(flag, type_bindings env bindings, [], type_class_expr env ce)
  | Pcl_constraint(ce, ct) -> Tcl_constraint(type_class_expr env ce, Some(type_class_type env ct), [], [], Types.Concr.empty)
  | Pcl_open(descr, ce) -> Tcl_open(type_open_description env descr, type_class_expr env ce)
  | Pcl_extension(_) -> failwith "Pcl_extension has no equivalent in T.class_expr_desc"

and type_class_field_kind (env: E.t) (kind: P.class_field_kind): T.class_field_kind =
  match kind with
  | Cfk_virtual(core) -> Tcfk_virtual(type_core env core)
  | Cfk_concrete(falg, expr) -> Tcfk_concrete(falg, type_expr env expr)

and type_open_declaration (env: E.t) (decl: P.open_declaration): T.open_declaration =
  let aux (env: E.t) (open_decl: P.module_expr P.open_infos): T.module_expr T.open_infos =
    {
      open_expr = type_module_expr env open_decl.popen_expr;
      open_bound_items = [];
      open_override = open_decl.popen_override;
      open_env = env;
      open_loc = open_decl.popen_loc;
      open_attributes = open_decl.popen_attributes;
    }
  in aux env decl

and type_module_expr (env: E.t) (expr: P.module_expr): T.module_expr =
  {
    mod_desc = type_module_expr_desc env expr.pmod_desc;
    mod_loc = expr.pmod_loc;
    mod_type = Mty_signature([]);
    mod_env = env;
    mod_attributes = expr.pmod_attributes;
  }

and type_module_expr_desc (env: E.t) (desc: P.module_expr_desc): T.module_expr_desc =
  match desc with
  | Pmod_ident(id) -> Tmod_ident(fake_path (), id)
  | Pmod_structure(structure) -> Tmod_structure(type_structure structure)
  | Pmod_functor(param, modl_expr) -> Tmod_functor(type_functor env param, type_module_expr env modl_expr)
  | Pmod_apply(modl_expr1, modl_expr2) -> Tmod_apply(type_module_expr env modl_expr1, type_module_expr env modl_expr2, Tcoerce_none)
  | Pmod_constraint(modl_expr, modl_type) -> Tmod_constraint(type_module_expr env modl_expr, Mty_signature([]), Tmodtype_explicit(type_module_type env modl_type), Tcoerce_none)
  | Pmod_unpack(expr) -> Tmod_unpack(type_expr env expr, Mty_signature([]))
  | Pmod_extension(_) -> failwith "Pmod_extension has no equivalent in T.module_expr_desc"

and type_module_binding (env: E.t) (binding: P.module_binding): T.module_binding =
  {
    mb_id = None;
    mb_name = binding.pmb_name;
    mb_presence = Mp_present;
    mb_expr = type_module_expr env binding.pmb_expr;
    mb_attributes = binding.pmb_attributes;
    mb_loc = binding.pmb_loc;
  }

and type_class_declaration (env: E.t) (decl: P.class_declaration): T.class_declaration =
  let aux (env: E.t) (class_decl: P.class_expr P.class_infos): T.class_expr T.class_infos =
    {
      ci_virt = class_decl.pci_virt;
      ci_params = List.map (fun (core, (v, i)) -> (type_core env core, (v, i))) class_decl.pci_params;
      ci_id_name = class_decl.pci_name;
      ci_id_class = Ident.create_local "";
      ci_id_class_type = Ident.create_local "";
      ci_id_object = Ident.create_local "";
      ci_id_typehash = Ident.create_local "";
      ci_expr = type_class_expr env class_decl.pci_expr;
      ci_decl = types_class_expr_declaration class_decl;
      ci_type_decl = types_class_expr_type_declaration class_decl;
      ci_loc = class_decl.pci_loc;
      ci_attributes = class_decl.pci_attributes;
    }
  in aux env decl

and type_include_declaration (env: E.t) (decl: P.include_declaration): T.include_declaration =
  let aux (env: E.t) (incl: P.module_expr P.include_infos): T.module_expr T.include_infos =
  {
    incl_mod = type_module_expr env incl.pincl_mod;
    incl_type = [];
    incl_loc = incl.pincl_loc;
    incl_attributes = incl.pincl_attributes;
  }
  in aux env decl

and type_binding_op (env: E.t) (option: P.binding_op): T.binding_op =
  {
    bop_op_path = fake_path ();
    bop_op_name = option.pbop_op;
    bop_op_val = types_value_description_none ();
    bop_op_type = fake_type;
    bop_exp = type_expr env option.pbop_exp;
    bop_loc = option.pbop_loc;
  }

and types_expr_description (): Types.value_description =
  {
    val_type = fake_type;
    val_kind = Val_reg;
    val_loc = {
                loc_start = Lexing.dummy_pos;
                loc_end = Lexing.dummy_pos;
                loc_ghost = false;
              };
    val_attributes = [];
    val_uid = Types.Uid.mk ~current_unit:"";
  }

and types_label (name: Longident.t loc): Types.label_description =
  {
    lbl_name = Longident.last name.txt;
    lbl_res = fake_type;
    lbl_arg = fake_type;
    lbl_mut = Immutable;
    lbl_pos = 0;
    lbl_all = [||];
    lbl_repres = Record_regular;
    lbl_private = Public;
    lbl_loc = name.loc;
    lbl_attributes = [];
    lbl_uid = Types.Uid.mk ~current_unit:"";
  }

and types_label_pat (id: Longident.t loc) (pat: P.pattern): Types.label_description =
  {
    lbl_name = Longident.last id.txt;
    lbl_res = fake_type;
    lbl_arg = fake_type;
    lbl_mut = Immutable;
    lbl_pos = 0;
    lbl_all = [||];
    lbl_repres = Record_regular;
    lbl_private = Public;
    lbl_loc = id.loc;
    lbl_attributes = pat.ppat_attributes;
    lbl_uid = Types.Uid.mk ~current_unit: (Longident.last id.txt);
  }

and types_declaration (decl: P.type_declaration): Types.type_declaration =
  {
    type_params = [];
    type_arity = 0;
    type_kind = Type_open;
    type_private = decl.ptype_private;
    type_manifest = None;
    type_variance = [];
    type_separability = [];
    type_is_newtype = false;
    type_expansion_scope = 0;
    type_loc = decl.ptype_loc;
    type_attributes = decl.ptype_attributes;
    type_immediate = Unknown;
    type_unboxed_default = false;
    type_uid = Types.Uid.mk ~current_unit:"";
  }

and types_value_description (value: P.value_description): Types.value_description =
  {
    val_type = fake_type;
    val_kind = Val_reg;
    val_loc = value.pval_loc;
    val_attributes = value.pval_attributes;
    val_uid = Types.Uid.mk ~current_unit:value.pval_name.txt ;
  }

and types_extension_constructor (ext: P.extension_constructor): Types.extension_constructor =
  {
    ext_type_path = fake_path ();
    ext_type_params = [];
    ext_args = Cstr_tuple([]);
    ext_ret_type = None;
    ext_private = Public;
    ext_loc = ext.pext_loc;
    ext_attributes = ext.pext_attributes;
    ext_uid = Types.Uid.mk ~current_unit:ext.pext_name.txt;
  }

and types_class_declaration (decl: P.class_type P.class_infos): Types.class_declaration =
  {
    cty_params = [];
    cty_type = Cty_signature(types_class_signature ());
    cty_path = fake_path ();
    cty_new = None;
    cty_variance = [];
    cty_loc = decl.pci_name.loc;
    cty_attributes = decl.pci_attributes;
    cty_uid = Types.Uid.mk ~current_unit:decl.pci_name.txt;
  }

and types_class_declaration_none (): Types.class_declaration =
  {
    cty_params = [];
    cty_type = Cty_signature(types_class_signature ());
    cty_path = fake_path ();
    cty_new = None;
    cty_variance = [];
    cty_loc = Location.none;
    cty_attributes = [];
    cty_uid = Types.Uid.mk ~current_unit:"";
  }

and types_class_signature (): Types.class_signature =
  {
    csig_self = fake_type;
    csig_vars = Types.Vars.empty;
    csig_concr = Types.Concr.empty;
    csig_inher = [];
  }

and types_class_type_declaration (decl: P.class_type P.class_infos): Types.class_type_declaration =
  {
    clty_params = [];
    clty_type = Cty_signature(types_class_signature ());
    clty_path = fake_path ();
    clty_variance = [];
    clty_loc = decl.pci_name.loc;
    clty_attributes = decl.pci_attributes;
    clty_uid = Types.Uid.mk ~current_unit:decl.pci_name.txt;
  }

and types_class_expr_declaration (decl: P.class_expr P.class_infos): Types.class_declaration =
  {
    cty_params = [];
    cty_type = Cty_signature(types_class_signature ());
    cty_path = fake_path ();
    cty_new = None;
    cty_variance = [];
    cty_loc = decl.pci_name.loc;
    cty_attributes = decl.pci_attributes;
    cty_uid = Types.Uid.mk ~current_unit:decl.pci_name.txt;
  }

and types_class_expr_type_declaration (decl: P.class_expr P.class_infos): Types.class_type_declaration =
  {
    clty_params = [];
    clty_type = Cty_signature(types_class_signature ());
    clty_path = fake_path ();
    clty_variance = [];
    clty_loc = decl.pci_name.loc;
    clty_attributes = decl.pci_attributes;
    clty_uid = Types.Uid.mk ~current_unit:decl.pci_name.txt;
  }

and types_value_description_none (): Types.value_description =
  {
    val_type = fake_type;
    val_kind = Val_reg;
    val_loc = Location.none;
    val_attributes = [];
    val_uid = Types.Uid.mk ~current_unit:"";
  }

and types_row_desc (): Types.row_desc ref =
  let aux (): Types.row_desc=
  {
    row_fields = [];
    row_more = fake_type; 
    row_bound = ();
    row_closed = false;
    row_fixed = None;
    row_name = None;
  }
  in ref (aux ())

and type_structure (structure: P.structure): T.structure =
  (typed structure).structure

and typed (parsetree: P.structure): T.implementation =
  let empty_str: T.structure = {
    str_items = [];
    str_type = [];
    str_final_env = E.empty;
  }
  in
  let empty: T.implementation = {
    structure = empty_str;
    coercion = Tcoerce_none;
    signature = [];
  }
  in typing parsetree empty