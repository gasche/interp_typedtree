open Asttypes

module T = Typedtree
module P = Parsetree
module E = Env

exception Type_error

val typing : P.structure -> T.implementation -> T.implementation
val typed : P.structure -> T.implementation

val type_structure : P.structure -> T.structure
val type_expr : E.t -> P.expression -> T.expression
val type_pattern : E.t -> P.pattern -> T.pattern
val type_bindings : E.t -> P.value_binding list -> T.value_binding list
val type_constant : P.constant -> constant

val types_expr_description : unit -> Types.value_description