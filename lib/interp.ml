open Data
open Envir

let typer ~print ~check path =
  let module Comp = Ocaml_common.Compile_common in
  try
    Comp.with_info
      ~native:false
      ~tool_name:"interp_typedtree"
      ~source_file:path
      ~output_prefix:(Filename.remove_extension path)
      ~dump_ext:".dinterp"
      @@ fun info ->
        let parsetree = Comp.parse_impl info in
        if check then
          let typedtree = Typing.typed parsetree in
          (if print then Printtyped.implementation Format.std_formatter typedtree.structure);
          typedtree
        else
          let typedtree = Comp.typecheck_impl info parsetree in
          (if print then Printtyped.implementation Format.std_formatter typedtree.structure);
          typedtree
  with exn ->
    Ocaml_common.Location.report_exception Format.err_formatter exn;
    exit 2

let eval_env_flag ~loc env flag =
  match flag with
  | Open module_ident ->
     let module_ident = Location.mkloc module_ident loc in
     Envir.env_extend false env (Envir.env_get_module_data env module_ident)

let interpreter ~print ~check ?(env=Runtime_base.initial_env) (files: (env_flag list * string) list) =
  match files with
  | [] -> assert false
  | [(_, file)] ->
    begin
      match Eval.eval_implem Primitives.prims env (typer ~print:print ~check:check file) with
      | env -> env
      | exception (InternalException e) ->
        Format.eprintf "Code raised exception: %a@." pp_print_value e;
        assert false
    end
  | _ ->
    let unit_paths = List.map snd files in
    let env = List.fold_left Envir.declare_unit env unit_paths in
    List.fold_left
      (fun global_env (flags, unit_path) ->
        let module_name = Data.module_name_of_unit_path unit_path in
        if Conf.debug then Format.eprintf "Loading %s from %s@." module_name unit_path;
        let module_contents =
          let loc = Location.in_file unit_path in
          let local_env = List.fold_left (eval_env_flag ~loc) global_env flags in
            match Eval.eval_implem Primitives.prims local_env (typer ~print:print ~check:check unit_path) with
            | env -> env
            | exception (InternalException e) ->
              Format.eprintf "Code raised exception: %a@." pp_print_value e;
              assert false
        in
        Envir.define_unit global_env unit_path (Envir.make_module_data module_contents))
      env
      files

let interp ~print ~check ?(env=Runtime_base.initial_env) (files: (env_flag list * string) list) =
  pp_print_env Format.std_formatter (interpreter ~print ~check ~env files)

let ocaml_stdlib ~print ~check =
  interp ~print:print ~check:check (Ocaml_stdlib.stdlib ())

let ocaml_compiler ~print ~check =
  interp ~print:print ~check:check ~env:(interpreter ~print:print ~check:check (Ocaml_stdlib.stdlib ())) (Ocaml_compiler.bytecode_compiler_units ())
