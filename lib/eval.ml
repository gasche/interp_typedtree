open Data
open Envir
open Conf

module T = Typedtree
module A = Asttypes

type prims = value_ Ptr.t SMap.t
exception Match_fail

let rec lident_name = function
  | Longident.Lident s -> s
  | Longident.Ldot (_, s) -> s
  | Longident.Lapply (_l1, l2) -> lident_name l2

and lookup_viewed_object obj =
  let rec lookup obj = function
    | [] -> obj
    | parent :: parent_view ->
       lookup (SMap.find parent obj.named_parents) parent_view
  in lookup obj obj.parent_view

let mismatch loc =
  Format.eprintf "%a: mismatch@."
    Location.print_loc loc

let rec eval_implem (prims: prims) (env: env) (tree: T.implementation): env =
  eval_structure prims env tree.structure

and eval_structure (prims: prims) (env: env) (structure: T.structure): env =
  let items = structure.str_items in
  List.fold_left (eval_item prims) (prevent_export env) items

and apply_ (prims: prims) (vf: value) ((_, v): A.arg_label * value) =
  let func_value = Ptr.get @@ vf in
  match func_value with
  | Function(_, cases, env) -> eval_cases prims env v cases
  | Prim(prim) -> prim v
  | _ -> Format.fprintf Format.std_formatter "%a\n" pp_print_value vf; assert false

and apply (prims: prims) (vf: value) (args : (A.arg_label * value) list) = List.fold_left (apply_ prims) vf args

and tracer call args (f: T.expression) =
  if traceend then
  begin
    let d = !tracedepth in
    tracedepth := d + 1;
    let print_action name =
      let fname =
        match f.exp_desc with
        | Texp_ident(_, lident, _) -> String.concat "." (Longident.flatten lident.txt)
        | _ -> "<unknown>"
      in
      Format.eprintf "(%d) %s %s@." d name fname
    in
    print_action "apply";
    match call () with
    | r -> print_action "leave"; tracedepth := d; r
    | exception e -> print_action "error"; tracedepth := d; raise e
  end else
  begin
    if trace
    then (
      match f.exp_desc with
      | Texp_ident(_, lident, _) ->
        Format.eprintf
          "apply %s"
          (String.concat "." (Longident.flatten lident.txt));
        tracecur := Int64.succ !tracecur;
        if !tracecur > tracearg_from
        then
          Format.eprintf
            " %a"
            (Format.pp_print_list
              ~pp_sep:(fun ff () -> Format.fprintf ff " ")
              (fun ff (_, v) -> Format.fprintf ff "%a" (pp_print_value) v))
            args;
        Format.eprintf "@."
      | _ -> ());
    call ()
  end

and eval_item (prims: prims) (env: env) (item: T.structure_item) =
  let desc: T.structure_item_desc = item.str_desc in
  match desc with
  | Tstr_eval(expr, _) -> ignore (eval_expr prims env expr); env
  | Tstr_value(flag, bindings) ->
    eval_bindings prims env flag bindings
  | Tstr_primitive({val_name = {txt = name; loc}; val_prim = l; _ }) ->
    begin
      let prim prim_name =
        try SMap.find prim_name prims
        with Not_found ->
          ptr @@ Prim
            (fun _ ->
              Format.eprintf "%a: Unimplemented primitive %s@." Location.print_loc loc prim_name;
              failwith ("Unimplemented primitive " ^ prim_name))
      in
      match l with
      | [] ->
        env_set_value name (prim name) env
      | prim_name::_ ->
        env_set_value name (prim prim_name) env
    end
  | Tstr_type(_, l_decls) ->
    begin
      let eval_declaration (env: env) (t: T.type_declaration) =
        let ttype: T.type_kind = t.typ_kind in
        match ttype with
        | Ttype_variant l ->
          let _, _, env =
            let constructors(u, v, env) (cd: T.constructor_declaration) =
                match cd.cd_args with
                | Cstr_tuple [] ->
                  (u + 1, v, env_set_constr cd.cd_name.txt u env)
                | _ -> (u, v + 1, env_set_constr cd.cd_name.txt v env)
            in
            List.fold_left
              constructors
              (0, 0, env)
              l
          in
          env
        | _ -> env
      in
      List.fold_left
        eval_declaration
        env
        l_decls
    end
  | Tstr_typext(_) -> env
  | Tstr_exception(e) ->
    begin
      let constructor = e.tyexn_constructor in
      let kind = constructor.ext_kind in
      let name = constructor.ext_name in
      match kind with
      | Text_decl _ ->
        let d = next_exn_id () in
        env_set_constr name.txt d env
      | Text_rebind(_, id) -> env_set_constr name.txt (env_get_constr env id) env
    end
  | Tstr_module({ mb_name = name; mb_expr = expr_; _ } ) ->
    begin
      match name.txt with
      | None -> env
      | Some(name_) -> env_set_module name_ (eval_module_expr prims env expr_) env
    end
  | Tstr_recmodule(_) -> Format.printf "Tstr_recmodule not implemented\n"; assert false (* Wasn't present in Camlboot *)
  | Tstr_modtype(_) -> env
  | Tstr_open(decl) -> eval_open env decl
  | Tstr_class(l_decls) -> 
     let forward_env: env ref = ref env in
     let add_class ((decl, _): (T.class_declaration * string list)) env =
       let name = decl.ci_id_name.txt in
       let class_expr = decl.ci_expr in
       env_set_class name (class_expr, forward_env) env in
     let env = List.fold_right add_class l_decls env in
     env
  | Tstr_class_type(_) -> env
  | Tstr_include(decl) ->
    let m = eval_module_expr prims env (decl.incl_mod) in
    env_extend true env (get_module_data (decl.incl_loc) m)
  | Tstr_attribute(_) -> env

and eval_expr (prims: prims) (env: env) (expr: T.expression) =
  let desc: T.expression_desc = expr.exp_desc in
  match desc with
  | Texp_ident(_, id, _) ->
     begin match env_get_value_or_lvar env id with
       | Value v -> v
       | Instance_variable (obj, name) ->
          let var = SMap.find name obj.variables in
          !var
     end
  | Texp_constant(x) -> value_of_constant x
  | Texp_let(flag, bindings, expr_) ->
    eval_expr prims (eval_bindings prims env flag bindings) expr_
  | Texp_function({arg_label = lbl; param = _; cases = cases; partial = _}) -> ptr @@ Function(lbl, cases, env)
  | Texp_apply(f, l) ->
    (
      let func_value = Ptr.get @@ eval_expr prims env f
      in
      match func_value with
      | Function _ | Prim _ ->
        begin
          let args = List.map (fun (lab, e) -> (lab, eval_expr_option prims env e)) l in
          tracer (fun () -> apply prims (ptr @@ func_value) args) args f
        end
      | Fexpr fexpr ->
        begin
          let loc = expr.exp_loc in
          match fexpr loc l with
          | None ->
            Format.eprintf "%a@.F-expr failure.@." Location.print_loc loc;
            assert false
          | Some expr -> eval_expr prims env expr
        end
    | _ -> Format.printf "Tried to apply something other than a function\n"; assert false
    )
  | Texp_match(expr_, cases, _) -> eval_match prims env (eval_expr prims env expr_) cases
  | Texp_try(expr_, cases) -> 
    (try eval_expr prims env expr_
     with InternalException v ->
       (try eval_cases prims env v cases
        with Match_fail -> raise (InternalException v)))
  | Texp_tuple(exprs) ->
    let args = List.map (eval_expr prims env) exprs in
    ptr @@ Tuple(args)
  | Texp_construct(id, _, exprs) ->
    let cn = Longident.last id.txt in
    let d = env_get_constr env id in
      (* Format.printf "\nexpr=%s#%d\n" cn d; *)
    let ee expr =
      match expr with
      | [] -> None
      | h::_ -> Some(eval_expr prims env h)
    in
    ptr @@ Constructor(cn, d, ee exprs)
  | Texp_record(record) ->
    begin
    let base =
      let expr_: T.expression option = record.extended_expression in
      match expr_ with
      | None -> SMap.empty
      | Some e ->
        match Ptr.get @@ eval_expr prims env e with
        | Record r -> r
        | _ -> mismatch expr.exp_loc; assert false
    in
    ptr @@ Record
      (Array.fold_left
        (fun (rc: value ref SMap.t) ((lbl, rld): Types.label_description * T.record_label_definition) ->
          match rld with
          | Kept _ -> rc
          | Overridden(_, expr_) -> SMap.add lbl.lbl_name (ref (eval_expr prims env expr_)) rc)
        base
        record.fields)
    end
  | Texp_sequence(expr_f, expr_s) -> ignore (eval_expr prims env expr_f); eval_expr prims env expr_s
  | Texp_array(exprs) ->
    let args = Array.of_list (List.map (eval_expr prims env) exprs) in
    ptr @@ Array(args)
  | Texp_ifthenelse(if_expr, then_expr, else_expr) ->
    if is_true (eval_expr prims env if_expr)
    then eval_expr prims env then_expr
    else
    begin
      match else_expr with
      | None -> Ptr.dummy ()
      | Some(e) -> eval_expr prims env e
    end
  | Texp_while(cond, body) ->
    while is_true (eval_expr prims env cond)
    do ignore (eval_expr prims env body) done;
    Ptr.dummy ()
  | Texp_for(_, pat, start, final, flag, expr_) ->
    let v1 = Runtime_base.unwrap_int (eval_expr prims env start) in
    let v2 = Runtime_base.unwrap_int (eval_expr prims env final) in
    if flag = Upto
    then
      for x = v1 to v2 do
        let vx = Runtime_base.wrap_int x in
        let t_pat = Typing.type_pattern (Env.empty) pat in
        ignore (eval_expr prims (bind_pattern prims env t_pat vx) expr_)
      done
    else
      for x = v1 downto v2 do
        let vx = Runtime_base.wrap_int x in
        ignore (eval_expr prims (bind_pattern prims env (Typing.type_pattern (Env.empty) pat) vx) expr_)
      done;
    unit
  | Texp_assert(expr_) ->
    if not (is_true (eval_expr prims env expr_))
    then raise (Stdlib.Assert_failure ("",1,2))
    else Ptr.dummy()
  | Texp_variant(_, expr_option) ->
    begin
      match expr_option with
      | None -> raise Match_fail
      | Some(e) -> eval_expr prims env e
    end
  | Texp_field(expr_, id, _) ->
    begin
    let v = Ptr.get @@ eval_expr prims env expr_ in
    match v with
    | Record r -> !(SMap.find (lident_name id.txt) r)
    | _ -> assert false
    end
  | Texp_setfield(expr1, id, _, expr2) ->
    begin
      let v1 = eval_expr prims env expr1 in
      let v2 = eval_expr prims env expr2 in
      let v = Ptr.get @@ v1 in
      match v with
      | Record r ->
        SMap.find (lident_name id.txt) r := v2;
        unit
      | _ -> assert false
    end
  | Texp_send(obj_expr, meth, _) ->
    begin
      let obj = eval_expr prims env obj_expr in
      let v = Ptr.get @@ obj in
      match v with
        | Object obj -> eval_obj_send prims obj meth
        | _ -> assert false
    end
  | Texp_new(_, id, _) ->
     let (class_expr, class_env) = env_get_class env id in
     eval_obj_new prims !class_env class_expr
  | Texp_instvar(_, _, _) -> Format.printf "Texp_instvar not implemented\n"; assert false
  | Texp_setinstvar(_, _, id, expr_) ->
     let v = eval_expr prims env expr_ in
     let id = { id with Location.txt = Longident.Lident id.txt } in
     begin match env_get_value_or_lvar env id with
       | Value _ -> mismatch expr_.exp_loc; assert false
       | Instance_variable (obj, name) ->
          let var = SMap.find name obj.variables in
          var := v
     end;
     Runtime_base.wrap_unit ()
  | Texp_override(_, fields) ->
     begin match env.current_object with
       | None -> assert false
       | Some obj ->
          let new_obj = eval_obj_override prims env obj fields in
          ptr @@ Object new_obj
     end
  | Texp_letmodule(_, lbl, _, me, expr_) ->
    begin
      let m = eval_module_expr prims env me in
      match lbl.txt with
      | None -> mismatch expr_.exp_loc; assert false
      | Some(s) -> eval_expr prims (env_set_module s m env) expr_
    end
  | Texp_letexception(constr, expr_) ->
    begin
      let kind = constr.ext_kind in
      let name = constr.ext_name in
      let nenv =
        match kind with
        | Text_decl(_) ->
          let d = next_exn_id () in
          env_set_constr name.txt d env
        | Text_rebind(_, id) ->
          env_set_constr name.txt (env_get_constr env id) env
      in
      eval_expr prims nenv expr_
    end
  | Texp_lazy(expr_) -> ptr @@ Lz (ref (fun () -> eval_expr prims env expr_))
  | Texp_object(_, _) -> Format.printf "Texp_object not implemented\n"; assert false
  | Texp_pack(me) -> ptr @@ ModVal (eval_module_expr prims env me)
  | Texp_letop(_) -> Format.printf "Texp_letop not implemented\n"; assert false
  | Texp_unreachable -> Format.printf "Texp_unreachable not implemented\n"; assert false
  | Texp_extension_constructor(id, _) ->
      let cn = Longident.last id.txt in
      let d = env_get_constr env id in
      (* Format.printf "\next=%s#%d\n" cn d; *)
      ptr @@ Constructor(cn, d, None)
  | Texp_open(o_decl, expr_) ->
      let o_expr = o_decl.open_expr in
      let nenv =
        match eval_module_expr prims env o_expr with
        | exception Not_found ->
          (* Module might be a .mli only *)
          env
        | mdl -> env_extend false env (get_module_data expr.exp_loc mdl)
      in
      eval_expr prims nenv expr_

and eval_expr_option (prims: prims) (env: env) (expr: T.expression option) =
  match expr with
  | None -> raise Match_fail
  | Some e -> eval_expr prims env e

and eval_bindings (prims: prims) (env: env) (flag: Asttypes.rec_flag) (bindings: T.value_binding list) =
  match flag with
    | Nonrecursive ->
       List.fold_left (bind_value prims) env bindings
    | Recursive ->
      let dummies = List.map (fun _ -> Ptr.dummy ()) bindings in
      let declare env (vb: T.value_binding) dummy =
        bind_pattern prims env vb.vb_pat dummy in
      let define env (vb: T.value_binding) dummy =
        let v = eval_expr prims env vb.vb_expr in
        let v_ = Ptr.get @@ v in
        Ptr.backpatch dummy v_ in
      let nenv = List.fold_left2 declare env bindings dummies in
      List.iter2 (define nenv) bindings dummies;
      nenv

and eval_cases (prims: prims) (env: env) (v: value_ Ptr.t) (cases: T.value T.case list) =
  match cases with
  | [] -> raise Match_fail
  | c::t ->
    begin
      let lhs: 'a T.general_pattern = c.c_lhs in
      let guard: T.expression option = c.c_guard in
      let rhs: T.expression = c.c_rhs in
      try (
        let env_ = bind_pattern prims env lhs v in
        match guard with
        | None -> eval_expr prims env_ rhs
        | Some(expr) ->
          let b = eval_expr prims env_ expr in
          if is_true b then
            eval_expr prims env_ rhs
          else
            raise Match_fail
      )
      with Match_fail -> eval_cases prims env v t
    end

and eval_match (prims: prims) (env: env) (v: value_ Ptr.t) (cases: T.computation T.case list) =
  match cases with
  | [] -> raise Match_fail
  | c::t ->
    begin
      let lhs = c.c_lhs in
      let guard = c.c_guard in
      let rhs = c.c_rhs in
      try (
        match T.split_pattern lhs with
          | (None, _) -> raise Match_fail
          | (Some(lhs), _) ->
            let env_ = bind_pattern prims env lhs v in
            match guard with
            | None -> eval_expr prims env_ rhs
            | Some(expr) ->
              let expr_ = eval_expr prims env_ expr in
              if is_true expr_ then
                eval_expr prims env_ rhs
              else
                raise Match_fail
      )
      with Match_fail -> eval_match prims env v t
    end

and bind_value (prims: prims) (env: env) (binding: T.value_binding): env =
  let expr = eval_expr prims env binding.vb_expr in
  bind_pattern prims env binding.vb_pat expr

and bind_pattern (prims: prims) (env: env) (pattern: T.pattern) (v: value_ Ptr.t): env =
  let desc: 'a T.pattern_desc = pattern.pat_desc in
  match desc with
  | Tpat_any -> env
  | Tpat_constant(x) -> 
    if value_equal (value_of_constant x) v then env else raise Match_fail
  | Tpat_var((_, lbl)) -> env_set_value lbl.txt v env
  | Tpat_alias(pat, _, s) -> env_set_value s.txt v (bind_pattern prims env pat v)
  | Tpat_tuple(l_pat) ->
    begin
      match Ptr.get v with
        | Tuple vl ->
          assert (List.length l_pat = List.length vl);
          List.fold_left2 (bind_pattern prims) env l_pat vl
        | _ -> mismatch pattern.pat_loc; assert false
    end
  | Tpat_construct(id, _, l_pat, _) ->
    begin
      let cn = Longident.last id.txt in
      let dn = env_get_constr env id in
      (* Format.printf "\npat=%s#%d\n" cn dn; *)
      match Ptr.get v with
        | Constructor (ccn, ddn, e) ->
          if cn <> ccn then raise Match_fail;
          if dn <> ddn then raise Match_fail;
          (match (l_pat, e) with
          | [], None -> env
          | p::_, Some e -> bind_pattern prims env p e
          | _ -> mismatch pattern.pat_loc; assert false)
        | String s ->
          assert (lident_name id.txt = "Format");
          let p =
            match l_pat with
            | [] -> mismatch pattern.pat_loc; assert false
            | p::_ -> p
          in
          let fmt_ebb_of_string =
            let lid =
              Longident.(Ldot (Lident "CamlinternalFormat", "fmt_ebb_of_string"))
            in
            match env_get_value_or_lvar env { loc = id.loc; txt = lid } with
              | Instance_variable _ -> assert false
              | Value v -> v
          in
          let fmt = apply prims fmt_ebb_of_string [ (Nolabel, ptr @@ String s) ] in
          let fmt =
            match Ptr.get fmt with
            | Constructor ("Fmt_EBB", _, Some fmt) -> fmt
            | _ -> mismatch pattern.pat_loc; assert false
          in
          bind_pattern prims env p (ptr @@ Tuple [ fmt; v ])
        | _ ->
          Format.eprintf "cn = %s; v = %a\n" cn pp_print_value v;
          assert false
    end
  | Tpat_variant(name, pat, _) ->
    begin
      match Ptr.get v with
      | Constructor (cn, _, e) ->
        if cn <> name then raise Match_fail;
        (match (pat, e) with
        | None, None -> env
        | Some p, Some e -> bind_pattern prims env p e
        | _ -> mismatch pattern.pat_loc; assert false)
      | _ -> mismatch pattern.pat_loc; assert false
    end
  | Tpat_record(rp, _) ->
    begin
      match Ptr.get v with
      | Record r ->
        List.fold_left
          (fun env ((id, _, p): (Longident.t Location.loc * Types.label_description * T.value T.general_pattern)) ->
            match SMap.find (lident_name id.txt) r with
            | v -> bind_pattern prims env p !v
            | exception Not_found -> raise Match_fail
          )
          env
          rp
      | _ -> mismatch pattern.pat_loc; assert false
    end
  | Tpat_array(l_pat) ->
    begin
      match Ptr.get v with
        | Array vs ->
          let vs = Array.to_list vs in
          if List.length l_pat <> List.length vs then raise Match_fail;
          List.fold_left2 (fun env p v -> bind_pattern prims env p v) env l_pat vs
        | _ -> mismatch pattern.pat_loc; assert false
    end
  | Tpat_lazy(_) -> Format.printf "Tpat_lazy not implemented\n"; mismatch pattern.pat_loc; assert false
  | Tpat_or(p1, p2, _) ->
    try bind_pattern prims env p1 v
     with Match_fail -> bind_pattern prims env p2 v

and eval_module_expr (prims: prims) (env: env) (expr: T.module_expr): mdl =
  let desc: T.module_expr_desc = expr.mod_desc in
  match desc with
  | Tmod_ident(_, id) -> env_get_module env id
  | Tmod_structure(str) ->  Module(make_module_data (eval_structure prims env str))
  | Tmod_functor(param, expr) ->
    begin
      match param with
      | Unit -> Functor("", expr, env)
      | Named(_, name, _) ->
        begin
        match name.txt with
        | None -> Functor("", expr, env)
        | Some(name) -> Functor(name, expr, env)
        end
    end
  | Tmod_apply(expr1, expr2, _) ->
    let mod1 = eval_module_expr prims env expr1 in
    let mod2 = eval_module_expr prims env expr2 in
    let arg_name, body, env = eval_functor_data prims mod1 in
    eval_module_expr prims (env_set_module arg_name mod2 env) body
  | Tmod_constraint(expr_, _, _, _) -> eval_module_expr prims env expr_
  | Tmod_unpack(expr_, _) ->
    match Ptr.get @@ eval_expr prims env expr_ with
    | ModVal m -> m
    | _ -> mismatch expr.mod_loc; assert false

and eval_functor_data (prims: prims) (modl: mdl): string * T.module_expr * env =
  match modl with
  | Module _ -> ignore prims; failwith "tried to apply a simple module"
  | Unit _ -> failwith "tried to apply a simple module unit"
  | Functor (arg_name, body, env) -> (arg_name, body, env)

and eval_obj_send (prims: prims) (obj: object_value) (meth: T.meth): value =
  match meth with
  | Tmeth_name(s) ->
    begin
      try (
        let expr = SMap.find s (lookup_viewed_object obj).methods in
        eval_expr_in_object prims obj expr
      ) with
      | _ -> assert false
    end
  | Tmeth_val(id) ->
    begin
      try (
        let expr = SMap.find (Ident.name id) (lookup_viewed_object obj).methods in
        eval_expr_in_object prims obj expr
      ) with
      | _ -> assert false
    end

and eval_expr_in_object (prims: prims) (obj: object_value) (expr_in_object: expr_in_object): value =
  let expr_env = match expr_in_object.source with
      | Parent parent -> parent.env
      | Current_object -> (lookup_viewed_object obj).env
  in
  let bind_self obj env =
    let self_view = { obj with parent_view = [] } in
    let self_pattern = match expr_in_object.source with
      | Parent parent -> parent.self
      | Current_object -> (lookup_viewed_object obj).self in
    bind_pattern prims env self_pattern (ptr @@ Object self_view) in
  let add_parent obj name env =
    let parent_view =
      { obj with parent_view = name :: obj.parent_view } in
    env_set_value name (ptr @@ Object parent_view) env in
  let add_variable name env =
    env_set_lvar name obj env in
  let activate_object obj env =
    { env with current_object = Some obj } in
  let env =
    expr_env
    |> bind_self obj
    |> SSet.fold (add_parent obj) expr_in_object.named_parents_scope
    |> SSet.fold add_variable expr_in_object.instance_variable_scope
    |> activate_object obj
  in
  eval_expr prims env expr_in_object.expr

and eval_obj_new (prims: prims) (env: env) (class_expr: T.class_expr): value =
  match Ptr.get @@ eval_class_expr prims env class_expr with
    | Object obj ->
      eval_obj_initializers prims env obj;
      ptr @@ Object obj
    | other -> ptr @@ other

and eval_class_expr (prims: prims) (env: env) (class_expr: T.class_expr) =
  let desc = class_expr.cl_desc in
  match desc with
  | Tcl_ident(_, id, _) -> 
    let (class_expr, class_env) = env_get_class env id in
     eval_obj_new prims !class_env class_expr
  | Tcl_fun(_, _, _, _, _) -> failwith "TODO: eval_class_expr Tcl_fun"
      (* ptr @@ Function(lbl, cases, env) *)
  | Tcl_apply(cle, args) ->
    let func_value = eval_class_expr prims env cle in
    let args = List.map (fun (lab, e) -> (lab, eval_expr_option prims env e)) args in
    apply prims func_value args
  | Tcl_let(rec_flag, bindings, _, cle) -> 
      eval_class_expr prims (eval_bindings prims env rec_flag bindings) cle
  | Tcl_constraint(expr, _, _, _, _) -> eval_class_expr prims env expr
  | Tcl_open(open_, cle) ->
      let (_, id) = open_.open_expr in
    let nenv =
      match env_get_module_data env id with
      | exception Not_found ->
        (* Module might be a .mli only *)
        env
      | module_data -> env_extend false env module_data
    in
    eval_class_expr prims nenv cle
  | Tcl_structure(class_structure) ->
    let obj: object_value = eval_class_structure prims env class_structure in
    ptr @@ Object obj

and eval_obj_initializers (prims: prims) (env: env) (obj: object_value) =
  ignore env;
  let eval_init expr =
    Runtime_base.unwrap_unit (eval_expr_in_object prims obj expr) in
  List.iter eval_init obj.initializers

and eval_class_structure (prims: prims) (env: env) (class_structure: T.class_structure) =
  let eval_obj_field ((rev_inits,
                       parents, parents_in_scope,
                       variables, variables_in_scope,
                       methods) as state) (class_field: T.class_field) =
    let in_object expr = {
        source = Current_object;
        instance_variable_scope = variables_in_scope;
        named_parents_scope = parents_in_scope;
        expr;
      } in
    match class_field.cf_desc with
    | Tcf_val(str, _, _, Tcfk_virtual _, _) ->
       (rev_inits,
        parents,
        SSet.remove str.txt parents_in_scope,
        variables,
        variables_in_scope,
        methods)
    | Tcf_val(str, _, _, Tcfk_concrete (_ov_flag, expr), _) ->
       let v = eval_expr prims env expr in
       (rev_inits,
        parents,
        SSet.remove str.txt parents_in_scope,
        SMap.add str.txt (ref v) variables,
        SSet.add str.txt variables_in_scope,
        methods)
    | Tcf_method (_lab, _priv_flag, Tcfk_virtual _) ->
        state
    | Tcf_method (lab, _priv_flag, Tcfk_concrete (_ov_flag, expr)) ->
       (rev_inits,
        parents, parents_in_scope,
        variables, variables_in_scope,
        SMap.add lab.txt (in_object expr) methods)
    | Tcf_initializer expr ->
       (in_object expr :: rev_inits,
        parents, parents_in_scope,
        variables, variables_in_scope,
        methods)
    | Tcf_inherit(_ov_flag, class_expr, parent_name, _, _) ->
       let in_parent parent expr_in_object =
         match expr_in_object.source with
           | Parent _ -> expr_in_object
           | Current_object -> { expr_in_object with source = Parent parent } in
       begin
       let v = Ptr.get @@ eval_class_expr prims env class_expr in
       match v with
         | Object parent ->
            let rev_inits =
              let parent_initializers =
                List.map (in_parent parent) parent.initializers in
              List.rev_append parent_initializers rev_inits in
            let parents, parents_in_scope =
              match parent_name with
              | None -> parents, parents_in_scope
              | Some name ->
                  SMap.add name parent parents,
                  SSet.add name parents_in_scope in
            let variables =
              SMap.union (fun _k _old new_ -> Some new_)
              variables
              parent.variables
            in
            let set_of_keys dict =
              SMap.to_seq dict
              |> Seq.map fst
              |> SSet.of_seq in
            let variables_in_scope =
              (* first add the parent variables *)
              SSet.union variables_in_scope
                (set_of_keys parent.variables) in
            let variables_in_scope =
              (* then remove the 'super' name if any *)
              match parent_name with
                | None -> variables_in_scope
                | Some name ->
                   SSet.remove name variables_in_scope in
            let methods =
              SMap.union (fun _k _old new_ -> Some new_)
              methods
              (SMap.map (in_parent parent) parent.methods)
            in
            (rev_inits,
             parents, parents_in_scope,
             variables, variables_in_scope,
             methods)
         | _ -> assert false
       end
    | Tcf_constraint _ ->
       state
    | Tcf_attribute _ ->
       state
  in
  let self = class_structure.cstr_self in
  let fields = class_structure.cstr_fields in
  let (rev_inits,
       parents, _parents_in_scope,
       variables, _variables_in_scope,
       methods) =
    List.fold_left eval_obj_field
      ([],
       SMap.empty, SSet.empty,
       SMap.empty, SSet.empty,
       SMap.empty) fields in
  {
    env;
    self;
    named_parents = parents;
    initializers = List.rev rev_inits;
    variables;
    methods;
    parent_view = [];
  }

and eval_open (env: env) (decl: T.open_declaration)=
  let desc = decl.open_expr.mod_desc in
  match desc with
  | T.Tmod_ident(_, l) ->
    env_extend false env (env_get_module_data env l)
  | _ -> Format.printf "Unexpected open declaration\n"; raise Match_fail

and eval_obj_override (prims: prims) (env: env) (obj: object_value) (fields: (Path.t * string Location.loc * T.expression) list) =
  let override_field (p, _, e) obj =
    let v = eval_expr prims env e in
    { obj with variables = SMap.add (Path.last p) (ref v) obj.variables } in
  let obj = List.fold_right override_field fields obj in
  obj

