external create : int -> bytes = "caml_create_bytes"
external unsafe_fill : bytes -> int -> int -> char -> unit = "caml_fill_bytes" [@@noalloc]

let aux c =
  let s = create 0 in
  unsafe_fill s 0 0 c (* does not recognise 'a' as a char *)

let () = aux 'a'