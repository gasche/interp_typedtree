let f1 x = 1;;

let g x = (fun _ -> 1) x;;

let h = function x -> 1;;

let f x y = 1;;

let g1 x =
  let y = 1
  in y;;

let a = f1 1;;

let b = h 0

let c = f 1 2

let d = g (f 1 2)