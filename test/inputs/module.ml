module type S = 
   sig
      type t
      val f : int -> t
   end
module M : S =
  struct
      type t = int
      let x = 1
      let f y = x
   end

let b = M.f 1
let a = M.f M.x