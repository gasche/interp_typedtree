(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

module Pervasives = struct
(* type 'a option = None | Some of 'a *)

(* Exceptions *)

external register_named_value : string -> 'a -> unit
                              = "caml_register_named_value"

let () =
  (* for asmrun/fail.c *)
  register_named_value "Pervasives.array_bound_error"
    (Invalid_argument "index out of bounds")


external raise : exn -> 'a = "%raise"
external raise_notrace : exn -> 'a = "%raise_notrace"

let failwith s = raise(Failure s)
let invalid_arg s = raise(Invalid_argument s)

exception Exit

(* Composition operators *)

external ( |> ) : 'a -> ('a -> 'b) -> 'b = "%revapply"
external ( @@ ) : ('a -> 'b) -> 'a -> 'b = "%apply"

(* Debugging *)

external __LOC__ : string = "%loc_LOC"
external __FILE__ : string = "%loc_FILE"
external __LINE__ : int = "%loc_LINE"
external __MODULE__ : string = "%loc_MODULE"
external __POS__ : string * int * int * int = "%loc_POS"

external __LOC_OF__ : 'a -> string * 'a = "%loc_LOC"
external __LINE_OF__ : 'a -> int * 'a = "%loc_LINE"
external __POS_OF__ : 'a -> (string * int * int * int) * 'a = "%loc_POS"

(* Comparisons *)

external ( = ) : 'a -> 'a -> bool = "%equal"
external ( <> ) : 'a -> 'a -> bool = "%notequal"
external ( < ) : 'a -> 'a -> bool = "%lessthan"
external ( > ) : 'a -> 'a -> bool = "%greaterthan"
external ( <= ) : 'a -> 'a -> bool = "%lessequal"
external ( >= ) : 'a -> 'a -> bool = "%greaterequal"
external compare : 'a -> 'a -> int = "%compare"

let min x y = if x <= y then x else y
let max x y = if x >= y then x else y

external ( == ) : 'a -> 'a -> bool = "%eq"
external ( != ) : 'a -> 'a -> bool = "%noteq"

(* Boolean operations *)

external not : bool -> bool = "%boolnot"
external ( & ) : bool -> bool -> bool = "%sequand"
external ( && ) : bool -> bool -> bool = "%sequand"
external ( or ) : bool -> bool -> bool = "%sequor"
external ( || ) : bool -> bool -> bool = "%sequor"

(* Integer operations *)

external ( ~- ) : int -> int = "%negint"
external ( ~+ ) : int -> int = "%identity"
external succ : int -> int = "%succint"
external pred : int -> int = "%predint"
external ( + ) : int -> int -> int = "%addint"
external ( - ) : int -> int -> int = "%subint"
external ( * ) : int -> int -> int = "%mulint"
external ( / ) : int -> int -> int = "%divint"
external ( mod ) : int -> int -> int = "%modint"

let abs x = if x >= 0 then x else -x

external ( land ) : int -> int -> int = "%andint"
external ( lor ) : int -> int -> int = "%orint"
external ( lxor ) : int -> int -> int = "%xorint"

let lnot x = x lxor (-1)

external ( lsl ) : int -> int -> int = "%lslint"
external ( lsr ) : int -> int -> int = "%lsrint"
external ( asr ) : int -> int -> int = "%asrint"

let max_int = (-1) lsr 1
let min_int = max_int + 1

(* Floating-point operations *)

external ( ~-. ) : float -> float = "%negfloat"
external ( ~+. ) : float -> float = "%identity"
external ( +. ) : float -> float -> float = "%addfloat"
external ( -. ) : float -> float -> float = "%subfloat"
external ( *. ) : float -> float -> float = "%mulfloat"
external ( /. ) : float -> float -> float = "%divfloat"
external ( ** ) : float -> float -> float = "caml_power_float" "pow"
  [@@unboxed] [@@noalloc]
external exp : float -> float = "caml_exp_float" "exp" [@@unboxed] [@@noalloc]
external expm1 : float -> float = "caml_expm1_float" "caml_expm1"
  [@@unboxed] [@@noalloc]
external acos : float -> float = "caml_acos_float" "acos"
  [@@unboxed] [@@noalloc]
external asin : float -> float = "caml_asin_float" "asin"
  [@@unboxed] [@@noalloc]
external atan : float -> float = "caml_atan_float" "atan"
  [@@unboxed] [@@noalloc]
external atan2 : float -> float -> float = "caml_atan2_float" "atan2"
  [@@unboxed] [@@noalloc]
external hypot : float -> float -> float
               = "caml_hypot_float" "caml_hypot" [@@unboxed] [@@noalloc]
external cos : float -> float = "caml_cos_float" "cos" [@@unboxed] [@@noalloc]
external cosh : float -> float = "caml_cosh_float" "cosh"
  [@@unboxed] [@@noalloc]
external log : float -> float = "caml_log_float" "log" [@@unboxed] [@@noalloc]
external log10 : float -> float = "caml_log10_float" "log10"
  [@@unboxed] [@@noalloc]
external log1p : float -> float = "caml_log1p_float" "caml_log1p"
  [@@unboxed] [@@noalloc]
external sin : float -> float = "caml_sin_float" "sin" [@@unboxed] [@@noalloc]
external sinh : float -> float = "caml_sinh_float" "sinh"
  [@@unboxed] [@@noalloc]
external sqrt : float -> float = "caml_sqrt_float" "sqrt"
  [@@unboxed] [@@noalloc]
external tan : float -> float = "caml_tan_float" "tan" [@@unboxed] [@@noalloc]
external tanh : float -> float = "caml_tanh_float" "tanh"
  [@@unboxed] [@@noalloc]
external ceil : float -> float = "caml_ceil_float" "ceil"
  [@@unboxed] [@@noalloc]
external floor : float -> float = "caml_floor_float" "floor"
  [@@unboxed] [@@noalloc]
external abs_float : float -> float = "%absfloat"
external copysign : float -> float -> float
                  = "caml_copysign_float" "caml_copysign"
                  [@@unboxed] [@@noalloc]
external mod_float : float -> float -> float = "caml_fmod_float" "fmod"
  [@@unboxed] [@@noalloc]
external frexp : float -> float * int = "caml_frexp_float"
external ldexp : (float [@unboxed]) -> (int [@untagged]) -> (float [@unboxed]) =
  "caml_ldexp_float" "caml_ldexp_float_unboxed" [@@noalloc]
external modf : float -> float * float = "caml_modf_float"
external float : int -> float = "%floatofint"
external float_of_int : int -> float = "%floatofint"
external truncate : float -> int = "%intoffloat"
external int_of_float : float -> int = "%intoffloat"
external float_of_bits : int64 -> float
  = "caml_int64_float_of_bits" "caml_int64_float_of_bits_unboxed"
  [@@unboxed] [@@noalloc]
let infinity =
  float_of_bits 0x7F_F0_00_00_00_00_00_00L
let neg_infinity =
  float_of_bits 0xFF_F0_00_00_00_00_00_00L
let nan =
  float_of_bits 0x7F_F0_00_00_00_00_00_01L
let max_float =
  float_of_bits 0x7F_EF_FF_FF_FF_FF_FF_FFL
let min_float =
  float_of_bits 0x00_10_00_00_00_00_00_00L
let epsilon_float =
  float_of_bits 0x3C_B0_00_00_00_00_00_00L

type fpclass =
    FP_normal
  | FP_subnormal
  | FP_zero
  | FP_infinite
  | FP_nan
external classify_float : (float [@unboxed]) -> fpclass =
  "caml_classify_float" "caml_classify_float_unboxed" [@@noalloc]

(* String and byte sequence operations -- more in modules String and Bytes *)

external string_length : string -> int = "%string_length"
external bytes_length : bytes -> int = "%bytes_length"
external bytes_create : int -> bytes = "caml_create_bytes"
external string_blit : string -> int -> bytes -> int -> int -> unit
                     = "caml_blit_string" [@@noalloc]
external bytes_blit : bytes -> int -> bytes -> int -> int -> unit
                        = "caml_blit_bytes" [@@noalloc]
external bytes_unsafe_to_string : bytes -> string = "%bytes_to_string"

let ( ^ ) s1 s2 =
  let l1 = string_length s1 and l2 = string_length s2 in
  let s = bytes_create (l1 + l2) in
  string_blit s1 0 s 0 l1;
  string_blit s2 0 s l1 l2;
  bytes_unsafe_to_string s

(* Character operations -- more in module Char *)

external int_of_char : char -> int = "%identity"
external unsafe_char_of_int : int -> char = "%identity"
let char_of_int n =
  if n < 0 || n > 255 then invalid_arg "char_of_int" else unsafe_char_of_int n

(* Unit operations *)

external ignore : 'a -> unit = "%ignore"

(* Pair operations *)

external fst : 'a * 'b -> 'a = "%field0"
external snd : 'a * 'b -> 'b = "%field1"

(* References *)

type 'a ref = { mutable contents : 'a }
external ref : 'a -> 'a ref = "%makemutable"
external ( ! ) : 'a ref -> 'a = "%field0"
external ( := ) : 'a ref -> 'a -> unit = "%setfield0"
external incr : int ref -> unit = "%incr"
external decr : int ref -> unit = "%decr"

(* Result type *)

type ('a,'b) result = Ok of 'a | Error of 'b

(* String conversion functions *)

external format_int : string -> int -> string = "caml_format_int"
external format_float : string -> float -> string = "caml_format_float"

let string_of_bool b =
  if b then "true" else "false"
let bool_of_string = function
  | "true" -> true
  | "false" -> false
  | _ -> invalid_arg "bool_of_string"

let bool_of_string_opt = function
  | "true" -> Some true
  | "false" -> Some false
  | _ -> None

let string_of_int n =
  format_int "%d" n

external int_of_string : string -> int = "caml_int_of_string"

let int_of_string_opt s =
  (* TODO: provide this directly as a non-raising primitive. *)
  try Some (int_of_string s)
  with Failure _ -> None

external string_get : string -> int -> char = "%string_safe_get"

let valid_float_lexem s =
  let l = string_length s in
  let rec loop i =
    if i >= l then s ^ "." else
    match string_get s i with
    | '0' .. '9' | '-' -> loop (i + 1)
    | _ -> s
  in
  loop 0

let string_of_float f = valid_float_lexem (format_float "%.12g" f)

external float_of_string : string -> float = "caml_float_of_string"

let float_of_string_opt s =
  (* TODO: provide this directly as a non-raising primitive. *)
  try Some (float_of_string s)
  with Failure _ -> None

(* List operations -- more in module List *)

let rec ( @ ) l1 l2 =
  match l1 with
    [] -> l2
  | hd :: tl -> hd :: (tl @ l2)

(* I/O operations *)

type in_channel
type out_channel

external open_descriptor_out : int -> out_channel
                             = "caml_ml_open_descriptor_out"
external open_descriptor_in : int -> in_channel = "caml_ml_open_descriptor_in"

let stdin = open_descriptor_in 0
let stdout = open_descriptor_out 1
let stderr = open_descriptor_out 2

(* General output functions *)

type open_flag =
    Open_rdonly | Open_wronly | Open_append
  | Open_creat | Open_trunc | Open_excl
  | Open_binary | Open_text | Open_nonblock

external open_desc : string -> open_flag list -> int -> int = "caml_sys_open"

external set_out_channel_name: out_channel -> string -> unit =
  "caml_ml_set_channel_name"

let open_out_gen mode perm name =
  let c = open_descriptor_out(open_desc name mode perm) in
  set_out_channel_name c name;
  c

let open_out name =
  open_out_gen [Open_wronly; Open_creat; Open_trunc; Open_text] 0o666 name

let open_out_bin name =
  open_out_gen [Open_wronly; Open_creat; Open_trunc; Open_binary] 0o666 name

external flush : out_channel -> unit = "caml_ml_flush"

external out_channels_list : unit -> out_channel list
                           = "caml_ml_out_channels_list"

let flush_all () =
  let rec iter = function
      [] -> ()
    | a::l ->
        begin try
            flush a
        with Sys_error _ ->
          () (* ignore channels closed during a preceding flush. *)
        end;
        iter l
  in iter (out_channels_list ())

external unsafe_output : out_channel -> bytes -> int -> int -> unit
                       = "caml_ml_output_bytes"
external unsafe_output_string : out_channel -> string -> int -> int -> unit
                              = "caml_ml_output"

external output_char : out_channel -> char -> unit = "caml_ml_output_char"

let output_bytes oc s =
  unsafe_output oc s 0 (bytes_length s)

let output_string oc s =
  unsafe_output_string oc s 0 (string_length s)

let output oc s ofs len =
  if ofs < 0 || len < 0 || ofs > bytes_length s - len
  then invalid_arg "output"
  else unsafe_output oc s ofs len

let output_substring oc s ofs len =
  if ofs < 0 || len < 0 || ofs > string_length s - len
  then invalid_arg "output_substring"
  else unsafe_output_string oc s ofs len

external output_byte : out_channel -> int -> unit = "caml_ml_output_char"
external output_binary_int : out_channel -> int -> unit = "caml_ml_output_int"

external marshal_to_channel : out_channel -> 'a -> unit list -> unit
     = "caml_output_value"
let output_value chan v = marshal_to_channel chan v []

external seek_out : out_channel -> int -> unit = "caml_ml_seek_out"
external pos_out : out_channel -> int = "caml_ml_pos_out"
external out_channel_length : out_channel -> int = "caml_ml_channel_size"
external close_out_channel : out_channel -> unit = "caml_ml_close_channel"
let close_out oc = flush oc; close_out_channel oc
let close_out_noerr oc =
  (try flush oc with _ -> ());
  (try close_out_channel oc with _ -> ())
external set_binary_mode_out : out_channel -> bool -> unit
                             = "caml_ml_set_binary_mode"

(* General input functions *)

external set_in_channel_name: in_channel -> string -> unit =
  "caml_ml_set_channel_name"

let open_in_gen mode perm name =
  let c = open_descriptor_in(open_desc name mode perm) in
  set_in_channel_name c name;
  c

let open_in name =
  open_in_gen [Open_rdonly; Open_text] 0 name

let open_in_bin name =
  open_in_gen [Open_rdonly; Open_binary] 0 name

external input_char : in_channel -> char = "caml_ml_input_char"

external unsafe_input : in_channel -> bytes -> int -> int -> int
                      = "caml_ml_input"

let input ic s ofs len =
  if ofs < 0 || len < 0 || ofs > bytes_length s - len
  then invalid_arg "input"
  else unsafe_input ic s ofs len

let rec unsafe_really_input ic s ofs len =
  if len <= 0 then () else begin
    let r = unsafe_input ic s ofs len in
    if r = 0
    then raise End_of_file
    else unsafe_really_input ic s (ofs + r) (len - r)
  end

let really_input ic s ofs len =
  if ofs < 0 || len < 0 || ofs > bytes_length s - len
  then invalid_arg "really_input"
  else unsafe_really_input ic s ofs len

let really_input_string ic len =
  let s = bytes_create len in
  really_input ic s 0 len;
  bytes_unsafe_to_string s

external input_scan_line : in_channel -> int = "caml_ml_input_scan_line"

let input_line chan =
  let rec build_result buf pos = function
    [] -> buf
  | hd :: tl ->
      let len = bytes_length hd in
      bytes_blit hd 0 buf (pos - len) len;
      build_result buf (pos - len) tl in
  let rec scan accu len =
    let n = input_scan_line chan in
    if n = 0 then begin                   (* n = 0: we are at EOF *)
      match accu with
        [] -> raise End_of_file
      | _  -> build_result (bytes_create len) len accu
    end else if n > 0 then begin          (* n > 0: newline found in buffer *)
      let res = bytes_create (n - 1) in
      ignore (unsafe_input chan res 0 (n - 1));
      ignore (input_char chan);           (* skip the newline *)
      match accu with
        [] -> res
      |  _ -> let len = len + n - 1 in
              build_result (bytes_create len) len (res :: accu)
    end else begin                        (* n < 0: newline not found *)
      let beg = bytes_create (-n) in
      ignore(unsafe_input chan beg 0 (-n));
      scan (beg :: accu) (len - n)
    end
  in bytes_unsafe_to_string (scan [] 0)

external input_byte : in_channel -> int = "caml_ml_input_char"
external input_binary_int : in_channel -> int = "caml_ml_input_int"
external input_value : in_channel -> 'a = "caml_input_value"
external seek_in : in_channel -> int -> unit = "caml_ml_seek_in"
external pos_in : in_channel -> int = "caml_ml_pos_in"
external in_channel_length : in_channel -> int = "caml_ml_channel_size"
external close_in : in_channel -> unit = "caml_ml_close_channel"
let close_in_noerr ic = (try close_in ic with _ -> ())
external set_binary_mode_in : in_channel -> bool -> unit
                            = "caml_ml_set_binary_mode"

(* Output functions on standard output *)

let print_char c = output_char stdout c
let print_string s = output_string stdout s
let print_bytes s = output_bytes stdout s
let print_int i = output_string stdout (string_of_int i)
let print_float f = output_string stdout (string_of_float f)
let print_endline s =
  output_string stdout s; output_char stdout '\n'; flush stdout
let print_newline () = output_char stdout '\n'; flush stdout

(* Output functions on standard error *)

let prerr_char c = output_char stderr c
let prerr_string s = output_string stderr s
let prerr_bytes s = output_bytes stderr s
let prerr_int i = output_string stderr (string_of_int i)
let prerr_float f = output_string stderr (string_of_float f)
let prerr_endline s =
  output_string stderr s; output_char stderr '\n'; flush stderr
let prerr_newline () = output_char stderr '\n'; flush stderr

(* Input functions on standard input *)

let read_line () = flush stdout; input_line stdin
let read_int () = int_of_string(read_line())
let read_int_opt () = int_of_string_opt(read_line())
let read_float () = float_of_string(read_line())
let read_float_opt () = float_of_string_opt(read_line())

(* Operations on large files *)

module LargeFile =
  struct
    external seek_out : out_channel -> int64 -> unit = "caml_ml_seek_out_64"
    external pos_out : out_channel -> int64 = "caml_ml_pos_out_64"
    external out_channel_length : out_channel -> int64
                                = "caml_ml_channel_size_64"
    external seek_in : in_channel -> int64 -> unit = "caml_ml_seek_in_64"
    external pos_in : in_channel -> int64 = "caml_ml_pos_in_64"
    external in_channel_length : in_channel -> int64 = "caml_ml_channel_size_64"
  end

(* Formats *)

type ('a, 'b, 'c, 'd, 'e, 'f) format6
   = ('a, 'b, 'c, 'd, 'e, 'f) CamlinternalFormatBasics.format6
   = Format of ('a, 'b, 'c, 'd, 'e, 'f) CamlinternalFormatBasics.fmt
               * string

type ('a, 'b, 'c, 'd) format4 = ('a, 'b, 'c, 'c, 'c, 'd) format6

type ('a, 'b, 'c) format = ('a, 'b, 'c, 'c) format4

let string_of_format (Format (_fmt, str)) = str

external format_of_string :
 ('a, 'b, 'c, 'd, 'e, 'f) format6 ->
 ('a, 'b, 'c, 'd, 'e, 'f) format6 = "%identity"

let ( ^^ ) (Format (fmt1, str1)) (Format (fmt2, str2)) =
  Format (CamlinternalFormatBasics.concat_fmt fmt1 fmt2,
          str1 ^ "%," ^ str2)

(* Miscellaneous *)

external sys_exit : int -> 'a = "caml_sys_exit"

let exit_function = ref flush_all

let at_exit f =
  let g = !exit_function in
  (* MPR#7253, MPR#7796: make sure "f" is executed only once *)
  let f_already_ran = ref false in
  exit_function :=
    (fun () -> 
      if not !f_already_ran then begin f_already_ran := true; f() end;
      g())

let do_at_exit () = (!exit_function) ()

let exit retcode =
  do_at_exit ();
  sys_exit retcode

let _ = register_named_value "Pervasives.do_at_exit" do_at_exit
end

include Pervasives

module Sys = struct
    (**************************************************************************)
    (*                                                                        *)
    (*                                 OCaml                                  *)
    (*                                                                        *)
    (*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
    (*                                                                        *)
    (*   Copyright 1996 Institut National de Recherche en Informatique et     *)
    (*     en Automatique.                                                    *)
    (*                                                                        *)
    (*   All rights reserved.  This file is distributed under the terms of    *)
    (*   the GNU Lesser General Public License version 2.1, with the          *)
    (*   special exception on linking described in the file LICENSE.          *)
    (*                                                                        *)
    (**************************************************************************)

    (* WARNING: sys.ml is generated from sys.mlp.  DO NOT EDIT sys.ml or
    your changes will be lost.
    *)

    type backend_type =
    | Native
    | Bytecode
    | Other of string
    (* System interface *)

    external get_config: unit -> string * int * bool = "caml_sys_get_config"
    external get_argv: unit -> string * string array = "caml_sys_get_argv"
    external big_endian : unit -> bool = "%big_endian"
    external word_size : unit -> int = "%word_size"
    external int_size : unit -> int = "%int_size"
    external max_wosize : unit -> int = "%max_wosize"
    external unix : unit -> bool = "%ostype_unix"
    external win32 : unit -> bool = "%ostype_win32"
    external cygwin : unit -> bool = "%ostype_cygwin"
    external get_backend_type : unit -> backend_type = "%backend_type"

    let (executable_name, argv) = get_argv()
    let (os_type, _, _) = get_config()
    let backend_type = get_backend_type ()
    let big_endian = big_endian ()
    let word_size = word_size ()
    let int_size = int_size ()
    let unix = unix ()
    let win32 = win32 ()
    let cygwin = cygwin ()
    let max_array_length = max_wosize ()
    let max_string_length = word_size / 8 * max_array_length - 1
    external runtime_variant : unit -> string = "caml_runtime_variant"
    external runtime_parameters : unit -> string = "caml_runtime_parameters"

    external file_exists: string -> bool = "caml_sys_file_exists"
    external is_directory : string -> bool = "caml_sys_is_directory"
    external remove: string -> unit = "caml_sys_remove"
    external rename : string -> string -> unit = "caml_sys_rename"
    external getenv: string -> string = "caml_sys_getenv"

    let getenv_opt s =
    (* TODO: expose a non-raising primitive directly. *)
    try Some (getenv s)
    with Not_found -> None

    external command: string -> int = "caml_sys_system_command"
    external time: unit -> (float [@unboxed]) =
    "caml_sys_time" "caml_sys_time_unboxed" [@@noalloc]
    external chdir: string -> unit = "caml_sys_chdir"
    external getcwd: unit -> string = "caml_sys_getcwd"
    external readdir : string -> string array = "caml_sys_read_directory"

    let interactive = ref false

    type signal_behavior =
        Signal_default
    | Signal_ignore
    | Signal_handle of (int -> unit)

    external signal : int -> signal_behavior -> signal_behavior
                    = "caml_install_signal_handler"

    let set_signal sig_num sig_beh = ignore(signal sig_num sig_beh)

    let sigabrt = -1
    let sigalrm = -2
    let sigfpe = -3
    let sighup = -4
    let sigill = -5
    let sigint = -6
    let sigkill = -7
    let sigpipe = -8
    let sigquit = -9
    let sigsegv = -10
    let sigterm = -11
    let sigusr1 = -12
    let sigusr2 = -13
    let sigchld = -14
    let sigcont = -15
    let sigstop = -16
    let sigtstp = -17
    let sigttin = -18
    let sigttou = -19
    let sigvtalrm = -20
    let sigprof = -21
    let sigbus = -22
    let sigpoll = -23
    let sigsys = -24
    let sigtrap = -25
    let sigurg = -26
    let sigxcpu = -27
    let sigxfsz = -28

    exception Break

    let catch_break on =
    if on then
        set_signal sigint (Signal_handle(fun _ -> raise Break))
    else
        set_signal sigint Signal_default


    external enable_runtime_warnings: bool -> unit =
    "caml_ml_enable_runtime_warnings"
    external runtime_warnings_enabled: unit -> bool =
    "caml_ml_runtime_warnings_enabled"

    (* The version string is found in file ../VERSION *)

    let ocaml_version = "%%VERSION%%"

    (* Optimization *)

    external opaque_identity : 'a -> 'a = "%opaque"
end

module Callback = struct
    (**************************************************************************)
    (*                                                                        *)
    (*                                 OCaml                                  *)
    (*                                                                        *)
    (*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
    (*                                                                        *)
    (*   Copyright 1996 Institut National de Recherche en Informatique et     *)
    (*     en Automatique.                                                    *)
    (*                                                                        *)
    (*   All rights reserved.  This file is distributed under the terms of    *)
    (*   the GNU Lesser General Public License version 2.1, with the          *)
    (*   special exception on linking described in the file LICENSE.          *)
    (*                                                                        *)
    (**************************************************************************)

    (* Registering OCaml values with the C runtime for later callbacks *)

    external register_named_value : string -> Obj.t -> unit
                                = "caml_register_named_value"

    let register name v =
    register_named_value name (Obj.repr v)

    let register_exception name (exn : exn) =
    let exn = Obj.repr exn in
    let slot = if Obj.tag exn = Obj.object_tag then exn else Obj.field exn 0 in
    register_named_value name slot
end

module Complex = struct
    (**************************************************************************)
    (*                                                                        *)
    (*                                 OCaml                                  *)
    (*                                                                        *)
    (*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
    (*                                                                        *)
    (*   Copyright 2002 Institut National de Recherche en Informatique et     *)
    (*     en Automatique.                                                    *)
    (*                                                                        *)
    (*   All rights reserved.  This file is distributed under the terms of    *)
    (*   the GNU Lesser General Public License version 2.1, with the          *)
    (*   special exception on linking described in the file LICENSE.          *)
    (*                                                                        *)
    (**************************************************************************)

    (* Complex numbers *)

    type t = { re: float; im: float }

    let zero = { re = 0.0; im = 0.0 }
    let one = { re = 1.0; im = 0.0 }
    let i = { re = 0.0; im = 1.0 }

    let add x y = { re = x.re +. y.re; im = x.im +. y.im }

    let sub x y = { re = x.re -. y.re; im = x.im -. y.im }

    let neg x = { re = -. x.re; im = -. x.im }

    let conj x = { re = x.re; im = -. x.im }

    let mul x y = { re = x.re *. y.re -. x.im *. y.im;
                    im = x.re *. y.im +. x.im *. y.re }

    let div x y =
    if abs_float y.re >= abs_float y.im then
        let r = y.im /. y.re in
        let d = y.re +. r *. y.im in
        { re = (x.re +. r *. x.im) /. d;
        im = (x.im -. r *. x.re) /. d }
    else
        let r = y.re /. y.im in
        let d = y.im +. r *. y.re in
        { re = (r *. x.re +. x.im) /. d;
        im = (r *. x.im -. x.re) /. d }

    let inv x = div one x

    let norm2 x = x.re *. x.re +. x.im *. x.im

    let norm x =
    (* Watch out for overflow in computing re^2 + im^2 *)
    let r = abs_float x.re and i = abs_float x.im in
    if r = 0.0 then i
    else if i = 0.0 then r
    else if r >= i then
        let q = i /. r in r *. sqrt(1.0 +. q *. q)
    else
        let q = r /. i in i *. sqrt(1.0 +. q *. q)

    let arg x = atan2 x.im x.re

    let polar n a = { re = cos a *. n; im = sin a *. n }

    let sqrt x =
    if x.re = 0.0 && x.im = 0.0 then { re = 0.0; im = 0.0 }
    else begin
        let r = abs_float x.re and i = abs_float x.im in
        let w =
        if r >= i then begin
            let q = i /. r in
            sqrt(r) *. sqrt(0.5 *. (1.0 +. sqrt(1.0 +. q *. q)))
        end else begin
            let q = r /. i in
            sqrt(i) *. sqrt(0.5 *. (q +. sqrt(1.0 +. q *. q)))
        end in
        if x.re >= 0.0
        then { re = w;  im = 0.5 *. x.im /. w }
        else { re = 0.5 *. i /. w;  im = if x.im >= 0.0 then w else -. w }
    end

    let exp x =
    let e = exp x.re in { re = e *. cos x.im; im = e *. sin x.im }

    let log x = { re = log (norm x); im = atan2 x.im x.re }

    let pow x y = exp (mul y (log x))
end

module Float = struct
    (**************************************************************************)
    (*                                                                        *)
    (*                                 OCaml                                  *)
    (*                                                                        *)
    (*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
    (*                        Nicolas Ojeda Bar, LexiFi                       *)
    (*                                                                        *)
    (*   Copyright 2018 Institut National de Recherche en Informatique et     *)
    (*     en Automatique.                                                    *)
    (*                                                                        *)
    (*   All rights reserved.  This file is distributed under the terms of    *)
    (*   the GNU Lesser General Public License version 2.1, with the          *)
    (*   special exception on linking described in the file LICENSE.          *)
    (*                                                                        *)
    (**************************************************************************)

    external neg : float -> float = "%negfloat"
    external add : float -> float -> float = "%addfloat"
    external sub : float -> float -> float = "%subfloat"
    external mul : float -> float -> float = "%mulfloat"
    external div : float -> float -> float = "%divfloat"
    external rem : float -> float -> float = "caml_fmod_float" "fmod"
    [@@unboxed] [@@noalloc]
    external abs : float -> float = "%absfloat"
    let infinity = Pervasives.infinity
    let neg_infinity = Pervasives.neg_infinity
    let nan = Pervasives.nan
    let pi = 0x1.921fb54442d18p+1
    let max_float = Pervasives.max_float
    let min_float = Pervasives.min_float
    let epsilon = Pervasives.epsilon_float
    external of_int : int -> float = "%floatofint"
    external to_int : float -> int = "%intoffloat"
    external of_string : string -> float = "caml_float_of_string"
    let of_string_opt = Pervasives.float_of_string_opt
    let to_string = Pervasives.string_of_float
    type fpclass = Pervasives.fpclass =
        FP_normal
    | FP_subnormal
    | FP_zero
    | FP_infinite
    | FP_nan
    external classify_float : (float [@unboxed]) -> fpclass =
    "caml_classify_float" "caml_classify_float_unboxed" [@@noalloc]
    external pow : float -> float -> float = "caml_power_float" "pow"
    [@@unboxed] [@@noalloc]
    external sqrt : float -> float = "caml_sqrt_float" "sqrt"
    [@@unboxed] [@@noalloc]
    external exp : float -> float = "caml_exp_float" "exp" [@@unboxed] [@@noalloc]
    external log : float -> float = "caml_log_float" "log" [@@unboxed] [@@noalloc]
    external log10 : float -> float = "caml_log10_float" "log10"
    [@@unboxed] [@@noalloc]
    external expm1 : float -> float = "caml_expm1_float" "caml_expm1"
    [@@unboxed] [@@noalloc]
    external log1p : float -> float = "caml_log1p_float" "caml_log1p"
    [@@unboxed] [@@noalloc]
    external cos : float -> float = "caml_cos_float" "cos" [@@unboxed] [@@noalloc]
    external sin : float -> float = "caml_sin_float" "sin" [@@unboxed] [@@noalloc]
    external tan : float -> float = "caml_tan_float" "tan" [@@unboxed] [@@noalloc]
    external acos : float -> float = "caml_acos_float" "acos"
    [@@unboxed] [@@noalloc]
    external asin : float -> float = "caml_asin_float" "asin"
    [@@unboxed] [@@noalloc]
    external atan : float -> float = "caml_atan_float" "atan"
    [@@unboxed] [@@noalloc]
    external atan2 : float -> float -> float = "caml_atan2_float" "atan2"
    [@@unboxed] [@@noalloc]
    external hypot : float -> float -> float
                = "caml_hypot_float" "caml_hypot" [@@unboxed] [@@noalloc]
    external cosh : float -> float = "caml_cosh_float" "cosh"
    [@@unboxed] [@@noalloc]
    external sinh : float -> float = "caml_sinh_float" "sinh"
    [@@unboxed] [@@noalloc]
    external tanh : float -> float = "caml_tanh_float" "tanh"
    [@@unboxed] [@@noalloc]
    external ceil : float -> float = "caml_ceil_float" "ceil"
    [@@unboxed] [@@noalloc]
    external floor : float -> float = "caml_floor_float" "floor"
    [@@unboxed] [@@noalloc]
    external copysign : float -> float -> float
                    = "caml_copysign_float" "caml_copysign"
                    [@@unboxed] [@@noalloc]
    external frexp : float -> float * int = "caml_frexp_float"
    external ldexp : (float [@unboxed]) -> (int [@untagged]) -> (float [@unboxed]) =
    "caml_ldexp_float" "caml_ldexp_float_unboxed" [@@noalloc]
    external modf : float -> float * float = "caml_modf_float"
    type t = float
    external compare : float -> float -> int = "%compare"
    let equal x y = compare x y = 0
    external seeded_hash_param : int -> int -> int -> float -> int = "caml_hash" [@@noalloc]
    let hash x = seeded_hash_param 10 100 0 x

    module Array = struct
    type t = floatarray
    external create : int -> t = "caml_floatarray_create"
    external length : t -> int = "%floatarray_length"
    external get : t -> int -> float = "%floatarray_safe_get"
    external set : t -> int -> float -> unit = "%floatarray_safe_set"
    external unsafe_get : t -> int -> float = "%floatarray_unsafe_get"
    external unsafe_set : t -> int -> float -> unit = "%floatarray_unsafe_set"
    end
end

module Char = struct
    (**************************************************************************)
    (*                                                                        *)
    (*                                 OCaml                                  *)
    (*                                                                        *)
    (*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
    (*                                                                        *)
    (*   Copyright 1996 Institut National de Recherche en Informatique et     *)
    (*     en Automatique.                                                    *)
    (*                                                                        *)
    (*   All rights reserved.  This file is distributed under the terms of    *)
    (*   the GNU Lesser General Public License version 2.1, with the          *)
    (*   special exception on linking described in the file LICENSE.          *)
    (*                                                                        *)
    (**************************************************************************)

    (* Character operations *)

    external code: char -> int = "%identity"
    external unsafe_chr: int -> char = "%identity"

    let chr n =
    if n < 0 || n > 255 then invalid_arg "Char.chr" else unsafe_chr n

    external bytes_create: int -> bytes = "caml_create_bytes"
    external bytes_unsafe_set : bytes -> int -> char -> unit
                            = "%bytes_unsafe_set"
    external unsafe_to_string : bytes -> string = "%bytes_to_string"

    let escaped = function
    | '\'' -> "\\'"
    | '\\' -> "\\\\"
    | '\n' -> "\\n"
    | '\t' -> "\\t"
    | '\r' -> "\\r"
    | '\b' -> "\\b"
    | ' ' .. '~' as c ->
        let s = bytes_create 1 in
        bytes_unsafe_set s 0 c;
        unsafe_to_string s
    | c ->
        let n = code c in
        let s = bytes_create 4 in
        bytes_unsafe_set s 0 '\\';
        bytes_unsafe_set s 1 (unsafe_chr (48 + n / 100));
        bytes_unsafe_set s 2 (unsafe_chr (48 + (n / 10) mod 10));
        bytes_unsafe_set s 3 (unsafe_chr (48 + n mod 10));
        unsafe_to_string s

    let lowercase c =
    if (c >= 'A' && c <= 'Z')
    || (c >= '\192' && c <= '\214')
    || (c >= '\216' && c <= '\222')
    then unsafe_chr(code c + 32)
    else c

    let uppercase c =
    if (c >= 'a' && c <= 'z')
    || (c >= '\224' && c <= '\246')
    || (c >= '\248' && c <= '\254')
    then unsafe_chr(code c - 32)
    else c

    let lowercase_ascii c =
    if (c >= 'A' && c <= 'Z')
    then unsafe_chr(code c + 32)
    else c

    let uppercase_ascii c =
    if (c >= 'a' && c <= 'z')
    then unsafe_chr(code c - 32)
    else c

    type t = char

    let compare c1 c2 = code c1 - code c2
    let equal (c1: t) (c2: t) = compare c1 c2 = 0
end

module Bytes = struct
    (**************************************************************************)
    (*                                                                        *)
    (*                                 OCaml                                  *)
    (*                                                                        *)
    (*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
    (*                                                                        *)
    (*   Copyright 1996 Institut National de Recherche en Informatique et     *)
    (*     en Automatique.                                                    *)
    (*                                                                        *)
    (*   All rights reserved.  This file is distributed under the terms of    *)
    (*   the GNU Lesser General Public License version 2.1, with the          *)
    (*   special exception on linking described in the file LICENSE.          *)
    (*                                                                        *)
    (**************************************************************************)

    (* Byte sequence operations *)

    (* WARNING: Some functions in this file are duplicated in string.ml for
    efficiency reasons. When you modify the one in this file you need to
    modify its duplicate in string.ml.
    These functions have a "duplicated" comment above their definition.
    *)

    external length : bytes -> int = "%bytes_length"
    external string_length : string -> int = "%string_length"
    external get : bytes -> int -> char = "%bytes_safe_get"
    external set : bytes -> int -> char -> unit = "%bytes_safe_set"
    external create : int -> bytes = "caml_create_bytes"
    external unsafe_get : bytes -> int -> char = "%bytes_unsafe_get"
    external unsafe_set : bytes -> int -> char -> unit = "%bytes_unsafe_set"
    external unsafe_fill : bytes -> int -> int -> char -> unit
                        = "caml_fill_bytes" [@@noalloc]
    external unsafe_to_string : bytes -> string = "%bytes_to_string"
    external unsafe_of_string : string -> bytes = "%bytes_of_string"

    external unsafe_blit : bytes -> int -> bytes -> int -> int -> unit
                        = "caml_blit_bytes" [@@noalloc]
    external unsafe_blit_string : string -> int -> bytes -> int -> int -> unit
                        = "caml_blit_string" [@@noalloc]

    let make n c =
    let s = create n in
    unsafe_fill s 0 n c;
    s

    let init n f =
    let s = create n in
    for i = 0 to n - 1 do
        unsafe_set s i (f i)
    done;
    s

    let empty = create 0

    let copy s =
    let len = length s in
    let r = create len in
    unsafe_blit s 0 r 0 len;
    r

    let to_string b = unsafe_to_string (copy b)
    let of_string s = copy (unsafe_of_string s)

    let sub s ofs len =
    if ofs < 0 || len < 0 || ofs > length s - len
    then invalid_arg "String.sub / Bytes.sub"
    else begin
        let r = create len in
        unsafe_blit s ofs r 0 len;
        r
    end

    let sub_string b ofs len = unsafe_to_string (sub b ofs len)

    (* addition with an overflow check *)
    let (++) a b =
    let c = a + b in
    match a < 0, b < 0, c < 0 with
    | true , true , false
    | false, false, true  -> invalid_arg "Bytes.extend" (* overflow *)
    | _ -> c

    let extend s left right =
    let len = length s ++ left ++ right in
    let r = create len in
    let (srcoff, dstoff) = if left < 0 then -left, 0 else 0, left in
    let cpylen = min (length s - srcoff) (len - dstoff) in
    if cpylen > 0 then unsafe_blit s srcoff r dstoff cpylen;
    r

    let fill s ofs len c =
    if ofs < 0 || len < 0 || ofs > length s - len
    then invalid_arg "String.fill / Bytes.fill"
    else unsafe_fill s ofs len c

    let blit s1 ofs1 s2 ofs2 len =
    if len < 0 || ofs1 < 0 || ofs1 > length s1 - len
                || ofs2 < 0 || ofs2 > length s2 - len
    then invalid_arg "Bytes.blit"
    else unsafe_blit s1 ofs1 s2 ofs2 len

    let blit_string s1 ofs1 s2 ofs2 len =
    if len < 0 || ofs1 < 0 || ofs1 > string_length s1 - len
                || ofs2 < 0 || ofs2 > length s2 - len
    then invalid_arg "String.blit / Bytes.blit_string"
    else unsafe_blit_string s1 ofs1 s2 ofs2 len

    (* duplicated in string.ml *)
    let iter f a =
    for i = 0 to length a - 1 do f(unsafe_get a i) done

    (* duplicated in string.ml *)
    let iteri f a =
    for i = 0 to length a - 1 do f i (unsafe_get a i) done

    let ensure_ge (x:int) y = if x >= y then x else invalid_arg "Bytes.concat"

    let rec sum_lengths acc seplen = function
    | [] -> acc
    | hd :: [] -> length hd + acc
    | hd :: tl -> sum_lengths (ensure_ge (length hd + seplen + acc) acc) seplen tl

    let rec unsafe_blits dst pos sep seplen = function
        [] -> dst
    | hd :: [] ->
        unsafe_blit hd 0 dst pos (length hd); dst
    | hd :: tl ->
        unsafe_blit hd 0 dst pos (length hd);
        unsafe_blit sep 0 dst (pos + length hd) seplen;
        unsafe_blits dst (pos + length hd + seplen) sep seplen tl

    let concat sep = function
        [] -> empty
    | l -> let seplen = length sep in
            unsafe_blits
                (create (sum_lengths 0 seplen l))
                0 sep seplen l

    let cat s1 s2 =
    let l1 = length s1 in
    let l2 = length s2 in
    let r = create (l1 + l2) in
    unsafe_blit s1 0 r 0 l1;
    unsafe_blit s2 0 r l1 l2;
    r


    external char_code: char -> int = "%identity"
    external char_chr: int -> char = "%identity"

    let is_space = function
    | ' ' | '\012' | '\n' | '\r' | '\t' -> true
    | _ -> false

    let trim s =
    let len = length s in
    let i = ref 0 in
    while !i < len && is_space (unsafe_get s !i) do
        incr i
    done;
    let j = ref (len - 1) in
    while !j >= !i && is_space (unsafe_get s !j) do
        decr j
    done;
    if !j >= !i then
        sub s !i (!j - !i + 1)
    else
        empty

    let escaped s =
    let n = ref 0 in
    for i = 0 to length s - 1 do
        n := !n +
        (match unsafe_get s i with
        | '\"' | '\\' | '\n' | '\t' | '\r' | '\b' -> 2
        | ' ' .. '~' -> 1
        | _ -> 4)
    done;
    if !n = length s then copy s else begin
        let s' = create !n in
        n := 0;
        for i = 0 to length s - 1 do
        begin match unsafe_get s i with
        | ('\"' | '\\') as c ->
            unsafe_set s' !n '\\'; incr n; unsafe_set s' !n c
        | '\n' ->
            unsafe_set s' !n '\\'; incr n; unsafe_set s' !n 'n'
        | '\t' ->
            unsafe_set s' !n '\\'; incr n; unsafe_set s' !n 't'
        | '\r' ->
            unsafe_set s' !n '\\'; incr n; unsafe_set s' !n 'r'
        | '\b' ->
            unsafe_set s' !n '\\'; incr n; unsafe_set s' !n 'b'
        | (' ' .. '~') as c -> unsafe_set s' !n c
        | c ->
            let a = char_code c in
            unsafe_set s' !n '\\';
            incr n;
            unsafe_set s' !n (char_chr (48 + a / 100));
            incr n;
            unsafe_set s' !n (char_chr (48 + (a / 10) mod 10));
            incr n;
            unsafe_set s' !n (char_chr (48 + a mod 10));
        end;
        incr n
        done;
        s'
    end

    let map f s =
    let l = length s in
    if l = 0 then s else begin
        let r = create l in
        for i = 0 to l - 1 do unsafe_set r i (f (unsafe_get s i)) done;
        r
    end

    let mapi f s =
    let l = length s in
    if l = 0 then s else begin
        let r = create l in
        for i = 0 to l - 1 do unsafe_set r i (f i (unsafe_get s i)) done;
        r
    end

    let uppercase_ascii s = map Char.uppercase_ascii s
    let lowercase_ascii s = map Char.lowercase_ascii s

    let apply1 f s =
    if length s = 0 then s else begin
        let r = copy s in
        unsafe_set r 0 (f(unsafe_get s 0));
        r
    end

    let capitalize_ascii s = apply1 Char.uppercase_ascii s
    let uncapitalize_ascii s = apply1 Char.lowercase_ascii s

    (* duplicated in string.ml *)
    let rec index_rec s lim i c =
    if i >= lim then raise Not_found else
    if unsafe_get s i = c then i else index_rec s lim (i + 1) c

    (* duplicated in string.ml *)
    let index s c = index_rec s (length s) 0 c

    (* duplicated in string.ml *)
    let rec index_rec_opt s lim i c =
    if i >= lim then None else
    if unsafe_get s i = c then Some i else index_rec_opt s lim (i + 1) c

    (* duplicated in string.ml *)
    let index_opt s c = index_rec_opt s (length s) 0 c

    (* duplicated in string.ml *)
    let index_from s i c =
    let l = length s in
    if i < 0 || i > l then invalid_arg "String.index_from / Bytes.index_from" else
    index_rec s l i c

    (* duplicated in string.ml *)
    let index_from_opt s i c =
    let l = length s in
    if i < 0 || i > l then invalid_arg "String.index_from_opt / Bytes.index_from_opt" else
    index_rec_opt s l i c

    (* duplicated in string.ml *)
    let rec rindex_rec s i c =
    if i < 0 then raise Not_found else
    if unsafe_get s i = c then i else rindex_rec s (i - 1) c

    (* duplicated in string.ml *)
    let rindex s c = rindex_rec s (length s - 1) c

    (* duplicated in string.ml *)
    let rindex_from s i c =
    if i < -1 || i >= length s then
        invalid_arg "String.rindex_from / Bytes.rindex_from"
    else
        rindex_rec s i c

    (* duplicated in string.ml *)
    let rec rindex_rec_opt s i c =
    if i < 0 then None else
    if unsafe_get s i = c then Some i else rindex_rec_opt s (i - 1) c

    (* duplicated in string.ml *)
    let rindex_opt s c = rindex_rec_opt s (length s - 1) c

    (* duplicated in string.ml *)
    let rindex_from_opt s i c =
    if i < -1 || i >= length s then
        invalid_arg "String.rindex_from_opt / Bytes.rindex_from_opt"
    else
        rindex_rec_opt s i c


    (* duplicated in string.ml *)
    let contains_from s i c =
    let l = length s in
    if i < 0 || i > l then
        invalid_arg "String.contains_from / Bytes.contains_from"
    else
        try ignore (index_rec s l i c); true with Not_found -> false


    (* duplicated in string.ml *)
    let contains s c = contains_from s 0 c

    (* duplicated in string.ml *)
    let rcontains_from s i c =
    if i < 0 || i >= length s then
        invalid_arg "String.rcontains_from / Bytes.rcontains_from"
    else
        try ignore (rindex_rec s i c); true with Not_found -> false


    type t = bytes

    let compare (x: t) (y: t) = Pervasives.compare x y
    external equal : t -> t -> bool = "caml_bytes_equal"

    (* Deprecated functions implemented via other deprecated functions *)
    [@@@ocaml.warning "-3"]
    let uppercase s = map Char.uppercase s
    let lowercase s = map Char.lowercase s

    let capitalize s = apply1 Char.uppercase s
    let uncapitalize s = apply1 Char.lowercase s

    (** {6 Iterators} *)

    let to_seq s =
    let rec aux i () =
        if i = length s then Seq.Nil
        else
        let x = get s i in
        Seq.Cons (x, aux (i+1))
    in
    aux 0

    let to_seqi s =
    let rec aux i () =
        if i = length s then Seq.Nil
        else
        let x = get s i in
        Seq.Cons ((i,x), aux (i+1))
    in
    aux 0

    let of_seq i =
    let n = ref 0 in
    let buf = ref (make 256 '\000') in
    let resize () =
        (* resize *)
        let new_len = min (2 * length !buf) Sys.max_string_length in
        if length !buf = new_len then failwith "Bytes.of_seq: cannot grow bytes";
        let new_buf = make new_len '\000' in
        blit !buf 0 new_buf 0 !n;
        buf := new_buf
    in
    Seq.iter
        (fun c ->
        if !n = length !buf then resize();
        set !buf !n c;
        incr n)
        i;
    sub !buf 0 !n
end

module String = struct
    (**************************************************************************)
    (*                                                                        *)
    (*                                 OCaml                                  *)
    (*                                                                        *)
    (*           Damien Doligez, projet Gallium, INRIA Rocquencourt           *)
    (*                                                                        *)
    (*   Copyright 2014 Institut National de Recherche en Informatique et     *)
    (*     en Automatique.                                                    *)
    (*                                                                        *)
    (*   All rights reserved.  This file is distributed under the terms of    *)
    (*   the GNU Lesser General Public License version 2.1, with the          *)
    (*   special exception on linking described in the file LICENSE.          *)
    (*                                                                        *)
    (**************************************************************************)

    (* String operations, based on byte sequence operations *)

    (* WARNING: Some functions in this file are duplicated in bytes.ml for
    efficiency reasons. When you modify the one in this file you need to
    modify its duplicate in bytes.ml.
    These functions have a "duplicated" comment above their definition.
    *)

    external length : string -> int = "%string_length"
    external get : string -> int -> char = "%string_safe_get"
    external set : bytes -> int -> char -> unit = "%string_safe_set"
    external create : int -> bytes = "caml_create_string"
    external unsafe_get : string -> int -> char = "%string_unsafe_get"
    external unsafe_set : bytes -> int -> char -> unit = "%string_unsafe_set"
    external unsafe_blit : string -> int ->  bytes -> int -> int -> unit
                        = "caml_blit_string" [@@noalloc]
    external unsafe_fill : bytes -> int -> int -> char -> unit
                        = "caml_fill_string" [@@noalloc]

    module B = Bytes

    let bts = B.unsafe_to_string
    let bos = B.unsafe_of_string

    let make n c =
    B.make n c |> bts
    let init n f =
    B.init n f |> bts
    let copy s =
    B.copy (bos s) |> bts
    let sub s ofs len =
    B.sub (bos s) ofs len |> bts
    let fill =
    B.fill
    let blit =
    B.blit_string

    let ensure_ge (x:int) y = if x >= y then x else invalid_arg "String.concat"

    let rec sum_lengths acc seplen = function
    | [] -> acc
    | hd :: [] -> length hd + acc
    | hd :: tl -> sum_lengths (ensure_ge (length hd + seplen + acc) acc) seplen tl

    let rec unsafe_blits dst pos sep seplen = function
        [] -> dst
    | hd :: [] ->
        unsafe_blit hd 0 dst pos (length hd); dst
    | hd :: tl ->
        unsafe_blit hd 0 dst pos (length hd);
        unsafe_blit sep 0 dst (pos + length hd) seplen;
        unsafe_blits dst (pos + length hd + seplen) sep seplen tl

    let concat sep = function
        [] -> ""
    | l -> let seplen = length sep in bts @@
            unsafe_blits
                (B.create (sum_lengths 0 seplen l))
                0 sep seplen l

    (* duplicated in bytes.ml *)
    let iter f s =
    for i = 0 to length s - 1 do f (unsafe_get s i) done

    (* duplicated in bytes.ml *)
    let iteri f s =
    for i = 0 to length s - 1 do f i (unsafe_get s i) done

    let map f s =
    B.map f (bos s) |> bts
    let mapi f s =
    B.mapi f (bos s) |> bts

    (* Beware: we cannot use B.trim or B.escape because they always make a
    copy, but String.mli spells out some cases where we are not allowed
    to make a copy. *)

    let is_space = function
    | ' ' | '\012' | '\n' | '\r' | '\t' -> true
    | _ -> false

    let trim s =
    if s = "" then s
    else if is_space (unsafe_get s 0) || is_space (unsafe_get s (length s - 1))
        then bts (B.trim (bos s))
    else s

    let escaped s =
    let rec escape_if_needed s n i =
        if i >= n then s else
        match unsafe_get s i with
        | '\"' | '\\' | '\000'..'\031' | '\127'.. '\255' -> bts (B.escaped (bos s))
        | _ -> escape_if_needed s n (i+1)
    in
    escape_if_needed s (length s) 0

    (* duplicated in bytes.ml *)
    let rec index_rec s lim i c =
    if i >= lim then raise Not_found else
    if unsafe_get s i = c then i else index_rec s lim (i + 1) c

    (* duplicated in bytes.ml *)
    let index s c = index_rec s (length s) 0 c

    (* duplicated in bytes.ml *)
    let rec index_rec_opt s lim i c =
    if i >= lim then None else
    if unsafe_get s i = c then Some i else index_rec_opt s lim (i + 1) c

    (* duplicated in bytes.ml *)
    let index_opt s c = index_rec_opt s (length s) 0 c

    (* duplicated in bytes.ml *)
    let index_from s i c =
    let l = length s in
    if i < 0 || i > l then invalid_arg "String.index_from / Bytes.index_from" else
        index_rec s l i c

    (* duplicated in bytes.ml *)
    let index_from_opt s i c =
    let l = length s in
    if i < 0 || i > l then invalid_arg "String.index_from_opt / Bytes.index_from_opt" else
    index_rec_opt s l i c

    (* duplicated in bytes.ml *)
    let rec rindex_rec s i c =
    if i < 0 then raise Not_found else
    if unsafe_get s i = c then i else rindex_rec s (i - 1) c

    (* duplicated in bytes.ml *)
    let rindex s c = rindex_rec s (length s - 1) c

    (* duplicated in bytes.ml *)
    let rindex_from s i c =
    if i < -1 || i >= length s then
        invalid_arg "String.rindex_from / Bytes.rindex_from"
    else
        rindex_rec s i c

    (* duplicated in bytes.ml *)
    let rec rindex_rec_opt s i c =
    if i < 0 then None else
    if unsafe_get s i = c then Some i else rindex_rec_opt s (i - 1) c

    (* duplicated in bytes.ml *)
    let rindex_opt s c = rindex_rec_opt s (length s - 1) c

    (* duplicated in bytes.ml *)
    let rindex_from_opt s i c =
    if i < -1 || i >= length s then
        invalid_arg "String.rindex_from_opt / Bytes.rindex_from_opt"
    else
        rindex_rec_opt s i c

    (* duplicated in bytes.ml *)
    let contains_from s i c =
    let l = length s in
    if i < 0 || i > l then
        invalid_arg "String.contains_from / Bytes.contains_from"
    else
        try ignore (index_rec s l i c); true with Not_found -> false

    (* duplicated in bytes.ml *)
    let contains s c = contains_from s 0 c

    (* duplicated in bytes.ml *)
    let rcontains_from s i c =
    if i < 0 || i >= length s then
        invalid_arg "String.rcontains_from / Bytes.rcontains_from"
    else
        try ignore (rindex_rec s i c); true with Not_found -> false

    let uppercase_ascii s =
    B.uppercase_ascii (bos s) |> bts
    let lowercase_ascii s =
    B.lowercase_ascii (bos s) |> bts
    let capitalize_ascii s =
    B.capitalize_ascii (bos s) |> bts
    let uncapitalize_ascii s =
    B.uncapitalize_ascii (bos s) |> bts

    type t = string

    let compare (x: t) (y: t) = Pervasives.compare x y
    external equal : string -> string -> bool = "caml_string_equal"

    let split_on_char sep s =
    let r = ref [] in
    let j = ref (length s) in
    for i = length s - 1 downto 0 do
        if unsafe_get s i = sep then begin
        r := sub s (i + 1) (!j - i - 1) :: !r;
        j := i
        end
    done;
    sub s 0 !j :: !r

    (* Deprecated functions implemented via other deprecated functions *)
    [@@@ocaml.warning "-3"]
    let uppercase s =
    B.uppercase (bos s) |> bts
    let lowercase s =
    B.lowercase (bos s) |> bts
    let capitalize s =
    B.capitalize (bos s) |> bts
    let uncapitalize s =
    B.uncapitalize (bos s) |> bts

    (** {6 Iterators} *)

    let to_seq s = bos s |> B.to_seq

    let to_seqi s = bos s |> B.to_seqi

    let of_seq g = B.of_seq g |> bts
end

module BytesLabels = struct
    (**************************************************************************)
    (*                                                                        *)
    (*                                 OCaml                                  *)
    (*                                                                        *)
    (*                Jacques Garrigue, Kyoto University RIMS                 *)
    (*                                                                        *)
    (*   Copyright 2001 Institut National de Recherche en Informatique et     *)
    (*     en Automatique.                                                    *)
    (*                                                                        *)
    (*   All rights reserved.  This file is distributed under the terms of    *)
    (*   the GNU Lesser General Public License version 2.1, with the          *)
    (*   special exception on linking described in the file LICENSE.          *)
    (*                                                                        *)
    (**************************************************************************)

    (* Module [BytesLabels]: labelled Bytes module *)

    include Bytes
end

module StringLabels = struct
    (**************************************************************************)
    (*                                                                        *)
    (*                                 OCaml                                  *)
    (*                                                                        *)
    (*                Jacques Garrigue, Kyoto University RIMS                 *)
    (*                                                                        *)
    (*   Copyright 2001 Institut National de Recherche en Informatique et     *)
    (*     en Automatique.                                                    *)
    (*                                                                        *)
    (*   All rights reserved.  This file is distributed under the terms of    *)
    (*   the GNU Lesser General Public License version 2.1, with the          *)
    (*   special exception on linking described in the file LICENSE.          *)
    (*                                                                        *)
    (**************************************************************************)

    (* Module [StringLabels]: labelled String module *)

    include String
end

module Seq = struct
    (**************************************************************************)
    (*                                                                        *)
    (*                                 OCaml                                  *)
    (*                                                                        *)
    (*                 Simon Cruanes                                          *)
    (*                                                                        *)
    (*   Copyright 2017 Institut National de Recherche en Informatique et     *)
    (*     en Automatique.                                                    *)
    (*                                                                        *)
    (*   All rights reserved.  This file is distributed under the terms of    *)
    (*   the GNU Lesser General Public License version 2.1, with the          *)
    (*   special exception on linking described in the file LICENSE.          *)
    (*                                                                        *)
    (**************************************************************************)

    (* Module [Seq]: functional iterators *)

    type +'a node =
    | Nil
    | Cons of 'a * 'a t

    and 'a t = unit -> 'a node

    let empty () = Nil

    let return x () = Cons (x, empty)

    let rec map f seq () = match seq() with
    | Nil -> Nil
    | Cons (x, next) -> Cons (f x, map f next)

    let rec filter_map f seq () = match seq() with
    | Nil -> Nil
    | Cons (x, next) ->
        match f x with
            | None -> filter_map f next ()
            | Some y -> Cons (y, filter_map f next)

    let rec filter f seq () = match seq() with
    | Nil -> Nil
    | Cons (x, next) ->
        if f x
        then Cons (x, filter f next)
        else filter f next ()

    let rec flat_map f seq () = match seq () with
    | Nil -> Nil
    | Cons (x, next) ->
        flat_map_app f (f x) next ()

    (* this is [append seq (flat_map f tail)] *)
    and flat_map_app f seq tail () = match seq () with
    | Nil -> flat_map f tail ()
    | Cons (x, next) ->
        Cons (x, flat_map_app f next tail)

    let fold_left f acc seq =
    let rec aux f acc seq = match seq () with
        | Nil -> acc
        | Cons (x, next) ->
            let acc = f acc x in
            aux f acc next
    in
    aux f acc seq

    let iter f seq =
    let rec aux seq = match seq () with
        | Nil -> ()
        | Cons (x, next) ->
            f x;
            aux next
    in
    aux seq
end

module List = struct
    (**************************************************************************)
    (*                                                                        *)
    (*                                 OCaml                                  *)
    (*                                                                        *)
    (*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
    (*                                                                        *)
    (*   Copyright 1996 Institut National de Recherche en Informatique et     *)
    (*     en Automatique.                                                    *)
    (*                                                                        *)
    (*   All rights reserved.  This file is distributed under the terms of    *)
    (*   the GNU Lesser General Public License version 2.1, with the          *)
    (*   special exception on linking described in the file LICENSE.          *)
    (*                                                                        *)
    (**************************************************************************)

    (* List operations *)

    let rec length_aux len = function
        [] -> len
    | _::l -> length_aux (len + 1) l

    let length l = length_aux 0 l

    let cons a l = a::l

    let hd = function
        [] -> failwith "hd"
    | a::_ -> a

    let tl = function
        [] -> failwith "tl"
    | _::l -> l

    let nth l n =
    if n < 0 then invalid_arg "List.nth" else
    let rec nth_aux l n =
        match l with
        | [] -> failwith "nth"
        | a::l -> if n = 0 then a else nth_aux l (n-1)
    in nth_aux l n

    let nth_opt l n =
    if n < 0 then invalid_arg "List.nth" else
    let rec nth_aux l n =
        match l with
        | [] -> None
        | a::l -> if n = 0 then Some a else nth_aux l (n-1)
    in nth_aux l n

    let append = (@)

    let rec rev_append l1 l2 =
    match l1 with
        [] -> l2
    | a :: l -> rev_append l (a :: l2)

    let rev l = rev_append l []

    let rec init_tailrec_aux acc i n f =
    if i >= n then acc
    else init_tailrec_aux (f i :: acc) (i+1) n f

    let rec init_aux i n f =
    if i >= n then []
    else
        let r = f i in
        r :: init_aux (i+1) n f

    let rev_init_threshold =
    match Sys.backend_type with
    | Sys.Native | Sys.Bytecode -> 10_000
    (* We don't known the size of the stack, better be safe and assume it's small. *)
    | Sys.Other _ -> 50

    let init len f =
    if len < 0 then invalid_arg "List.init" else
    if len > rev_init_threshold then rev (init_tailrec_aux [] 0 len f)
    else init_aux 0 len f

    let rec flatten = function
        [] -> []
    | l::r -> l @ flatten r

    let concat = flatten

    let rec map f = function
        [] -> []
    | a::l -> let r = f a in r :: map f l

    let rec mapi i f = function
        [] -> []
    | a::l -> let r = f i a in r :: mapi (i + 1) f l

    let mapi f l = mapi 0 f l

    let rev_map f l =
    let rec rmap_f accu = function
        | [] -> accu
        | a::l -> rmap_f (f a :: accu) l
    in
    rmap_f [] l


    let rec iter f = function
        [] -> ()
    | a::l -> f a; iter f l

    let rec iteri i f = function
        [] -> ()
    | a::l -> f i a; iteri (i + 1) f l

    let iteri f l = iteri 0 f l

    let rec fold_left f accu l =
    match l with
        [] -> accu
    | a::l -> fold_left f (f accu a) l

    let rec fold_right f l accu =
    match l with
        [] -> accu
    | a::l -> f a (fold_right f l accu)

    let rec map2 f l1 l2 =
    match (l1, l2) with
        ([], []) -> []
    | (a1::l1, a2::l2) -> let r = f a1 a2 in r :: map2 f l1 l2
    | (_, _) -> invalid_arg "List.map2"

    let rev_map2 f l1 l2 =
    let rec rmap2_f accu l1 l2 =
        match (l1, l2) with
        | ([], []) -> accu
        | (a1::l1, a2::l2) -> rmap2_f (f a1 a2 :: accu) l1 l2
        | (_, _) -> invalid_arg "List.rev_map2"
    in
    rmap2_f [] l1 l2


    let rec iter2 f l1 l2 =
    match (l1, l2) with
        ([], []) -> ()
    | (a1::l1, a2::l2) -> f a1 a2; iter2 f l1 l2
    | (_, _) -> invalid_arg "List.iter2"

    let rec fold_left2 f accu l1 l2 =
    match (l1, l2) with
        ([], []) -> accu
    | (a1::l1, a2::l2) -> fold_left2 f (f accu a1 a2) l1 l2
    | (_, _) -> invalid_arg "List.fold_left2"

    let rec fold_right2 f l1 l2 accu =
    match (l1, l2) with
        ([], []) -> accu
    | (a1::l1, a2::l2) -> f a1 a2 (fold_right2 f l1 l2 accu)
    | (_, _) -> invalid_arg "List.fold_right2"

    let rec for_all p = function
        [] -> true
    | a::l -> p a && for_all p l

    let rec exists p = function
        [] -> false
    | a::l -> p a || exists p l

    let rec for_all2 p l1 l2 =
    match (l1, l2) with
        ([], []) -> true
    | (a1::l1, a2::l2) -> p a1 a2 && for_all2 p l1 l2
    | (_, _) -> invalid_arg "List.for_all2"

    let rec exists2 p l1 l2 =
    match (l1, l2) with
        ([], []) -> false
    | (a1::l1, a2::l2) -> p a1 a2 || exists2 p l1 l2
    | (_, _) -> invalid_arg "List.exists2"

    let rec mem x = function
        [] -> false
    | a::l -> compare a x = 0 || mem x l

    let rec memq x = function
        [] -> false
    | a::l -> a == x || memq x l

    let rec assoc x = function
        [] -> raise Not_found
    | (a,b)::l -> if compare a x = 0 then b else assoc x l

    let rec assoc_opt x = function
        [] -> None
    | (a,b)::l -> if compare a x = 0 then Some b else assoc_opt x l

    let rec assq x = function
        [] -> raise Not_found
    | (a,b)::l -> if a == x then b else assq x l

    let rec assq_opt x = function
        [] -> None
    | (a,b)::l -> if a == x then Some b else assq_opt x l

    let rec mem_assoc x = function
    | [] -> false
    | (a, _) :: l -> compare a x = 0 || mem_assoc x l

    let rec mem_assq x = function
    | [] -> false
    | (a, _) :: l -> a == x || mem_assq x l

    let rec remove_assoc x = function
    | [] -> []
    | (a, _ as pair) :: l ->
        if compare a x = 0 then l else pair :: remove_assoc x l

    let rec remove_assq x = function
    | [] -> []
    | (a, _ as pair) :: l -> if a == x then l else pair :: remove_assq x l

    let rec find p = function
    | [] -> raise Not_found
    | x :: l -> if p x then x else find p l

    let rec find_opt p = function
    | [] -> None
    | x :: l -> if p x then Some x else find_opt p l

    let find_all p =
    let rec find accu = function
    | [] -> rev accu
    | x :: l -> if p x then find (x :: accu) l else find accu l in
    find []

    let filter = find_all

    let partition p l =
    let rec part yes no = function
    | [] -> (rev yes, rev no)
    | x :: l -> if p x then part (x :: yes) no l else part yes (x :: no) l in
    part [] [] l

    let rec split = function
        [] -> ([], [])
    | (x,y)::l ->
        let (rx, ry) = split l in (x::rx, y::ry)

    let rec combine l1 l2 =
    match (l1, l2) with
        ([], []) -> []
    | (a1::l1, a2::l2) -> (a1, a2) :: combine l1 l2
    | (_, _) -> invalid_arg "List.combine"

    (** sorting *)

    let rec merge cmp l1 l2 =
    match l1, l2 with
    | [], l2 -> l2
    | l1, [] -> l1
    | h1 :: t1, h2 :: t2 ->
        if cmp h1 h2 <= 0
        then h1 :: merge cmp t1 l2
        else h2 :: merge cmp l1 t2


    let rec chop k l =
    if k = 0 then l else begin
        match l with
        | _::t -> chop (k-1) t
        | _ -> assert false
    end


    let stable_sort cmp l =
    let rec rev_merge l1 l2 accu =
        match l1, l2 with
        | [], l2 -> rev_append l2 accu
        | l1, [] -> rev_append l1 accu
        | h1::t1, h2::t2 ->
            if cmp h1 h2 <= 0
            then rev_merge t1 l2 (h1::accu)
            else rev_merge l1 t2 (h2::accu)
    in
    let rec rev_merge_rev l1 l2 accu =
        match l1, l2 with
        | [], l2 -> rev_append l2 accu
        | l1, [] -> rev_append l1 accu
        | h1::t1, h2::t2 ->
            if cmp h1 h2 > 0
            then rev_merge_rev t1 l2 (h1::accu)
            else rev_merge_rev l1 t2 (h2::accu)
    in
    let rec sort n l =
        match n, l with
        | 2, x1 :: x2 :: _ ->
        if cmp x1 x2 <= 0 then [x1; x2] else [x2; x1]
        | 3, x1 :: x2 :: x3 :: _ ->
        if cmp x1 x2 <= 0 then begin
            if cmp x2 x3 <= 0 then [x1; x2; x3]
            else if cmp x1 x3 <= 0 then [x1; x3; x2]
            else [x3; x1; x2]
        end else begin
            if cmp x1 x3 <= 0 then [x2; x1; x3]
            else if cmp x2 x3 <= 0 then [x2; x3; x1]
            else [x3; x2; x1]
        end
        | n, l ->
        let n1 = n asr 1 in
        let n2 = n - n1 in
        let l2 = chop n1 l in
        let s1 = rev_sort n1 l in
        let s2 = rev_sort n2 l2 in
        rev_merge_rev s1 s2 []
    and rev_sort n l =
        match n, l with
        | 2, x1 :: x2 :: _ ->
        if cmp x1 x2 > 0 then [x1; x2] else [x2; x1]
        | 3, x1 :: x2 :: x3 :: _ ->
        if cmp x1 x2 > 0 then begin
            if cmp x2 x3 > 0 then [x1; x2; x3]
            else if cmp x1 x3 > 0 then [x1; x3; x2]
            else [x3; x1; x2]
        end else begin
            if cmp x1 x3 > 0 then [x2; x1; x3]
            else if cmp x2 x3 > 0 then [x2; x3; x1]
            else [x3; x2; x1]
        end
        | n, l ->
        let n1 = n asr 1 in
        let n2 = n - n1 in
        let l2 = chop n1 l in
        let s1 = sort n1 l in
        let s2 = sort n2 l2 in
        rev_merge s1 s2 []
    in
    let len = length l in
    if len < 2 then l else sort len l


    let sort = stable_sort
    let fast_sort = stable_sort

    (* Note: on a list of length between about 100000 (depending on the minor
    heap size and the type of the list) and Sys.max_array_size, it is
    actually faster to use the following, but it might also use more memory
    because the argument list cannot be deallocated incrementally.

    Also, there seems to be a bug in this code or in the
    implementation of obj_truncate.

    external obj_truncate : 'a array -> int -> unit = "caml_obj_truncate"

    let array_to_list_in_place a =
    let l = Array.length a in
    let rec loop accu n p =
        if p <= 0 then accu else begin
        if p = n then begin
            obj_truncate a p;
            loop (a.(p-1) :: accu) (n-1000) (p-1)
        end else begin
            loop (a.(p-1) :: accu) n (p-1)
        end
        end
    in
    loop [] (l-1000) l


    let stable_sort cmp l =
    let a = Array.of_list l in
    Array.stable_sort cmp a;
    array_to_list_in_place a

    *)


    (** sorting + removing duplicates *)

    let sort_uniq cmp l =
    let rec rev_merge l1 l2 accu =
        match l1, l2 with
        | [], l2 -> rev_append l2 accu
        | l1, [] -> rev_append l1 accu
        | h1::t1, h2::t2 ->
            let c = cmp h1 h2 in
            if c = 0 then rev_merge t1 t2 (h1::accu)
            else if c < 0
            then rev_merge t1 l2 (h1::accu)
            else rev_merge l1 t2 (h2::accu)
    in
    let rec rev_merge_rev l1 l2 accu =
        match l1, l2 with
        | [], l2 -> rev_append l2 accu
        | l1, [] -> rev_append l1 accu
        | h1::t1, h2::t2 ->
            let c = cmp h1 h2 in
            if c = 0 then rev_merge_rev t1 t2 (h1::accu)
            else if c > 0
            then rev_merge_rev t1 l2 (h1::accu)
            else rev_merge_rev l1 t2 (h2::accu)
    in
    let rec sort n l =
        match n, l with
        | 2, x1 :: x2 :: _ ->
        let c = cmp x1 x2 in
        if c = 0 then [x1]
        else if c < 0 then [x1; x2] else [x2; x1]
        | 3, x1 :: x2 :: x3 :: _ ->
        let c = cmp x1 x2 in
        if c = 0 then begin
            let c = cmp x2 x3 in
            if c = 0 then [x2]
            else if c < 0 then [x2; x3] else [x3; x2]
        end else if c < 0 then begin
            let c = cmp x2 x3 in
            if c = 0 then [x1; x2]
            else if c < 0 then [x1; x2; x3]
            else let c = cmp x1 x3 in
            if c = 0 then [x1; x2]
            else if c < 0 then [x1; x3; x2]
            else [x3; x1; x2]
        end else begin
            let c = cmp x1 x3 in
            if c = 0 then [x2; x1]
            else if c < 0 then [x2; x1; x3]
            else let c = cmp x2 x3 in
            if c = 0 then [x2; x1]
            else if c < 0 then [x2; x3; x1]
            else [x3; x2; x1]
        end
        | n, l ->
        let n1 = n asr 1 in
        let n2 = n - n1 in
        let l2 = chop n1 l in
        let s1 = rev_sort n1 l in
        let s2 = rev_sort n2 l2 in
        rev_merge_rev s1 s2 []
    and rev_sort n l =
        match n, l with
        | 2, x1 :: x2 :: _ ->
        let c = cmp x1 x2 in
        if c = 0 then [x1]
        else if c > 0 then [x1; x2] else [x2; x1]
        | 3, x1 :: x2 :: x3 :: _ ->
        let c = cmp x1 x2 in
        if c = 0 then begin
            let c = cmp x2 x3 in
            if c = 0 then [x2]
            else if c > 0 then [x2; x3] else [x3; x2]
        end else if c > 0 then begin
            let c = cmp x2 x3 in
            if c = 0 then [x1; x2]
            else if c > 0 then [x1; x2; x3]
            else let c = cmp x1 x3 in
            if c = 0 then [x1; x2]
            else if c > 0 then [x1; x3; x2]
            else [x3; x1; x2]
        end else begin
            let c = cmp x1 x3 in
            if c = 0 then [x2; x1]
            else if c > 0 then [x2; x1; x3]
            else let c = cmp x2 x3 in
            if c = 0 then [x2; x1]
            else if c > 0 then [x2; x3; x1]
            else [x3; x2; x1]
        end
        | n, l ->
        let n1 = n asr 1 in
        let n2 = n - n1 in
        let l2 = chop n1 l in
        let s1 = sort n1 l in
        let s2 = sort n2 l2 in
        rev_merge s1 s2 []
    in
    let len = length l in
    if len < 2 then l else sort len l

    let rec compare_lengths l1 l2 =
    match l1, l2 with
    | [], [] -> 0
    | [], _ -> -1
    | _, [] -> 1
    | _ :: l1, _ :: l2 -> compare_lengths l1 l2
    ;;

    let rec compare_length_with l n =
    match l with
    | [] ->
        if n = 0 then 0 else
        if n > 0 then -1 else 1
    | _ :: l ->
        if n <= 0 then 1 else
        compare_length_with l (n-1)
    ;;

    (** {6 Iterators} *)

    let to_seq l =
    let rec aux l () = match l with
        | [] -> Seq.Nil
        | x :: tail -> Seq.Cons (x, aux tail)
    in
    aux l

    let of_seq seq =
    let rec direct depth seq : _ list =
        if depth=0
        then
        Seq.fold_left (fun acc x -> x::acc) [] seq
        |> rev (* tailrec *)
        else match seq() with
        | Seq.Nil -> []
        | Seq.Cons (x, next) -> x :: direct (depth-1) next
    in
    direct 500 seq
end

module ListLabels = struct
    (**************************************************************************)
    (*                                                                        *)
    (*                                 OCaml                                  *)
    (*                                                                        *)
    (*                Jacques Garrigue, Kyoto University RIMS                 *)
    (*                                                                        *)
    (*   Copyright 2001 Institut National de Recherche en Informatique et     *)
    (*     en Automatique.                                                    *)
    (*                                                                        *)
    (*   All rights reserved.  This file is distributed under the terms of    *)
    (*   the GNU Lesser General Public License version 2.1, with the          *)
    (*   special exception on linking described in the file LICENSE.          *)
    (*                                                                        *)
    (**************************************************************************)

    (* Module [ListLabels]: labelled List module *)

    include List
end

module Set = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Sets over ordered types *)

module type OrderedType =
  sig
    type t
    val compare: t -> t -> int
  end

module type S =
  sig
    type elt
    type t
    val empty: t
    val is_empty: t -> bool
    val mem: elt -> t -> bool
    val add: elt -> t -> t
    val singleton: elt -> t
    val remove: elt -> t -> t
    val union: t -> t -> t
    val inter: t -> t -> t
    val diff: t -> t -> t
    val compare: t -> t -> int
    val equal: t -> t -> bool
    val subset: t -> t -> bool
    val iter: (elt -> unit) -> t -> unit
    val map: (elt -> elt) -> t -> t
    val fold: (elt -> 'a -> 'a) -> t -> 'a -> 'a
    val for_all: (elt -> bool) -> t -> bool
    val exists: (elt -> bool) -> t -> bool
    val filter: (elt -> bool) -> t -> t
    val partition: (elt -> bool) -> t -> t * t
    val cardinal: t -> int
    val elements: t -> elt list
    val min_elt: t -> elt
    val min_elt_opt: t -> elt option
    val max_elt: t -> elt
    val max_elt_opt: t -> elt option
    val choose: t -> elt
    val choose_opt: t -> elt option
    val split: elt -> t -> t * bool * t
    val find: elt -> t -> elt
    val find_opt: elt -> t -> elt option
    val find_first: (elt -> bool) -> t -> elt
    val find_first_opt: (elt -> bool) -> t -> elt option
    val find_last: (elt -> bool) -> t -> elt
    val find_last_opt: (elt -> bool) -> t -> elt option
    val of_list: elt list -> t
    val to_seq_from : elt -> t -> elt Seq.t
    val to_seq : t -> elt Seq.t
    val add_seq : elt Seq.t -> t -> t
    val of_seq : elt Seq.t -> t
  end

module Make(Ord: OrderedType) =
  struct
    type elt = Ord.t
    type t = Empty | Node of {l:t; v:elt; r:t; h:int}

    (* Sets are represented by balanced binary trees (the heights of the
       children differ by at most 2 *)

    let height = function
        Empty -> 0
      | Node {h} -> h

    (* Creates a new node with left son l, value v and right son r.
       We must have all elements of l < v < all elements of r.
       l and r must be balanced and | height l - height r | <= 2.
       Inline expansion of height for better speed. *)

    let create l v r =
      let hl = match l with Empty -> 0 | Node {h} -> h in
      let hr = match r with Empty -> 0 | Node {h} -> h in
      Node{l; v; r; h=(if hl >= hr then hl + 1 else hr + 1)}

    (* Same as create, but performs one step of rebalancing if necessary.
       Assumes l and r balanced and | height l - height r | <= 3.
       Inline expansion of create for better speed in the most frequent case
       where no rebalancing is required. *)

    let bal l v r =
      let hl = match l with Empty -> 0 | Node {h} -> h in
      let hr = match r with Empty -> 0 | Node {h} -> h in
      if hl > hr + 2 then begin
        match l with
          Empty -> invalid_arg "Set.bal"
        | Node{l=ll; v=lv; r=lr} ->
            if height ll >= height lr then
              create ll lv (create lr v r)
            else begin
              match lr with
                Empty -> invalid_arg "Set.bal"
              | Node{l=lrl; v=lrv; r=lrr}->
                  create (create ll lv lrl) lrv (create lrr v r)
            end
      end else if hr > hl + 2 then begin
        match r with
          Empty -> invalid_arg "Set.bal"
        | Node{l=rl; v=rv; r=rr} ->
            if height rr >= height rl then
              create (create l v rl) rv rr
            else begin
              match rl with
                Empty -> invalid_arg "Set.bal"
              | Node{l=rll; v=rlv; r=rlr} ->
                  create (create l v rll) rlv (create rlr rv rr)
            end
      end else
        Node{l; v; r; h=(if hl >= hr then hl + 1 else hr + 1)}

    (* Insertion of one element *)

    let rec add x = function
        Empty -> Node{l=Empty; v=x; r=Empty; h=1}
      | Node{l; v; r} as t ->
          let c = Ord.compare x v in
          if c = 0 then t else
          if c < 0 then
            let ll = add x l in
            if l == ll then t else bal ll v r
          else
            let rr = add x r in
            if r == rr then t else bal l v rr

    let singleton x = Node{l=Empty; v=x; r=Empty; h=1}

    (* Beware: those two functions assume that the added v is *strictly*
       smaller (or bigger) than all the present elements in the tree; it
       does not test for equality with the current min (or max) element.
       Indeed, they are only used during the "join" operation which
       respects this precondition.
    *)

    let rec add_min_element x = function
      | Empty -> singleton x
      | Node {l; v; r} ->
        bal (add_min_element x l) v r

    let rec add_max_element x = function
      | Empty -> singleton x
      | Node {l; v; r} ->
        bal l v (add_max_element x r)

    (* Same as create and bal, but no assumptions are made on the
       relative heights of l and r. *)

    let rec join l v r =
      match (l, r) with
        (Empty, _) -> add_min_element v r
      | (_, Empty) -> add_max_element v l
      | (Node{l=ll; v=lv; r=lr; h=lh}, Node{l=rl; v=rv; r=rr; h=rh}) ->
          if lh > rh + 2 then bal ll lv (join lr v r) else
          if rh > lh + 2 then bal (join l v rl) rv rr else
          create l v r

    (* Smallest and greatest element of a set *)

    let rec min_elt = function
        Empty -> raise Not_found
      | Node{l=Empty; v} -> v
      | Node{l} -> min_elt l

    let rec min_elt_opt = function
        Empty -> None
      | Node{l=Empty; v} -> Some v
      | Node{l} -> min_elt_opt l

    let rec max_elt = function
        Empty -> raise Not_found
      | Node{v; r=Empty} -> v
      | Node{r} -> max_elt r

    let rec max_elt_opt = function
        Empty -> None
      | Node{v; r=Empty} -> Some v
      | Node{r} -> max_elt_opt r

    (* Remove the smallest element of the given set *)

    let rec remove_min_elt = function
        Empty -> invalid_arg "Set.remove_min_elt"
      | Node{l=Empty; r} -> r
      | Node{l; v; r} -> bal (remove_min_elt l) v r

    (* Merge two trees l and r into one.
       All elements of l must precede the elements of r.
       Assume | height l - height r | <= 2. *)

    let merge t1 t2 =
      match (t1, t2) with
        (Empty, t) -> t
      | (t, Empty) -> t
      | (_, _) -> bal t1 (min_elt t2) (remove_min_elt t2)

    (* Merge two trees l and r into one.
       All elements of l must precede the elements of r.
       No assumption on the heights of l and r. *)

    let concat t1 t2 =
      match (t1, t2) with
        (Empty, t) -> t
      | (t, Empty) -> t
      | (_, _) -> join t1 (min_elt t2) (remove_min_elt t2)

    (* Splitting.  split x s returns a triple (l, present, r) where
        - l is the set of elements of s that are < x
        - r is the set of elements of s that are > x
        - present is false if s contains no element equal to x,
          or true if s contains an element equal to x. *)

    let rec split x = function
        Empty ->
          (Empty, false, Empty)
      | Node{l; v; r} ->
          let c = Ord.compare x v in
          if c = 0 then (l, true, r)
          else if c < 0 then
            let (ll, pres, rl) = split x l in (ll, pres, join rl v r)
          else
            let (lr, pres, rr) = split x r in (join l v lr, pres, rr)

    (* Implementation of the set operations *)

    let empty = Empty

    let is_empty = function Empty -> true | _ -> false

    let rec mem x = function
        Empty -> false
      | Node{l; v; r} ->
          let c = Ord.compare x v in
          c = 0 || mem x (if c < 0 then l else r)

    let rec remove x = function
        Empty -> Empty
      | (Node{l; v; r} as t) ->
          let c = Ord.compare x v in
          if c = 0 then merge l r
          else
            if c < 0 then
              let ll = remove x l in
              if l == ll then t
              else bal ll v r
            else
              let rr = remove x r in
              if r == rr then t
              else bal l v rr

    let rec union s1 s2 =
      match (s1, s2) with
        (Empty, t2) -> t2
      | (t1, Empty) -> t1
      | (Node{l=l1; v=v1; r=r1; h=h1}, Node{l=l2; v=v2; r=r2; h=h2}) ->
          if h1 >= h2 then
            if h2 = 1 then add v2 s1 else begin
              let (l2, _, r2) = split v1 s2 in
              join (union l1 l2) v1 (union r1 r2)
            end
          else
            if h1 = 1 then add v1 s2 else begin
              let (l1, _, r1) = split v2 s1 in
              join (union l1 l2) v2 (union r1 r2)
            end

    let rec inter s1 s2 =
      match (s1, s2) with
        (Empty, _) -> Empty
      | (_, Empty) -> Empty
      | (Node{l=l1; v=v1; r=r1}, t2) ->
          match split v1 t2 with
            (l2, false, r2) ->
              concat (inter l1 l2) (inter r1 r2)
          | (l2, true, r2) ->
              join (inter l1 l2) v1 (inter r1 r2)

    let rec diff s1 s2 =
      match (s1, s2) with
        (Empty, _) -> Empty
      | (t1, Empty) -> t1
      | (Node{l=l1; v=v1; r=r1}, t2) ->
          match split v1 t2 with
            (l2, false, r2) ->
              join (diff l1 l2) v1 (diff r1 r2)
          | (l2, true, r2) ->
              concat (diff l1 l2) (diff r1 r2)

    type enumeration = End | More of elt * t * enumeration

    let rec cons_enum s e =
      match s with
        Empty -> e
      | Node{l; v; r} -> cons_enum l (More(v, r, e))

    let rec compare_aux e1 e2 =
        match (e1, e2) with
        (End, End) -> 0
      | (End, _)  -> -1
      | (_, End) -> 1
      | (More(v1, r1, e1), More(v2, r2, e2)) ->
          let c = Ord.compare v1 v2 in
          if c <> 0
          then c
          else compare_aux (cons_enum r1 e1) (cons_enum r2 e2)

    let compare s1 s2 =
      compare_aux (cons_enum s1 End) (cons_enum s2 End)

    let equal s1 s2 =
      compare s1 s2 = 0

    let rec subset s1 s2 =
      match (s1, s2) with
        Empty, _ ->
          true
      | _, Empty ->
          false
      | Node {l=l1; v=v1; r=r1}, (Node {l=l2; v=v2; r=r2} as t2) ->
          let c = Ord.compare v1 v2 in
          if c = 0 then
            subset l1 l2 && subset r1 r2
          else if c < 0 then
            subset (Node {l=l1; v=v1; r=Empty; h=0}) l2 && subset r1 t2
          else
            subset (Node {l=Empty; v=v1; r=r1; h=0}) r2 && subset l1 t2

    let rec iter f = function
        Empty -> ()
      | Node{l; v; r} -> iter f l; f v; iter f r

    let rec fold f s accu =
      match s with
        Empty -> accu
      | Node{l; v; r} -> fold f r (f v (fold f l accu))

    let rec for_all p = function
        Empty -> true
      | Node{l; v; r} -> p v && for_all p l && for_all p r

    let rec exists p = function
        Empty -> false
      | Node{l; v; r} -> p v || exists p l || exists p r

    let rec filter p = function
        Empty -> Empty
      | (Node{l; v; r}) as t ->
          (* call [p] in the expected left-to-right order *)
          let l' = filter p l in
          let pv = p v in
          let r' = filter p r in
          if pv then
            if l==l' && r==r' then t else join l' v r'
          else concat l' r'

    let rec partition p = function
        Empty -> (Empty, Empty)
      | Node{l; v; r} ->
          (* call [p] in the expected left-to-right order *)
          let (lt, lf) = partition p l in
          let pv = p v in
          let (rt, rf) = partition p r in
          if pv
          then (join lt v rt, concat lf rf)
          else (concat lt rt, join lf v rf)

    let rec cardinal = function
        Empty -> 0
      | Node{l; r} -> cardinal l + 1 + cardinal r

    let rec elements_aux accu = function
        Empty -> accu
      | Node{l; v; r} -> elements_aux (v :: elements_aux accu r) l

    let elements s =
      elements_aux [] s

    let choose = min_elt

    let choose_opt = min_elt_opt

    let rec find x = function
        Empty -> raise Not_found
      | Node{l; v; r} ->
          let c = Ord.compare x v in
          if c = 0 then v
          else find x (if c < 0 then l else r)

    let rec find_first_aux v0 f = function
        Empty ->
          v0
      | Node{l; v; r} ->
          if f v then
            find_first_aux v f l
          else
            find_first_aux v0 f r

    let rec find_first f = function
        Empty ->
          raise Not_found
      | Node{l; v; r} ->
          if f v then
            find_first_aux v f l
          else
            find_first f r

    let rec find_first_opt_aux v0 f = function
        Empty ->
          Some v0
      | Node{l; v; r} ->
          if f v then
            find_first_opt_aux v f l
          else
            find_first_opt_aux v0 f r

    let rec find_first_opt f = function
        Empty ->
          None
      | Node{l; v; r} ->
          if f v then
            find_first_opt_aux v f l
          else
            find_first_opt f r

    let rec find_last_aux v0 f = function
        Empty ->
          v0
      | Node{l; v; r} ->
          if f v then
            find_last_aux v f r
          else
            find_last_aux v0 f l

    let rec find_last f = function
        Empty ->
          raise Not_found
      | Node{l; v; r} ->
          if f v then
            find_last_aux v f r
          else
            find_last f l

    let rec find_last_opt_aux v0 f = function
        Empty ->
          Some v0
      | Node{l; v; r} ->
          if f v then
            find_last_opt_aux v f r
          else
            find_last_opt_aux v0 f l

    let rec find_last_opt f = function
        Empty ->
          None
      | Node{l; v; r} ->
          if f v then
            find_last_opt_aux v f r
          else
            find_last_opt f l

    let rec find_opt x = function
        Empty -> None
      | Node{l; v; r} ->
          let c = Ord.compare x v in
          if c = 0 then Some v
          else find_opt x (if c < 0 then l else r)

    let try_join l v r =
      (* [join l v r] can only be called when (elements of l < v <
         elements of r); use [try_join l v r] when this property may
         not hold, but you hope it does hold in the common case *)
      if (l = Empty || Ord.compare (max_elt l) v < 0)
      && (r = Empty || Ord.compare v (min_elt r) < 0)
      then join l v r
      else union l (add v r)

    let rec map f = function
      | Empty -> Empty
      | Node{l; v; r} as t ->
         (* enforce left-to-right evaluation order *)
         let l' = map f l in
         let v' = f v in
         let r' = map f r in
         if l == l' && v == v' && r == r' then t
         else try_join l' v' r'

    let of_sorted_list l =
      let rec sub n l =
        match n, l with
        | 0, l -> Empty, l
        | 1, x0 :: l -> Node {l=Empty; v=x0; r=Empty; h=1}, l
        | 2, x0 :: x1 :: l ->
            Node{l=Node{l=Empty; v=x0; r=Empty; h=1}; v=x1; r=Empty; h=2}, l
        | 3, x0 :: x1 :: x2 :: l ->
            Node{l=Node{l=Empty; v=x0; r=Empty; h=1}; v=x1;
                 r=Node{l=Empty; v=x2; r=Empty; h=1}; h=2}, l
        | n, l ->
          let nl = n / 2 in
          let left, l = sub nl l in
          match l with
          | [] -> assert false
          | mid :: l ->
            let right, l = sub (n - nl - 1) l in
            create left mid right, l
      in
      fst (sub (List.length l) l)

    let of_list l =
      match l with
      | [] -> empty
      | [x0] -> singleton x0
      | [x0; x1] -> add x1 (singleton x0)
      | [x0; x1; x2] -> add x2 (add x1 (singleton x0))
      | [x0; x1; x2; x3] -> add x3 (add x2 (add x1 (singleton x0)))
      | [x0; x1; x2; x3; x4] -> add x4 (add x3 (add x2 (add x1 (singleton x0))))
      | _ -> of_sorted_list (List.sort_uniq Ord.compare l)

    let add_seq i m =
      Seq.fold_left (fun s x -> add x s) m i

    let of_seq i = add_seq i empty

    let rec seq_of_enum_ c () = match c with
      | End -> Seq.Nil
      | More (x, t, rest) -> Seq.Cons (x, seq_of_enum_ (cons_enum t rest))

    let to_seq c = seq_of_enum_ (cons_enum c End)

    let to_seq_from low s =
      let rec aux low s c = match s with
        | Empty -> c
        | Node {l; r; v; _} ->
            begin match Ord.compare v low with
              | 0 -> More (v, r, c)
              | n when n<0 -> aux low r c
              | _ -> aux low l (More (v, r, c))
            end
      in
      seq_of_enum_ (aux low s End)
  end
end

module Map = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

module type OrderedType =
  sig
    type t
    val compare: t -> t -> int
  end

module type S =
  sig
    type key
    type +'a t
    val empty: 'a t
    val is_empty: 'a t -> bool
    val mem:  key -> 'a t -> bool
    val add: key -> 'a -> 'a t -> 'a t
    val update: key -> ('a option -> 'a option) -> 'a t -> 'a t
    val singleton: key -> 'a -> 'a t
    val remove: key -> 'a t -> 'a t
    val merge:
          (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t
    val union: (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t
    val compare: ('a -> 'a -> int) -> 'a t -> 'a t -> int
    val equal: ('a -> 'a -> bool) -> 'a t -> 'a t -> bool
    val iter: (key -> 'a -> unit) -> 'a t -> unit
    val fold: (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
    val for_all: (key -> 'a -> bool) -> 'a t -> bool
    val exists: (key -> 'a -> bool) -> 'a t -> bool
    val filter: (key -> 'a -> bool) -> 'a t -> 'a t
    val partition: (key -> 'a -> bool) -> 'a t -> 'a t * 'a t
    val cardinal: 'a t -> int
    val bindings: 'a t -> (key * 'a) list
    val min_binding: 'a t -> (key * 'a)
    val min_binding_opt: 'a t -> (key * 'a) option
    val max_binding: 'a t -> (key * 'a)
    val max_binding_opt: 'a t -> (key * 'a) option
    val choose: 'a t -> (key * 'a)
    val choose_opt: 'a t -> (key * 'a) option
    val split: key -> 'a t -> 'a t * 'a option * 'a t
    val find: key -> 'a t -> 'a
    val find_opt: key -> 'a t -> 'a option
    val find_first: (key -> bool) -> 'a t -> key * 'a
    val find_first_opt: (key -> bool) -> 'a t -> (key * 'a) option
    val find_last: (key -> bool) -> 'a t -> key * 'a
    val find_last_opt: (key -> bool) -> 'a t -> (key * 'a) option
    val map: ('a -> 'b) -> 'a t -> 'b t
    val mapi: (key -> 'a -> 'b) -> 'a t -> 'b t
    val to_seq : 'a t -> (key * 'a) Seq.t
    val to_seq_from : key -> 'a t -> (key * 'a) Seq.t
    val add_seq : (key * 'a) Seq.t -> 'a t -> 'a t
    val of_seq : (key * 'a) Seq.t -> 'a t
  end

module Make(Ord: OrderedType) = struct

    type key = Ord.t

    type 'a t =
        Empty
      | Node of {l:'a t; v:key; d:'a; r:'a t; h:int}

    let height = function
        Empty -> 0
      | Node {h} -> h

    let create l x d r =
      let hl = height l and hr = height r in
      Node{l; v=x; d; r; h=(if hl >= hr then hl + 1 else hr + 1)}

    let singleton x d = Node{l=Empty; v=x; d; r=Empty; h=1}

    let bal l x d r =
      let hl = match l with Empty -> 0 | Node {h} -> h in
      let hr = match r with Empty -> 0 | Node {h} -> h in
      if hl > hr + 2 then begin
        match l with
          Empty -> invalid_arg "Map.bal"
        | Node{l=ll; v=lv; d=ld; r=lr} ->
            if height ll >= height lr then
              create ll lv ld (create lr x d r)
            else begin
              match lr with
                Empty -> invalid_arg "Map.bal"
              | Node{l=lrl; v=lrv; d=lrd; r=lrr}->
                  create (create ll lv ld lrl) lrv lrd (create lrr x d r)
            end
      end else if hr > hl + 2 then begin
        match r with
          Empty -> invalid_arg "Map.bal"
        | Node{l=rl; v=rv; d=rd; r=rr} ->
            if height rr >= height rl then
              create (create l x d rl) rv rd rr
            else begin
              match rl with
                Empty -> invalid_arg "Map.bal"
              | Node{l=rll; v=rlv; d=rld; r=rlr} ->
                  create (create l x d rll) rlv rld (create rlr rv rd rr)
            end
      end else
        Node{l; v=x; d; r; h=(if hl >= hr then hl + 1 else hr + 1)}

    let empty = Empty

    let is_empty = function Empty -> true | _ -> false

    let rec add x data = function
        Empty ->
          Node{l=Empty; v=x; d=data; r=Empty; h=1}
      | Node {l; v; d; r; h} as m ->
          let c = Ord.compare x v in
          if c = 0 then
            if d == data then m else Node{l; v=x; d=data; r; h}
          else if c < 0 then
            let ll = add x data l in
            if l == ll then m else bal ll v d r
          else
            let rr = add x data r in
            if r == rr then m else bal l v d rr

    let rec find x = function
        Empty ->
          raise Not_found
      | Node {l; v; d; r} ->
          let c = Ord.compare x v in
          if c = 0 then d
          else find x (if c < 0 then l else r)

    let rec find_first_aux v0 d0 f = function
        Empty ->
          (v0, d0)
      | Node {l; v; d; r} ->
          if f v then
            find_first_aux v d f l
          else
            find_first_aux v0 d0 f r

    let rec find_first f = function
        Empty ->
          raise Not_found
      | Node {l; v; d; r} ->
          if f v then
            find_first_aux v d f l
          else
            find_first f r

    let rec find_first_opt_aux v0 d0 f = function
        Empty ->
          Some (v0, d0)
      | Node {l; v; d; r} ->
          if f v then
            find_first_opt_aux v d f l
          else
            find_first_opt_aux v0 d0 f r

    let rec find_first_opt f = function
        Empty ->
          None
      | Node {l; v; d; r} ->
          if f v then
            find_first_opt_aux v d f l
          else
            find_first_opt f r

    let rec find_last_aux v0 d0 f = function
        Empty ->
          (v0, d0)
      | Node {l; v; d; r} ->
          if f v then
            find_last_aux v d f r
          else
            find_last_aux v0 d0 f l

    let rec find_last f = function
        Empty ->
          raise Not_found
      | Node {l; v; d; r} ->
          if f v then
            find_last_aux v d f r
          else
            find_last f l

    let rec find_last_opt_aux v0 d0 f = function
        Empty ->
          Some (v0, d0)
      | Node {l; v; d; r} ->
          if f v then
            find_last_opt_aux v d f r
          else
            find_last_opt_aux v0 d0 f l

    let rec find_last_opt f = function
        Empty ->
          None
      | Node {l; v; d; r} ->
          if f v then
            find_last_opt_aux v d f r
          else
            find_last_opt f l

    let rec find_opt x = function
        Empty ->
          None
      | Node {l; v; d; r} ->
          let c = Ord.compare x v in
          if c = 0 then Some d
          else find_opt x (if c < 0 then l else r)

    let rec mem x = function
        Empty ->
          false
      | Node {l; v; r} ->
          let c = Ord.compare x v in
          c = 0 || mem x (if c < 0 then l else r)

    let rec min_binding = function
        Empty -> raise Not_found
      | Node {l=Empty; v; d} -> (v, d)
      | Node {l} -> min_binding l

    let rec min_binding_opt = function
        Empty -> None
      | Node {l=Empty; v; d} -> Some (v, d)
      | Node {l}-> min_binding_opt l

    let rec max_binding = function
        Empty -> raise Not_found
      | Node {v; d; r=Empty} -> (v, d)
      | Node {r} -> max_binding r

    let rec max_binding_opt = function
        Empty -> None
      | Node {v; d; r=Empty} -> Some (v, d)
      | Node {r} -> max_binding_opt r

    let rec remove_min_binding = function
        Empty -> invalid_arg "Map.remove_min_elt"
      | Node {l=Empty; r} -> r
      | Node {l; v; d; r} -> bal (remove_min_binding l) v d r

    let merge t1 t2 =
      match (t1, t2) with
        (Empty, t) -> t
      | (t, Empty) -> t
      | (_, _) ->
          let (x, d) = min_binding t2 in
          bal t1 x d (remove_min_binding t2)

    let rec remove x = function
        Empty ->
          Empty
      | (Node {l; v; d; r} as m) ->
          let c = Ord.compare x v in
          if c = 0 then merge l r
          else if c < 0 then
            let ll = remove x l in if l == ll then m else bal ll v d r
          else
            let rr = remove x r in if r == rr then m else bal l v d rr

    let rec update x f = function
        Empty ->
          begin match f None with
          | None -> Empty
          | Some data -> Node{l=Empty; v=x; d=data; r=Empty; h=1}
          end
      | Node {l; v; d; r; h} as m ->
          let c = Ord.compare x v in
          if c = 0 then begin
            match f (Some d) with
            | None -> merge l r
            | Some data ->
                if d == data then m else Node{l; v=x; d=data; r; h}
          end else if c < 0 then
            let ll = update x f l in
            if l == ll then m else bal ll v d r
          else
            let rr = update x f r in
            if r == rr then m else bal l v d rr

    let rec iter f = function
        Empty -> ()
      | Node {l; v; d; r} ->
          iter f l; f v d; iter f r

    let rec map f = function
        Empty ->
          Empty
      | Node {l; v; d; r; h} ->
          let l' = map f l in
          let d' = f d in
          let r' = map f r in
          Node{l=l'; v; d=d'; r=r'; h}

    let rec mapi f = function
        Empty ->
          Empty
      | Node {l; v; d; r; h} ->
          let l' = mapi f l in
          let d' = f v d in
          let r' = mapi f r in
          Node{l=l'; v; d=d'; r=r'; h}

    let rec fold f m accu =
      match m with
        Empty -> accu
      | Node {l; v; d; r} ->
          fold f r (f v d (fold f l accu))

    let rec for_all p = function
        Empty -> true
      | Node {l; v; d; r} -> p v d && for_all p l && for_all p r

    let rec exists p = function
        Empty -> false
      | Node {l; v; d; r} -> p v d || exists p l || exists p r

    (* Beware: those two functions assume that the added k is *strictly*
       smaller (or bigger) than all the present keys in the tree; it
       does not test for equality with the current min (or max) key.

       Indeed, they are only used during the "join" operation which
       respects this precondition.
    *)

    let rec add_min_binding k x = function
      | Empty -> singleton k x
      | Node {l; v; d; r} ->
        bal (add_min_binding k x l) v d r

    let rec add_max_binding k x = function
      | Empty -> singleton k x
      | Node {l; v; d; r} ->
        bal l v d (add_max_binding k x r)

    (* Same as create and bal, but no assumptions are made on the
       relative heights of l and r. *)

    let rec join l v d r =
      match (l, r) with
        (Empty, _) -> add_min_binding v d r
      | (_, Empty) -> add_max_binding v d l
      | (Node{l=ll; v=lv; d=ld; r=lr; h=lh}, Node{l=rl; v=rv; d=rd; r=rr; h=rh}) ->
          if lh > rh + 2 then bal ll lv ld (join lr v d r) else
          if rh > lh + 2 then bal (join l v d rl) rv rd rr else
          create l v d r

    (* Merge two trees l and r into one.
       All elements of l must precede the elements of r.
       No assumption on the heights of l and r. *)

    let concat t1 t2 =
      match (t1, t2) with
        (Empty, t) -> t
      | (t, Empty) -> t
      | (_, _) ->
          let (x, d) = min_binding t2 in
          join t1 x d (remove_min_binding t2)

    let concat_or_join t1 v d t2 =
      match d with
      | Some d -> join t1 v d t2
      | None -> concat t1 t2

    let rec split x = function
        Empty ->
          (Empty, None, Empty)
      | Node {l; v; d; r} ->
          let c = Ord.compare x v in
          if c = 0 then (l, Some d, r)
          else if c < 0 then
            let (ll, pres, rl) = split x l in (ll, pres, join rl v d r)
          else
            let (lr, pres, rr) = split x r in (join l v d lr, pres, rr)

    let rec merge f s1 s2 =
      match (s1, s2) with
        (Empty, Empty) -> Empty
      | (Node {l=l1; v=v1; d=d1; r=r1; h=h1}, _) when h1 >= height s2 ->
          let (l2, d2, r2) = split v1 s2 in
          concat_or_join (merge f l1 l2) v1 (f v1 (Some d1) d2) (merge f r1 r2)
      | (_, Node {l=l2; v=v2; d=d2; r=r2}) ->
          let (l1, d1, r1) = split v2 s1 in
          concat_or_join (merge f l1 l2) v2 (f v2 d1 (Some d2)) (merge f r1 r2)
      | _ ->
          assert false

    let rec union f s1 s2 =
      match (s1, s2) with
      | (Empty, s) | (s, Empty) -> s
      | (Node {l=l1; v=v1; d=d1; r=r1; h=h1}, Node {l=l2; v=v2; d=d2; r=r2; h=h2}) ->
          if h1 >= h2 then
            let (l2, d2, r2) = split v1 s2 in
            let l = union f l1 l2 and r = union f r1 r2 in
            match d2 with
            | None -> join l v1 d1 r
            | Some d2 -> concat_or_join l v1 (f v1 d1 d2) r
          else
            let (l1, d1, r1) = split v2 s1 in
            let l = union f l1 l2 and r = union f r1 r2 in
            match d1 with
            | None -> join l v2 d2 r
            | Some d1 -> concat_or_join l v2 (f v2 d1 d2) r

    let rec filter p = function
        Empty -> Empty
      | Node {l; v; d; r} as m ->
          (* call [p] in the expected left-to-right order *)
          let l' = filter p l in
          let pvd = p v d in
          let r' = filter p r in
          if pvd then if l==l' && r==r' then m else join l' v d r'
          else concat l' r'

    let rec partition p = function
        Empty -> (Empty, Empty)
      | Node {l; v; d; r} ->
          (* call [p] in the expected left-to-right order *)
          let (lt, lf) = partition p l in
          let pvd = p v d in
          let (rt, rf) = partition p r in
          if pvd
          then (join lt v d rt, concat lf rf)
          else (concat lt rt, join lf v d rf)

    type 'a enumeration = End | More of key * 'a * 'a t * 'a enumeration

    let rec cons_enum m e =
      match m with
        Empty -> e
      | Node {l; v; d; r} -> cons_enum l (More(v, d, r, e))

    let compare cmp m1 m2 =
      let rec compare_aux e1 e2 =
          match (e1, e2) with
          (End, End) -> 0
        | (End, _)  -> -1
        | (_, End) -> 1
        | (More(v1, d1, r1, e1), More(v2, d2, r2, e2)) ->
            let c = Ord.compare v1 v2 in
            if c <> 0 then c else
            let c = cmp d1 d2 in
            if c <> 0 then c else
            compare_aux (cons_enum r1 e1) (cons_enum r2 e2)
      in compare_aux (cons_enum m1 End) (cons_enum m2 End)

    let equal cmp m1 m2 =
      let rec equal_aux e1 e2 =
          match (e1, e2) with
          (End, End) -> true
        | (End, _)  -> false
        | (_, End) -> false
        | (More(v1, d1, r1, e1), More(v2, d2, r2, e2)) ->
            Ord.compare v1 v2 = 0 && cmp d1 d2 &&
            equal_aux (cons_enum r1 e1) (cons_enum r2 e2)
      in equal_aux (cons_enum m1 End) (cons_enum m2 End)

    let rec cardinal = function
        Empty -> 0
      | Node {l; r} -> cardinal l + 1 + cardinal r

    let rec bindings_aux accu = function
        Empty -> accu
      | Node {l; v; d; r} -> bindings_aux ((v, d) :: bindings_aux accu r) l

    let bindings s =
      bindings_aux [] s

    let choose = min_binding

    let choose_opt = min_binding_opt

    let add_seq i m =
      Seq.fold_left (fun m (k,v) -> add k v m) m i

    let of_seq i = add_seq i empty

    let rec seq_of_enum_ c () = match c with
      | End -> Seq.Nil
      | More (k,v,t,rest) -> Seq.Cons ((k,v), seq_of_enum_ (cons_enum t rest))

    let to_seq m =
      seq_of_enum_ (cons_enum m End)

    let to_seq_from low m =
      let rec aux low m c = match m with
        | Empty -> c
        | Node {l; v; d; r; _} ->
            begin match Ord.compare v low with
              | 0 -> More (v, d, r, c)
              | n when n<0 -> aux low r c
              | _ -> aux low l (More (v, d, r, c))
            end
      in
      seq_of_enum_ (aux low m End)
end
end

module Char = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Character operations *)

external code: char -> int = "%identity"
external unsafe_chr: int -> char = "%identity"

let chr n =
  if n < 0 || n > 255 then invalid_arg "Char.chr" else unsafe_chr n

external bytes_create: int -> bytes = "caml_create_bytes"
external bytes_unsafe_set : bytes -> int -> char -> unit
                           = "%bytes_unsafe_set"
external unsafe_to_string : bytes -> string = "%bytes_to_string"

let escaped = function
  | '\'' -> "\\'"
  | '\\' -> "\\\\"
  | '\n' -> "\\n"
  | '\t' -> "\\t"
  | '\r' -> "\\r"
  | '\b' -> "\\b"
  | ' ' .. '~' as c ->
      let s = bytes_create 1 in
      bytes_unsafe_set s 0 c;
      unsafe_to_string s
  | c ->
      let n = code c in
      let s = bytes_create 4 in
      bytes_unsafe_set s 0 '\\';
      bytes_unsafe_set s 1 (unsafe_chr (48 + n / 100));
      bytes_unsafe_set s 2 (unsafe_chr (48 + (n / 10) mod 10));
      bytes_unsafe_set s 3 (unsafe_chr (48 + n mod 10));
      unsafe_to_string s

let lowercase c =
  if (c >= 'A' && c <= 'Z')
  || (c >= '\192' && c <= '\214')
  || (c >= '\216' && c <= '\222')
  then unsafe_chr(code c + 32)
  else c

let uppercase c =
  if (c >= 'a' && c <= 'z')
  || (c >= '\224' && c <= '\246')
  || (c >= '\248' && c <= '\254')
  then unsafe_chr(code c - 32)
  else c

let lowercase_ascii c =
  if (c >= 'A' && c <= 'Z')
  then unsafe_chr(code c + 32)
  else c

let uppercase_ascii c =
  if (c >= 'a' && c <= 'z')
  then unsafe_chr(code c - 32)
  else c

type t = char

let compare c1 c2 = code c1 - code c2
let equal (c1: t) (c2: t) = compare c1 c2 = 0
end

module Uchar = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                           Daniel C. Buenzli                            *)
(*                                                                        *)
(*   Copyright 2014 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

external format_int : string -> int -> string = "caml_format_int"

let err_no_pred = "U+0000 has no predecessor"
let err_no_succ = "U+10FFFF has no successor"
let err_not_sv i = format_int "%X" i ^ " is not an Unicode scalar value"
let err_not_latin1 u = "U+" ^ format_int "%04X" u ^ " is not a latin1 character"

type t = int

let min = 0x0000
let max = 0x10FFFF
let lo_bound = 0xD7FF
let hi_bound = 0xE000

let bom = 0xFEFF
let rep = 0xFFFD

let succ u =
  if u = lo_bound then hi_bound else
  if u = max then invalid_arg err_no_succ else
  u + 1

let pred u =
  if u = hi_bound then lo_bound else
  if u = min then invalid_arg err_no_pred else
  u - 1

let is_valid i = (min <= i && i <= lo_bound) || (hi_bound <= i && i <= max)
let of_int i = if is_valid i then i else invalid_arg (err_not_sv i)
external unsafe_of_int : int -> t = "%identity"
external to_int : t -> int = "%identity"

let is_char u = u < 256
let of_char c = Char.code c
let to_char u =
  if u > 255 then invalid_arg (err_not_latin1 u) else
  Char.unsafe_chr u

let unsafe_to_char = Char.unsafe_chr

let equal : int -> int -> bool = ( = )
let compare : int -> int -> int = Pervasives.compare
let hash = to_int
end

module Buffer = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*    Pierre Weis and Xavier Leroy, projet Cristal, INRIA Rocquencourt    *)
(*                                                                        *)
(*   Copyright 1999 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Extensible buffers *)

type t =
 {mutable buffer : bytes;
  mutable position : int;
  mutable length : int;
  initial_buffer : bytes}

let create n =
 let n = if n < 1 then 1 else n in
 let n = if n > Sys.max_string_length then Sys.max_string_length else n in
 let s = Bytes.create n in
 {buffer = s; position = 0; length = n; initial_buffer = s}

let contents b = Bytes.sub_string b.buffer 0 b.position
let to_bytes b = Bytes.sub b.buffer 0 b.position

let sub b ofs len =
  if ofs < 0 || len < 0 || ofs > b.position - len
  then invalid_arg "Buffer.sub"
  else Bytes.sub_string b.buffer ofs len


let blit src srcoff dst dstoff len =
  if len < 0 || srcoff < 0 || srcoff > src.position - len
             || dstoff < 0 || dstoff > (Bytes.length dst) - len
  then invalid_arg "Buffer.blit"
  else
    Bytes.unsafe_blit src.buffer srcoff dst dstoff len


let nth b ofs =
  if ofs < 0 || ofs >= b.position then
   invalid_arg "Buffer.nth"
  else Bytes.unsafe_get b.buffer ofs


let length b = b.position

let clear b = b.position <- 0

let reset b =
  b.position <- 0; b.buffer <- b.initial_buffer;
  b.length <- Bytes.length b.buffer

let resize b more =
  let len = b.length in
  let new_len = ref len in
  while b.position + more > !new_len do new_len := 2 * !new_len done;
  if !new_len > Sys.max_string_length then begin
    if b.position + more <= Sys.max_string_length
    then new_len := Sys.max_string_length
    else failwith "Buffer.add: cannot grow buffer"
  end;
  let new_buffer = Bytes.create !new_len in
  (* PR#6148: let's keep using [blit] rather than [unsafe_blit] in
     this tricky function that is slow anyway. *)
  Bytes.blit b.buffer 0 new_buffer 0 b.position;
  b.buffer <- new_buffer;
  b.length <- !new_len

let add_char b c =
  let pos = b.position in
  if pos >= b.length then resize b 1;
  Bytes.unsafe_set b.buffer pos c;
  b.position <- pos + 1

 let add_utf_8_uchar b u = match Uchar.to_int u with
 | u when u < 0 -> assert false
 | u when u <= 0x007F ->
     add_char b (Char.unsafe_chr u)
 | u when u <= 0x07FF ->
     let pos = b.position in
     if pos + 2 > b.length then resize b 2;
     Bytes.unsafe_set b.buffer (pos    )
       (Char.unsafe_chr (0xC0 lor (u lsr 6)));
     Bytes.unsafe_set b.buffer (pos + 1)
       (Char.unsafe_chr (0x80 lor (u land 0x3F)));
     b.position <- pos + 2
 | u when u <= 0xFFFF ->
     let pos = b.position in
     if pos + 3 > b.length then resize b 3;
     Bytes.unsafe_set b.buffer (pos    )
       (Char.unsafe_chr (0xE0 lor (u lsr 12)));
     Bytes.unsafe_set b.buffer (pos + 1)
       (Char.unsafe_chr (0x80 lor ((u lsr 6) land 0x3F)));
     Bytes.unsafe_set b.buffer (pos + 2)
       (Char.unsafe_chr (0x80 lor (u land 0x3F)));
     b.position <- pos + 3
 | u when u <= 0x10FFFF ->
     let pos = b.position in
     if pos + 4 > b.length then resize b 4;
     Bytes.unsafe_set b.buffer (pos    )
       (Char.unsafe_chr (0xF0 lor (u lsr 18)));
     Bytes.unsafe_set b.buffer (pos + 1)
       (Char.unsafe_chr (0x80 lor ((u lsr 12) land 0x3F)));
     Bytes.unsafe_set b.buffer (pos + 2)
       (Char.unsafe_chr (0x80 lor ((u lsr 6) land 0x3F)));
     Bytes.unsafe_set b.buffer (pos + 3)
       (Char.unsafe_chr (0x80 lor (u land 0x3F)));
     b.position <- pos + 4
 | _ -> assert false

 let add_utf_16be_uchar b u = match Uchar.to_int u with
 | u when u < 0 -> assert false
 | u when u <= 0xFFFF ->
     let pos = b.position in
     if pos + 2 > b.length then resize b 2;
     Bytes.unsafe_set b.buffer (pos    ) (Char.unsafe_chr (u lsr 8));
     Bytes.unsafe_set b.buffer (pos + 1) (Char.unsafe_chr (u land 0xFF));
     b.position <- pos + 2
 | u when u <= 0x10FFFF ->
     let u' = u - 0x10000 in
     let hi = 0xD800 lor (u' lsr 10) in
     let lo = 0xDC00 lor (u' land 0x3FF) in
     let pos = b.position in
     if pos + 4 > b.length then resize b 4;
     Bytes.unsafe_set b.buffer (pos    ) (Char.unsafe_chr (hi lsr 8));
     Bytes.unsafe_set b.buffer (pos + 1) (Char.unsafe_chr (hi land 0xFF));
     Bytes.unsafe_set b.buffer (pos + 2) (Char.unsafe_chr (lo lsr 8));
     Bytes.unsafe_set b.buffer (pos + 3) (Char.unsafe_chr (lo land 0xFF));
     b.position <- pos + 4
 | _ -> assert false

 let add_utf_16le_uchar b u = match Uchar.to_int u with
 | u when u < 0 -> assert false
 | u when u <= 0xFFFF ->
     let pos = b.position in
     if pos + 2 > b.length then resize b 2;
     Bytes.unsafe_set b.buffer (pos    ) (Char.unsafe_chr (u land 0xFF));
     Bytes.unsafe_set b.buffer (pos + 1) (Char.unsafe_chr (u lsr 8));
     b.position <- pos + 2
 | u when u <= 0x10FFFF ->
     let u' = u - 0x10000 in
     let hi = 0xD800 lor (u' lsr 10) in
     let lo = 0xDC00 lor (u' land 0x3FF) in
     let pos = b.position in
     if pos + 4 > b.length then resize b 4;
     Bytes.unsafe_set b.buffer (pos    ) (Char.unsafe_chr (hi land 0xFF));
     Bytes.unsafe_set b.buffer (pos + 1) (Char.unsafe_chr (hi lsr 8));
     Bytes.unsafe_set b.buffer (pos + 2) (Char.unsafe_chr (lo land 0xFF));
     Bytes.unsafe_set b.buffer (pos + 3) (Char.unsafe_chr (lo lsr 8));
     b.position <- pos + 4
 | _ -> assert false

let add_substring b s offset len =
  if offset < 0 || len < 0 || offset > String.length s - len
  then invalid_arg "Buffer.add_substring/add_subbytes";
  let new_position = b.position + len in
  if new_position > b.length then resize b len;
  Bytes.blit_string s offset b.buffer b.position len;
  b.position <- new_position

let add_subbytes b s offset len =
  add_substring b (Bytes.unsafe_to_string s) offset len

let add_string b s =
  let len = String.length s in
  let new_position = b.position + len in
  if new_position > b.length then resize b len;
  Bytes.blit_string s 0 b.buffer b.position len;
  b.position <- new_position

let add_bytes b s = add_string b (Bytes.unsafe_to_string s)

let add_buffer b bs =
  add_subbytes b bs.buffer 0 bs.position

(* read up to [len] bytes from [ic] into [b]. *)
let rec add_channel_rec b ic len =
  if len > 0 then (
    let n = input ic b.buffer b.position len in
    b.position <- b.position + n;
    if n = 0 then raise End_of_file
    else add_channel_rec b ic (len-n)   (* n <= len *)
  )

let add_channel b ic len =
  if len < 0 || len > Sys.max_string_length then   (* PR#5004 *)
    invalid_arg "Buffer.add_channel";
  if b.position + len > b.length then resize b len;
  add_channel_rec b ic len

let output_buffer oc b =
  output oc b.buffer 0 b.position

let closing = function
  | '(' -> ')'
  | '{' -> '}'
  | _ -> assert false

(* opening and closing: open and close characters, typically ( and )
   k: balance of opening and closing chars
   s: the string where we are searching
   start: the index where we start the search. *)
let advance_to_closing opening closing k s start =
  let rec advance k i lim =
    if i >= lim then raise Not_found else
    if s.[i] = opening then advance (k + 1) (i + 1) lim else
    if s.[i] = closing then
      if k = 0 then i else advance (k - 1) (i + 1) lim
    else advance k (i + 1) lim in
  advance k start (String.length s)

let advance_to_non_alpha s start =
  let rec advance i lim =
    if i >= lim then lim else
    match s.[i] with
    | 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' -> advance (i + 1) lim
    | _ -> i in
  advance start (String.length s)

(* We are just at the beginning of an ident in s, starting at start. *)
let find_ident s start lim =
  if start >= lim then raise Not_found else
  match s.[start] with
  (* Parenthesized ident ? *)
  | '(' | '{' as c ->
     let new_start = start + 1 in
     let stop = advance_to_closing c (closing c) 0 s new_start in
     String.sub s new_start (stop - start - 1), stop + 1
  (* Regular ident *)
  | _ ->
     let stop = advance_to_non_alpha s (start + 1) in
     String.sub s start (stop - start), stop

(* Substitute $ident, $(ident), or ${ident} in s,
    according to the function mapping f. *)
let add_substitute b f s =
  let lim = String.length s in
  let rec subst previous i =
    if i < lim then begin
      match s.[i] with
      | '$' as current when previous = '\\' ->
         add_char b current;
         subst ' ' (i + 1)
      | '$' ->
         let j = i + 1 in
         let ident, next_i = find_ident s j lim in
         add_string b (f ident);
         subst ' ' next_i
      | current when previous == '\\' ->
         add_char b '\\';
         add_char b current;
         subst ' ' (i + 1)
      | '\\' as current ->
         subst current (i + 1)
      | current ->
         add_char b current;
         subst current (i + 1)
    end else
    if previous = '\\' then add_char b previous in
  subst ' ' 0

let truncate b len =
    if len < 0 || len > length b then
      invalid_arg "Buffer.truncate"
    else
      b.position <- len

(** {6 Iterators} *)

let to_seq b =
  let rec aux i () =
    if i >= b.position then Seq.Nil
    else
      let x = Bytes.get b.buffer i in
      Seq.Cons (x, aux (i+1))
  in
  aux 0

let to_seqi b =
  let rec aux i () =
    if i >= b.position then Seq.Nil
    else
      let x = Bytes.get b.buffer i in
      Seq.Cons ((i,x), aux (i+1))
  in
  aux 0

let add_seq b seq = Seq.iter (add_char b) seq

let of_seq i =
  let b = create 32 in
  add_seq b i;
  b
end

module Stream = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*         Daniel de Rauglaudre, projet Cristal, INRIA Rocquencourt       *)
(*                                                                        *)
(*   Copyright 1997 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

type 'a t = 'a cell option
and 'a cell = { mutable count : int; mutable data : 'a data }
and 'a data =
    Sempty
  | Scons of 'a * 'a data
  | Sapp of 'a data * 'a data
  | Slazy of 'a data Lazy.t
  | Sgen of 'a gen
  | Sbuffio : buffio -> char data
and 'a gen = { mutable curr : 'a option option; func : int -> 'a option }
and buffio =
  { ic : in_channel; buff : bytes; mutable len : int; mutable ind : int }

exception Failure
exception Error of string

let count = function
  | None -> 0
  | Some { count } -> count
let data = function
  | None -> Sempty
  | Some { data } -> data

let fill_buff b =
  b.len <- input b.ic b.buff 0 (Bytes.length b.buff); b.ind <- 0


let rec get_data : type v. int -> v data -> v data = fun count d -> match d with
 (* Returns either Sempty or Scons(a, _) even when d is a generator
    or a buffer. In those cases, the item a is seen as extracted from
 the generator/buffer.
 The count parameter is used for calling `Sgen-functions'.  *)
   Sempty | Scons (_, _) -> d
 | Sapp (d1, d2) ->
     begin match get_data count d1 with
       Scons (a, d11) -> Scons (a, Sapp (d11, d2))
     | Sempty -> get_data count d2
     | _ -> assert false
     end
 | Sgen {curr = Some None} -> Sempty
 | Sgen ({curr = Some(Some a)} as g) ->
     g.curr <- None; Scons(a, d)
 | Sgen g ->
     begin match g.func count with
       None -> g.curr <- Some(None); Sempty
     | Some a -> Scons(a, d)
         (* Warning: anyone using g thinks that an item has been read *)
     end
 | Sbuffio b ->
     if b.ind >= b.len then fill_buff b;
     if b.len == 0 then Sempty else
       let r = Bytes.unsafe_get b.buff b.ind in
       (* Warning: anyone using g thinks that an item has been read *)
       b.ind <- succ b.ind; Scons(r, d)
 | Slazy f -> get_data count (Lazy.force f)


let rec peek_data : type v. v cell -> v option = fun s ->
 (* consult the first item of s *)
 match s.data with
   Sempty -> None
 | Scons (a, _) -> Some a
 | Sapp (_, _) ->
     begin match get_data s.count s.data with
       Scons(a, _) as d -> s.data <- d; Some a
     | Sempty -> None
     | _ -> assert false
     end
 | Slazy f -> s.data <- (Lazy.force f); peek_data s
 | Sgen {curr = Some a} -> a
 | Sgen g -> let x = g.func s.count in g.curr <- Some x; x
 | Sbuffio b ->
     if b.ind >= b.len then fill_buff b;
     if b.len == 0 then begin s.data <- Sempty; None end
     else Some (Bytes.unsafe_get b.buff b.ind)


let peek = function
  | None -> None
  | Some s -> peek_data s


let rec junk_data : type v. v cell -> unit = fun s ->
  match s.data with
    Scons (_, d) -> s.count <- (succ s.count); s.data <- d
  | Sgen ({curr = Some _} as g) -> s.count <- (succ s.count); g.curr <- None
  | Sbuffio b ->
      if b.ind >= b.len then fill_buff b;
      if b.len == 0 then s.data <- Sempty
      else (s.count <- (succ s.count); b.ind <- succ b.ind)
  | _ ->
      match peek_data s with
        None -> ()
      | Some _ -> junk_data s


let junk = function
  | None -> ()
  | Some data -> junk_data data

let rec nget_data n s =
  if n <= 0 then [], s.data, 0
  else
    match peek_data s with
      Some a ->
        junk_data s;
        let (al, d, k) = nget_data (pred n) s in a :: al, Scons (a, d), succ k
    | None -> [], s.data, 0


let npeek_data n s =
  let (al, d, len) = nget_data n s in
  s.count <- (s.count - len);
  s.data <- d;
  al


let npeek n = function
  | None -> []
  | Some d -> npeek_data n d

let next s =
  match peek s with
    Some a -> junk s; a
  | None -> raise Failure


let empty s =
  match peek s with
    Some _ -> raise Failure
  | None -> ()


let iter f strm =
  let rec do_rec () =
    match peek strm with
      Some a -> junk strm; ignore(f a); do_rec ()
    | None -> ()
  in
  do_rec ()


(* Stream building functions *)

let from f = Some {count = 0; data = Sgen {curr = None; func = f}}

let of_list l =
  Some {count = 0; data = List.fold_right (fun x l -> Scons (x, l)) l Sempty}


let of_string s =
  let count = ref 0 in
  from (fun _ ->
    (* We cannot use the index passed by the [from] function directly
       because it returns the current stream count, with absolutely no
       guarantee that it will start from 0. For example, in the case
       of [Stream.icons 'c' (Stream.from_string "ab")], the first
       access to the string will be made with count [1] already.
    *)
    let c = !count in
    if c < String.length s
    then (incr count; Some s.[c])
    else None)


let of_bytes s =
  let count = ref 0 in
  from (fun _ ->
    let c = !count in
    if c < Bytes.length s
    then (incr count; Some (Bytes.get s c))
    else None)


let of_channel ic =
  Some {count = 0;
        data = Sbuffio {ic = ic; buff = Bytes.create 4096; len = 0; ind = 0}}


(* Stream expressions builders *)

let iapp i s = Some {count = 0; data = Sapp (data i, data s)}
let icons i s = Some {count = 0; data = Scons (i, data s)}
let ising i = Some {count = 0; data = Scons (i, Sempty)}

let lapp f s =
  Some {count = 0; data = Slazy (lazy(Sapp (data (f ()), data s)))}

let lcons f s = Some {count = 0; data = Slazy (lazy(Scons (f (), data s)))}
let lsing f = Some {count = 0; data = Slazy (lazy(Scons (f (), Sempty)))}

let sempty = None
let slazy f = Some {count = 0; data = Slazy (lazy(data (f ())))}

(* For debugging use *)

let rec dump : type v. (v -> unit) -> v t -> unit = fun f s ->
  print_string "{count = ";
  print_int (count s);
  print_string "; data = ";
  dump_data f (data s);
  print_string "}";
  print_newline ()
and dump_data : type v. (v -> unit) -> v data -> unit = fun f ->
  function
    Sempty -> print_string "Sempty"
  | Scons (a, d) ->
      print_string "Scons (";
      f a;
      print_string ", ";
      dump_data f d;
      print_string ")"
  | Sapp (d1, d2) ->
      print_string "Sapp (";
      dump_data f d1;
      print_string ", ";
      dump_data f d2;
      print_string ")"
  | Slazy _ -> print_string "Slazy"
  | Sgen _ -> print_string "Sgen"
  | Sbuffio _ -> print_string "Sbuffio"
end

module Genlex = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*               Xavier Leroy, projet Cristal, INRIA Rocquencourt         *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

type token =
    Kwd of string
  | Ident of string
  | Int of int
  | Float of float
  | String of string
  | Char of char

(* The string buffering machinery *)

let initial_buffer = Bytes.create 32

let buffer = ref initial_buffer
let bufpos = ref 0

let reset_buffer () = buffer := initial_buffer; bufpos := 0

let store c =
  if !bufpos >= Bytes.length !buffer then begin
    let newbuffer = Bytes.create (2 * !bufpos) in
    Bytes.blit !buffer 0 newbuffer 0 !bufpos;
    buffer := newbuffer
  end;
  Bytes.set !buffer !bufpos c;
  incr bufpos

let get_string () =
  let s = Bytes.sub_string !buffer 0 !bufpos in buffer := initial_buffer; s

(* The lexer *)

let make_lexer keywords =
  let kwd_table = Hashtbl.create 17 in
  List.iter (fun s -> Hashtbl.add kwd_table s (Kwd s)) keywords;
  let ident_or_keyword id =
    try Hashtbl.find kwd_table id with
      Not_found -> Ident id
  and keyword_or_error c =
    let s = String.make 1 c in
    try Hashtbl.find kwd_table s with
      Not_found -> raise (Stream.Error ("Illegal character " ^ s))
  in
  let rec next_token (strm__ : _ Stream.t) =
    match Stream.peek strm__ with
      Some (' ' | '\010' | '\013' | '\009' | '\026' | '\012') ->
        Stream.junk strm__; next_token strm__
    | Some ('A'..'Z' | 'a'..'z' | '_' | '\192'..'\255' as c) ->
        Stream.junk strm__;
        let s = strm__ in reset_buffer (); store c; ident s
    | Some
        ('!' | '%' | '&' | '$' | '#' | '+' | '/' | ':' | '<' | '=' | '>' |
         '?' | '@' | '\\' | '~' | '^' | '|' | '*' as c) ->
        Stream.junk strm__;
        let s = strm__ in reset_buffer (); store c; ident2 s
    | Some ('0'..'9' as c) ->
        Stream.junk strm__;
        let s = strm__ in reset_buffer (); store c; number s
    | Some '\'' ->
        Stream.junk strm__;
        let c =
          try char strm__ with
            Stream.Failure -> raise (Stream.Error "")
        in
        begin match Stream.peek strm__ with
          Some '\'' -> Stream.junk strm__; Some (Char c)
        | _ -> raise (Stream.Error "")
        end
    | Some '\"' ->
        Stream.junk strm__;
        let s = strm__ in reset_buffer (); Some (String (string s))
    | Some '-' -> Stream.junk strm__; neg_number strm__
    | Some '(' -> Stream.junk strm__; maybe_comment strm__
    | Some c -> Stream.junk strm__; Some (keyword_or_error c)
    | _ -> None
  and ident (strm__ : _ Stream.t) =
    match Stream.peek strm__ with
      Some
        ('A'..'Z' | 'a'..'z' | '\192'..'\255' | '0'..'9' | '_' | '\'' as c) ->
        Stream.junk strm__; let s = strm__ in store c; ident s
    | _ -> Some (ident_or_keyword (get_string ()))
  and ident2 (strm__ : _ Stream.t) =
    match Stream.peek strm__ with
      Some
        ('!' | '%' | '&' | '$' | '#' | '+' | '-' | '/' | ':' | '<' | '=' |
         '>' | '?' | '@' | '\\' | '~' | '^' | '|' | '*' as c) ->
        Stream.junk strm__; let s = strm__ in store c; ident2 s
    | _ -> Some (ident_or_keyword (get_string ()))
  and neg_number (strm__ : _ Stream.t) =
    match Stream.peek strm__ with
      Some ('0'..'9' as c) ->
        Stream.junk strm__;
        let s = strm__ in reset_buffer (); store '-'; store c; number s
    | _ -> let s = strm__ in reset_buffer (); store '-'; ident2 s
  and number (strm__ : _ Stream.t) =
    match Stream.peek strm__ with
      Some ('0'..'9' as c) ->
        Stream.junk strm__; let s = strm__ in store c; number s
    | Some '.' ->
        Stream.junk strm__; let s = strm__ in store '.'; decimal_part s
    | Some ('e' | 'E') ->
        Stream.junk strm__; let s = strm__ in store 'E'; exponent_part s
    | _ -> Some (Int (int_of_string (get_string ())))
  and decimal_part (strm__ : _ Stream.t) =
    match Stream.peek strm__ with
      Some ('0'..'9' as c) ->
        Stream.junk strm__; let s = strm__ in store c; decimal_part s
    | Some ('e' | 'E') ->
        Stream.junk strm__; let s = strm__ in store 'E'; exponent_part s
    | _ -> Some (Float (float_of_string (get_string ())))
  and exponent_part (strm__ : _ Stream.t) =
    match Stream.peek strm__ with
      Some ('+' | '-' as c) ->
        Stream.junk strm__; let s = strm__ in store c; end_exponent_part s
    | _ -> end_exponent_part strm__
  and end_exponent_part (strm__ : _ Stream.t) =
    match Stream.peek strm__ with
      Some ('0'..'9' as c) ->
        Stream.junk strm__; let s = strm__ in store c; end_exponent_part s
    | _ -> Some (Float (float_of_string (get_string ())))
  and string (strm__ : _ Stream.t) =
    match Stream.peek strm__ with
      Some '\"' -> Stream.junk strm__; get_string ()
    | Some '\\' ->
        Stream.junk strm__;
        let c =
          try escape strm__ with
            Stream.Failure -> raise (Stream.Error "")
        in
        let s = strm__ in store c; string s
    | Some c -> Stream.junk strm__; let s = strm__ in store c; string s
    | _ -> raise Stream.Failure
  and char (strm__ : _ Stream.t) =
    match Stream.peek strm__ with
      Some '\\' ->
        Stream.junk strm__;
        begin try escape strm__ with
          Stream.Failure -> raise (Stream.Error "")
        end
    | Some c -> Stream.junk strm__; c
    | _ -> raise Stream.Failure
  and escape (strm__ : _ Stream.t) =
    match Stream.peek strm__ with
      Some 'n' -> Stream.junk strm__; '\n'
    | Some 'r' -> Stream.junk strm__; '\r'
    | Some 't' -> Stream.junk strm__; '\t'
    | Some ('0'..'9' as c1) ->
        Stream.junk strm__;
        begin match Stream.peek strm__ with
          Some ('0'..'9' as c2) ->
            Stream.junk strm__;
            begin match Stream.peek strm__ with
              Some ('0'..'9' as c3) ->
                Stream.junk strm__;
                Char.chr
                  ((Char.code c1 - 48) * 100 + (Char.code c2 - 48) * 10 +
                     (Char.code c3 - 48))
            | _ -> raise (Stream.Error "")
            end
        | _ -> raise (Stream.Error "")
        end
    | Some c -> Stream.junk strm__; c
    | _ -> raise Stream.Failure
  and maybe_comment (strm__ : _ Stream.t) =
    match Stream.peek strm__ with
      Some '*' ->
        Stream.junk strm__; let s = strm__ in comment s; next_token s
    | _ -> Some (keyword_or_error '(')
  and comment (strm__ : _ Stream.t) =
    match Stream.peek strm__ with
      Some '(' -> Stream.junk strm__; maybe_nested_comment strm__
    | Some '*' -> Stream.junk strm__; maybe_end_comment strm__
    | Some _ -> Stream.junk strm__; comment strm__
    | _ -> raise Stream.Failure
  and maybe_nested_comment (strm__ : _ Stream.t) =
    match Stream.peek strm__ with
      Some '*' -> Stream.junk strm__; let s = strm__ in comment s; comment s
    | Some _ -> Stream.junk strm__; comment strm__
    | _ -> raise Stream.Failure
  and maybe_end_comment (strm__ : _ Stream.t) =
    match Stream.peek strm__ with
      Some ')' -> Stream.junk strm__; ()
    | Some '*' -> Stream.junk strm__; maybe_end_comment strm__
    | Some _ -> Stream.junk strm__; comment strm__
    | _ -> raise Stream.Failure
  in
  fun input -> Stream.from (fun _count -> next_token input)
end

module CamlinternalFormatBasics = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                          Benoit Vaugon, ENSTA                          *)
(*                                                                        *)
(*   Copyright 2014 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Padding position. *)
type padty =
  | Left   (* Text is left justified ('-' option).               *)
  | Right  (* Text is right justified (no '-' option).           *)
  | Zeros  (* Text is right justified by zeros (see '0' option). *)

(***)

(* Integer conversion. *)
type int_conv =
  | Int_d | Int_pd | Int_sd        (*  %d | %+d | % d  *)
  | Int_i | Int_pi | Int_si        (*  %i | %+i | % i  *)
  | Int_x | Int_Cx                 (*  %x | %#x        *)
  | Int_X | Int_CX                 (*  %X | %#X        *)
  | Int_o | Int_Co                 (*  %o | %#o        *)
  | Int_u                          (*  %u              *)

(* Float conversion. *)
type float_conv =
  | Float_f | Float_pf | Float_sf  (*  %f | %+f | % f  *)
  | Float_e | Float_pe | Float_se  (*  %e | %+e | % e  *)
  | Float_E | Float_pE | Float_sE  (*  %E | %+E | % E  *)
  | Float_g | Float_pg | Float_sg  (*  %g | %+g | % g  *)
  | Float_G | Float_pG | Float_sG  (*  %G | %+G | % G  *)
  | Float_F                        (*  %F              *)
  | Float_h | Float_ph | Float_sh  (*  %h | %+h | % h  *)
  | Float_H | Float_pH | Float_sH  (*  %H | %+H | % H  *)

(***)

(* Char sets (see %[...]) are bitmaps implemented as 32-char strings. *)
type char_set = string

(***)

(* Counter used in Scanf. *)
type counter =
  | Line_counter     (*  %l      *)
  | Char_counter     (*  %n      *)
  | Token_counter    (*  %N, %L  *)

(***)

(* Padding of strings and numbers. *)
type ('a, 'b) padding =
  (* No padding (ex: "%d") *)
  | No_padding  : ('a, 'a) padding
  (* Literal padding (ex: "%8d") *)
  | Lit_padding : padty * int -> ('a, 'a) padding
  (* Padding as extra argument (ex: "%*d") *)
  | Arg_padding : padty -> (int -> 'a, 'a) padding

(* Some formats, such as %_d,
   only accept an optional number as padding option (no extra argument) *)
type pad_option = int option

(* Precision of floats and '0'-padding of integers. *)
type ('a, 'b) precision =
  (* No precision (ex: "%f") *)
  | No_precision : ('a, 'a) precision
  (* Literal precision (ex: "%.3f") *)
  | Lit_precision : int -> ('a, 'a) precision
  (* Precision as extra argument (ex: "%.*f") *)
  | Arg_precision : (int -> 'a, 'a) precision

(* Some formats, such as %_f,
   only accept an optional number as precision option (no extra argument) *)
type prec_option = int option

(* see the Custom format combinator *)
type ('a, 'b, 'c) custom_arity =
  | Custom_zero : ('a, string, 'a) custom_arity
  | Custom_succ : ('a, 'b, 'c) custom_arity ->
    ('a, 'x -> 'b, 'x -> 'c) custom_arity

(***)

(*        Relational format types

In the first format+gadts implementation, the type for %(..%) in the
fmt GADT was as follows:

| Format_subst :                                           (* %(...%) *)
    pad_option * ('d1, 'q1, 'd2, 'q2) reader_nb_unifier *
    ('x, 'b, 'c, 'd1, 'q1, 'u) fmtty *
    ('u, 'b, 'c, 'q1, 'e1, 'f) fmt ->
      (('x, 'b, 'c, 'd2, 'q2, 'u) format6 -> 'x, 'b, 'c, 'd1, 'e1, 'f) fmt

Notice that the 'u parameter in 'f position in the format argument
(('x, .., 'u) format6 -> ..) is equal to the 'u parameter in 'a
position in the format tail (('u, .., 'f) fmt). This means that the
type of the expected format parameter depends of where the %(...%)
are in the format string:

  # Printf.printf "%(%)"
  - : (unit, out_channel, unit, '_a, '_a, unit)
      CamlinternalFormatBasics.format6 -> unit
  = <fun>
  # Printf.printf "%(%)%d"
  - : (int -> unit, out_channel, unit, '_a, '_a, int -> unit)
      CamlinternalFormatBasics.format6 -> int -> unit
  = <fun>

On the contrary, the legacy typer gives a clever type that does not
depend on the position of %(..%) in the format string. For example,
%(%) will have the polymorphic type ('a, 'b, 'c, 'd, 'd, 'a): it can
be concatenated to any format type, and only enforces the constraint
that its 'a and 'f parameters are equal (no format arguments) and 'd
and 'e are equal (no reader argument).

The weakening of this parameter type in the GADT version broke user
code (in fact it essentially made %(...%) unusable except at the last
position of a format). In particular, the following would not work
anymore:

  fun sep ->
    Format.printf "foo%(%)bar%(%)baz" sep sep

As the type-checker would require two *incompatible* types for the %(%)
in different positions.

The solution to regain a general type for %(..%) is to generalize this
technique, not only on the 'd, 'e parameters, but on all six
parameters of a format: we introduce a "relational" type
  ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
   'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel
whose values are proofs that ('a1, .., 'f1) and ('a2, .., 'f2) morally
correspond to the same format type: 'a1 is obtained from 'f1,'b1,'c1
in the exact same way that 'a2 is obtained from 'f2,'b2,'c2, etc.

For example, the relation between two format types beginning with a Char
parameter is as follows:

| Char_ty :                                                 (* %c  *)
    ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
     'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel ->
    (char -> 'a1, 'b1, 'c1, 'd1, 'e1, 'f1,
     char -> 'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel

In the general case, the term structure of fmtty_rel is (almost[1])
isomorphic to the fmtty of the previous implementation: every
constructor is re-read with a binary, relational type, instead of the
previous unary typing. fmtty can then be re-defined as the diagonal of
fmtty_rel:

  type ('a, 'b, 'c, 'd, 'e, 'f) fmtty =
       ('a, 'b, 'c, 'd, 'e, 'f,
        'a, 'b, 'c, 'd, 'e, 'f) fmtty_rel

Once we have this fmtty_rel type in place, we can give the more
general type to %(...%):

| Format_subst :                                           (* %(...%) *)
    pad_option *
    ('g, 'h, 'i, 'j, 'k, 'l,
     'g2, 'b, 'c, 'j2, 'd, 'a) fmtty_rel *
    ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
    (('g, 'h, 'i, 'j, 'k, 'l) format6 -> 'g2, 'b, 'c, 'j2, 'e, 'f) fmt

We accept any format (('g, 'h, 'i, 'j, 'k, 'l) format6) (this is
completely unrelated to the type of the current format), but also
require a proof that this format is in relation to another format that
is concatenable to the format tail. When executing a %(...%) format
(in camlinternalFormat.ml:make_printf or scanf.ml:make_scanf), we
transtype the format along this relation using the 'recast' function
to transpose between related format types.

  val recast :
     ('a1, 'b1, 'c1, 'd1, 'e1, 'f1) fmt
  -> ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
      'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel
  -> ('a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmt

NOTE [1]: the typing of Format_subst_ty requires not one format type, but
two, one to establish the link between the format argument and the
first six parameters, and the other for the link between the format
argument and the last six parameters.

| Format_subst_ty :                                         (* %(...%) *)
    ('g, 'h, 'i, 'j, 'k, 'l,
     'g1, 'b1, 'c1, 'j1, 'd1, 'a1) fmtty_rel *
    ('g, 'h, 'i, 'j, 'k, 'l,
     'g2, 'b2, 'c2, 'j2, 'd2, 'a2) fmtty_rel *
    ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
     'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel ->
    (('g, 'h, 'i, 'j, 'k, 'l) format6 -> 'g1, 'b1, 'c1, 'j1, 'e1, 'f1,
     ('g, 'h, 'i, 'j, 'k, 'l) format6 -> 'g2, 'b2, 'c2, 'j2, 'e2, 'f2) fmtty_rel

When we generate a format AST, we generate exactly the same witness
for both relations, and the witness-conversion functions in
camlinternalFormat do rely on this invariant. For example, the
function that proves that the relation is transitive

  val trans :
     ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
      'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel
  -> ('a2, 'b2, 'c2, 'd2, 'e2, 'f2,
      'a3, 'b3, 'c3, 'd3, 'e3, 'f3) fmtty_rel
  -> ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
      'a3, 'b3, 'c3, 'd3, 'e3, 'f3) fmtty_rel

does assume that the two inputs have exactly the same term structure
(and is only every used for argument witnesses of the
Format_subst_ty constructor).
*)

(* Type of a block used by the Format pretty-printer. *)
type block_type =
  | Pp_hbox   (* Horizontal block no line breaking *)
  | Pp_vbox   (* Vertical block each break leads to a new line *)
  | Pp_hvbox  (* Horizontal-vertical block: same as vbox, except if this block
                 is small enough to fit on a single line *)
  | Pp_hovbox (* Horizontal or Vertical block: breaks lead to new line
                 only when necessary to print the content of the block *)
  | Pp_box    (* Horizontal or Indent block: breaks lead to new line
                 only when necessary to print the content of the block, or
                 when it leads to a new indentation of the current line *)
  | Pp_fits   (* Internal usage: when a block fits on a single line *)

(* Formatting element used by the Format pretty-printer. *)
type formatting_lit =
  | Close_box                                           (* @]   *)
  | Close_tag                                           (* @}   *)
  | Break of string * int * int          (* @, | @  | @; | @;<> *)
  | FFlush                                              (* @?   *)
  | Force_newline                                       (* @\n  *)
  | Flush_newline                                       (* @.   *)
  | Magic_size of string * int                          (* @<n> *)
  | Escaped_at                                          (* @@   *)
  | Escaped_percent                                     (* @%%  *)
  | Scan_indic of char                                  (* @X   *)

(* Formatting element used by the Format pretty-printer. *)
type ('a, 'b, 'c, 'd, 'e, 'f) formatting_gen =
  | Open_tag : ('a, 'b, 'c, 'd, 'e, 'f) format6 ->      (* @{   *)
    ('a, 'b, 'c, 'd, 'e, 'f) formatting_gen
  | Open_box : ('a, 'b, 'c, 'd, 'e, 'f) format6 ->      (* @[   *)
    ('a, 'b, 'c, 'd, 'e, 'f) formatting_gen

(***)

(* List of format type elements. *)
(* In particular used to represent %(...%) and %{...%} contents. *)
and ('a, 'b, 'c, 'd, 'e, 'f) fmtty =
     ('a, 'b, 'c, 'd, 'e, 'f,
      'a, 'b, 'c, 'd, 'e, 'f) fmtty_rel
and ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
     'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel =
  | Char_ty :                                                 (* %c  *)
      ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel ->
      (char -> 'a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       char -> 'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel
  | String_ty :                                               (* %s  *)
      ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel ->
      (string -> 'a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       string -> 'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel
  | Int_ty :                                                  (* %d  *)
      ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel ->
      (int -> 'a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       int -> 'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel
  | Int32_ty :                                                (* %ld *)
      ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel ->
      (int32 -> 'a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       int32 -> 'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel
  | Nativeint_ty :                                            (* %nd *)
      ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel ->
      (nativeint -> 'a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       nativeint -> 'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel
  | Int64_ty :                                                (* %Ld *)
      ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel ->
      (int64 -> 'a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       int64 -> 'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel
  | Float_ty :                                                (* %f  *)
      ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel ->
      (float -> 'a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       float -> 'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel
  | Bool_ty :                                                 (* %B  *)
      ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel ->
      (bool -> 'a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       bool -> 'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel

  | Format_arg_ty :                                           (* %{...%} *)
      ('g, 'h, 'i, 'j, 'k, 'l) fmtty *
      ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel ->
      (('g, 'h, 'i, 'j, 'k, 'l) format6 -> 'a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       ('g, 'h, 'i, 'j, 'k, 'l) format6 -> 'a2, 'b2, 'c2, 'd2, 'e2, 'f2)
           fmtty_rel
  | Format_subst_ty :                                         (* %(...%) *)
      ('g, 'h, 'i, 'j, 'k, 'l,
       'g1, 'b1, 'c1, 'j1, 'd1, 'a1) fmtty_rel *
      ('g, 'h, 'i, 'j, 'k, 'l,
       'g2, 'b2, 'c2, 'j2, 'd2, 'a2) fmtty_rel *
      ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel ->
      (('g, 'h, 'i, 'j, 'k, 'l) format6 -> 'g1, 'b1, 'c1, 'j1, 'e1, 'f1,
       ('g, 'h, 'i, 'j, 'k, 'l) format6 -> 'g2, 'b2, 'c2, 'j2, 'e2, 'f2)
           fmtty_rel

  (* Printf and Format specific constructors. *)
  | Alpha_ty :                                                (* %a  *)
      ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel ->
      (('b1 -> 'x -> 'c1) -> 'x -> 'a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       ('b2 -> 'x -> 'c2) -> 'x -> 'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel
  | Theta_ty :                                                (* %t  *)
      ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel ->
      (('b1 -> 'c1) -> 'a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       ('b2 -> 'c2) -> 'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel
  | Any_ty :                                    (* Used for custom formats *)
      ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel ->
      ('x -> 'a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       'x -> 'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel

  (* Scanf specific constructor. *)
  | Reader_ty :                                               (* %r  *)
      ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel ->
      ('x -> 'a1, 'b1, 'c1, ('b1 -> 'x) -> 'd1, 'e1, 'f1,
       'x -> 'a2, 'b2, 'c2, ('b2 -> 'x) -> 'd2, 'e2, 'f2) fmtty_rel
  | Ignored_reader_ty :                                       (* %_r  *)
      ('a1, 'b1, 'c1, 'd1, 'e1, 'f1,
       'a2, 'b2, 'c2, 'd2, 'e2, 'f2) fmtty_rel ->
      ('a1, 'b1, 'c1, ('b1 -> 'x) -> 'd1, 'e1, 'f1,
       'a2, 'b2, 'c2, ('b2 -> 'x) -> 'd2, 'e2, 'f2) fmtty_rel

  | End_of_fmtty :
      ('f1, 'b1, 'c1, 'd1, 'd1, 'f1,
       'f2, 'b2, 'c2, 'd2, 'd2, 'f2) fmtty_rel

(***)

(* List of format elements. *)
and ('a, 'b, 'c, 'd, 'e, 'f) fmt =
  | Char :                                                   (* %c *)
      ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        (char -> 'a, 'b, 'c, 'd, 'e, 'f) fmt
  | Caml_char :                                              (* %C *)
      ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        (char -> 'a, 'b, 'c, 'd, 'e, 'f) fmt
  | String :                                                 (* %s *)
      ('x, string -> 'a) padding * ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        ('x, 'b, 'c, 'd, 'e, 'f) fmt
  | Caml_string :                                            (* %S *)
      ('x, string -> 'a) padding * ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        ('x, 'b, 'c, 'd, 'e, 'f) fmt
  | Int :                                                    (* %[dixXuo] *)
      int_conv * ('x, 'y) padding * ('y, int -> 'a) precision *
      ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        ('x, 'b, 'c, 'd, 'e, 'f) fmt
  | Int32 :                                                  (* %l[dixXuo] *)
      int_conv * ('x, 'y) padding * ('y, int32 -> 'a) precision *
      ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        ('x, 'b, 'c, 'd, 'e, 'f) fmt
  | Nativeint :                                              (* %n[dixXuo] *)
      int_conv * ('x, 'y) padding * ('y, nativeint -> 'a) precision *
      ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        ('x, 'b, 'c, 'd, 'e, 'f) fmt
  | Int64 :                                                  (* %L[dixXuo] *)
      int_conv * ('x, 'y) padding * ('y, int64 -> 'a) precision *
      ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        ('x, 'b, 'c, 'd, 'e, 'f) fmt
  | Float :                                                  (* %[feEgGF] *)
      float_conv * ('x, 'y) padding * ('y, float -> 'a) precision *
      ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        ('x, 'b, 'c, 'd, 'e, 'f) fmt
  | Bool :                                                   (* %[bB] *)
      ('x, bool -> 'a) padding * ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        ('x, 'b, 'c, 'd, 'e, 'f) fmt
  | Flush :                                                  (* %! *)
      ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        ('a, 'b, 'c, 'd, 'e, 'f) fmt

  | String_literal :                                         (* abc *)
      string * ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        ('a, 'b, 'c, 'd, 'e, 'f) fmt
  | Char_literal :                                           (* x *)
      char * ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        ('a, 'b, 'c, 'd, 'e, 'f) fmt

  | Format_arg :                                             (* %{...%} *)
      pad_option * ('g, 'h, 'i, 'j, 'k, 'l) fmtty *
      ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        (('g, 'h, 'i, 'j, 'k, 'l) format6 -> 'a, 'b, 'c, 'd, 'e, 'f) fmt
  | Format_subst :                                           (* %(...%) *)
      pad_option *
      ('g, 'h, 'i, 'j, 'k, 'l,
       'g2, 'b, 'c, 'j2, 'd, 'a) fmtty_rel *
      ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
      (('g, 'h, 'i, 'j, 'k, 'l) format6 -> 'g2, 'b, 'c, 'j2, 'e, 'f) fmt

  (* Printf and Format specific constructor. *)
  | Alpha :                                                  (* %a *)
      ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        (('b -> 'x -> 'c) -> 'x -> 'a, 'b, 'c, 'd, 'e, 'f) fmt
  | Theta :                                                  (* %t *)
      ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        (('b -> 'c) -> 'a, 'b, 'c, 'd, 'e, 'f) fmt

  (* Format specific constructor: *)
  | Formatting_lit :                                         (* @_ *)
      formatting_lit * ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        ('a, 'b, 'c, 'd, 'e, 'f) fmt
  | Formatting_gen :                                             (* @_ *)
      ('a1, 'b, 'c, 'd1, 'e1, 'f1) formatting_gen *
      ('f1, 'b, 'c, 'e1, 'e2, 'f2) fmt -> ('a1, 'b, 'c, 'd1, 'e2, 'f2) fmt

  (* Scanf specific constructors: *)
  | Reader :                                                 (* %r *)
      ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        ('x -> 'a, 'b, 'c, ('b -> 'x) -> 'd, 'e, 'f) fmt
  | Scan_char_set :                                          (* %[...] *)
      pad_option * char_set * ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        (string -> 'a, 'b, 'c, 'd, 'e, 'f) fmt
  | Scan_get_counter :                                       (* %[nlNL] *)
      counter * ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
        (int -> 'a, 'b, 'c, 'd, 'e, 'f) fmt
  | Scan_next_char :                                         (* %0c *)
      ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
      (char -> 'a, 'b, 'c, 'd, 'e, 'f) fmt
  | Ignored_param :                                          (* %_ *)
      ('a, 'b, 'c, 'd, 'y, 'x) ignored * ('x, 'b, 'c, 'y, 'e, 'f) fmt ->
        ('a, 'b, 'c, 'd, 'e, 'f) fmt

  (* Custom printing format (PR#6452, GPR#140)

     We include a type Custom of "custom converters", where an
     arbitrary function can be used to convert one or more
     arguments. There is no syntax for custom converters, it is only
     intended for custom processors that wish to rely on the
     stdlib-defined format GADTs.

     For instance a pre-processor could choose to interpret strings
     prefixed with ["!"] as format strings where [%{{ ... }}] is
     a special form to pass a to_string function, so that one could
     write:

     {[
       type t = { x : int; y : int }

       let string_of_t t = Printf.sprintf "{ x = %d; y = %d }" t.x t.y

       Printf.printf !"t = %{{string_of_t}}" { x = 42; y = 42 }
     ]}
  *)
  | Custom :
      ('a, 'x, 'y) custom_arity * (unit -> 'x) * ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
      ('y, 'b, 'c, 'd, 'e, 'f) fmt

  (* end of a format specification *)
  | End_of_format :
        ('f, 'b, 'c, 'e, 'e, 'f) fmt

(***)

(* Type for ignored parameters (see "%_"). *)
and ('a, 'b, 'c, 'd, 'e, 'f) ignored =
  | Ignored_char :                                           (* %_c *)
      ('a, 'b, 'c, 'd, 'd, 'a) ignored
  | Ignored_caml_char :                                      (* %_C *)
      ('a, 'b, 'c, 'd, 'd, 'a) ignored
  | Ignored_string :                                         (* %_s *)
      pad_option -> ('a, 'b, 'c, 'd, 'd, 'a) ignored
  | Ignored_caml_string :                                    (* %_S *)
      pad_option -> ('a, 'b, 'c, 'd, 'd, 'a) ignored
  | Ignored_int :                                            (* %_d *)
      int_conv * pad_option -> ('a, 'b, 'c, 'd, 'd, 'a) ignored
  | Ignored_int32 :                                          (* %_ld *)
      int_conv * pad_option -> ('a, 'b, 'c, 'd, 'd, 'a) ignored
  | Ignored_nativeint :                                      (* %_nd *)
      int_conv * pad_option -> ('a, 'b, 'c, 'd, 'd, 'a) ignored
  | Ignored_int64 :                                          (* %_Ld *)
      int_conv * pad_option -> ('a, 'b, 'c, 'd, 'd, 'a) ignored
  | Ignored_float :                                          (* %_f *)
      pad_option * prec_option -> ('a, 'b, 'c, 'd, 'd, 'a) ignored
  | Ignored_bool :                                           (* %_B *)
      pad_option -> ('a, 'b, 'c, 'd, 'd, 'a) ignored
  | Ignored_format_arg :                                     (* %_{...%} *)
      pad_option * ('g, 'h, 'i, 'j, 'k, 'l) fmtty ->
        ('a, 'b, 'c, 'd, 'd, 'a) ignored
  | Ignored_format_subst :                                   (* %_(...%) *)
      pad_option * ('a, 'b, 'c, 'd, 'e, 'f) fmtty ->
        ('a, 'b, 'c, 'd, 'e, 'f) ignored
  | Ignored_reader :                                         (* %_r *)
      ('a, 'b, 'c, ('b -> 'x) -> 'd, 'd, 'a) ignored
  | Ignored_scan_char_set :                                  (* %_[...] *)
      pad_option * char_set -> ('a, 'b, 'c, 'd, 'd, 'a) ignored
  | Ignored_scan_get_counter :                               (* %_[nlNL] *)
      counter -> ('a, 'b, 'c, 'd, 'd, 'a) ignored
  | Ignored_scan_next_char :                                 (* %_0c *)
      ('a, 'b, 'c, 'd, 'd, 'a) ignored

and ('a, 'b, 'c, 'd, 'e, 'f) format6 =
  Format of ('a, 'b, 'c, 'd, 'e, 'f) fmt * string

let rec erase_rel : type a b c d e f g h i j k l .
  (a, b, c, d, e, f,
   g, h, i, j, k, l) fmtty_rel -> (a, b, c, d, e, f) fmtty
= function
  | Char_ty rest ->
    Char_ty (erase_rel rest)
  | String_ty rest ->
    String_ty (erase_rel rest)
  | Int_ty rest ->
    Int_ty (erase_rel rest)
  | Int32_ty rest ->
    Int32_ty (erase_rel rest)
  | Int64_ty rest ->
    Int64_ty (erase_rel rest)
  | Nativeint_ty rest ->
    Nativeint_ty (erase_rel rest)
  | Float_ty rest ->
    Float_ty (erase_rel rest)
  | Bool_ty rest ->
    Bool_ty (erase_rel rest)
  | Format_arg_ty (ty, rest) ->
    Format_arg_ty (ty, erase_rel rest)
  | Format_subst_ty (ty1, _ty2, rest) ->
    Format_subst_ty (ty1, ty1, erase_rel rest)
  | Alpha_ty rest ->
    Alpha_ty (erase_rel rest)
  | Theta_ty rest ->
    Theta_ty (erase_rel rest)
  | Any_ty rest ->
    Any_ty (erase_rel rest)
  | Reader_ty rest ->
    Reader_ty (erase_rel rest)
  | Ignored_reader_ty rest ->
    Ignored_reader_ty (erase_rel rest)
  | End_of_fmtty -> End_of_fmtty

(******************************************************************************)
                         (* Format type concatenation *)

(* Concatenate two format types. *)
(* Used by:
   * reader_nb_unifier_of_fmtty to count readers in an fmtty,
   * Scanf.take_fmtty_format_readers to extract readers inside %(...%),
   * CamlinternalFormat.fmtty_of_ignored_format to extract format type. *)

(*
let rec concat_fmtty : type a b c d e f g h .
    (a, b, c, d, e, f) fmtty ->
    (f, b, c, e, g, h) fmtty ->
    (a, b, c, d, g, h) fmtty =
*)
let rec concat_fmtty :
  type a1 b1 c1 d1 e1 f1
       a2 b2 c2 d2 e2 f2
       g1 j1 g2 j2
  .
    (g1, b1, c1, j1, d1, a1,
     g2, b2, c2, j2, d2, a2) fmtty_rel ->
    (a1, b1, c1, d1, e1, f1,
     a2, b2, c2, d2, e2, f2) fmtty_rel ->
    (g1, b1, c1, j1, e1, f1,
     g2, b2, c2, j2, e2, f2) fmtty_rel =
fun fmtty1 fmtty2 -> match fmtty1 with
  | Char_ty rest ->
    Char_ty (concat_fmtty rest fmtty2)
  | String_ty rest ->
    String_ty (concat_fmtty rest fmtty2)
  | Int_ty rest ->
    Int_ty (concat_fmtty rest fmtty2)
  | Int32_ty rest ->
    Int32_ty (concat_fmtty rest fmtty2)
  | Nativeint_ty rest ->
    Nativeint_ty (concat_fmtty rest fmtty2)
  | Int64_ty rest ->
    Int64_ty (concat_fmtty rest fmtty2)
  | Float_ty rest ->
    Float_ty (concat_fmtty rest fmtty2)
  | Bool_ty rest ->
    Bool_ty (concat_fmtty rest fmtty2)
  | Alpha_ty rest ->
    Alpha_ty (concat_fmtty rest fmtty2)
  | Theta_ty rest ->
    Theta_ty (concat_fmtty rest fmtty2)
  | Any_ty rest ->
    Any_ty (concat_fmtty rest fmtty2)
  | Reader_ty rest ->
    Reader_ty (concat_fmtty rest fmtty2)
  | Ignored_reader_ty rest ->
    Ignored_reader_ty (concat_fmtty rest fmtty2)
  | Format_arg_ty (ty, rest) ->
    Format_arg_ty (ty, concat_fmtty rest fmtty2)
  | Format_subst_ty (ty1, ty2, rest) ->
    Format_subst_ty (ty1, ty2, concat_fmtty rest fmtty2)
  | End_of_fmtty -> fmtty2

(******************************************************************************)
                           (* Format concatenation *)

(* Concatenate two formats. *)
let rec concat_fmt : type a b c d e f g h .
    (a, b, c, d, e, f) fmt ->
    (f, b, c, e, g, h) fmt ->
    (a, b, c, d, g, h) fmt =
fun fmt1 fmt2 -> match fmt1 with
  | String (pad, rest) ->
    String (pad, concat_fmt rest fmt2)
  | Caml_string (pad, rest) ->
    Caml_string (pad, concat_fmt rest fmt2)

  | Int (iconv, pad, prec, rest) ->
    Int (iconv, pad, prec, concat_fmt rest fmt2)
  | Int32 (iconv, pad, prec, rest) ->
    Int32 (iconv, pad, prec, concat_fmt rest fmt2)
  | Nativeint (iconv, pad, prec, rest) ->
    Nativeint (iconv, pad, prec, concat_fmt rest fmt2)
  | Int64 (iconv, pad, prec, rest) ->
    Int64 (iconv, pad, prec, concat_fmt rest fmt2)
  | Float (fconv, pad, prec, rest) ->
    Float (fconv, pad, prec, concat_fmt rest fmt2)

  | Char (rest) ->
    Char (concat_fmt rest fmt2)
  | Caml_char rest ->
    Caml_char (concat_fmt rest fmt2)
  | Bool (pad, rest) ->
    Bool (pad, concat_fmt rest fmt2)
  | Alpha rest ->
    Alpha (concat_fmt rest fmt2)
  | Theta rest ->
    Theta (concat_fmt rest fmt2)
  | Custom (arity, f, rest) ->
    Custom (arity, f, concat_fmt rest fmt2)
  | Reader rest ->
    Reader (concat_fmt rest fmt2)
  | Flush rest ->
    Flush (concat_fmt rest fmt2)

  | String_literal (str, rest) ->
    String_literal (str, concat_fmt rest fmt2)
  | Char_literal (chr, rest) ->
    Char_literal   (chr, concat_fmt rest fmt2)

  | Format_arg (pad, fmtty, rest) ->
    Format_arg   (pad, fmtty, concat_fmt rest fmt2)
  | Format_subst (pad, fmtty, rest) ->
    Format_subst (pad, fmtty, concat_fmt rest fmt2)

  | Scan_char_set (width_opt, char_set, rest) ->
    Scan_char_set (width_opt, char_set, concat_fmt rest fmt2)
  | Scan_get_counter (counter, rest) ->
    Scan_get_counter (counter, concat_fmt rest fmt2)
  | Scan_next_char (rest) ->
    Scan_next_char (concat_fmt rest fmt2)
  | Ignored_param (ign, rest) ->
    Ignored_param (ign, concat_fmt rest fmt2)

  | Formatting_lit (fmting_lit, rest) ->
    Formatting_lit (fmting_lit, concat_fmt rest fmt2)
  | Formatting_gen (fmting_gen, rest) ->
    Formatting_gen (fmting_gen, concat_fmt rest fmt2)

  | End_of_format ->
    fmt2
end

module CamlinternalFormat = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                          Benoit Vaugon, ENSTA                          *)
(*                                                                        *)
(*   Copyright 2014 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open CamlinternalFormatBasics

(******************************************************************************)
           (* Tools to manipulate scanning set of chars (see %[...]) *)

type mutable_char_set = bytes

(* Create a fresh, empty, mutable char set. *)
let create_char_set () = Bytes.make 32 '\000'

(* Add a char in a mutable char set. *)
let add_in_char_set char_set c =
  let ind = int_of_char c in
  let str_ind = ind lsr 3 and mask = 1 lsl (ind land 0b111) in
  Bytes.set char_set str_ind
    (char_of_int (int_of_char (Bytes.get char_set str_ind) lor mask))

let freeze_char_set char_set =
  Bytes.to_string char_set

(* Compute the complement of a char set. *)
let rev_char_set char_set =
  let char_set' = create_char_set () in
  for i = 0 to 31 do
    Bytes.set char_set' i
      (char_of_int (int_of_char (String.get char_set i) lxor 0xFF));
  done;
  Bytes.unsafe_to_string char_set'

(* Return true if a `c' is in `char_set'. *)
let is_in_char_set char_set c =
  let ind = int_of_char c in
  let str_ind = ind lsr 3 and mask = 1 lsl (ind land 0b111) in
  (int_of_char (String.get char_set str_ind) land mask) <> 0


(******************************************************************************)
                         (* Ignored param conversion *)

(* GADT used to abstract an existential type parameter. *)
(* See param_format_of_ignored_format. *)
type ('a, 'b, 'c, 'd, 'e, 'f) param_format_ebb = Param_format_EBB :
    ('x -> 'a, 'b, 'c, 'd, 'e, 'f) fmt ->
    ('a, 'b, 'c, 'd, 'e, 'f) param_format_ebb

(* Compute a padding associated to a pad_option (see "%_42d"). *)
let pad_of_pad_opt pad_opt = match pad_opt with
  | None -> No_padding
  | Some width -> Lit_padding (Right, width)

(* Compute a precision associated to a prec_option (see "%_.42f"). *)
let prec_of_prec_opt prec_opt = match prec_opt with
  | None -> No_precision
  | Some ndec -> Lit_precision ndec

(* Turn an ignored param into its equivalent not-ignored format node. *)
(* Used for format pretty-printing and Scanf. *)
let param_format_of_ignored_format : type a b c d e f x y .
    (a, b, c, d, y, x) ignored -> (x, b, c, y, e, f) fmt ->
      (a, b, c, d, e, f) param_format_ebb =
fun ign fmt -> match ign with
  | Ignored_char ->
    Param_format_EBB (Char fmt)
  | Ignored_caml_char ->
    Param_format_EBB (Caml_char fmt)
  | Ignored_string pad_opt ->
    Param_format_EBB (String (pad_of_pad_opt pad_opt, fmt))
  | Ignored_caml_string pad_opt ->
    Param_format_EBB (Caml_string (pad_of_pad_opt pad_opt, fmt))
  | Ignored_int (iconv, pad_opt) ->
    Param_format_EBB (Int (iconv, pad_of_pad_opt pad_opt, No_precision, fmt))
  | Ignored_int32 (iconv, pad_opt) ->
    Param_format_EBB
      (Int32 (iconv, pad_of_pad_opt pad_opt, No_precision, fmt))
  | Ignored_nativeint (iconv, pad_opt) ->
    Param_format_EBB
      (Nativeint (iconv, pad_of_pad_opt pad_opt, No_precision, fmt))
  | Ignored_int64 (iconv, pad_opt) ->
    Param_format_EBB
      (Int64 (iconv, pad_of_pad_opt pad_opt, No_precision, fmt))
  | Ignored_float (pad_opt, prec_opt) ->
    Param_format_EBB
      (Float (Float_f, pad_of_pad_opt pad_opt, prec_of_prec_opt prec_opt, fmt))
  | Ignored_bool pad_opt ->
    Param_format_EBB (Bool (pad_of_pad_opt pad_opt, fmt))
  | Ignored_format_arg (pad_opt, fmtty) ->
    Param_format_EBB (Format_arg (pad_opt, fmtty, fmt))
  | Ignored_format_subst (pad_opt, fmtty) ->
    Param_format_EBB
      (Format_subst (pad_opt, fmtty, fmt))
  | Ignored_reader ->
    Param_format_EBB (Reader fmt)
  | Ignored_scan_char_set (width_opt, char_set) ->
    Param_format_EBB (Scan_char_set (width_opt, char_set, fmt))
  | Ignored_scan_get_counter counter ->
    Param_format_EBB (Scan_get_counter (counter, fmt))
  | Ignored_scan_next_char ->
    Param_format_EBB (Scan_next_char fmt)


(******************************************************************************)
                                 (* Types *)

type ('b, 'c) acc_formatting_gen =
  | Acc_open_tag of ('b, 'c) acc
  | Acc_open_box of ('b, 'c) acc

(* Reversed list of printing atoms. *)
(* Used to accumulate printf arguments. *)
and ('b, 'c) acc =
  | Acc_formatting_lit of ('b, 'c) acc * formatting_lit
      (* Special fmtting (box) *)
  | Acc_formatting_gen of ('b, 'c) acc * ('b, 'c) acc_formatting_gen
      (* Special fmtting (box) *)
  | Acc_string_literal of ('b, 'c) acc * string     (* Literal string *)
  | Acc_char_literal   of ('b, 'c) acc * char       (* Literal char *)
  | Acc_data_string    of ('b, 'c) acc * string     (* Generated string *)
  | Acc_data_char      of ('b, 'c) acc * char       (* Generated char *)
  | Acc_delay          of ('b, 'c) acc * ('b -> 'c)
                                                (* Delayed printing (%a, %t) *)
  | Acc_flush          of ('b, 'c) acc              (* Flush *)
  | Acc_invalid_arg    of ('b, 'c) acc * string
      (* Raise Invalid_argument msg *)
  | End_of_acc

(* List of heterogeneous values. *)
(* Used to accumulate scanf callback arguments. *)
type ('a, 'b) heter_list =
  | Cons : 'c * ('a, 'b) heter_list -> ('c -> 'a, 'b) heter_list
  | Nil : ('b, 'b) heter_list

(* Existential Black Boxes. *)
(* Used to abstract some existential type parameters. *)

(* GADT type associating a padding and an fmtty. *)
(* See the type_padding function. *)
type ('a, 'b, 'c, 'd, 'e, 'f) padding_fmtty_ebb = Padding_fmtty_EBB :
     ('x, 'y) padding * ('y, 'b, 'c, 'd, 'e, 'f) fmtty ->
     ('x, 'b, 'c, 'd, 'e, 'f) padding_fmtty_ebb

(* GADT type associating a padding, a precision and an fmtty. *)
(* See the type_padprec function. *)
type ('a, 'b, 'c, 'd, 'e, 'f) padprec_fmtty_ebb = Padprec_fmtty_EBB :
     ('x, 'y) padding * ('y, 'z) precision * ('z, 'b, 'c, 'd, 'e, 'f) fmtty ->
     ('x, 'b, 'c, 'd, 'e, 'f) padprec_fmtty_ebb

(* GADT type associating a padding and an fmt. *)
(* See make_padding_fmt_ebb and parse_format functions. *)
type ('a, 'b, 'c, 'e, 'f) padding_fmt_ebb = Padding_fmt_EBB :
     (_, 'x -> 'a) padding *
     ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
     ('x, 'b, 'c, 'e, 'f) padding_fmt_ebb

(* GADT type associating a precision and an fmt. *)
(* See make_precision_fmt_ebb and parse_format functions. *)
type ('a, 'b, 'c, 'e, 'f) precision_fmt_ebb = Precision_fmt_EBB :
     (_, 'x -> 'a) precision *
     ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
     ('x, 'b, 'c, 'e, 'f) precision_fmt_ebb

(* GADT type associating a padding, a precision and an fmt. *)
(* See make_padprec_fmt_ebb and parse_format functions. *)
type ('p, 'b, 'c, 'e, 'f) padprec_fmt_ebb = Padprec_fmt_EBB :
     ('x, 'y) padding * ('y, 'p -> 'a) precision *
     ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
     ('p, 'b, 'c, 'e, 'f) padprec_fmt_ebb

(* Abstract the 'a and 'd parameters of an fmt. *)
(* Output type of the format parsing function. *)
type ('b, 'c, 'e, 'f) fmt_ebb = Fmt_EBB :
     ('a, 'b, 'c, 'd, 'e, 'f) fmt ->
     ('b, 'c, 'e, 'f) fmt_ebb

(* GADT type associating an fmtty and an fmt. *)
(* See the type_format_gen function. *)
type ('a, 'b, 'c, 'd, 'e, 'f) fmt_fmtty_ebb = Fmt_fmtty_EBB :
     ('a, 'b, 'c, 'd, 'y, 'x) fmt *
     ('x, 'b, 'c, 'y, 'e, 'f) fmtty ->
     ('a, 'b, 'c, 'd, 'e, 'f) fmt_fmtty_ebb

(* GADT type associating an fmtty and an fmt. *)
(* See the type_ignored_format_substitution function. *)
type ('a, 'b, 'c, 'd, 'e, 'f) fmtty_fmt_ebb = Fmtty_fmt_EBB :
     ('a, 'b, 'c, 'd, 'y, 'x) fmtty *
     ('x, 'b, 'c, 'y, 'e, 'f) fmt_fmtty_ebb ->
     ('a, 'b, 'c, 'd, 'e, 'f) fmtty_fmt_ebb

(* Abstract all fmtty type parameters. *)
(* Used to compare format types. *)
type fmtty_ebb = Fmtty_EBB : ('a, 'b, 'c, 'd, 'e, 'f) fmtty -> fmtty_ebb

(* Abstract all padding type parameters. *)
(* Used to compare paddings. *)
type padding_ebb = Padding_EBB : ('a, 'b) padding -> padding_ebb

(* Abstract all precision type parameters. *)
(* Used to compare precisions. *)
type precision_ebb = Precision_EBB : ('a, 'b) precision -> precision_ebb

(******************************************************************************)
                               (* Constants *)

(* Default precision for float printing. *)
let default_float_precision = -6
  (* For %h and %H formats, a negative precision means "as many digits as
     necessary".  For the other FP formats, we take the absolute value
     of the precision, hence 6 digits by default. *)

(******************************************************************************)
                               (* Externals *)

external format_float: string -> float -> string
  = "caml_format_float"
external format_int: string -> int -> string
  = "caml_format_int"
external format_int32: string -> int32 -> string
  = "caml_int32_format"
external format_nativeint: string -> nativeint -> string
  = "caml_nativeint_format"
external format_int64: string -> int64 -> string
  = "caml_int64_format"
external hexstring_of_float: float -> int -> char -> string
  = "caml_hexstring_of_float"

(******************************************************************************)
                     (* Tools to pretty-print formats *)

(* Type of extensible character buffers. *)
type buffer = {
  mutable ind : int;
  mutable bytes : bytes;
}

(* Create a fresh buffer. *)
let buffer_create init_size = { ind = 0; bytes = Bytes.create init_size }

(* Check size of the buffer and grow it if needed. *)
let buffer_check_size buf overhead =
  let len = Bytes.length buf.bytes in
  let min_len = buf.ind + overhead in
  if min_len > len then (
    let new_len = max (len * 2) min_len in
    let new_str = Bytes.create new_len in
    Bytes.blit buf.bytes 0 new_str 0 len;
    buf.bytes <- new_str;
  )

(* Add the character `c' to the buffer `buf'. *)
let buffer_add_char buf c =
  buffer_check_size buf 1;
  Bytes.set buf.bytes buf.ind c;
  buf.ind <- buf.ind + 1

(* Add the string `s' to the buffer `buf'. *)
let buffer_add_string buf s =
  let str_len = String.length s in
  buffer_check_size buf str_len;
  String.blit s 0 buf.bytes buf.ind str_len;
  buf.ind <- buf.ind + str_len

(* Get the content of the buffer. *)
let buffer_contents buf =
  Bytes.sub_string buf.bytes 0 buf.ind

(***)

(* Convert an integer conversion to char. *)
let char_of_iconv iconv = match iconv with
  | Int_d | Int_pd | Int_sd -> 'd' | Int_i | Int_pi | Int_si -> 'i'
  | Int_x | Int_Cx -> 'x' | Int_X | Int_CX -> 'X' | Int_o | Int_Co -> 'o'
  | Int_u -> 'u'

(* Convert a float conversion to char. *)
let char_of_fconv fconv = match fconv with
  | Float_f | Float_pf | Float_sf -> 'f' | Float_e | Float_pe | Float_se -> 'e'
  | Float_E | Float_pE | Float_sE -> 'E' | Float_g | Float_pg | Float_sg -> 'g'
  | Float_G | Float_pG | Float_sG -> 'G' | Float_F -> 'F'
  | Float_h | Float_ph | Float_sh -> 'h' | Float_H | Float_pH | Float_sH -> 'H'


(* Convert a scanning counter to char. *)
let char_of_counter counter = match counter with
  | Line_counter  -> 'l'
  | Char_counter  -> 'n'
  | Token_counter -> 'N'

(***)

(* Print a char_set in a buffer with the OCaml format lexical convention. *)
let bprint_char_set buf char_set =
  let rec print_start set =
    let is_alone c =
      let before, after = Char.(chr (code c - 1), chr (code c + 1)) in
      is_in_char_set set c
      && not (is_in_char_set set before && is_in_char_set set after) in
    if is_alone ']' then buffer_add_char buf ']';
    print_out set 1;
    if is_alone '-' then buffer_add_char buf '-';
  and print_out set i =
    if i < 256 then
      if is_in_char_set set (char_of_int i) then print_first set i
      else print_out set (i + 1)
  and print_first set i =
    match char_of_int i with
    | '\255' -> print_char buf 255;
    | ']' | '-' -> print_out set (i + 1);
    | _ -> print_second set (i + 1);
  and print_second set i =
    if is_in_char_set set (char_of_int i) then
      match char_of_int i with
      | '\255' ->
        print_char buf 254;
        print_char buf 255;
      | ']' | '-' when not (is_in_char_set set (char_of_int (i + 1))) ->
        print_char buf (i - 1);
        print_out set (i + 1);
      | _ when not (is_in_char_set set (char_of_int (i + 1))) ->
        print_char buf (i - 1);
        print_char buf i;
        print_out set (i + 2);
      | _ ->
        print_in set (i - 1) (i + 2);
    else (
      print_char buf (i - 1);
      print_out set (i + 1);
    )
  and print_in set i j =
    if j = 256 || not (is_in_char_set set (char_of_int j)) then (
      print_char buf i;
      print_char buf (int_of_char '-');
      print_char buf (j - 1);
      if j < 256 then print_out set (j + 1);
    ) else
      print_in set i (j + 1);
  and print_char buf i = match char_of_int i with
    | '%' -> buffer_add_char buf '%'; buffer_add_char buf '%';
    | '@' -> buffer_add_char buf '%'; buffer_add_char buf '@';
    | c   -> buffer_add_char buf c;
  in
  buffer_add_char buf '[';
  print_start (
    if is_in_char_set char_set '\000'
    then ( buffer_add_char buf '^'; rev_char_set char_set )
    else char_set
  );
  buffer_add_char buf ']'

(***)

(* Print a padty in a buffer with the format-like syntax. *)
let bprint_padty buf padty = match padty with
  | Left  -> buffer_add_char buf '-'
  | Right -> ()
  | Zeros -> buffer_add_char buf '0'

(* Print the '_' of an ignored flag if needed. *)
let bprint_ignored_flag buf ign_flag =
  if ign_flag then buffer_add_char buf '_'

(***)

let bprint_pad_opt buf pad_opt = match pad_opt with
  | None -> ()
  | Some width -> buffer_add_string buf (string_of_int width)

(***)

(* Print padding in a buffer with the format-like syntax. *)
let bprint_padding : type a b . buffer -> (a, b) padding -> unit =
fun buf pad -> match pad with
  | No_padding -> ()
  | Lit_padding (padty, n) ->
    bprint_padty buf padty;
    buffer_add_string buf (string_of_int n);
  | Arg_padding padty ->
    bprint_padty buf padty;
    buffer_add_char buf '*'

(* Print precision in a buffer with the format-like syntax. *)
let bprint_precision : type a b . buffer -> (a, b) precision -> unit =
  fun buf prec -> match prec with
  | No_precision -> ()
  | Lit_precision n ->
    buffer_add_char buf '.';
    buffer_add_string buf (string_of_int n);
  | Arg_precision ->
    buffer_add_string buf ".*"

(***)

(* Print the optional '+', ' ' or '#' associated to an int conversion. *)
let bprint_iconv_flag buf iconv = match iconv with
  | Int_pd | Int_pi -> buffer_add_char buf '+'
  | Int_sd | Int_si -> buffer_add_char buf ' '
  | Int_Cx | Int_CX | Int_Co -> buffer_add_char buf '#'
  | Int_d | Int_i | Int_x | Int_X | Int_o | Int_u -> ()

(* Print an complete int format in a buffer (ex: "%3.*d"). *)
let bprint_int_fmt buf ign_flag iconv pad prec =
  buffer_add_char buf '%';
  bprint_ignored_flag buf ign_flag;
  bprint_iconv_flag buf iconv;
  bprint_padding buf pad;
  bprint_precision buf prec;
  buffer_add_char buf (char_of_iconv iconv)

(* Print a complete int32, nativeint or int64 format in a buffer. *)
let bprint_altint_fmt buf ign_flag iconv pad prec c =
  buffer_add_char buf '%';
  bprint_ignored_flag buf ign_flag;
  bprint_iconv_flag buf iconv;
  bprint_padding buf pad;
  bprint_precision buf prec;
  buffer_add_char buf c;
  buffer_add_char buf (char_of_iconv iconv)

(***)

(* Print the optional '+' associated to a float conversion. *)
let bprint_fconv_flag buf fconv = match fconv with
  | Float_pf | Float_pe | Float_pE
  | Float_pg | Float_pG | Float_ph | Float_pH ->
    buffer_add_char buf '+'
  | Float_sf | Float_se | Float_sE
  | Float_sg | Float_sG | Float_sh | Float_sH ->
    buffer_add_char buf ' '
  | Float_f | Float_e | Float_E
  | Float_g | Float_G | Float_F | Float_h | Float_H ->
    ()

(* Print a complete float format in a buffer (ex: "%+*.3f"). *)
let bprint_float_fmt buf ign_flag fconv pad prec =
  buffer_add_char buf '%';
  bprint_ignored_flag buf ign_flag;
  bprint_fconv_flag buf fconv;
  bprint_padding buf pad;
  bprint_precision buf prec;
  buffer_add_char buf (char_of_fconv fconv)

(* Compute the literal string representation of a formatting_lit. *)
(* Also used by Printf and Scanf where formatting is not interpreted. *)
let string_of_formatting_lit formatting_lit = match formatting_lit with
  | Close_box            -> "@]"
  | Close_tag            -> "@}"
  | Break (str, _, _)    -> str
  | FFlush               -> "@?"
  | Force_newline        -> "@\n"
  | Flush_newline        -> "@."
  | Magic_size (str, _)  -> str
  | Escaped_at           -> "@@"
  | Escaped_percent      -> "@%"
  | Scan_indic c -> "@" ^ (String.make 1 c)

(* Compute the literal string representation of a formatting. *)
(* Also used by Printf and Scanf where formatting is not interpreted. *)
let string_of_formatting_gen : type a b c d e f .
    (a, b, c, d, e, f) formatting_gen -> string =
  fun formatting_gen -> match formatting_gen with
  | Open_tag (Format (_, str)) -> str
  | Open_box (Format (_, str)) -> str

(***)

(* Print a literal char in a buffer, escape '%' by "%%". *)
let bprint_char_literal buf chr = match chr with
  | '%' -> buffer_add_string buf "%%"
  | _ -> buffer_add_char buf chr

(* Print a literal string in a buffer, escape all '%' by "%%". *)
let bprint_string_literal buf str =
  for i = 0 to String.length str - 1 do
    bprint_char_literal buf str.[i]
  done

(******************************************************************************)
                          (* Format pretty-printing *)

(* Print a complete format type (an fmtty) in a buffer. *)
let rec bprint_fmtty : type a b c d e f g h i j k l .
    buffer -> (a, b, c, d, e, f, g, h, i, j, k, l) fmtty_rel -> unit =
fun buf fmtty -> match fmtty with
  | Char_ty rest      -> buffer_add_string buf "%c";  bprint_fmtty buf rest;
  | String_ty rest    -> buffer_add_string buf "%s";  bprint_fmtty buf rest;
  | Int_ty rest       -> buffer_add_string buf "%i";  bprint_fmtty buf rest;
  | Int32_ty rest     -> buffer_add_string buf "%li"; bprint_fmtty buf rest;
  | Nativeint_ty rest -> buffer_add_string buf "%ni"; bprint_fmtty buf rest;
  | Int64_ty rest     -> buffer_add_string buf "%Li"; bprint_fmtty buf rest;
  | Float_ty rest     -> buffer_add_string buf "%f";  bprint_fmtty buf rest;
  | Bool_ty rest      -> buffer_add_string buf "%B";  bprint_fmtty buf rest;
  | Alpha_ty rest     -> buffer_add_string buf "%a";  bprint_fmtty buf rest;
  | Theta_ty rest     -> buffer_add_string buf "%t";  bprint_fmtty buf rest;
  | Any_ty rest       -> buffer_add_string buf "%?";  bprint_fmtty buf rest;
  | Reader_ty rest    -> buffer_add_string buf "%r";  bprint_fmtty buf rest;

  | Ignored_reader_ty rest ->
    buffer_add_string buf "%_r";
    bprint_fmtty buf rest;

  | Format_arg_ty (sub_fmtty, rest) ->
    buffer_add_string buf "%{"; bprint_fmtty buf sub_fmtty;
    buffer_add_string buf "%}"; bprint_fmtty buf rest;
  | Format_subst_ty (sub_fmtty, _, rest) ->
    buffer_add_string buf "%("; bprint_fmtty buf sub_fmtty;
    buffer_add_string buf "%)"; bprint_fmtty buf rest;

  | End_of_fmtty -> ()

(***)

let rec int_of_custom_arity : type a b c .
  (a, b, c) custom_arity -> int =
  function
  | Custom_zero -> 0
  | Custom_succ x -> 1 + int_of_custom_arity x

(* Print a complete format in a buffer. *)
let bprint_fmt buf fmt =
  let rec fmtiter : type a b c d e f .
      (a, b, c, d, e, f) fmt -> bool -> unit =
  fun fmt ign_flag -> match fmt with
    | String (pad, rest) ->
      buffer_add_char buf '%'; bprint_ignored_flag buf ign_flag;
      bprint_padding buf pad; buffer_add_char buf 's';
      fmtiter rest false;
    | Caml_string (pad, rest) ->
      buffer_add_char buf '%'; bprint_ignored_flag buf ign_flag;
      bprint_padding buf pad; buffer_add_char buf 'S';
      fmtiter rest false;

    | Int (iconv, pad, prec, rest) ->
      bprint_int_fmt buf ign_flag iconv pad prec;
      fmtiter rest false;
    | Int32 (iconv, pad, prec, rest) ->
      bprint_altint_fmt buf ign_flag iconv pad prec 'l';
      fmtiter rest false;
    | Nativeint (iconv, pad, prec, rest) ->
      bprint_altint_fmt buf ign_flag iconv pad prec 'n';
      fmtiter rest false;
    | Int64 (iconv, pad, prec, rest) ->
      bprint_altint_fmt buf ign_flag iconv pad prec 'L';
      fmtiter rest false;
    | Float (fconv, pad, prec, rest) ->
      bprint_float_fmt buf ign_flag fconv pad prec;
      fmtiter rest false;

    | Char rest ->
      buffer_add_char buf '%'; bprint_ignored_flag buf ign_flag;
      buffer_add_char buf 'c'; fmtiter rest false;
    | Caml_char rest ->
      buffer_add_char buf '%'; bprint_ignored_flag buf ign_flag;
      buffer_add_char buf 'C'; fmtiter rest false;
    | Bool (pad, rest) ->
      buffer_add_char buf '%'; bprint_ignored_flag buf ign_flag;
      bprint_padding buf pad; buffer_add_char buf 'B';
      fmtiter rest false;
    | Alpha rest ->
      buffer_add_char buf '%'; bprint_ignored_flag buf ign_flag;
      buffer_add_char buf 'a'; fmtiter rest false;
    | Theta rest ->
      buffer_add_char buf '%'; bprint_ignored_flag buf ign_flag;
      buffer_add_char buf 't'; fmtiter rest false;
    | Custom (arity, _, rest) ->
      for _i = 1 to int_of_custom_arity arity do
        buffer_add_char buf '%'; bprint_ignored_flag buf ign_flag;
        buffer_add_char buf '?';
      done;
      fmtiter rest false;
    | Reader rest ->
      buffer_add_char buf '%'; bprint_ignored_flag buf ign_flag;
      buffer_add_char buf 'r'; fmtiter rest false;
    | Flush rest ->
      buffer_add_string buf "%!";
      fmtiter rest ign_flag;

    | String_literal (str, rest) ->
      bprint_string_literal buf str;
      fmtiter rest ign_flag;
    | Char_literal (chr, rest) ->
      bprint_char_literal buf chr;
      fmtiter rest ign_flag;

    | Format_arg (pad_opt, fmtty, rest) ->
      buffer_add_char buf '%'; bprint_ignored_flag buf ign_flag;
      bprint_pad_opt buf pad_opt; buffer_add_char buf '{';
      bprint_fmtty buf fmtty; buffer_add_char buf '%'; buffer_add_char buf '}';
      fmtiter rest false;
    | Format_subst (pad_opt, fmtty, rest) ->
      buffer_add_char buf '%'; bprint_ignored_flag buf ign_flag;
      bprint_pad_opt buf pad_opt; buffer_add_char buf '(';
      bprint_fmtty buf fmtty; buffer_add_char buf '%'; buffer_add_char buf ')';
      fmtiter rest false;

    | Scan_char_set (width_opt, char_set, rest) ->
      buffer_add_char buf '%'; bprint_ignored_flag buf ign_flag;
      bprint_pad_opt buf width_opt; bprint_char_set buf char_set;
      fmtiter rest false;
    | Scan_get_counter (counter, rest) ->
      buffer_add_char buf '%'; bprint_ignored_flag buf ign_flag;
      buffer_add_char buf (char_of_counter counter);
      fmtiter rest false;
    | Scan_next_char rest ->
      buffer_add_char buf '%'; bprint_ignored_flag buf ign_flag;
      bprint_string_literal buf "0c"; fmtiter rest false;

    | Ignored_param (ign, rest) ->
      let Param_format_EBB fmt' = param_format_of_ignored_format ign rest in
      fmtiter fmt' true;

    | Formatting_lit (fmting_lit, rest) ->
      bprint_string_literal buf (string_of_formatting_lit fmting_lit);
      fmtiter rest ign_flag;
    | Formatting_gen (fmting_gen, rest) ->
      bprint_string_literal buf "@{";
      bprint_string_literal buf (string_of_formatting_gen fmting_gen);
      fmtiter rest ign_flag;

    | End_of_format -> ()

  in fmtiter fmt false

(***)

(* Convert a format to string. *)
let string_of_fmt fmt =
  let buf = buffer_create 16 in
  bprint_fmt buf fmt;
  buffer_contents buf

(******************************************************************************)
                          (* Type extraction *)

type (_, _) eq = Refl : ('a, 'a) eq

(* Invariant: this function is the identity on values.

   In particular, if (ty1, ty2) have equal values, then
   (trans (symm ty1) ty2) respects the 'trans' precondition. *)
let rec symm : type a1 b1 c1 d1 e1 f1 a2 b2 c2 d2 e2 f2 .
   (a1, b1, c1, d1, e1, f1,
    a2, b2, c2, d2, e2, f2) fmtty_rel
-> (a2, b2, c2, d2, e2, f2,
    a1, b1, c1, d1, e1, f1) fmtty_rel
= function
  | Char_ty rest -> Char_ty (symm rest)
  | Int_ty rest -> Int_ty (symm rest)
  | Int32_ty rest -> Int32_ty (symm rest)
  | Int64_ty rest -> Int64_ty (symm rest)
  | Nativeint_ty rest -> Nativeint_ty (symm rest)
  | Float_ty rest -> Float_ty (symm rest)
  | Bool_ty rest -> Bool_ty (symm rest)
  | String_ty rest -> String_ty (symm rest)
  | Theta_ty rest -> Theta_ty (symm rest)
  | Alpha_ty rest -> Alpha_ty (symm rest)
  | Any_ty rest -> Any_ty (symm rest)
  | Reader_ty rest -> Reader_ty (symm rest)
  | Ignored_reader_ty rest -> Ignored_reader_ty (symm rest)
  | Format_arg_ty (ty, rest) ->
    Format_arg_ty (ty, symm rest)
  | Format_subst_ty (ty1, ty2, rest) ->
    Format_subst_ty (ty2, ty1, symm rest)
  | End_of_fmtty -> End_of_fmtty

let rec fmtty_rel_det : type a1 b c d1 e1 f1 a2 d2 e2 f2 .
  (a1, b, c, d1, e1, f1,
   a2, b, c, d2, e2, f2) fmtty_rel ->
    ((f1, f2) eq -> (a1, a2) eq)
  * ((a1, a2) eq -> (f1, f2) eq)
  * ((e1, e2) eq -> (d1, d2) eq)
  * ((d1, d2) eq -> (e1, e2) eq)
= function
  | End_of_fmtty ->
    (fun Refl -> Refl),
    (fun Refl -> Refl),
    (fun Refl -> Refl),
    (fun Refl -> Refl)
  | Char_ty rest ->
    let fa, af, ed, de = fmtty_rel_det rest in
    (fun Refl -> let Refl = fa Refl in Refl),
    (fun Refl -> let Refl = af Refl in Refl),
    ed, de
  | String_ty rest ->
    let fa, af, ed, de = fmtty_rel_det rest in
    (fun Refl -> let Refl = fa Refl in Refl),
    (fun Refl -> let Refl = af Refl in Refl),
    ed, de
  | Int_ty rest ->
    let fa, af, ed, de = fmtty_rel_det rest in
    (fun Refl -> let Refl = fa Refl in Refl),
    (fun Refl -> let Refl = af Refl in Refl),
    ed, de
  | Int32_ty rest ->
    let fa, af, ed, de = fmtty_rel_det rest in
    (fun Refl -> let Refl = fa Refl in Refl),
    (fun Refl -> let Refl = af Refl in Refl),
    ed, de
  | Int64_ty rest ->
    let fa, af, ed, de = fmtty_rel_det rest in
    (fun Refl -> let Refl = fa Refl in Refl),
    (fun Refl -> let Refl = af Refl in Refl),
    ed, de
  | Nativeint_ty rest ->
    let fa, af, ed, de = fmtty_rel_det rest in
    (fun Refl -> let Refl = fa Refl in Refl),
    (fun Refl -> let Refl = af Refl in Refl),
    ed, de
  | Float_ty rest ->
    let fa, af, ed, de = fmtty_rel_det rest in
    (fun Refl -> let Refl = fa Refl in Refl),
    (fun Refl -> let Refl = af Refl in Refl),
    ed, de
  | Bool_ty rest ->
    let fa, af, ed, de = fmtty_rel_det rest in
    (fun Refl -> let Refl = fa Refl in Refl),
    (fun Refl -> let Refl = af Refl in Refl),
    ed, de

  | Theta_ty rest ->
    let fa, af, ed, de = fmtty_rel_det rest in
    (fun Refl -> let Refl = fa Refl in Refl),
    (fun Refl -> let Refl = af Refl in Refl),
    ed, de
  | Alpha_ty rest ->
    let fa, af, ed, de = fmtty_rel_det rest in
    (fun Refl -> let Refl = fa Refl in Refl),
    (fun Refl -> let Refl = af Refl in Refl),
    ed, de
  | Any_ty rest ->
    let fa, af, ed, de = fmtty_rel_det rest in
    (fun Refl -> let Refl = fa Refl in Refl),
    (fun Refl -> let Refl = af Refl in Refl),
    ed, de
  | Reader_ty rest ->
    let fa, af, ed, de = fmtty_rel_det rest in
    (fun Refl -> let Refl = fa Refl in Refl),
    (fun Refl -> let Refl = af Refl in Refl),
    (fun Refl -> let Refl = ed Refl in Refl),
    (fun Refl -> let Refl = de Refl in Refl)
  | Ignored_reader_ty rest ->
    let fa, af, ed, de = fmtty_rel_det rest in
    (fun Refl -> let Refl = fa Refl in Refl),
    (fun Refl -> let Refl = af Refl in Refl),
    (fun Refl -> let Refl = ed Refl in Refl),
    (fun Refl -> let Refl = de Refl in Refl)
  | Format_arg_ty (_ty, rest) ->
    let fa, af, ed, de = fmtty_rel_det rest in
    (fun Refl -> let Refl = fa Refl in Refl),
    (fun Refl -> let Refl = af Refl in Refl),
    ed, de
  | Format_subst_ty (ty1, ty2, rest) ->
    let fa, af, ed, de = fmtty_rel_det rest in
    let ty = trans (symm ty1) ty2 in
    let ag, ga, dj, jd = fmtty_rel_det ty in
    (fun Refl -> let Refl = fa Refl in let Refl = ag Refl in Refl),
    (fun Refl -> let Refl = ga Refl in let Refl = af Refl in Refl),
    (fun Refl -> let Refl = ed Refl in let Refl = dj Refl in Refl),
    (fun Refl -> let Refl = jd Refl in let Refl = de Refl in Refl)

(* Precondition: we assume that the two fmtty_rel arguments have equal
   values (at possibly distinct types); this invariant comes from the way
   fmtty_rel witnesses are produced by the type-checker

   The code below uses (assert false) when this assumption is broken. The
   code pattern is the following:

     | Foo x, Foo y ->
       (* case where indeed both values
          start with constructor Foo *)
     | Foo _, _
     | _, Foo _ ->
       (* different head constructors: broken precondition *)
       assert false
*)
and trans : type
  a1 b1 c1 d1 e1 f1
  a2 b2 c2 d2 e2 f2
  a3 b3 c3 d3 e3 f3
.
   (a1, b1, c1, d1, e1, f1,
    a2, b2, c2, d2, e2, f2) fmtty_rel
-> (a2, b2, c2, d2, e2, f2,
    a3, b3, c3, d3, e3, f3) fmtty_rel
-> (a1, b1, c1, d1, e1, f1,
    a3, b3, c3, d3, e3, f3) fmtty_rel
= fun ty1 ty2 -> match ty1, ty2 with
  | Char_ty rest1, Char_ty rest2 -> Char_ty (trans rest1 rest2)
  | String_ty rest1, String_ty rest2 -> String_ty (trans rest1 rest2)
  | Bool_ty rest1, Bool_ty rest2 -> Bool_ty (trans rest1 rest2)
  | Int_ty rest1, Int_ty rest2 -> Int_ty (trans rest1 rest2)
  | Int32_ty rest1, Int32_ty rest2 -> Int32_ty (trans rest1 rest2)
  | Int64_ty rest1, Int64_ty rest2 -> Int64_ty (trans rest1 rest2)
  | Nativeint_ty rest1, Nativeint_ty rest2 -> Nativeint_ty (trans rest1 rest2)
  | Float_ty rest1, Float_ty rest2 -> Float_ty (trans rest1 rest2)

  | Alpha_ty rest1, Alpha_ty rest2 -> Alpha_ty (trans rest1 rest2)
  | Alpha_ty _, _ -> assert false
  | _, Alpha_ty _ -> assert false

  | Theta_ty rest1, Theta_ty rest2 -> Theta_ty (trans rest1 rest2)
  | Theta_ty _, _ -> assert false
  | _, Theta_ty _ -> assert false

  | Any_ty rest1, Any_ty rest2 -> Any_ty (trans rest1 rest2)
  | Any_ty _, _ -> assert false
  | _, Any_ty _ -> assert false

  | Reader_ty rest1, Reader_ty rest2 -> Reader_ty (trans rest1 rest2)
  | Reader_ty _, _ -> assert false
  | _, Reader_ty _ -> assert false

  | Ignored_reader_ty rest1, Ignored_reader_ty rest2 ->
    Ignored_reader_ty (trans rest1 rest2)
  | Ignored_reader_ty _, _ -> assert false
  | _, Ignored_reader_ty _ -> assert false

  | Format_arg_ty (ty1, rest1), Format_arg_ty (ty2, rest2) ->
    Format_arg_ty (trans ty1 ty2, trans rest1 rest2)
  | Format_arg_ty _, _ -> assert false
  | _, Format_arg_ty _ -> assert false

  | Format_subst_ty (ty11, ty12, rest1),
    Format_subst_ty (ty21, ty22, rest2) ->
    let ty = trans (symm ty12) ty21 in
    let _, f2, _, f4 = fmtty_rel_det ty in
    let Refl = f2 Refl in
    let Refl = f4 Refl in
    Format_subst_ty (ty11, ty22, trans rest1 rest2)
  | Format_subst_ty _, _ -> assert false
  | _, Format_subst_ty _ -> assert false

  | End_of_fmtty, End_of_fmtty -> End_of_fmtty
  | End_of_fmtty, _ -> assert false
  | _, End_of_fmtty -> assert false

let rec fmtty_of_formatting_gen : type a b c d e f .
  (a, b, c, d, e, f) formatting_gen ->
    (a, b, c, d, e, f) fmtty =
fun formatting_gen -> match formatting_gen with
  | Open_tag (Format (fmt, _)) -> fmtty_of_fmt fmt
  | Open_box (Format (fmt, _)) -> fmtty_of_fmt fmt

(* Extract the type representation (an fmtty) of a format. *)
and fmtty_of_fmt : type a b c d e f .
  (a, b, c, d, e, f) fmt -> (a, b, c, d, e, f) fmtty =
fun fmtty -> match fmtty with
  | String (pad, rest) ->
    fmtty_of_padding_fmtty pad (String_ty (fmtty_of_fmt rest))
  | Caml_string (pad, rest) ->
    fmtty_of_padding_fmtty pad (String_ty (fmtty_of_fmt rest))

  | Int (_, pad, prec, rest) ->
    let ty_rest = fmtty_of_fmt rest in
    let prec_ty = fmtty_of_precision_fmtty prec (Int_ty ty_rest) in
    fmtty_of_padding_fmtty pad prec_ty
  | Int32 (_, pad, prec, rest) ->
    let ty_rest = fmtty_of_fmt rest in
    let prec_ty = fmtty_of_precision_fmtty prec (Int32_ty ty_rest) in
    fmtty_of_padding_fmtty pad prec_ty
  | Nativeint (_, pad, prec, rest) ->
    let ty_rest = fmtty_of_fmt rest in
    let prec_ty = fmtty_of_precision_fmtty prec (Nativeint_ty ty_rest) in
    fmtty_of_padding_fmtty pad prec_ty
  | Int64 (_, pad, prec, rest) ->
    let ty_rest = fmtty_of_fmt rest in
    let prec_ty = fmtty_of_precision_fmtty prec (Int64_ty ty_rest) in
    fmtty_of_padding_fmtty pad prec_ty
  | Float (_, pad, prec, rest) ->
    let ty_rest = fmtty_of_fmt rest in
    let prec_ty = fmtty_of_precision_fmtty prec (Float_ty ty_rest) in
    fmtty_of_padding_fmtty pad prec_ty

  | Char rest                  -> Char_ty (fmtty_of_fmt rest)
  | Caml_char rest             -> Char_ty (fmtty_of_fmt rest)
  | Bool (pad, rest)           -> fmtty_of_padding_fmtty pad (Bool_ty (fmtty_of_fmt rest))
  | Alpha rest                 -> Alpha_ty (fmtty_of_fmt rest)
  | Theta rest                 -> Theta_ty (fmtty_of_fmt rest)
  | Custom (arity, _, rest)    -> fmtty_of_custom arity (fmtty_of_fmt rest)
  | Reader rest                -> Reader_ty (fmtty_of_fmt rest)

  | Format_arg (_, ty, rest) ->
    Format_arg_ty (ty, fmtty_of_fmt rest)
  | Format_subst (_, ty, rest) ->
    Format_subst_ty (ty, ty, fmtty_of_fmt rest)

  | Flush rest                 -> fmtty_of_fmt rest
  | String_literal (_, rest)   -> fmtty_of_fmt rest
  | Char_literal (_, rest)     -> fmtty_of_fmt rest

  | Scan_char_set (_, _, rest) -> String_ty (fmtty_of_fmt rest)
  | Scan_get_counter (_, rest) -> Int_ty (fmtty_of_fmt rest)
  | Scan_next_char rest        -> Char_ty (fmtty_of_fmt rest)
  | Ignored_param (ign, rest)  -> fmtty_of_ignored_format ign rest
  | Formatting_lit (_, rest)   -> fmtty_of_fmt rest
  | Formatting_gen (fmting_gen, rest)  ->
    concat_fmtty (fmtty_of_formatting_gen fmting_gen) (fmtty_of_fmt rest)

  | End_of_format              -> End_of_fmtty

and fmtty_of_custom : type x y a b c d e f .
  (a, x, y) custom_arity -> (a, b, c, d, e, f) fmtty ->
  (y, b, c, d, e, f) fmtty =
fun arity fmtty -> match arity with
  | Custom_zero -> fmtty
  | Custom_succ arity -> Any_ty (fmtty_of_custom arity fmtty)

(* Extract the fmtty of an ignored parameter followed by the rest of
   the format. *)
and fmtty_of_ignored_format : type x y a b c d e f .
    (a, b, c, d, y, x) ignored ->
    (x, b, c, y, e, f) fmt ->
    (a, b, c, d, e, f) fmtty =
fun ign fmt -> match ign with
  | Ignored_char                    -> fmtty_of_fmt fmt
  | Ignored_caml_char               -> fmtty_of_fmt fmt
  | Ignored_string _                -> fmtty_of_fmt fmt
  | Ignored_caml_string _           -> fmtty_of_fmt fmt
  | Ignored_int (_, _)              -> fmtty_of_fmt fmt
  | Ignored_int32 (_, _)            -> fmtty_of_fmt fmt
  | Ignored_nativeint (_, _)        -> fmtty_of_fmt fmt
  | Ignored_int64 (_, _)            -> fmtty_of_fmt fmt
  | Ignored_float (_, _)            -> fmtty_of_fmt fmt
  | Ignored_bool _                  -> fmtty_of_fmt fmt
  | Ignored_format_arg _            -> fmtty_of_fmt fmt
  | Ignored_format_subst (_, fmtty) -> concat_fmtty fmtty (fmtty_of_fmt fmt)
  | Ignored_reader                  -> Ignored_reader_ty (fmtty_of_fmt fmt)
  | Ignored_scan_char_set _         -> fmtty_of_fmt fmt
  | Ignored_scan_get_counter _      -> fmtty_of_fmt fmt
  | Ignored_scan_next_char          -> fmtty_of_fmt fmt

(* Add an Int_ty node if padding is taken as an extra argument (ex: "%*s"). *)
and fmtty_of_padding_fmtty : type x a b c d e f .
    (x, a) padding -> (a, b, c, d, e, f) fmtty -> (x, b, c, d, e, f) fmtty =
  fun pad fmtty -> match pad with
    | No_padding    -> fmtty
    | Lit_padding _ -> fmtty
    | Arg_padding _ -> Int_ty fmtty

(* Add an Int_ty node if precision is taken as an extra argument (ex: "%.*f").*)
and fmtty_of_precision_fmtty : type x a b c d e f .
    (x, a) precision -> (a, b, c, d, e, f) fmtty -> (x, b, c, d, e, f) fmtty =
  fun prec fmtty -> match prec with
    | No_precision    -> fmtty
    | Lit_precision _ -> fmtty
    | Arg_precision   -> Int_ty fmtty

(******************************************************************************)
                            (* Format typing *)

(* Exception raised when a format does not match a given format type. *)
exception Type_mismatch

(* Type a padding. *)
(* Take an Int_ty from the fmtty if the integer should be kept as argument. *)
(* Raise Type_mismatch in case of type mismatch. *)
let type_padding : type a b c d e f x y .
    (x, y) padding -> (a, b, c, d, e, f) fmtty ->
      (a, b, c, d, e, f) padding_fmtty_ebb =
fun pad fmtty -> match pad, fmtty with
  | No_padding, _ -> Padding_fmtty_EBB (No_padding, fmtty)
  | Lit_padding (padty, w), _ -> Padding_fmtty_EBB (Lit_padding (padty,w),fmtty)
  | Arg_padding padty, Int_ty rest -> Padding_fmtty_EBB (Arg_padding padty,rest)
  | _ -> raise Type_mismatch

(* Convert a (upadding, uprecision) to a (padding, precision). *)
(* Take one or two Int_ty from the fmtty if needed. *)
(* Raise Type_mismatch in case of type mismatch. *)
let type_padprec : type a b c d e f x y z .
  (x, y) padding -> (y, z) precision -> (a, b, c, d, e, f) fmtty ->
    (a, b, c, d, e, f) padprec_fmtty_ebb =
fun pad prec fmtty -> match prec, type_padding pad fmtty with
  | No_precision, Padding_fmtty_EBB (pad, rest) ->
    Padprec_fmtty_EBB (pad, No_precision, rest)
  | Lit_precision p, Padding_fmtty_EBB (pad, rest) ->
    Padprec_fmtty_EBB (pad, Lit_precision p, rest)
  | Arg_precision, Padding_fmtty_EBB (pad, Int_ty rest) ->
    Padprec_fmtty_EBB (pad, Arg_precision, rest)
  | _, Padding_fmtty_EBB (_, _) -> raise Type_mismatch

(* Type a format according to an fmtty. *)
(* If typing succeed, generate a copy of the format with the same
    type parameters as the fmtty. *)
(* Raise [Failure] with an error message in case of type mismatch. *)
let rec type_format :
  type a1 b1 c1 d1 e1 f1
       a2 b2 c2 d2 e2 f2  .
     (a1, b1, c1, d1, e1, f1) fmt
  -> (a2, b2, c2, d2, e2, f2) fmtty
  -> (a2, b2, c2, d2, e2, f2) fmt
= fun fmt fmtty -> match type_format_gen fmt fmtty with
  | Fmt_fmtty_EBB (fmt', End_of_fmtty) -> fmt'
  | _ -> raise Type_mismatch

and type_format_gen :
  type a1 b1 c1 d1 e1 f1
       a2 b2 c2 d2 e2 f2  .
     (a1, b1, c1, d1, e1, f1) fmt
  -> (a2, b2, c2, d2, e2, f2) fmtty
  -> (a2, b2, c2, d2, e2, f2) fmt_fmtty_ebb
= fun fmt fmtty -> match fmt, fmtty with
  | Char fmt_rest, Char_ty fmtty_rest ->
    let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
    Fmt_fmtty_EBB (Char fmt', fmtty')
  | Caml_char fmt_rest, Char_ty fmtty_rest ->
    let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
    Fmt_fmtty_EBB (Caml_char fmt', fmtty')
  | String (pad, fmt_rest), _ -> (
    match type_padding pad fmtty with
    | Padding_fmtty_EBB (pad, String_ty fmtty_rest) ->
      let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
      Fmt_fmtty_EBB (String (pad, fmt'), fmtty')
    | Padding_fmtty_EBB (_, _) -> raise Type_mismatch
  )
  | Caml_string (pad, fmt_rest), _ -> (
    match type_padding pad fmtty with
    | Padding_fmtty_EBB (pad, String_ty fmtty_rest) ->
      let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
      Fmt_fmtty_EBB (Caml_string (pad, fmt'), fmtty')
    | Padding_fmtty_EBB (_, _) -> raise Type_mismatch
  )
  | Int (iconv, pad, prec, fmt_rest), _ -> (
    match type_padprec pad prec fmtty with
    | Padprec_fmtty_EBB (pad, prec, Int_ty fmtty_rest) ->
      let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
      Fmt_fmtty_EBB (Int (iconv, pad, prec, fmt'), fmtty')
    | Padprec_fmtty_EBB (_, _, _) -> raise Type_mismatch
  )
  | Int32 (iconv, pad, prec, fmt_rest), _ -> (
    match type_padprec pad prec fmtty with
    | Padprec_fmtty_EBB (pad, prec, Int32_ty fmtty_rest) ->
      let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
      Fmt_fmtty_EBB (Int32 (iconv, pad, prec, fmt'), fmtty')
    | Padprec_fmtty_EBB (_, _, _) -> raise Type_mismatch
  )
  | Nativeint (iconv, pad, prec, fmt_rest), _ -> (
    match type_padprec pad prec fmtty with
    | Padprec_fmtty_EBB (pad, prec, Nativeint_ty fmtty_rest) ->
      let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
      Fmt_fmtty_EBB (Nativeint (iconv, pad, prec, fmt'), fmtty')
    | Padprec_fmtty_EBB (_, _, _) -> raise Type_mismatch
  )
  | Int64 (iconv, pad, prec, fmt_rest), _ -> (
    match type_padprec pad prec fmtty with
    | Padprec_fmtty_EBB (pad, prec, Int64_ty fmtty_rest) ->
      let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
      Fmt_fmtty_EBB (Int64 (iconv, pad, prec, fmt'), fmtty')
    | Padprec_fmtty_EBB (_, _, _) -> raise Type_mismatch
  )
  | Float (fconv, pad, prec, fmt_rest), _ -> (
    match type_padprec pad prec fmtty with
    | Padprec_fmtty_EBB (pad, prec, Float_ty fmtty_rest) ->
      let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
      Fmt_fmtty_EBB (Float (fconv, pad, prec, fmt'), fmtty')
    | Padprec_fmtty_EBB (_, _, _) -> raise Type_mismatch
  )
  | Bool (pad, fmt_rest), _ -> (
    match type_padding pad fmtty with
    | Padding_fmtty_EBB (pad, Bool_ty fmtty_rest) ->
      let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
      Fmt_fmtty_EBB (Bool (pad, fmt'), fmtty')
    | Padding_fmtty_EBB (_, _) -> raise Type_mismatch
  )
  | Flush fmt_rest, fmtty_rest ->
    let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
    Fmt_fmtty_EBB (Flush fmt', fmtty')

  | String_literal (str, fmt_rest), fmtty_rest ->
    let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
    Fmt_fmtty_EBB (String_literal (str, fmt'), fmtty')
  | Char_literal (chr, fmt_rest), fmtty_rest ->
    let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
    Fmt_fmtty_EBB (Char_literal (chr, fmt'), fmtty')

  | Format_arg (pad_opt, sub_fmtty, fmt_rest),
    Format_arg_ty (sub_fmtty', fmtty_rest) ->
    if Fmtty_EBB sub_fmtty <> Fmtty_EBB sub_fmtty' then raise Type_mismatch;
    let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
    Fmt_fmtty_EBB (Format_arg (pad_opt, sub_fmtty', fmt'), fmtty')
  | Format_subst (pad_opt, sub_fmtty, fmt_rest),
    Format_subst_ty (sub_fmtty1, _sub_fmtty2, fmtty_rest) ->
    if Fmtty_EBB (erase_rel sub_fmtty) <> Fmtty_EBB (erase_rel sub_fmtty1) then
      raise Type_mismatch;
    let Fmt_fmtty_EBB (fmt', fmtty') =
      type_format_gen fmt_rest (erase_rel fmtty_rest)
    in
    Fmt_fmtty_EBB (Format_subst (pad_opt, sub_fmtty1, fmt'), fmtty')
  (* Printf and Format specific constructors: *)
  | Alpha fmt_rest, Alpha_ty fmtty_rest ->
    let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
    Fmt_fmtty_EBB (Alpha fmt', fmtty')
  | Theta fmt_rest, Theta_ty fmtty_rest ->
    let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
    Fmt_fmtty_EBB (Theta fmt', fmtty')

  (* Format specific constructors: *)
  | Formatting_lit (formatting_lit, fmt_rest), fmtty_rest ->
    let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
    Fmt_fmtty_EBB (Formatting_lit (formatting_lit, fmt'), fmtty')
  | Formatting_gen (formatting_gen, fmt_rest), fmtty_rest ->
    type_formatting_gen formatting_gen fmt_rest fmtty_rest

  (* Scanf specific constructors: *)
  | Reader fmt_rest, Reader_ty fmtty_rest ->
    let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
    Fmt_fmtty_EBB (Reader fmt', fmtty')
  | Scan_char_set (width_opt, char_set, fmt_rest), String_ty fmtty_rest ->
    let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
    Fmt_fmtty_EBB (Scan_char_set (width_opt, char_set, fmt'), fmtty')
  | Scan_get_counter (counter, fmt_rest), Int_ty fmtty_rest ->
    let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt_rest fmtty_rest in
    Fmt_fmtty_EBB (Scan_get_counter (counter, fmt'), fmtty')
  | Ignored_param (ign, rest), fmtty_rest ->
    type_ignored_param ign rest fmtty_rest

  | End_of_format, fmtty_rest -> Fmt_fmtty_EBB (End_of_format, fmtty_rest)

  | _ -> raise Type_mismatch

and type_formatting_gen : type a1 a3 b1 b3 c1 c3 d1 d3 e1 e2 e3 f1 f2 f3 .
    (a1, b1, c1, d1, e1, f1) formatting_gen ->
    (f1, b1, c1, e1, e2, f2) fmt ->
    (a3, b3, c3, d3, e3, f3) fmtty ->
    (a3, b3, c3, d3, e3, f3) fmt_fmtty_ebb =
fun formatting_gen fmt0 fmtty0 -> match formatting_gen with
  | Open_tag (Format (fmt1, str)) ->
    let Fmt_fmtty_EBB (fmt2, fmtty2) = type_format_gen fmt1 fmtty0 in
    let Fmt_fmtty_EBB (fmt3, fmtty3) = type_format_gen fmt0 fmtty2 in
    Fmt_fmtty_EBB (Formatting_gen (Open_tag (Format (fmt2, str)), fmt3), fmtty3)
  | Open_box (Format (fmt1, str)) ->
    let Fmt_fmtty_EBB (fmt2, fmtty2) = type_format_gen fmt1 fmtty0 in
    let Fmt_fmtty_EBB (fmt3, fmtty3) = type_format_gen fmt0 fmtty2 in
    Fmt_fmtty_EBB (Formatting_gen (Open_box (Format (fmt2, str)), fmt3), fmtty3)

(* Type an Ignored_param node according to an fmtty. *)
and type_ignored_param : type p q x y z t u v a b c d e f .
    (x, y, z, t, q, p) ignored ->
    (p, y, z, q, u, v) fmt ->
    (a, b, c, d, e, f) fmtty ->
    (a, b, c, d, e, f) fmt_fmtty_ebb =
fun ign fmt fmtty -> match ign with
  | Ignored_char               as ign' -> type_ignored_param_one ign' fmt fmtty
  | Ignored_caml_char          as ign' -> type_ignored_param_one ign' fmt fmtty
  | Ignored_string _           as ign' -> type_ignored_param_one ign' fmt fmtty
  | Ignored_caml_string _      as ign' -> type_ignored_param_one ign' fmt fmtty
  | Ignored_int _              as ign' -> type_ignored_param_one ign' fmt fmtty
  | Ignored_int32 _            as ign' -> type_ignored_param_one ign' fmt fmtty
  | Ignored_nativeint _        as ign' -> type_ignored_param_one ign' fmt fmtty
  | Ignored_int64 _            as ign' -> type_ignored_param_one ign' fmt fmtty
  | Ignored_float _            as ign' -> type_ignored_param_one ign' fmt fmtty
  | Ignored_bool _             as ign' -> type_ignored_param_one ign' fmt fmtty
  | Ignored_scan_char_set _    as ign' -> type_ignored_param_one ign' fmt fmtty
  | Ignored_scan_get_counter _ as ign' -> type_ignored_param_one ign' fmt fmtty
  | Ignored_scan_next_char     as ign' -> type_ignored_param_one ign' fmt fmtty
  | Ignored_format_arg (pad_opt, sub_fmtty) ->
    type_ignored_param_one (Ignored_format_arg (pad_opt, sub_fmtty)) fmt fmtty
  | Ignored_format_subst (pad_opt, sub_fmtty) ->
    let Fmtty_fmt_EBB (sub_fmtty', Fmt_fmtty_EBB (fmt', fmtty')) =
      type_ignored_format_substitution sub_fmtty fmt fmtty in
    Fmt_fmtty_EBB (Ignored_param (Ignored_format_subst (pad_opt, sub_fmtty'),
                                  fmt'),
                   fmtty')
  | Ignored_reader -> (
    match fmtty with
    | Ignored_reader_ty fmtty_rest ->
      let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt fmtty_rest in
      Fmt_fmtty_EBB (Ignored_param (Ignored_reader, fmt'), fmtty')
    | _ -> raise Type_mismatch
  )

and type_ignored_param_one : type a1 a2 b1 b2 c1 c2 d1 d2 e1 e2 f1 f2 .
    (a2, b2, c2, d2, d2, a2) ignored ->
    (a1, b1, c1, d1, e1, f1) fmt ->
    (a2, b2, c2, d2, e2, f2) fmtty ->
    (a2, b2, c2, d2, e2, f2) fmt_fmtty_ebb
= fun ign fmt fmtty ->
  let Fmt_fmtty_EBB (fmt', fmtty') = type_format_gen fmt fmtty in
  Fmt_fmtty_EBB (Ignored_param (ign, fmt'), fmtty')

(* Typing of the complex case: "%_(...%)". *)
and type_ignored_format_substitution : type w x y z p s t u a b c d e f .
    (w, x, y, z, s, p) fmtty ->
    (p, x, y, s, t, u) fmt ->
    (a, b, c, d, e, f) fmtty -> (a, b, c, d, e, f) fmtty_fmt_ebb =
fun sub_fmtty fmt fmtty -> match sub_fmtty, fmtty with
  | Char_ty sub_fmtty_rest, Char_ty fmtty_rest ->
    let Fmtty_fmt_EBB (sub_fmtty_rest', fmt') =
      type_ignored_format_substitution sub_fmtty_rest fmt fmtty_rest in
    Fmtty_fmt_EBB (Char_ty sub_fmtty_rest', fmt')
  | String_ty sub_fmtty_rest, String_ty fmtty_rest ->
    let Fmtty_fmt_EBB (sub_fmtty_rest', fmt') =
      type_ignored_format_substitution sub_fmtty_rest fmt fmtty_rest in
    Fmtty_fmt_EBB (String_ty sub_fmtty_rest', fmt')
  | Int_ty sub_fmtty_rest, Int_ty fmtty_rest ->
    let Fmtty_fmt_EBB (sub_fmtty_rest', fmt') =
      type_ignored_format_substitution sub_fmtty_rest fmt fmtty_rest in
    Fmtty_fmt_EBB (Int_ty sub_fmtty_rest', fmt')
  | Int32_ty sub_fmtty_rest, Int32_ty fmtty_rest ->
    let Fmtty_fmt_EBB (sub_fmtty_rest', fmt') =
      type_ignored_format_substitution sub_fmtty_rest fmt fmtty_rest in
    Fmtty_fmt_EBB (Int32_ty sub_fmtty_rest', fmt')
  | Nativeint_ty sub_fmtty_rest, Nativeint_ty fmtty_rest ->
    let Fmtty_fmt_EBB (sub_fmtty_rest', fmt') =
      type_ignored_format_substitution sub_fmtty_rest fmt fmtty_rest in
    Fmtty_fmt_EBB (Nativeint_ty sub_fmtty_rest', fmt')
  | Int64_ty sub_fmtty_rest, Int64_ty fmtty_rest ->
    let Fmtty_fmt_EBB (sub_fmtty_rest', fmt') =
      type_ignored_format_substitution sub_fmtty_rest fmt fmtty_rest in
    Fmtty_fmt_EBB (Int64_ty sub_fmtty_rest', fmt')
  | Float_ty sub_fmtty_rest, Float_ty fmtty_rest ->
    let Fmtty_fmt_EBB (sub_fmtty_rest', fmt') =
      type_ignored_format_substitution sub_fmtty_rest fmt fmtty_rest in
    Fmtty_fmt_EBB (Float_ty sub_fmtty_rest', fmt')
  | Bool_ty sub_fmtty_rest, Bool_ty fmtty_rest ->
    let Fmtty_fmt_EBB (sub_fmtty_rest', fmt') =
      type_ignored_format_substitution sub_fmtty_rest fmt fmtty_rest in
    Fmtty_fmt_EBB (Bool_ty sub_fmtty_rest', fmt')
  | Alpha_ty sub_fmtty_rest, Alpha_ty fmtty_rest ->
    let Fmtty_fmt_EBB (sub_fmtty_rest', fmt') =
      type_ignored_format_substitution sub_fmtty_rest fmt fmtty_rest in
    Fmtty_fmt_EBB (Alpha_ty sub_fmtty_rest', fmt')
  | Theta_ty sub_fmtty_rest, Theta_ty fmtty_rest ->
    let Fmtty_fmt_EBB (sub_fmtty_rest', fmt') =
      type_ignored_format_substitution sub_fmtty_rest fmt fmtty_rest in
    Fmtty_fmt_EBB (Theta_ty sub_fmtty_rest', fmt')
  | Reader_ty sub_fmtty_rest, Reader_ty fmtty_rest ->
    let Fmtty_fmt_EBB (sub_fmtty_rest', fmt') =
      type_ignored_format_substitution sub_fmtty_rest fmt fmtty_rest in
    Fmtty_fmt_EBB (Reader_ty sub_fmtty_rest', fmt')
  | Ignored_reader_ty sub_fmtty_rest, Ignored_reader_ty fmtty_rest ->
    let Fmtty_fmt_EBB (sub_fmtty_rest', fmt') =
      type_ignored_format_substitution sub_fmtty_rest fmt fmtty_rest in
    Fmtty_fmt_EBB (Ignored_reader_ty sub_fmtty_rest', fmt')

  | Format_arg_ty (sub2_fmtty, sub_fmtty_rest),
    Format_arg_ty (sub2_fmtty', fmtty_rest) ->
    if Fmtty_EBB sub2_fmtty <> Fmtty_EBB sub2_fmtty' then raise Type_mismatch;
    let Fmtty_fmt_EBB (sub_fmtty_rest', fmt') =
      type_ignored_format_substitution sub_fmtty_rest fmt fmtty_rest in
    Fmtty_fmt_EBB (Format_arg_ty (sub2_fmtty', sub_fmtty_rest'), fmt')
  | Format_subst_ty (sub1_fmtty,  sub2_fmtty,  sub_fmtty_rest),
    Format_subst_ty (sub1_fmtty', sub2_fmtty', fmtty_rest) ->
    (* TODO define Fmtty_rel_EBB to remove those erase_rel *)
    if Fmtty_EBB (erase_rel sub1_fmtty) <> Fmtty_EBB (erase_rel sub1_fmtty')
    then raise Type_mismatch;
    if Fmtty_EBB (erase_rel sub2_fmtty) <> Fmtty_EBB (erase_rel sub2_fmtty')
    then raise Type_mismatch;
    let sub_fmtty' = trans (symm sub1_fmtty') sub2_fmtty' in
    let _, f2, _, f4 = fmtty_rel_det sub_fmtty' in
    let Refl = f2 Refl in
    let Refl = f4 Refl in
    let Fmtty_fmt_EBB (sub_fmtty_rest', fmt') =
      type_ignored_format_substitution (erase_rel sub_fmtty_rest) fmt fmtty_rest
    in
    Fmtty_fmt_EBB (Format_subst_ty (sub1_fmtty', sub2_fmtty',
                                    symm sub_fmtty_rest'),
                   fmt')
  | End_of_fmtty, fmtty ->
    Fmtty_fmt_EBB (End_of_fmtty, type_format_gen fmt fmtty)
  | _ -> raise Type_mismatch

(* This implementation of `recast` is a bit disappointing. The
   invariant provided by the type are very strong: the input format's
   type is in relation to the output type's as witnessed by the
   fmtty_rel argument. One would at first expect this function to be
   total, and implementable by exhaustive pattern matching. Instead,
   we reuse the highly partial and much less well-defined function
   `type_format` that has lost all knowledge of the correspondence
   between the argument's types.

   Besides the fact that this function reuses a lot of the
   `type_format` logic (eg.: seeing Int_ty in the fmtty parameter does
   not let you match on Int only, as you may in fact have Float
   (Arg_padding, ...) ("%.*d") beginning with an Int_ty), it is also
   a partial function, because the typing information in a format is
   not quite enough to reconstruct it unambiguously. For example, the
   format types of "%d%_r" and "%_r%d" have the same format6
   parameters, but they are not at all exchangeable, and putting one
   in place of the other must result in a dynamic failure.

   Given that:
   - we'd have to duplicate a lot of non-trivial typing logic from type_format
   - this wouldn't even eliminate (all) the dynamic failures
   we decided to just reuse type_format directly for now.
*)
let recast :
  type a1 b1 c1 d1 e1 f1
       a2 b2 c2 d2 e2 f2
  .
     (a1, b1, c1, d1, e1, f1) fmt
  -> (a1, b1, c1, d1, e1, f1,
      a2, b2, c2, d2, e2, f2) fmtty_rel
  -> (a2, b2, c2, d2, e2, f2) fmt
= fun fmt fmtty ->
  type_format fmt (erase_rel (symm fmtty))

(******************************************************************************)
                             (* Printing tools *)

(* Add padding spaces around a string. *)
let fix_padding padty width str =
  let len = String.length str in
  let width, padty =
    abs width,
    (* while literal padding widths are always non-negative,
       dynamically-set widths (Arg_padding, eg. %*d) may be negative;
       we interpret those as specifying a padding-to-the-left; this
       means that '0' may get dropped even if it was explicitly set,
       but:
       - this is what the legacy implementation does, and
         we preserve compatibility if possible
       - we could only signal this issue by failing at runtime,
         which is not very nice... *)
    if width < 0 then Left else padty in
  if width <= len then str else
    let res = Bytes.make width (if padty = Zeros then '0' else ' ') in
    begin match padty with
    | Left  -> String.blit str 0 res 0 len
    | Right -> String.blit str 0 res (width - len) len
    | Zeros when len > 0 && (str.[0] = '+' || str.[0] = '-' || str.[0] = ' ') ->
      Bytes.set res 0 str.[0];
      String.blit str 1 res (width - len + 1) (len - 1)
    | Zeros when len > 1 && str.[0] = '0' && (str.[1] = 'x' || str.[1] = 'X') ->
      Bytes.set res 1 str.[1];
      String.blit str 2 res (width - len + 2) (len - 2)
    | Zeros ->
      String.blit str 0 res (width - len) len
    end;
    Bytes.unsafe_to_string res

(* Add '0' padding to int, int32, nativeint or int64 string representation. *)
let fix_int_precision prec str =
  let prec = abs prec in
  let len = String.length str in
  match str.[0] with
  | ('+' | '-' | ' ') as c when prec + 1 > len ->
    let res = Bytes.make (prec + 1) '0' in
    Bytes.set res 0 c;
    String.blit str 1 res (prec - len + 2) (len - 1);
    Bytes.unsafe_to_string res
  | '0' when prec + 2 > len && len > 1 && (str.[1] = 'x' || str.[1] = 'X') ->
    let res = Bytes.make (prec + 2) '0' in
    Bytes.set res 1 str.[1];
    String.blit str 2 res (prec - len + 4) (len - 2);
    Bytes.unsafe_to_string res
  | '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' when prec > len ->
    let res = Bytes.make prec '0' in
    String.blit str 0 res (prec - len) len;
    Bytes.unsafe_to_string res
  | _ ->
    str

(* Escape a string according to the OCaml lexing convention. *)
let string_to_caml_string str =
  let str = String.escaped str in
  let l = String.length str in
  let res = Bytes.make (l + 2) '\"' in
  String.unsafe_blit str 0 res 1 l;
  Bytes.unsafe_to_string res

(* Generate the format_int/int32/nativeint/int64 first argument
   from an int_conv. *)
let format_of_iconv = function
  | Int_d -> "%d" | Int_pd -> "%+d" | Int_sd -> "% d"
  | Int_i -> "%i" | Int_pi -> "%+i" | Int_si -> "% i"
  | Int_x -> "%x" | Int_Cx -> "%#x"
  | Int_X -> "%X" | Int_CX -> "%#X"
  | Int_o -> "%o" | Int_Co -> "%#o"
  | Int_u -> "%u"

let format_of_iconvL = function
  | Int_d -> "%Ld" | Int_pd -> "%+Ld" | Int_sd -> "% Ld"
  | Int_i -> "%Li" | Int_pi -> "%+Li" | Int_si -> "% Li"
  | Int_x -> "%Lx" | Int_Cx -> "%#Lx"
  | Int_X -> "%LX" | Int_CX -> "%#LX"
  | Int_o -> "%Lo" | Int_Co -> "%#Lo"
  | Int_u -> "%Lu"

let format_of_iconvl = function
  | Int_d -> "%ld" | Int_pd -> "%+ld" | Int_sd -> "% ld"
  | Int_i -> "%li" | Int_pi -> "%+li" | Int_si -> "% li"
  | Int_x -> "%lx" | Int_Cx -> "%#lx"
  | Int_X -> "%lX" | Int_CX -> "%#lX"
  | Int_o -> "%lo" | Int_Co -> "%#lo"
  | Int_u -> "%lu"

let format_of_iconvn = function
  | Int_d -> "%nd" | Int_pd -> "%+nd" | Int_sd -> "% nd"
  | Int_i -> "%ni" | Int_pi -> "%+ni" | Int_si -> "% ni"
  | Int_x -> "%nx" | Int_Cx -> "%#nx"
  | Int_X -> "%nX" | Int_CX -> "%#nX"
  | Int_o -> "%no" | Int_Co -> "%#no"
  | Int_u -> "%nu"

(* Generate the format_float first argument form a float_conv. *)
let format_of_fconv fconv prec =
  if fconv = Float_F then "%.12g" else
    let prec = abs prec in
    let symb = char_of_fconv fconv in
    let buf = buffer_create 16 in
    buffer_add_char buf '%';
    bprint_fconv_flag buf fconv;
    buffer_add_char buf '.';
    buffer_add_string buf (string_of_int prec);
    buffer_add_char buf symb;
    buffer_contents buf

(* Convert an integer to a string according to a conversion. *)
let convert_int iconv n = format_int (format_of_iconv iconv) n
let convert_int32 iconv n = format_int32 (format_of_iconvl iconv) n
let convert_nativeint iconv n = format_nativeint (format_of_iconvn iconv) n
let convert_int64 iconv n = format_int64 (format_of_iconvL iconv) n

(* Convert a float to string. *)
(* Fix special case of "OCaml float format". *)
let convert_float fconv prec x =
  match fconv with
  | Float_h | Float_ph | Float_sh | Float_H | Float_pH | Float_sH ->
    let sign =
      match fconv with
      | Float_ph | Float_pH -> '+'
      | Float_sh | Float_sH -> ' '
      | _ -> '-' in
    let str = hexstring_of_float x prec sign in
    begin match fconv with
    | Float_H | Float_pH | Float_sH -> String.uppercase_ascii str
    | _ -> str
    end
  | _ ->
    let str = format_float (format_of_fconv fconv prec) x in
    if fconv <> Float_F then str else
      let len = String.length str in
      let rec is_valid i =
        if i = len then false else
          match str.[i] with
          | '.' | 'e' | 'E' -> true
          | _ -> is_valid (i + 1)
      in
      match classify_float x with
      | FP_normal | FP_subnormal | FP_zero ->
        if is_valid 0 then str else str ^ "."
      | FP_infinite ->
        if x < 0.0 then "neg_infinity" else "infinity"
      | FP_nan -> "nan"

(* Convert a char to a string according to the OCaml lexical convention. *)
let format_caml_char c =
  let str = Char.escaped c in
  let l = String.length str in
  let res = Bytes.make (l + 2) '\'' in
  String.unsafe_blit str 0 res 1 l;
  Bytes.unsafe_to_string res

(* Convert a format type to string *)
let string_of_fmtty fmtty =
  let buf = buffer_create 16 in
  bprint_fmtty buf fmtty;
  buffer_contents buf

(******************************************************************************)
                        (* Generic printing function *)

(* Make a generic printing function. *)
(* Used to generate Printf and Format printing functions. *)
(* Parameters:
     k: a continuation finally applied to the output stream and the accumulator.
     o: the output stream (see k, %a and %t).
     acc: rev list of printing entities (string, char, flush, formatting, ...).
     fmt: the format. *)
let rec make_printf : type a b c d e f .
    (b -> (b, c) acc -> f) -> b -> (b, c) acc ->
    (a, b, c, d, e, f) fmt -> a =
fun k o acc fmt -> match fmt with
  | Char rest ->
    fun c ->
      let new_acc = Acc_data_char (acc, c) in
      make_printf k o new_acc rest
  | Caml_char rest ->
    fun c ->
      let new_acc = Acc_data_string (acc, format_caml_char c) in
      make_printf k o new_acc rest
  | String (pad, rest) ->
    make_padding k o acc rest pad (fun str -> str)
  | Caml_string (pad, rest) ->
    make_padding k o acc rest pad string_to_caml_string
  | Int (iconv, pad, prec, rest) ->
    make_int_padding_precision k o acc rest pad prec convert_int iconv
  | Int32 (iconv, pad, prec, rest) ->
    make_int_padding_precision k o acc rest pad prec convert_int32 iconv
  | Nativeint (iconv, pad, prec, rest) ->
    make_int_padding_precision k o acc rest pad prec convert_nativeint iconv
  | Int64 (iconv, pad, prec, rest) ->
    make_int_padding_precision k o acc rest pad prec convert_int64 iconv
  | Float (fconv, pad, prec, rest) ->
    make_float_padding_precision k o acc rest pad prec fconv
  | Bool (pad, rest) ->
    make_padding k o acc rest pad string_of_bool
  | Alpha rest ->
    fun f x -> make_printf k o (Acc_delay (acc, fun o -> f o x)) rest
  | Theta rest ->
    fun f -> make_printf k o (Acc_delay (acc, f)) rest
  | Custom (arity, f, rest) ->
    make_custom k o acc rest arity (f ())
  | Reader _ ->
    (* This case is impossible, by typing of formats. *)
    (* Indeed, since printf and co. take a format4 as argument, the 'd and 'e
       type parameters of fmt are obviously equals. The Reader is the
       only constructor which touch 'd and 'e type parameters of the format
       type, it adds an (->) to the 'd parameters. Consequently, a format4
       cannot contain a Reader node, except in the sub-format associated to
       an %{...%}. It's not a problem because make_printf do not call
       itself recursively on the sub-format associated to %{...%}. *)
    assert false
  | Flush rest ->
    make_printf k o (Acc_flush acc) rest

  | String_literal (str, rest) ->
    make_printf k o (Acc_string_literal (acc, str)) rest
  | Char_literal (chr, rest) ->
    make_printf k o (Acc_char_literal (acc, chr)) rest

  | Format_arg (_, sub_fmtty, rest) ->
    let ty = string_of_fmtty sub_fmtty in
    (fun str ->
      ignore str;
      make_printf k o (Acc_data_string (acc, ty)) rest)
  | Format_subst (_, fmtty, rest) ->
    fun (Format (fmt, _)) -> make_printf k o acc
      (concat_fmt (recast fmt fmtty) rest)

  | Scan_char_set (_, _, rest) ->
    let new_acc = Acc_invalid_arg (acc, "Printf: bad conversion %[") in
    fun _ -> make_printf k o new_acc rest
  | Scan_get_counter (_, rest) ->
    (* This case should be refused for Printf. *)
    (* Accepted for backward compatibility. *)
    (* Interpret %l, %n and %L as %u. *)
    fun n ->
      let new_acc = Acc_data_string (acc, format_int "%u" n) in
      make_printf k o new_acc rest
  | Scan_next_char rest ->
    fun c ->
      let new_acc = Acc_data_char (acc, c) in
      make_printf k o new_acc rest
  | Ignored_param (ign, rest) ->
    make_ignored_param k o acc ign rest

  | Formatting_lit (fmting_lit, rest) ->
    make_printf k o (Acc_formatting_lit (acc, fmting_lit)) rest
  | Formatting_gen (Open_tag (Format (fmt', _)), rest) ->
    let k' koc kacc =
      make_printf k koc (Acc_formatting_gen (acc, Acc_open_tag kacc)) rest in
    make_printf k' o End_of_acc fmt'
  | Formatting_gen (Open_box (Format (fmt', _)), rest) ->
    let k' koc kacc =
      make_printf k koc (Acc_formatting_gen (acc, Acc_open_box kacc)) rest in
    make_printf k' o End_of_acc fmt'

  | End_of_format ->
    k o acc

(* Delay the error (Invalid_argument "Printf: bad conversion %_"). *)
(* Generate functions to take remaining arguments (after the "%_"). *)
and make_ignored_param : type x y a b c d e f .
    (b -> (b, c) acc -> f) -> b -> (b, c) acc ->
    (a, b, c, d, y, x) ignored ->
    (x, b, c, y, e, f) fmt -> a =
fun k o acc ign fmt -> match ign with
  | Ignored_char                    -> make_invalid_arg k o acc fmt
  | Ignored_caml_char               -> make_invalid_arg k o acc fmt
  | Ignored_string _                -> make_invalid_arg k o acc fmt
  | Ignored_caml_string _           -> make_invalid_arg k o acc fmt
  | Ignored_int (_, _)              -> make_invalid_arg k o acc fmt
  | Ignored_int32 (_, _)            -> make_invalid_arg k o acc fmt
  | Ignored_nativeint (_, _)        -> make_invalid_arg k o acc fmt
  | Ignored_int64 (_, _)            -> make_invalid_arg k o acc fmt
  | Ignored_float (_, _)            -> make_invalid_arg k o acc fmt
  | Ignored_bool _                  -> make_invalid_arg k o acc fmt
  | Ignored_format_arg _            -> make_invalid_arg k o acc fmt
  | Ignored_format_subst (_, fmtty) -> make_from_fmtty k o acc fmtty fmt
  | Ignored_reader                  -> assert false
  | Ignored_scan_char_set _         -> make_invalid_arg k o acc fmt
  | Ignored_scan_get_counter _      -> make_invalid_arg k o acc fmt
  | Ignored_scan_next_char          -> make_invalid_arg k o acc fmt


(* Special case of printf "%_(". *)
and make_from_fmtty : type x y a b c d e f .
    (b -> (b, c) acc -> f) -> b -> (b, c) acc ->
    (a, b, c, d, y, x) fmtty ->
    (x, b, c, y, e, f) fmt -> a =
fun k o acc fmtty fmt -> match fmtty with
  | Char_ty rest            -> fun _ -> make_from_fmtty k o acc rest fmt
  | String_ty rest          -> fun _ -> make_from_fmtty k o acc rest fmt
  | Int_ty rest             -> fun _ -> make_from_fmtty k o acc rest fmt
  | Int32_ty rest           -> fun _ -> make_from_fmtty k o acc rest fmt
  | Nativeint_ty rest       -> fun _ -> make_from_fmtty k o acc rest fmt
  | Int64_ty rest           -> fun _ -> make_from_fmtty k o acc rest fmt
  | Float_ty rest           -> fun _ -> make_from_fmtty k o acc rest fmt
  | Bool_ty rest            -> fun _ -> make_from_fmtty k o acc rest fmt
  | Alpha_ty rest           -> fun _ _ -> make_from_fmtty k o acc rest fmt
  | Theta_ty rest           -> fun _ -> make_from_fmtty k o acc rest fmt
  | Any_ty rest             -> fun _ -> make_from_fmtty k o acc rest fmt
  | Reader_ty _             -> assert false
  | Ignored_reader_ty _     -> assert false
  | Format_arg_ty (_, rest) -> fun _ -> make_from_fmtty k o acc rest fmt
  | End_of_fmtty            -> make_invalid_arg k o acc fmt
  | Format_subst_ty (ty1, ty2, rest) ->
    let ty = trans (symm ty1) ty2 in
    fun _ -> make_from_fmtty k o acc (concat_fmtty ty rest) fmt

(* Insert an Acc_invalid_arg in the accumulator and continue to generate
   closures to get the remaining arguments. *)
and make_invalid_arg : type a b c d e f .
    (b -> (b, c) acc -> f) -> b -> (b, c) acc ->
    (a, b, c, d, e, f) fmt -> a =
fun k o acc fmt ->
  make_printf k o (Acc_invalid_arg (acc, "Printf: bad conversion %_")) fmt

(* Fix padding, take it as an extra integer argument if needed. *)
and make_padding : type x z a b c d e f .
    (b -> (b, c) acc -> f) -> b -> (b, c) acc ->
    (a, b, c, d, e, f) fmt ->
    (x, z -> a) padding -> (z -> string) -> x =
  fun k o acc fmt pad trans -> match pad with
  | No_padding ->
    fun x ->
      let new_acc = Acc_data_string (acc, trans x) in
      make_printf k o new_acc fmt
  | Lit_padding (padty, width) ->
    fun x ->
      let new_acc = Acc_data_string (acc, fix_padding padty width (trans x)) in
      make_printf k o new_acc fmt
  | Arg_padding padty ->
    fun w x ->
      let new_acc = Acc_data_string (acc, fix_padding padty w (trans x)) in
      make_printf k o new_acc fmt

(* Fix padding and precision for int, int32, nativeint or int64. *)
(* Take one or two extra integer arguments if needed. *)
and make_int_padding_precision : type x y z a b c d e f .
    (b -> (b, c) acc -> f) -> b -> (b, c) acc ->
    (a, b, c, d, e, f) fmt ->
    (x, y) padding -> (y, z -> a) precision -> (int_conv -> z -> string) ->
    int_conv -> x =
  fun k o acc fmt pad prec trans iconv -> match pad, prec with
  | No_padding, No_precision ->
    fun x ->
      let str = trans iconv x in
      make_printf k o (Acc_data_string (acc, str)) fmt
  | No_padding, Lit_precision p ->
    fun x ->
      let str = fix_int_precision p (trans iconv x) in
      make_printf k o (Acc_data_string (acc, str)) fmt
  | No_padding, Arg_precision ->
    fun p x ->
      let str = fix_int_precision p (trans iconv x) in
      make_printf k o (Acc_data_string (acc, str)) fmt
  | Lit_padding (padty, w), No_precision ->
    fun x ->
      let str = fix_padding padty w (trans iconv x) in
      make_printf k o (Acc_data_string (acc, str)) fmt
  | Lit_padding (padty, w), Lit_precision p ->
    fun x ->
      let str = fix_padding padty w (fix_int_precision p (trans iconv x)) in
      make_printf k o (Acc_data_string (acc, str)) fmt
  | Lit_padding (padty, w), Arg_precision ->
    fun p x ->
      let str = fix_padding padty w (fix_int_precision p (trans iconv x)) in
      make_printf k o (Acc_data_string (acc, str)) fmt
  | Arg_padding padty, No_precision ->
    fun w x ->
      let str = fix_padding padty w (trans iconv x) in
      make_printf k o (Acc_data_string (acc, str)) fmt
  | Arg_padding padty, Lit_precision p ->
    fun w x ->
      let str = fix_padding padty w (fix_int_precision p (trans iconv x)) in
      make_printf k o (Acc_data_string (acc, str)) fmt
  | Arg_padding padty, Arg_precision ->
    fun w p x ->
      let str = fix_padding padty w (fix_int_precision p (trans iconv x)) in
      make_printf k o (Acc_data_string (acc, str)) fmt

(* Convert a float, fix padding and precision if needed. *)
(* Take the float argument and one or two extra integer arguments if needed. *)
and make_float_padding_precision : type x y a b c d e f .
    (b -> (b, c) acc -> f) -> b -> (b, c) acc ->
    (a, b, c, d, e, f) fmt ->
    (x, y) padding -> (y, float -> a) precision -> float_conv -> x =
  fun k o acc fmt pad prec fconv -> match pad, prec with
  | No_padding, No_precision ->
    fun x ->
      let str = convert_float fconv default_float_precision x in
      make_printf k o (Acc_data_string (acc, str)) fmt
  | No_padding, Lit_precision p ->
    fun x ->
      let str = convert_float fconv p x in
      make_printf k o (Acc_data_string (acc, str)) fmt
  | No_padding, Arg_precision ->
    fun p x ->
      let str = convert_float fconv p x in
      make_printf k o (Acc_data_string (acc, str)) fmt
  | Lit_padding (padty, w), No_precision ->
    fun x ->
      let str = convert_float fconv default_float_precision x in
      let str' = fix_padding padty w str in
      make_printf k o (Acc_data_string (acc, str')) fmt
  | Lit_padding (padty, w), Lit_precision p ->
    fun x ->
      let str = fix_padding padty w (convert_float fconv p x) in
      make_printf k o (Acc_data_string (acc, str)) fmt
  | Lit_padding (padty, w), Arg_precision ->
    fun p x ->
      let str = fix_padding padty w (convert_float fconv p x) in
      make_printf k o (Acc_data_string (acc, str)) fmt
  | Arg_padding padty, No_precision ->
    fun w x ->
      let str = convert_float fconv default_float_precision x in
      let str' = fix_padding padty w str in
      make_printf k o (Acc_data_string (acc, str')) fmt
  | Arg_padding padty, Lit_precision p ->
    fun w x ->
      let str = fix_padding padty w (convert_float fconv p x) in
      make_printf k o (Acc_data_string (acc, str)) fmt
  | Arg_padding padty, Arg_precision ->
    fun w p x ->
      let str = fix_padding padty w (convert_float fconv p x) in
      make_printf k o (Acc_data_string (acc, str)) fmt
and make_custom : type x y a b c d e f .
  (b -> (b, c) acc -> f) -> b -> (b, c) acc ->
  (a, b, c, d, e, f) fmt ->
  (a, x, y) custom_arity -> x -> y =
  fun k o acc rest arity f -> match arity with
  | Custom_zero -> make_printf k o (Acc_data_string (acc, f)) rest
  | Custom_succ arity ->
    fun x ->
      make_custom k o acc rest arity (f x)

let const x _ = x

let rec make_iprintf : type a b c d e f.
  (b -> f) -> b -> (a, b, c, d, e, f) fmt -> a =
  fun k o fmt -> match fmt with
    | Char rest ->
        const (make_iprintf k o rest)
    | Caml_char rest ->
        const (make_iprintf k o rest)
    | String (No_padding, rest) ->
        const (make_iprintf k o rest)
    | String (Lit_padding _, rest) ->
        const (make_iprintf k o rest)
    | String (Arg_padding _, rest) ->
        const (const (make_iprintf k o rest))
    | Caml_string (No_padding, rest) ->
        const (make_iprintf k o rest)
    | Caml_string (Lit_padding _, rest) ->
        const (make_iprintf k o rest)
    | Caml_string (Arg_padding _, rest) ->
        const (const (make_iprintf k o rest))
    | Int (_, pad, prec, rest) ->
        fn_of_padding_precision k o rest pad prec
    | Int32 (_, pad, prec, rest) ->
        fn_of_padding_precision k o rest pad prec
    | Nativeint (_, pad, prec, rest) ->
        fn_of_padding_precision k o rest pad prec
    | Int64 (_, pad, prec, rest) ->
        fn_of_padding_precision k o rest pad prec
    | Float (_, pad, prec, rest) ->
        fn_of_padding_precision k o rest pad prec
    | Bool (No_padding, rest) ->
        const (make_iprintf k o rest)
    | Bool (Lit_padding _, rest) ->
        const (make_iprintf k o rest)
    | Bool (Arg_padding _, rest) ->
        const (const (make_iprintf k o rest))
    | Alpha rest ->
        const (const (make_iprintf k o rest))
    | Theta rest ->
        const (make_iprintf k o rest)
    | Custom (arity, _, rest) ->
        fn_of_custom_arity k o rest arity
    | Reader _ ->
        (* This case is impossible, by typing of formats.  See the
           note in the corresponding case for make_printf. *)
        assert false
    | Flush rest ->
        make_iprintf k o rest
    | String_literal (_, rest) ->
        make_iprintf k o rest
    | Char_literal (_, rest) ->
        make_iprintf k o rest
    | Format_arg (_, _, rest) ->
        const (make_iprintf k o rest)
    | Format_subst (_, fmtty, rest) ->
        fun (Format (fmt, _)) ->
          make_iprintf k o
            (concat_fmt (recast fmt fmtty) rest)
    | Scan_char_set (_, _, rest) ->
        const (make_iprintf k o rest)
    | Scan_get_counter (_, rest) ->
        const (make_iprintf k o rest)
    | Scan_next_char rest ->
        const (make_iprintf k o rest)
    | Ignored_param (ign, rest) ->
        make_ignored_param (fun x _ -> k x) o (End_of_acc) ign rest
    | Formatting_lit (_, rest) ->
        make_iprintf k o rest
    | Formatting_gen (Open_tag (Format (fmt', _)), rest) ->
        make_iprintf (fun koc -> make_iprintf k koc rest) o fmt'
    | Formatting_gen (Open_box (Format (fmt', _)), rest) ->
        make_iprintf (fun koc -> make_iprintf k koc rest) o fmt'
    | End_of_format ->
        k o
and fn_of_padding_precision :
  type x y z a b c d e f.
  (b -> f) -> b -> (a, b, c, d, e, f) fmt ->
  (x, y) padding -> (y, z -> a) precision -> x =
  fun k o fmt pad prec -> match pad, prec with
    | No_padding   , No_precision    ->
        const (make_iprintf k o fmt)
    | No_padding   , Lit_precision _ ->
        const (make_iprintf k o fmt)
    | No_padding   , Arg_precision   ->
        const (const (make_iprintf k o fmt))
    | Lit_padding _, No_precision    ->
        const (make_iprintf k o fmt)
    | Lit_padding _, Lit_precision _ ->
        const (make_iprintf k o fmt)
    | Lit_padding _, Arg_precision   ->
        const (const (make_iprintf k o fmt))
    | Arg_padding _, No_precision    ->
        const (const (make_iprintf k o fmt))
    | Arg_padding _, Lit_precision _ ->
        const (const (make_iprintf k o fmt))
    | Arg_padding _, Arg_precision   ->
        const (const (const (make_iprintf k o fmt)))
and fn_of_custom_arity : type x y a b c d e f .
  (b -> f) -> b -> (a, b, c, d, e, f) fmt -> (a, x, y) custom_arity -> y =
  fun k o fmt -> function
    | Custom_zero ->
        make_iprintf k o fmt
    | Custom_succ arity ->
        const (fn_of_custom_arity k o fmt arity)

(******************************************************************************)
                          (* Continuations for make_printf *)

(* Recursively output an "accumulator" containing a reversed list of
   printing entities (string, char, flus, ...) in an output_stream. *)
(* Used as a continuation of make_printf. *)
let rec output_acc o acc = match acc with
  | Acc_formatting_lit (p, fmting_lit) ->
    let s = string_of_formatting_lit fmting_lit in
    output_acc o p; output_string o s;
  | Acc_formatting_gen (p, Acc_open_tag acc') ->
    output_acc o p; output_string o "@{"; output_acc o acc';
  | Acc_formatting_gen (p, Acc_open_box acc') ->
    output_acc o p; output_string o "@["; output_acc o acc';
  | Acc_string_literal (p, s)
  | Acc_data_string (p, s)   -> output_acc o p; output_string o s
  | Acc_char_literal (p, c)
  | Acc_data_char (p, c)     -> output_acc o p; output_char o c
  | Acc_delay (p, f)         -> output_acc o p; f o
  | Acc_flush p              -> output_acc o p; flush o
  | Acc_invalid_arg (p, msg) -> output_acc o p; invalid_arg msg;
  | End_of_acc               -> ()

(* Recursively output an "accumulator" containing a reversed list of
   printing entities (string, char, flus, ...) in a buffer. *)
(* Used as a continuation of make_printf. *)
let rec bufput_acc b acc = match acc with
  | Acc_formatting_lit (p, fmting_lit) ->
    let s = string_of_formatting_lit fmting_lit in
    bufput_acc b p; Buffer.add_string b s;
  | Acc_formatting_gen (p, Acc_open_tag acc') ->
    bufput_acc b p; Buffer.add_string b "@{"; bufput_acc b acc';
  | Acc_formatting_gen (p, Acc_open_box acc') ->
    bufput_acc b p; Buffer.add_string b "@["; bufput_acc b acc';
  | Acc_string_literal (p, s)
  | Acc_data_string (p, s)   -> bufput_acc b p; Buffer.add_string b s
  | Acc_char_literal (p, c)
  | Acc_data_char (p, c)     -> bufput_acc b p; Buffer.add_char b c
  | Acc_delay (p, f)         -> bufput_acc b p; f b
  | Acc_flush p              -> bufput_acc b p;
  | Acc_invalid_arg (p, msg) -> bufput_acc b p; invalid_arg msg;
  | End_of_acc               -> ()

(* Recursively output an "accumulator" containing a reversed list of
   printing entities (string, char, flus, ...) in a buffer. *)
(* Differ from bufput_acc by the interpretation of %a and %t. *)
(* Used as a continuation of make_printf. *)
let rec strput_acc b acc = match acc with
  | Acc_formatting_lit (p, fmting_lit) ->
    let s = string_of_formatting_lit fmting_lit in
    strput_acc b p; Buffer.add_string b s;
  | Acc_formatting_gen (p, Acc_open_tag acc') ->
    strput_acc b p; Buffer.add_string b "@{"; strput_acc b acc';
  | Acc_formatting_gen (p, Acc_open_box acc') ->
    strput_acc b p; Buffer.add_string b "@["; strput_acc b acc';
  | Acc_string_literal (p, s)
  | Acc_data_string (p, s)   -> strput_acc b p; Buffer.add_string b s
  | Acc_char_literal (p, c)
  | Acc_data_char (p, c)     -> strput_acc b p; Buffer.add_char b c
  | Acc_delay (p, f)         -> strput_acc b p; Buffer.add_string b (f ())
  | Acc_flush p              -> strput_acc b p;
  | Acc_invalid_arg (p, msg) -> strput_acc b p; invalid_arg msg;
  | End_of_acc               -> ()

(******************************************************************************)
                          (* Error management *)

(* Raise [Failure] with a pretty-printed error message. *)
let failwith_message (Format (fmt, _)) =
  let buf = Buffer.create 256 in
  let k () acc = strput_acc buf acc; failwith (Buffer.contents buf) in
  make_printf k () End_of_acc fmt

(******************************************************************************)
                            (* Formatting tools *)

(* Convert a string to an open block description (indent, block_type) *)
let open_box_of_string str =
  if str = "" then (0, Pp_box) else
    let len = String.length str in
    let invalid_box () = failwith_message "invalid box description %S" str in
    let rec parse_spaces i =
      if i = len then i else
        match str.[i] with
        | ' ' | '\t' -> parse_spaces (i + 1)
        | _ -> i
    and parse_lword i j =
      if j = len then j else
        match str.[j] with
        | 'a' .. 'z' -> parse_lword i (j + 1)
        | _ -> j
    and parse_int i j =
      if j = len then j else
        match str.[j] with
        | '0' .. '9' | '-' -> parse_int i (j + 1)
        | _ -> j in
    let wstart = parse_spaces 0 in
    let wend = parse_lword wstart wstart in
    let box_name = String.sub str wstart (wend - wstart) in
    let nstart = parse_spaces wend in
    let nend = parse_int nstart nstart in
    let indent =
      if nstart = nend then 0 else
        try int_of_string (String.sub str nstart (nend - nstart))
        with Failure _ -> invalid_box () in
    let exp_end = parse_spaces nend in
    if exp_end <> len then invalid_box ();
    let box_type = match box_name with
      | "" | "b" -> Pp_box
      | "h"      -> Pp_hbox
      | "v"      -> Pp_vbox
      | "hv"     -> Pp_hvbox
      | "hov"    -> Pp_hovbox
      | _        -> invalid_box () in
    (indent, box_type)

(******************************************************************************)
                            (* Parsing tools *)

(* Create a padding_fmt_ebb from a padding and a format. *)
(* Copy the padding to disjoin the type parameters of argument and result. *)
let make_padding_fmt_ebb : type x y .
    (x, y) padding -> (_, _, _, _, _, _) fmt ->
      (_, _, _, _, _) padding_fmt_ebb =
fun pad fmt -> match pad with
  | No_padding         -> Padding_fmt_EBB (No_padding, fmt)
  | Lit_padding (s, w) -> Padding_fmt_EBB (Lit_padding (s, w), fmt)
  | Arg_padding s      -> Padding_fmt_EBB (Arg_padding s, fmt)

(* Create a precision_fmt_ebb from a precision and a format. *)
(* Copy the precision to disjoin the type parameters of argument and result. *)
let make_precision_fmt_ebb : type x y .
    (x, y) precision -> (_, _, _, _, _, _) fmt ->
      (_, _, _, _, _) precision_fmt_ebb =
fun prec fmt -> match prec with
  | No_precision    -> Precision_fmt_EBB (No_precision, fmt)
  | Lit_precision p -> Precision_fmt_EBB (Lit_precision p, fmt)
  | Arg_precision   -> Precision_fmt_EBB (Arg_precision, fmt)

(* Create a padprec_fmt_ebb from a padding, a precision and a format. *)
(* Copy the padding and the precision to disjoin type parameters of arguments
   and result. *)
let make_padprec_fmt_ebb : type x y z t .
    (x, y) padding -> (z, t) precision ->
    (_, _, _, _, _, _) fmt ->
    (_, _, _, _, _) padprec_fmt_ebb =
fun pad prec fmt ->
  let Precision_fmt_EBB (prec, fmt') = make_precision_fmt_ebb prec fmt in
  match pad with
  | No_padding         -> Padprec_fmt_EBB (No_padding, prec, fmt')
  | Lit_padding (s, w) -> Padprec_fmt_EBB (Lit_padding (s, w), prec, fmt')
  | Arg_padding s      -> Padprec_fmt_EBB (Arg_padding s, prec, fmt')

(******************************************************************************)
                             (* Format parsing *)

(* Parse a string representing a format and create a fmt_ebb. *)
(* Raise [Failure] in case of invalid format. *)
let fmt_ebb_of_string ?legacy_behavior str =
  (* Parameters naming convention:                                    *)
  (*   - lit_start: start of the literal sequence.                    *)
  (*   - str_ind: current index in the string.                        *)
  (*   - end_ind: end of the current (sub-)format.                    *)
  (*   - pct_ind: index of the '%' in the current micro-format.       *)
  (*   - zero:  is the '0' flag defined in the current micro-format.  *)
  (*   - minus: is the '-' flag defined in the current micro-format.  *)
  (*   - plus:  is the '+' flag defined in the current micro-format.  *)
  (*   - hash:  is the '#' flag defined in the current micro-format.  *)
  (*   - space: is the ' ' flag defined in the current micro-format.  *)
  (*   - ign:   is the '_' flag defined in the current micro-format.  *)
  (*   - pad: padding of the current micro-format.                    *)
  (*   - prec: precision of the current micro-format.                 *)
  (*   - symb: char representing the conversion ('c', 's', 'd', ...). *)
  (*   - char_set: set of characters as bitmap (see scanf %[...]).    *)

  let legacy_behavior = match legacy_behavior with
    | Some flag -> flag
    | None -> true
  (*  When this flag is enabled, the format parser tries to behave as
      the <4.02 implementations, in particular it ignores most benine
      nonsensical format. When the flag is disabled, it will reject any
      format that is not accepted by the specification.

      A typical example would be "%+ d": specifying both '+' (if the
      number is positive, pad with a '+' to get the same width as
      negative numbers) and ' ' (if the number is positive, pad with
      a space) does not make sense, but the legacy (< 4.02)
      implementation was happy to just ignore the space.
  *)
  in

  (* Raise [Failure] with a friendly error message. *)
  let invalid_format_message str_ind msg =
    failwith_message
      "invalid format %S: at character number %d, %s"
      str str_ind msg
  in

  (* Used when the end of the format (or the current sub-format) was encountered
      unexpectedly. *)
  let unexpected_end_of_format end_ind =
    invalid_format_message end_ind
      "unexpected end of format"
  in

  (* Used for %0c: no other widths are implemented *)
  let invalid_nonnull_char_width str_ind =
    invalid_format_message str_ind
      "non-zero widths are unsupported for %c conversions"
  in
  (* Raise [Failure] with a friendly error message about an option dependency
     problem. *)
  let invalid_format_without str_ind c s =
    failwith_message
      "invalid format %S: at character number %d, '%c' without %s"
      str str_ind c s
  in

  (* Raise [Failure] with a friendly error message about an unexpected
     character. *)
  let expected_character str_ind expected read =
    failwith_message
     "invalid format %S: at character number %d, %s expected, read %C"
      str str_ind expected read
  in

  (* Parse the string from beg_ind (included) to end_ind (excluded). *)
  let rec parse : type e f . int -> int -> (_, _, e, f) fmt_ebb =
  fun beg_ind end_ind -> parse_literal beg_ind beg_ind end_ind

  (* Read literal characters up to '%' or '@' special characters. *)
  and parse_literal : type e f . int -> int -> int -> (_, _, e, f) fmt_ebb =
  fun lit_start str_ind end_ind ->
    if str_ind = end_ind then add_literal lit_start str_ind End_of_format else
      match str.[str_ind] with
      | '%' ->
        let Fmt_EBB fmt_rest = parse_format str_ind end_ind in
        add_literal lit_start str_ind fmt_rest
      | '@' ->
        let Fmt_EBB fmt_rest = parse_after_at (str_ind + 1) end_ind in
        add_literal lit_start str_ind fmt_rest
      | _ ->
        parse_literal lit_start (str_ind + 1) end_ind

  (* Parse a format after '%' *)
  and parse_format : type e f . int -> int -> (_, _, e, f) fmt_ebb =
  fun pct_ind end_ind -> parse_ign pct_ind (pct_ind + 1) end_ind

  and parse_ign : type e f . int -> int -> int -> (_, _, e, f) fmt_ebb =
  fun pct_ind str_ind end_ind ->
    if str_ind = end_ind then unexpected_end_of_format end_ind;
    match str.[str_ind] with
      | '_' -> parse_flags pct_ind (str_ind+1) end_ind true
      | _ -> parse_flags pct_ind str_ind end_ind false

  and parse_flags : type e f . int -> int -> int -> bool -> (_, _, e, f) fmt_ebb
  =
  fun pct_ind str_ind end_ind ign ->
    let zero = ref false and minus = ref false
    and plus = ref false and space = ref false
    and hash = ref false in
    let set_flag str_ind flag =
      (* in legacy mode, duplicate flags are accepted *)
      if !flag && not legacy_behavior then
        failwith_message
          "invalid format %S: at character number %d, duplicate flag %C"
          str str_ind str.[str_ind];
      flag := true;
    in
    let rec read_flags str_ind =
      if str_ind = end_ind then unexpected_end_of_format end_ind;
      begin match str.[str_ind] with
      | '0' -> set_flag str_ind zero;  read_flags (str_ind + 1)
      | '-' -> set_flag str_ind minus; read_flags (str_ind + 1)
      | '+' -> set_flag str_ind plus;  read_flags (str_ind + 1)
      | '#' -> set_flag str_ind hash; read_flags (str_ind + 1)
      | ' ' -> set_flag str_ind space; read_flags (str_ind + 1)
      | _ ->
        parse_padding pct_ind str_ind end_ind
          !zero !minus !plus !hash !space ign
      end
    in
    read_flags str_ind

  (* Try to read a digital or a '*' padding. *)
  and parse_padding : type e f .
      int -> int -> int -> bool -> bool -> bool -> bool -> bool -> bool ->
        (_, _, e, f) fmt_ebb =
  fun pct_ind str_ind end_ind zero minus plus hash space ign ->
    if str_ind = end_ind then unexpected_end_of_format end_ind;
    let padty = match zero, minus with
      | false, false -> Right
      | false, true  -> Left
      |  true, false -> Zeros
      |  true, true  ->
        if legacy_behavior then Left
        else incompatible_flag pct_ind str_ind '-' "0" in
    match str.[str_ind] with
    | '0' .. '9' ->
      let new_ind, width = parse_positive str_ind end_ind 0 in
      parse_after_padding pct_ind new_ind end_ind minus plus hash space ign
        (Lit_padding (padty, width))
    | '*' ->
      parse_after_padding pct_ind (str_ind + 1) end_ind minus plus hash space
        ign (Arg_padding padty)
    | _ ->
      begin match padty with
      | Left  ->
        if not legacy_behavior then
          invalid_format_without (str_ind - 1) '-' "padding";
        parse_after_padding pct_ind str_ind end_ind minus plus hash space ign
          No_padding
      | Zeros ->
         (* a '0' padding indication not followed by anything should
           be interpreted as a Right padding of width 0. This is used
           by scanning conversions %0s and %0c *)
        parse_after_padding pct_ind str_ind end_ind minus plus hash space ign
          (Lit_padding (Right, 0))
      | Right ->
        parse_after_padding pct_ind str_ind end_ind minus plus hash space ign
          No_padding
      end

  (* Is precision defined? *)
  and parse_after_padding : type x e f .
      int -> int -> int -> bool -> bool -> bool -> bool -> bool ->
        (x, _) padding -> (_, _, e, f) fmt_ebb =
  fun pct_ind str_ind end_ind minus plus hash space ign pad ->
    if str_ind = end_ind then unexpected_end_of_format end_ind;
    match str.[str_ind] with
    | '.' ->
      parse_precision pct_ind (str_ind + 1) end_ind minus plus hash space ign
        pad
    | symb ->
      parse_conversion pct_ind (str_ind + 1) end_ind plus hash space ign pad
        No_precision pad symb

  (* Read the digital or '*' precision. *)
  and parse_precision : type x e f .
      int -> int -> int -> bool -> bool -> bool -> bool -> bool ->
        (x, _) padding -> (_, _, e, f) fmt_ebb =
  fun pct_ind str_ind end_ind minus plus hash space ign pad ->
    if str_ind = end_ind then unexpected_end_of_format end_ind;
    let parse_literal minus str_ind =
      let new_ind, prec = parse_positive str_ind end_ind 0 in
      parse_after_precision pct_ind new_ind end_ind minus plus hash space ign
        pad (Lit_precision prec) in
    match str.[str_ind] with
    | '0' .. '9' -> parse_literal minus str_ind
    | ('+' | '-') as symb when legacy_behavior ->
      (* Legacy mode would accept and ignore '+' or '-' before the
         integer describing the desired precision; note that this
         cannot happen for padding width, as '+' and '-' already have
         a semantics there.

         That said, the idea (supported by this tweak) that width and
         precision literals are "integer literals" in the OCaml sense is
         still blatantly wrong, as 123_456 or 0xFF are rejected. *)
      parse_literal (minus || symb = '-') (str_ind + 1)
    | '*' ->
      parse_after_precision pct_ind (str_ind + 1) end_ind minus plus hash space
        ign pad Arg_precision
    | _ ->
      if legacy_behavior then
        (* note that legacy implementation did not ignore '.' without
           a number (as it does for padding indications), but
           interprets it as '.0' *)
        parse_after_precision pct_ind str_ind end_ind minus plus hash space ign
          pad (Lit_precision 0)
      else
        invalid_format_without (str_ind - 1) '.' "precision"

  (* Try to read the conversion. *)
  and parse_after_precision : type x y z t e f .
      int -> int -> int -> bool -> bool -> bool -> bool -> bool ->
        (x, y) padding -> (z, t) precision -> (_, _, e, f) fmt_ebb =
  fun pct_ind str_ind end_ind minus plus hash space ign pad prec ->
    if str_ind = end_ind then unexpected_end_of_format end_ind;
    let parse_conv (type u) (type v) (padprec : (u, v) padding) =
      parse_conversion pct_ind (str_ind + 1) end_ind plus hash space ign pad
        prec padprec str.[str_ind] in
    (* in legacy mode, some formats (%s and %S) accept a weird mix of
       padding and precision, which is merged as a single padding
       information. For example, in %.10s the precision is implicitly
       understood as padding %10s, but the left-padding component may
       be specified either as a left padding or a negative precision:
       %-.3s and %.-3s are equivalent to %-3s *)
    match pad with
    | No_padding -> (
      match minus, prec with
        | _, No_precision -> parse_conv No_padding
        | false, Lit_precision n -> parse_conv (Lit_padding (Right, n))
        | true, Lit_precision n -> parse_conv (Lit_padding (Left, n))
        | false, Arg_precision -> parse_conv (Arg_padding Right)
        | true, Arg_precision -> parse_conv (Arg_padding Left)
    )
    | pad -> parse_conv pad

  (* Case analysis on conversion. *)
  and parse_conversion : type x y z t u v e f .
      int -> int -> int -> bool -> bool -> bool -> bool -> (x, y) padding ->
        (z, t) precision -> (u, v) padding -> char -> (_, _, e, f) fmt_ebb =
  fun pct_ind str_ind end_ind plus hash space ign pad prec padprec symb ->
    (* Flags used to check option usages/compatibilities. *)
    let plus_used  = ref false and hash_used = ref false
    and space_used = ref false and ign_used   = ref false
    and pad_used   = ref false and prec_used  = ref false in

    (* Access to options, update flags. *)
    let get_plus    () = plus_used  := true; plus
    and get_hash   () = hash_used := true; hash
    and get_space   () = space_used := true; space
    and get_ign     () = ign_used   := true; ign
    and get_pad     () = pad_used   := true; pad
    and get_prec    () = prec_used  := true; prec
    and get_padprec () = pad_used   := true; padprec in

    let get_int_pad () =
      (* %5.3d is accepted and meaningful: pad to length 5 with
         spaces, but first pad with zeros upto length 3 (0-padding
         is the interpretation of "precision" for integer formats).

         %05.3d is redundant: pad to length 5 *with zeros*, but
         first pad with zeros... To add insult to the injury, the
         legacy implementation ignores the 0-padding indication and
         does the 5 padding with spaces instead. We reuse this
         interpretation for compatibility, but statically reject this
         format when the legacy mode is disabled, to protect strict
         users from this corner case. *)
       match get_pad (), get_prec () with
         | pad, No_precision -> pad
         | No_padding, _     -> No_padding
         | Lit_padding (Zeros, n), _ ->
           if legacy_behavior then Lit_padding (Right, n)
           else incompatible_flag pct_ind str_ind '0' "precision"
         | Arg_padding Zeros, _ ->
           if legacy_behavior then Arg_padding Right
           else incompatible_flag pct_ind str_ind '0' "precision"
         | Lit_padding _ as pad, _ -> pad
         | Arg_padding _ as pad, _ -> pad in

    (* Check that padty <> Zeros. *)
    let check_no_0 symb (type a) (type b) (pad : (a, b) padding) =
      match pad with
      | No_padding -> pad
      | Lit_padding ((Left | Right), _) -> pad
      | Arg_padding (Left | Right) -> pad
      | Lit_padding (Zeros, width) ->
        if legacy_behavior then Lit_padding (Right, width)
        else incompatible_flag pct_ind str_ind symb "0"
      | Arg_padding Zeros ->
        if legacy_behavior then Arg_padding Right
        else incompatible_flag pct_ind str_ind symb "0"
    in

    (* Get padding as a pad_option (see "%_", "%{", "%(" and "%[").
       (no need for legacy mode tweaking, those were rejected by the
       legacy parser as well) *)
    let opt_of_pad c (type a) (type b) (pad : (a, b) padding) = match pad with
      | No_padding -> None
      | Lit_padding (Right, width) -> Some width
      | Lit_padding (Zeros, width) ->
        if legacy_behavior then Some width
        else incompatible_flag pct_ind str_ind c "'0'"
      | Lit_padding (Left, width) ->
        if legacy_behavior then Some width
        else incompatible_flag pct_ind str_ind c "'-'"
      | Arg_padding _ -> incompatible_flag pct_ind str_ind c "'*'"
    in
    let get_pad_opt c = opt_of_pad c (get_pad ()) in
    let get_padprec_opt c = opt_of_pad c (get_padprec ()) in

    (* Get precision as a prec_option (see "%_f").
       (no need for legacy mode tweaking, those were rejected by the
       legacy parser as well) *)
    let get_prec_opt () = match get_prec () with
      | No_precision       -> None
      | Lit_precision ndec -> Some ndec
      | Arg_precision      -> incompatible_flag pct_ind str_ind '_' "'*'"
    in

    let fmt_result = match symb with
    | ',' ->
      parse str_ind end_ind
    | 'c' ->
      let char_format fmt_rest = (* %c *)
        if get_ign ()
        then Fmt_EBB (Ignored_param (Ignored_char, fmt_rest))
        else Fmt_EBB (Char fmt_rest)
      in
      let scan_format fmt_rest = (* %0c *)
        if get_ign ()
        then Fmt_EBB (Ignored_param (Ignored_scan_next_char, fmt_rest))
        else Fmt_EBB (Scan_next_char fmt_rest)
      in
      let Fmt_EBB fmt_rest = parse str_ind end_ind in
      begin match get_pad_opt 'c' with
        | None -> char_format fmt_rest
        | Some 0 -> scan_format fmt_rest
        | Some _n ->
           if not legacy_behavior
           then invalid_nonnull_char_width str_ind
           else (* legacy ignores %c widths *) char_format fmt_rest
      end
    | 'C' ->
      let Fmt_EBB fmt_rest = parse str_ind end_ind in
      if get_ign () then Fmt_EBB (Ignored_param (Ignored_caml_char,fmt_rest))
      else Fmt_EBB (Caml_char fmt_rest)
    | 's' ->
      let pad = check_no_0 symb (get_padprec ()) in
      let Fmt_EBB fmt_rest = parse str_ind end_ind in
      if get_ign () then
        let ignored = Ignored_string (get_padprec_opt '_') in
        Fmt_EBB (Ignored_param (ignored, fmt_rest))
      else
        let Padding_fmt_EBB (pad', fmt_rest') =
          make_padding_fmt_ebb pad fmt_rest in
        Fmt_EBB (String (pad', fmt_rest'))
    | 'S' ->
      let pad = check_no_0 symb (get_padprec ()) in
      let Fmt_EBB fmt_rest = parse str_ind end_ind in
      if get_ign () then
        let ignored = Ignored_caml_string (get_padprec_opt '_') in
        Fmt_EBB (Ignored_param (ignored, fmt_rest))
      else
        let Padding_fmt_EBB (pad', fmt_rest') =
          make_padding_fmt_ebb pad fmt_rest in
        Fmt_EBB (Caml_string (pad', fmt_rest'))
    | 'd' | 'i' | 'x' | 'X' | 'o' | 'u' ->
      let iconv = compute_int_conv pct_ind str_ind (get_plus ()) (get_hash ())
        (get_space ()) symb in
      let Fmt_EBB fmt_rest = parse str_ind end_ind in
      if get_ign () then
        let ignored = Ignored_int (iconv, get_pad_opt '_') in
        Fmt_EBB (Ignored_param (ignored, fmt_rest))
      else
        let Padprec_fmt_EBB (pad', prec', fmt_rest') =
          make_padprec_fmt_ebb (get_int_pad ()) (get_prec ()) fmt_rest in
        Fmt_EBB (Int (iconv, pad', prec', fmt_rest'))
    | 'N' ->
      let Fmt_EBB fmt_rest = parse str_ind end_ind in
      let counter = Token_counter in
      if get_ign () then
        let ignored = Ignored_scan_get_counter counter in
        Fmt_EBB (Ignored_param (ignored, fmt_rest))
      else
        Fmt_EBB (Scan_get_counter (counter, fmt_rest))
    | 'l' | 'n' | 'L' when str_ind=end_ind || not (is_int_base str.[str_ind]) ->
      let Fmt_EBB fmt_rest = parse str_ind end_ind in
      let counter = counter_of_char symb in
      if get_ign () then
        let ignored = Ignored_scan_get_counter counter in
        Fmt_EBB (Ignored_param (ignored, fmt_rest))
      else
        Fmt_EBB (Scan_get_counter (counter, fmt_rest))
    | 'l' ->
      let iconv =
        compute_int_conv pct_ind (str_ind + 1) (get_plus ()) (get_hash ())
          (get_space ()) str.[str_ind] in
      let Fmt_EBB fmt_rest = parse (str_ind + 1) end_ind in
      if get_ign () then
        let ignored = Ignored_int32 (iconv, get_pad_opt '_') in
        Fmt_EBB (Ignored_param (ignored, fmt_rest))
      else
        let Padprec_fmt_EBB (pad', prec', fmt_rest') =
          make_padprec_fmt_ebb (get_int_pad ()) (get_prec ()) fmt_rest in
        Fmt_EBB (Int32 (iconv, pad', prec', fmt_rest'))
    | 'n' ->
      let iconv =
        compute_int_conv pct_ind (str_ind + 1) (get_plus ())
          (get_hash ()) (get_space ()) str.[str_ind] in
      let Fmt_EBB fmt_rest = parse (str_ind + 1) end_ind in
      if get_ign () then
        let ignored = Ignored_nativeint (iconv, get_pad_opt '_') in
        Fmt_EBB (Ignored_param (ignored, fmt_rest))
      else
        let Padprec_fmt_EBB (pad', prec', fmt_rest') =
          make_padprec_fmt_ebb (get_int_pad ()) (get_prec ()) fmt_rest in
        Fmt_EBB (Nativeint (iconv, pad', prec', fmt_rest'))
    | 'L' ->
      let iconv =
        compute_int_conv pct_ind (str_ind + 1) (get_plus ()) (get_hash ())
          (get_space ()) str.[str_ind] in
      let Fmt_EBB fmt_rest = parse (str_ind + 1) end_ind in
      if get_ign () then
        let ignored = Ignored_int64 (iconv, get_pad_opt '_') in
        Fmt_EBB (Ignored_param (ignored, fmt_rest))
      else
        let Padprec_fmt_EBB (pad', prec', fmt_rest') =
          make_padprec_fmt_ebb (get_int_pad ()) (get_prec ()) fmt_rest in
        Fmt_EBB (Int64 (iconv, pad', prec', fmt_rest'))
    | 'f' | 'e' | 'E' | 'g' | 'G' | 'F' | 'h' | 'H' ->
      let fconv = compute_float_conv pct_ind str_ind (get_plus ())
        (get_space ()) symb in
      let Fmt_EBB fmt_rest = parse str_ind end_ind in
      if get_ign () then
        let ignored = Ignored_float (get_pad_opt '_', get_prec_opt ()) in
        Fmt_EBB (Ignored_param (ignored, fmt_rest))
      else
        let Padprec_fmt_EBB (pad', prec', fmt_rest') =
          make_padprec_fmt_ebb (get_pad ()) (get_prec ()) fmt_rest in
        Fmt_EBB (Float (fconv, pad', prec', fmt_rest'))
    | 'b' | 'B' ->
      let pad = check_no_0 symb (get_padprec ()) in
      let Fmt_EBB fmt_rest = parse str_ind end_ind in
      if get_ign () then
        let ignored = Ignored_bool (get_padprec_opt '_') in
        Fmt_EBB (Ignored_param (ignored, fmt_rest))
      else
        let Padding_fmt_EBB (pad', fmt_rest') =
          make_padding_fmt_ebb pad fmt_rest in
        Fmt_EBB (Bool (pad', fmt_rest'))
    | 'a' ->
      let Fmt_EBB fmt_rest = parse str_ind end_ind in
      Fmt_EBB (Alpha fmt_rest)
    | 't' ->
      let Fmt_EBB fmt_rest = parse str_ind end_ind in
      Fmt_EBB (Theta fmt_rest)
    | 'r' ->
      let Fmt_EBB fmt_rest = parse str_ind end_ind in
      if get_ign () then Fmt_EBB (Ignored_param (Ignored_reader, fmt_rest))
      else Fmt_EBB (Reader fmt_rest)
    | '!' ->
      let Fmt_EBB fmt_rest = parse str_ind end_ind in
      Fmt_EBB (Flush fmt_rest)
    | ('%' | '@') as c ->
      let Fmt_EBB fmt_rest = parse str_ind end_ind in
      Fmt_EBB (Char_literal (c, fmt_rest))
    | '{' ->
      let sub_end = search_subformat_end str_ind end_ind '}' in
      let Fmt_EBB sub_fmt = parse str_ind sub_end in
      let Fmt_EBB fmt_rest = parse (sub_end + 2) end_ind in
      let sub_fmtty = fmtty_of_fmt sub_fmt in
      if get_ign () then
        let ignored = Ignored_format_arg (get_pad_opt '_', sub_fmtty) in
        Fmt_EBB (Ignored_param (ignored, fmt_rest))
      else
        Fmt_EBB (Format_arg (get_pad_opt '{', sub_fmtty, fmt_rest))
    | '(' ->
      let sub_end = search_subformat_end str_ind end_ind ')' in
      let Fmt_EBB fmt_rest = parse (sub_end + 2) end_ind in
      let Fmt_EBB sub_fmt = parse str_ind sub_end in
      let sub_fmtty = fmtty_of_fmt sub_fmt in
      if get_ign () then
        let ignored = Ignored_format_subst (get_pad_opt '_', sub_fmtty) in
        Fmt_EBB (Ignored_param (ignored, fmt_rest))
      else
        Fmt_EBB (Format_subst (get_pad_opt '(', sub_fmtty, fmt_rest))
    | '[' ->
      let next_ind, char_set = parse_char_set str_ind end_ind in
      let Fmt_EBB fmt_rest = parse next_ind end_ind in
      if get_ign () then
        let ignored = Ignored_scan_char_set (get_pad_opt '_', char_set) in
        Fmt_EBB (Ignored_param (ignored, fmt_rest))
      else
        Fmt_EBB (Scan_char_set (get_pad_opt '[', char_set, fmt_rest))
    | '-' | '+' | '#' | ' ' | '_' ->
      failwith_message
        "invalid format %S: at character number %d, \
         flag %C is only allowed after the '%%', before padding and precision"
        str pct_ind symb
    | _ ->
      failwith_message
        "invalid format %S: at character number %d, \
         invalid conversion \"%%%c\"" str (str_ind - 1) symb
    in
    (* Check for unused options, and reject them as incompatible.

       Such checks need to be disabled in legacy mode, as the legacy
       parser silently ignored incompatible flags. *)
    if not legacy_behavior then begin
    if not !plus_used && plus then
      incompatible_flag pct_ind str_ind symb "'+'";
    if not !hash_used && hash then
      incompatible_flag pct_ind str_ind symb "'#'";
    if not !space_used && space then
      incompatible_flag pct_ind str_ind symb "' '";
    if not !pad_used  && Padding_EBB pad <> Padding_EBB No_padding then
      incompatible_flag pct_ind str_ind symb "`padding'";
    if not !prec_used && Precision_EBB prec <> Precision_EBB No_precision then
      incompatible_flag pct_ind str_ind (if ign then '_' else symb)
        "`precision'";
    if ign && plus then incompatible_flag pct_ind str_ind '_' "'+'";
    end;
    (* this last test must not be disabled in legacy mode,
       as ignoring it would typically result in a different typing
       than what the legacy parser used *)
    if not !ign_used && ign then
      begin match symb with
        (* argument-less formats can safely be ignored in legacy mode *)
        | ('@' | '%' | '!' | ',') when legacy_behavior -> ()
        | _ ->
          incompatible_flag pct_ind str_ind symb "'_'"
      end;
    fmt_result

  (* Parse formatting informations (after '@'). *)
  and parse_after_at : type e f . int -> int -> (_, _, e, f) fmt_ebb =
  fun str_ind end_ind ->
    if str_ind = end_ind then Fmt_EBB (Char_literal ('@', End_of_format))
    else
      match str.[str_ind] with
      | '[' ->
        parse_tag false (str_ind + 1) end_ind
      | ']' ->
        let Fmt_EBB fmt_rest = parse (str_ind + 1) end_ind in
        Fmt_EBB (Formatting_lit (Close_box, fmt_rest))
      | '{' ->
        parse_tag true (str_ind + 1) end_ind
      | '}' ->
        let Fmt_EBB fmt_rest = parse (str_ind + 1) end_ind in
        Fmt_EBB (Formatting_lit (Close_tag, fmt_rest))
      | ',' ->
        let Fmt_EBB fmt_rest = parse (str_ind + 1) end_ind in
        Fmt_EBB (Formatting_lit (Break ("@,", 0, 0), fmt_rest))
      | ' ' ->
        let Fmt_EBB fmt_rest = parse (str_ind + 1) end_ind in
        Fmt_EBB (Formatting_lit (Break ("@ ", 1, 0), fmt_rest))
      | ';' ->
        parse_good_break (str_ind + 1) end_ind
      | '?' ->
        let Fmt_EBB fmt_rest = parse (str_ind + 1) end_ind in
        Fmt_EBB (Formatting_lit (FFlush, fmt_rest))
      | '\n' ->
        let Fmt_EBB fmt_rest = parse (str_ind + 1) end_ind in
        Fmt_EBB (Formatting_lit (Force_newline, fmt_rest))
      | '.' ->
        let Fmt_EBB fmt_rest = parse (str_ind + 1) end_ind in
        Fmt_EBB (Formatting_lit (Flush_newline, fmt_rest))
      | '<' ->
        parse_magic_size (str_ind + 1) end_ind
      | '@' ->
        let Fmt_EBB fmt_rest = parse (str_ind + 1) end_ind in
        Fmt_EBB (Formatting_lit (Escaped_at, fmt_rest))
      | '%' when str_ind + 1 < end_ind && str.[str_ind + 1] = '%' ->
        let Fmt_EBB fmt_rest = parse (str_ind + 2) end_ind in
        Fmt_EBB (Formatting_lit (Escaped_percent, fmt_rest))
      | '%' ->
        let Fmt_EBB fmt_rest = parse str_ind end_ind in
        Fmt_EBB (Char_literal ('@', fmt_rest))
      | c ->
        let Fmt_EBB fmt_rest = parse (str_ind + 1) end_ind in
        Fmt_EBB (Formatting_lit (Scan_indic c, fmt_rest))

  and check_open_box : type a b c d e f . (a, b, c, d, e, f) fmt -> unit =
  fun fmt -> match fmt with
    | String_literal (str, End_of_format) -> (
      try ignore (open_box_of_string str) with Failure _ ->
        ((* Emit warning: invalid open box *))
    )
    | _ -> ()

  (* Try to read the optional <name> after "@{" or "@[". *)
  and parse_tag : type e f . bool -> int -> int -> (_, _, e, f) fmt_ebb =
  fun is_open_tag str_ind end_ind ->
    try
      if str_ind = end_ind then raise Not_found;
      match str.[str_ind] with
      | '<' ->
        let ind = String.index_from str (str_ind + 1) '>' in
        if ind >= end_ind then raise Not_found;
        let sub_str = String.sub str str_ind (ind - str_ind + 1) in
        let Fmt_EBB fmt_rest = parse (ind + 1) end_ind in
        let Fmt_EBB sub_fmt = parse str_ind (ind + 1) in
        let sub_format = Format (sub_fmt, sub_str) in
        let formatting = if is_open_tag then Open_tag sub_format else (
          check_open_box sub_fmt;
          Open_box sub_format) in
        Fmt_EBB (Formatting_gen (formatting, fmt_rest))
      | _ ->
        raise Not_found
    with Not_found ->
      let Fmt_EBB fmt_rest = parse str_ind end_ind in
      let sub_format = Format (End_of_format, "") in
      let formatting =
        if is_open_tag then Open_tag sub_format else Open_box sub_format in
      Fmt_EBB (Formatting_gen (formatting, fmt_rest))

  (* Try to read the optional <width offset> after "@;". *)
  and parse_good_break : type e f . int -> int -> (_, _, e, f) fmt_ebb =
  fun str_ind end_ind ->
    let next_ind, formatting_lit =
      try
        if str_ind = end_ind || str.[str_ind] <> '<' then raise Not_found;
        let str_ind_1 = parse_spaces (str_ind + 1) end_ind in
        match str.[str_ind_1] with
        | '0' .. '9' | '-' -> (
          let str_ind_2, width = parse_integer str_ind_1 end_ind in
            let str_ind_3 = parse_spaces str_ind_2 end_ind in
            match str.[str_ind_3] with
              | '>' ->
                let s = String.sub str (str_ind-2) (str_ind_3-str_ind+3) in
                str_ind_3 + 1, Break (s, width, 0)
              | '0' .. '9' | '-' ->
                let str_ind_4, offset = parse_integer str_ind_3 end_ind in
                let str_ind_5 = parse_spaces str_ind_4 end_ind in
                if str.[str_ind_5] <> '>' then raise Not_found;
                let s = String.sub str (str_ind-2) (str_ind_5-str_ind+3) in
                str_ind_5 + 1, Break (s, width, offset)
              | _ -> raise Not_found
        )
        | _ -> raise Not_found
      with Not_found | Failure _ ->
        str_ind, Break ("@;", 1, 0)
    in
    let Fmt_EBB fmt_rest = parse next_ind end_ind in
    Fmt_EBB (Formatting_lit (formatting_lit, fmt_rest))

  (* Parse the size in a <n>. *)
  and parse_magic_size : type e f . int -> int -> (_, _, e, f) fmt_ebb =
  fun str_ind end_ind ->
    match
      try
        let str_ind_1 = parse_spaces str_ind end_ind in
        match str.[str_ind_1] with
        | '0' .. '9' | '-' ->
          let str_ind_2, size = parse_integer str_ind_1 end_ind in
          let str_ind_3 = parse_spaces str_ind_2 end_ind in
          if str.[str_ind_3] <> '>' then raise Not_found;
          let s = String.sub str (str_ind - 2) (str_ind_3 - str_ind + 3) in
          Some (str_ind_3 + 1, Magic_size (s, size))
        | _ -> None
      with Not_found | Failure _ ->
        None
    with
    | Some (next_ind, formatting_lit) ->
      let Fmt_EBB fmt_rest = parse next_ind end_ind in
      Fmt_EBB (Formatting_lit (formatting_lit, fmt_rest))
    | None ->
      let Fmt_EBB fmt_rest = parse str_ind end_ind in
      Fmt_EBB (Formatting_lit (Scan_indic '<', fmt_rest))

  (* Parse and construct a char set. *)
  and parse_char_set str_ind end_ind =
    if str_ind = end_ind then unexpected_end_of_format end_ind;

    let char_set = create_char_set () in
    let add_char c =
      add_in_char_set char_set c;
    in
    let add_range c c' =
      for i = int_of_char c to int_of_char c' do
        add_in_char_set char_set (char_of_int i);
      done;
    in

    let fail_single_percent str_ind =
      failwith_message
        "invalid format %S: '%%' alone is not accepted in character sets, \
         use %%%% instead at position %d." str str_ind
    in

    (* Parse the first character of a char set. *)
    let rec parse_char_set_start str_ind end_ind =
      if str_ind = end_ind then unexpected_end_of_format end_ind;
      let c = str.[str_ind] in
      parse_char_set_after_char (str_ind + 1) end_ind c

    (* Parse the content of a char set until the first ']'. *)
    and parse_char_set_content str_ind end_ind =
      if str_ind = end_ind then unexpected_end_of_format end_ind;
      match str.[str_ind] with
      | ']' ->
        str_ind + 1
      | '-' ->
        add_char '-';
        parse_char_set_content (str_ind + 1) end_ind
      | c ->
        parse_char_set_after_char (str_ind + 1) end_ind c

    (* Test for range in char set. *)
    and parse_char_set_after_char str_ind end_ind c =
      if str_ind = end_ind then unexpected_end_of_format end_ind;
      match str.[str_ind] with
      | ']' ->
        add_char c;
        str_ind + 1
      | '-' ->
        parse_char_set_after_minus (str_ind + 1) end_ind c
      | ('%' | '@') as c' when c = '%' ->
        add_char c';
        parse_char_set_content (str_ind + 1) end_ind
      | c' ->
        if c = '%' then fail_single_percent str_ind;
        (* note that '@' alone is accepted, as done by the legacy
           implementation; the documentation specifically requires %@
           so we could warn on that *)
        add_char c;
        parse_char_set_after_char (str_ind + 1) end_ind c'

    (* Manage range in char set (except if the '-' the last char before ']') *)
    and parse_char_set_after_minus str_ind end_ind c =
      if str_ind = end_ind then unexpected_end_of_format end_ind;
      match str.[str_ind] with
      | ']' ->
        add_char c;
        add_char '-';
        str_ind + 1
      | '%' ->
        if str_ind + 1 = end_ind then unexpected_end_of_format end_ind;
        begin match str.[str_ind + 1] with
          | ('%' | '@') as c' ->
            add_range c c';
            parse_char_set_content (str_ind + 2) end_ind
          | _ -> fail_single_percent str_ind
        end
      | c' ->
        add_range c c';
        parse_char_set_content (str_ind + 1) end_ind
    in
    let str_ind, reverse =
      if str_ind = end_ind then unexpected_end_of_format end_ind;
      match str.[str_ind] with
        | '^' -> str_ind + 1, true
        | _ -> str_ind, false in
    let next_ind = parse_char_set_start str_ind end_ind in
    let char_set = freeze_char_set char_set in
    next_ind, (if reverse then rev_char_set char_set else char_set)

  (* Consume all next spaces, raise an Failure if end_ind is reached. *)
  and parse_spaces str_ind end_ind =
    if str_ind = end_ind then unexpected_end_of_format end_ind;
    if str.[str_ind] = ' ' then parse_spaces (str_ind + 1) end_ind else str_ind

  (* Read a positive integer from the string, raise a Failure if end_ind is
     reached. *)
  and parse_positive str_ind end_ind acc =
    if str_ind = end_ind then unexpected_end_of_format end_ind;
    match str.[str_ind] with
    | '0' .. '9' as c ->
      let new_acc = acc * 10 + (int_of_char c - int_of_char '0') in
      if new_acc > Sys.max_string_length then
        failwith_message
          "invalid format %S: integer %d is greater than the limit %d"
          str new_acc Sys.max_string_length
      else
        parse_positive (str_ind + 1) end_ind new_acc
    | _ -> str_ind, acc

  (* Read a positive or negative integer from the string, raise a Failure
     if end_ind is reached. *)
  and parse_integer str_ind end_ind =
    if str_ind = end_ind then unexpected_end_of_format end_ind;
    match str.[str_ind] with
    | '0' .. '9' -> parse_positive str_ind end_ind 0
    | '-' -> (
      if str_ind + 1 = end_ind then unexpected_end_of_format end_ind;
      match str.[str_ind + 1] with
      | '0' .. '9' ->
        let next_ind, n = parse_positive (str_ind + 1) end_ind 0 in
        next_ind, -n
      | c ->
        expected_character (str_ind + 1) "digit" c
    )
    | _ -> assert false

  (* Add a literal to a format from a literal character sub-sequence. *)
  and add_literal : type a d e f .
      int -> int -> (a, _, _, d, e, f) fmt ->
      (_, _, e, f) fmt_ebb =
  fun lit_start str_ind fmt -> match str_ind - lit_start with
    | 0    -> Fmt_EBB fmt
    | 1    -> Fmt_EBB (Char_literal (str.[lit_start], fmt))
    | size -> Fmt_EBB (String_literal (String.sub str lit_start size, fmt))

  (* Search the end of the current sub-format
     (i.e. the corresponding "%}" or "%)") *)
  and search_subformat_end str_ind end_ind c =
    if str_ind = end_ind then
      failwith_message
        "invalid format %S: unclosed sub-format, \
         expected \"%%%c\" at character number %d" str c end_ind;
    match str.[str_ind] with
    | '%' ->
      if str_ind + 1 = end_ind then unexpected_end_of_format end_ind;
      if str.[str_ind + 1] = c then (* End of format found *) str_ind else
        begin match str.[str_ind + 1] with
        | '_' ->
          (* Search for "%_(" or "%_{". *)
          if str_ind + 2 = end_ind then unexpected_end_of_format end_ind;
          begin match str.[str_ind + 2] with
          | '{' ->
            let sub_end = search_subformat_end (str_ind + 3) end_ind '}' in
            search_subformat_end (sub_end + 2) end_ind c
          | '(' ->
            let sub_end = search_subformat_end (str_ind + 3) end_ind ')' in
            search_subformat_end (sub_end + 2) end_ind c
          | _ -> search_subformat_end (str_ind + 3) end_ind c
          end
        | '{' ->
          (* %{...%} sub-format found. *)
          let sub_end = search_subformat_end (str_ind + 2) end_ind '}' in
          search_subformat_end (sub_end + 2) end_ind c
        | '(' ->
          (* %(...%) sub-format found. *)
          let sub_end = search_subformat_end (str_ind + 2) end_ind ')' in
          search_subformat_end (sub_end + 2) end_ind c
        | '}' ->
          (* Error: %(...%}. *)
          expected_character (str_ind + 1) "character ')'" '}'
        | ')' ->
          (* Error: %{...%). *)
          expected_character (str_ind + 1) "character '}'" ')'
        | _ ->
          search_subformat_end (str_ind + 2) end_ind c
        end
    | _ -> search_subformat_end (str_ind + 1) end_ind c

  (* Check if symb is a valid int conversion after "%l", "%n" or "%L" *)
  and is_int_base symb = match symb with
    | 'd' | 'i' | 'x' | 'X' | 'o' | 'u' -> true
    | _ -> false

  (* Convert a char (l, n or L) to its associated counter. *)
  and counter_of_char symb = match symb with
    | 'l' -> Line_counter  | 'n' -> Char_counter
    | 'L' -> Token_counter | _ -> assert false

  (* Convert (plus, symb) to its associated int_conv. *)
  and compute_int_conv pct_ind str_ind plus hash space symb =
    match plus, hash, space, symb with
    | false, false, false, 'd' -> Int_d  | false, false, false, 'i' -> Int_i
    | false, false,  true, 'd' -> Int_sd | false, false,  true, 'i' -> Int_si
    |  true, false, false, 'd' -> Int_pd |  true, false, false, 'i' -> Int_pi
    | false, false, false, 'x' -> Int_x  | false, false, false, 'X' -> Int_X
    | false,  true, false, 'x' -> Int_Cx | false,  true, false, 'X' -> Int_CX
    | false, false, false, 'o' -> Int_o
    | false,  true, false, 'o' -> Int_Co
    | false, false, false, 'u' -> Int_u
    | _, true, _, 'x' when legacy_behavior -> Int_Cx
    | _, true, _, 'X' when legacy_behavior -> Int_CX
    | _, true, _, 'o' when legacy_behavior -> Int_Co
    | _, true, _, ('d' | 'i' | 'u') ->
      if legacy_behavior then (* ignore *)
        compute_int_conv pct_ind str_ind plus false space symb
      else incompatible_flag pct_ind str_ind symb "'#'"
    | true, _, true, _ ->
      if legacy_behavior then
        (* plus and space: legacy implementation prefers plus *)
        compute_int_conv pct_ind str_ind plus hash false symb
      else incompatible_flag pct_ind str_ind ' ' "'+'"
    | false, _, true, _    ->
      if legacy_behavior then (* ignore *)
        compute_int_conv pct_ind str_ind plus hash false symb
      else incompatible_flag pct_ind str_ind symb "' '"
    | true, _, false, _    ->
      if legacy_behavior then (* ignore *)
        compute_int_conv pct_ind str_ind false hash space symb
      else incompatible_flag pct_ind str_ind symb "'+'"
    | false, _, false, _ -> assert false

  (* Convert (plus, symb) to its associated float_conv. *)
  and compute_float_conv pct_ind str_ind plus space symb =
  match plus, space, symb with
    | false, false, 'f' -> Float_f  | false, false, 'e' -> Float_e
    | false,  true, 'f' -> Float_sf | false,  true, 'e' -> Float_se
    |  true, false, 'f' -> Float_pf |  true, false, 'e' -> Float_pe
    | false, false, 'E' -> Float_E  | false, false, 'g' -> Float_g
    | false,  true, 'E' -> Float_sE | false,  true, 'g' -> Float_sg
    |  true, false, 'E' -> Float_pE |  true, false, 'g' -> Float_pg
    | false, false, 'G' -> Float_G
    | false,  true, 'G' -> Float_sG
    |  true, false, 'G' -> Float_pG
    | false, false, 'h' -> Float_h
    | false,  true, 'h' -> Float_sh
    |  true, false, 'h' -> Float_ph
    | false, false, 'H' -> Float_H
    | false,  true, 'H' -> Float_sH
    |  true, false, 'H' -> Float_pH
    | false, false, 'F' -> Float_F
    |  true,  true, _ ->
      if legacy_behavior then
        (* plus and space: legacy implementation prefers plus *)
        compute_float_conv pct_ind str_ind plus false symb
      else incompatible_flag pct_ind str_ind ' ' "'+'"
    | false,  true, _ ->
      if legacy_behavior then (* ignore *)
        compute_float_conv pct_ind str_ind plus false symb
      else incompatible_flag pct_ind str_ind symb "' '"
    |  true, false, _ ->
      if legacy_behavior then (* ignore *)
        compute_float_conv pct_ind str_ind false space symb
      else incompatible_flag pct_ind str_ind symb "'+'"
    | false, false, _ -> assert false

  (* Raise [Failure] with a friendly error message about incompatible options.*)
  and incompatible_flag : type a . int -> int -> char -> string -> a =
    fun pct_ind str_ind symb option ->
      let subfmt = String.sub str pct_ind (str_ind - pct_ind) in
      failwith_message
        "invalid format %S: at character number %d, \
         %s is incompatible with '%c' in sub-format %S"
        str pct_ind option symb subfmt

  in parse 0 (String.length str)

(******************************************************************************)
                  (* Guarded string to format conversions *)

(* Convert a string to a format according to an fmtty. *)
(* Raise [Failure] with an error message in case of type mismatch. *)
let format_of_string_fmtty str fmtty =
  let Fmt_EBB fmt = fmt_ebb_of_string str in
  try Format (type_format fmt fmtty, str)
  with Type_mismatch ->
    failwith_message
      "bad input: format type mismatch between %S and %S"
      str (string_of_fmtty fmtty)

(* Convert a string to a format compatible with an other format. *)
(* Raise [Failure] with an error message in case of type mismatch. *)
let format_of_string_format str (Format (fmt', str')) =
  let Fmt_EBB fmt = fmt_ebb_of_string str in
  try Format (type_format fmt (fmtty_of_fmt fmt'), str)
  with Type_mismatch ->
    failwith_message
      "bad input: format type mismatch between %S and %S" str str'
end

module Printf = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*   Xavier Leroy and Pierre Weis, projet Cristal, INRIA Rocquencourt     *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open CamlinternalFormatBasics
open CamlinternalFormat

let kfprintf k o (Format (fmt, _)) =
  make_printf (fun o acc -> output_acc o acc; k o) o End_of_acc fmt
let kbprintf k b (Format (fmt, _)) =
  make_printf (fun b acc -> bufput_acc b acc; k b) b End_of_acc fmt
let ikfprintf k oc (Format (fmt, _)) =
  make_iprintf k oc fmt

let fprintf oc fmt = kfprintf ignore oc fmt
let bprintf b fmt = kbprintf ignore b fmt
let ifprintf oc fmt = ikfprintf ignore oc fmt
let printf fmt = fprintf stdout fmt
let eprintf fmt = fprintf stderr fmt

let ksprintf k (Format (fmt, _)) =
  let k' () acc =
    let buf = Buffer.create 64 in
    strput_acc buf acc;
    k (Buffer.contents buf) in
  make_printf k' () End_of_acc fmt

let sprintf fmt = ksprintf (fun s -> s) fmt

let kprintf = ksprintf
end

module Scanf = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Pierre Weis, projet Cristal, INRIA Rocquencourt            *)
(*                                                                        *)
(*   Copyright 2002 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open CamlinternalFormatBasics
open CamlinternalFormat

(* alias to avoid warning for ambiguity between
   Pervasives.format6
   and CamlinternalFormatBasics.format6

   (the former is in fact an alias for the latter,
    but the ambiguity warning doesn't care)
*)
type ('a, 'b, 'c, 'd, 'e, 'f) format6 =
  ('a, 'b, 'c, 'd, 'e, 'f) Pervasives.format6


(* The run-time library for scanners. *)

(* Scanning buffers. *)
module type SCANNING = sig

  type in_channel

  type scanbuf = in_channel

  type file_name = string

  val stdin : in_channel
  (* The scanning buffer reading from [Pervasives.stdin].
     [stdib] is equivalent to [Scanning.from_channel Pervasives.stdin]. *)

  val stdib : in_channel
  (* An alias for [Scanf.stdin], the scanning buffer reading from
     [Pervasives.stdin]. *)

  val next_char : scanbuf -> char
  (* [Scanning.next_char ib] advance the scanning buffer for
     one character.
     If no more character can be read, sets a end of file condition and
     returns '\000'. *)

  val invalidate_current_char : scanbuf -> unit
  (* [Scanning.invalidate_current_char ib] mark the current_char as already
     scanned. *)

  val peek_char : scanbuf -> char
  (* [Scanning.peek_char ib] returns the current char available in
     the buffer or reads one if necessary (when the current character is
     already scanned).
     If no character can be read, sets an end of file condition and
     returns '\000'. *)

  val checked_peek_char : scanbuf -> char
  (* Same as [Scanning.peek_char] above but always returns a valid char or
     fails: instead of returning a null char when the reading method of the
     input buffer has reached an end of file, the function raises exception
     [End_of_file]. *)

  val store_char : int -> scanbuf -> char -> int
  (* [Scanning.store_char lim ib c] adds [c] to the token buffer
     of the scanning buffer [ib]. It also advances the scanning buffer for
     one character and returns [lim - 1], indicating the new limit for the
     length of the current token. *)

  val skip_char : int -> scanbuf -> int
  (* [Scanning.skip_char lim ib] ignores the current character. *)

  val ignore_char : int -> scanbuf -> int
  (* [Scanning.ignore_char ib lim] ignores the current character and
     decrements the limit. *)

  val token : scanbuf -> string
  (* [Scanning.token ib] returns the string stored into the token
     buffer of the scanning buffer: it returns the token matched by the
     format. *)

  val reset_token : scanbuf -> unit
  (* [Scanning.reset_token ib] resets the token buffer of
     the given scanning buffer. *)

  val char_count : scanbuf -> int
  (* [Scanning.char_count ib] returns the number of characters
     read so far from the given buffer. *)

  val line_count : scanbuf -> int
  (* [Scanning.line_count ib] returns the number of new line
     characters read so far from the given buffer. *)

  val token_count : scanbuf -> int
  (* [Scanning.token_count ib] returns the number of tokens read
     so far from [ib]. *)

  val eof : scanbuf -> bool
  (* [Scanning.eof ib] returns the end of input condition
     of the given buffer. *)

  val end_of_input : scanbuf -> bool
  (* [Scanning.end_of_input ib] tests the end of input condition
     of the given buffer (if no char has ever been read, an attempt to
     read one is performed). *)

  val beginning_of_input : scanbuf -> bool
  (* [Scanning.beginning_of_input ib] tests the beginning of input
     condition of the given buffer. *)

  val name_of_input : scanbuf -> string
  (* [Scanning.name_of_input ib] returns the name of the character
     source for input buffer [ib]. *)

  val open_in : file_name -> in_channel
  val open_in_bin : file_name -> in_channel
  val from_file : file_name -> in_channel
  val from_file_bin : file_name -> in_channel
  val from_string : string -> in_channel
  val from_function : (unit -> char) -> in_channel
  val from_channel : Pervasives.in_channel -> in_channel

  val close_in : in_channel -> unit

  val memo_from_channel : Pervasives.in_channel -> in_channel
  (* Obsolete. *)

end


module Scanning : SCANNING = struct

  (* The run-time library for scanf. *)

  type file_name = string

  type in_channel_name =
    | From_channel of Pervasives.in_channel
    | From_file of file_name * Pervasives.in_channel
    | From_function
    | From_string


  type in_channel = {
    mutable ic_eof : bool;
    mutable ic_current_char : char;
    mutable ic_current_char_is_valid : bool;
    mutable ic_char_count : int;
    mutable ic_line_count : int;
    mutable ic_token_count : int;
    mutable ic_get_next_char : unit -> char;
    ic_token_buffer : Buffer.t;
    ic_input_name : in_channel_name;
  }


  type scanbuf = in_channel

  let null_char = '\000'

  (* Reads a new character from input buffer.
     Next_char never fails, even in case of end of input:
     it then simply sets the end of file condition. *)
  let next_char ib =
    try
      let c = ib.ic_get_next_char () in
      ib.ic_current_char <- c;
      ib.ic_current_char_is_valid <- true;
      ib.ic_char_count <- succ ib.ic_char_count;
      if c = '\n' then ib.ic_line_count <- succ ib.ic_line_count;
      c with
    | End_of_file ->
      let c = null_char in
      ib.ic_current_char <- c;
      ib.ic_current_char_is_valid <- false;
      ib.ic_eof <- true;
      c


  let peek_char ib =
    if ib.ic_current_char_is_valid
    then ib.ic_current_char
    else next_char ib


  (* Returns a valid current char for the input buffer. In particular
     no irrelevant null character (as set by [next_char] in case of end
     of input) is returned, since [End_of_file] is raised when
     [next_char] sets the end of file condition while trying to read a
     new character. *)
  let checked_peek_char ib =
    let c = peek_char ib in
    if ib.ic_eof then raise End_of_file;
    c


  let end_of_input ib =
    ignore (peek_char ib);
    ib.ic_eof


  let eof ib = ib.ic_eof

  let beginning_of_input ib = ib.ic_char_count = 0

  let name_of_input ib =
    match ib.ic_input_name with
    | From_channel _ic -> "unnamed Pervasives input channel"
    | From_file (fname, _ic) -> fname
    | From_function -> "unnamed function"
    | From_string -> "unnamed character string"


  let char_count ib =
    if ib.ic_current_char_is_valid
    then ib.ic_char_count - 1
    else ib.ic_char_count


  let line_count ib = ib.ic_line_count

  let reset_token ib = Buffer.reset ib.ic_token_buffer

  let invalidate_current_char ib = ib.ic_current_char_is_valid <- false

  let token ib =
    let token_buffer = ib.ic_token_buffer in
    let tok = Buffer.contents token_buffer in
    Buffer.clear token_buffer;
    ib.ic_token_count <- succ ib.ic_token_count;
    tok


  let token_count ib = ib.ic_token_count

  let skip_char width ib =
    invalidate_current_char ib;
    width


  let ignore_char width ib = skip_char (width - 1) ib

  let store_char width ib c =
    Buffer.add_char ib.ic_token_buffer c;
    ignore_char width ib


  let default_token_buffer_size = 1024

  let create iname next = {
    ic_eof = false;
    ic_current_char = null_char;
    ic_current_char_is_valid = false;
    ic_char_count = 0;
    ic_line_count = 0;
    ic_token_count = 0;
    ic_get_next_char = next;
    ic_token_buffer = Buffer.create default_token_buffer_size;
    ic_input_name = iname;
  }


  let from_string s =
    let i = ref 0 in
    let len = String.length s in
    let next () =
      if !i >= len then raise End_of_file else
      let c = s.[!i] in
      incr i;
      c in
    create From_string next


  let from_function = create From_function

  (* Scanning from an input channel. *)

  (* Position of the problem:

     We cannot prevent the scanning mechanism to use one lookahead character,
     if needed by the semantics of the format string specifications (e.g. a
     trailing 'skip space' specification in the format string); in this case,
     the mandatory lookahead character is indeed read from the input and not
     used to return the token read. It is thus mandatory to be able to store
     an unused lookahead character somewhere to get it as the first character
     of the next scan.

     To circumvent this problem, all the scanning functions get a low level
     input buffer argument where they store the lookahead character when
     needed; additionally, the input buffer is the only source of character of
     a scanner. The [scanbuf] input buffers are defined in module {!Scanning}.

     Now we understand that it is extremely important that related and
     successive calls to scanners indeed read from the same input buffer.
     In effect, if a scanner [scan1] is reading from [ib1] and stores an
     unused lookahead character [c1] into its input buffer [ib1], then
     another scanner [scan2] not reading from the same buffer [ib1] will miss
     the character [c1], seemingly vanished in the air from the point of view
     of [scan2].

     This mechanism works perfectly to read from strings, from files, and from
     functions, since in those cases, allocating two buffers reading from the
     same source is unnatural.

     Still, there is a difficulty in the case of scanning from an input
     channel. In effect, when scanning from an input channel [ic], this channel
     may not have been allocated from within this library. Hence, it may be
     shared (two functions of the user's program may successively read from
     [ic]). This is highly error prone since, one of the function may seek the
     input channel, while the other function has still an unused lookahead
     character in its input buffer. In conclusion, you should never mix direct
     low level reading and high level scanning from the same input channel.

  *)

  (* Perform bufferized input to improve efficiency. *)
  let file_buffer_size = ref 1024

  (* The scanner closes the input channel at end of input. *)
  let scan_close_at_end ic = Pervasives.close_in ic; raise End_of_file

  (* The scanner does not close the input channel at end of input:
     it just raises [End_of_file]. *)
  let scan_raise_at_end _ic = raise End_of_file

  let from_ic scan_close_ic iname ic =
    let len = !file_buffer_size in
    let buf = Bytes.create len in
    let i = ref 0 in
    let lim = ref 0 in
    let eof = ref false in
    let next () =
      if !i < !lim then begin let c = Bytes.get buf !i in incr i; c end else
      if !eof then raise End_of_file else begin
        lim := input ic buf 0 len;
        if !lim = 0 then begin eof := true; scan_close_ic ic end else begin
          i := 1;
          Bytes.get buf 0
        end
      end in
    create iname next


  let from_ic_close_at_end = from_ic scan_close_at_end
  let from_ic_raise_at_end = from_ic scan_raise_at_end

  (* The scanning buffer reading from [Pervasives.stdin].
     One could try to define [stdib] as a scanning buffer reading a character
     at a time (no bufferization at all), but unfortunately the top-level
     interaction would be wrong. This is due to some kind of
     'race condition' when reading from [Pervasives.stdin],
     since the interactive compiler and [Scanf.scanf] will simultaneously
     read the material they need from [Pervasives.stdin]; then, confusion
     will result from what should be read by the top-level and what should be
     read by [Scanf.scanf].
     This is even more complicated by the one character lookahead that
     [Scanf.scanf] is sometimes obliged to maintain: the lookahead character
     will be available for the next [Scanf.scanf] entry, seemingly coming from
     nowhere.
     Also no [End_of_file] is raised when reading from stdin: if not enough
     characters have been read, we simply ask to read more. *)
  let stdin =
    from_ic scan_raise_at_end
      (From_file ("-", Pervasives.stdin)) Pervasives.stdin


  let stdib = stdin

  let open_in_file open_in fname =
    match fname with
    | "-" -> stdin
    | fname ->
      let ic = open_in fname in
      from_ic_close_at_end (From_file (fname, ic)) ic


  let open_in = open_in_file Pervasives.open_in
  let open_in_bin = open_in_file Pervasives.open_in_bin

  let from_file = open_in
  let from_file_bin = open_in_bin

  let from_channel ic =
    from_ic_raise_at_end (From_channel ic) ic


  let close_in ib =
    match ib.ic_input_name with
    | From_channel ic ->
      Pervasives.close_in ic
    | From_file (_fname, ic) -> Pervasives.close_in ic
    | From_function | From_string -> ()


  (*
     Obsolete: a memo [from_channel] version to build a [Scanning.in_channel]
     scanning buffer out of a [Pervasives.in_channel].
     This function was used to try to preserve the scanning
     semantics for the (now obsolete) function [fscanf].
     Given that all scanner must read from a [Scanning.in_channel] scanning
     buffer, [fscanf] must read from one!
     More precisely, given [ic], all successive calls [fscanf ic] must read
     from the same scanning buffer.
     This obliged this library to allocated scanning buffers that were
     not properly garbage collectable, hence leading to memory leaks.
     If you need to read from a [Pervasives.in_channel] input channel
     [ic], simply define a [Scanning.in_channel] formatted input channel as in
     [let ib = Scanning.from_channel ic], then use [Scanf.bscanf ib] as usual.
  *)
  let memo_from_ic =
    let memo = ref [] in
    (fun scan_close_ic ic ->
     try List.assq ic !memo with
     | Not_found ->
       let ib =
         from_ic scan_close_ic (From_channel ic) ic in
       memo := (ic, ib) :: !memo;
       ib)


  (* Obsolete: see {!memo_from_ic} above. *)
  let memo_from_channel = memo_from_ic scan_raise_at_end

end


(* Formatted input functions. *)

type ('a, 'b, 'c, 'd) scanner =
     ('a, Scanning.in_channel, 'b, 'c, 'a -> 'd, 'd) format6 -> 'c


(* Reporting errors. *)
exception Scan_failure of string

let bad_input s = raise (Scan_failure s)

let bad_input_escape c =
  bad_input (Printf.sprintf "illegal escape character %C" c)


let bad_token_length message =
  bad_input
    (Printf.sprintf
       "scanning of %s failed: \
        the specified length was too short for token"
       message)


let bad_end_of_input message =
  bad_input
    (Printf.sprintf
       "scanning of %s failed: \
        premature end of file occurred before end of token"
       message)


let bad_float () =
  bad_input "no dot or exponent part found in float token"


let bad_hex_float () =
  bad_input "not a valid float in hexadecimal notation"


let character_mismatch_err c ci =
  Printf.sprintf "looking for %C, found %C" c ci


let character_mismatch c ci =
  bad_input (character_mismatch_err c ci)


let rec skip_whites ib =
  let c = Scanning.peek_char ib in
  if not (Scanning.eof ib) then begin
    match c with
    | ' ' | '\t' | '\n' | '\r' ->
      Scanning.invalidate_current_char ib; skip_whites ib
    | _ -> ()
  end


(* Checking that [c] is indeed in the input, then skips it.
   In this case, the character [c] has been explicitly specified in the
   format as being mandatory in the input; hence we should fail with
   [End_of_file] in case of end_of_input.
   (Remember that [Scan_failure] is raised only when (we can prove by
   evidence) that the input does not match the format string given. We must
   thus differentiate [End_of_file] as an error due to lack of input, and
   [Scan_failure] which is due to provably wrong input. I am not sure this is
   worth the burden: it is complex and somehow subliminal; should be clearer
   to fail with Scan_failure "Not enough input to complete scanning"!)

   That's why, waiting for a better solution, we use checked_peek_char here.
   We are also careful to treat "\r\n" in the input as an end of line marker:
   it always matches a '\n' specification in the input format string. *)
let rec check_char ib c =
  match c with
  | ' ' -> skip_whites ib
  | '\n' -> check_newline ib
  | c -> check_this_char ib c

and check_this_char ib c =
  let ci = Scanning.checked_peek_char ib in
  if ci = c then Scanning.invalidate_current_char ib else
  character_mismatch c ci

and check_newline ib =
  let ci = Scanning.checked_peek_char ib in
  match ci with
  | '\n' -> Scanning.invalidate_current_char ib
  | '\r' -> Scanning.invalidate_current_char ib; check_this_char ib '\n'
  | _ -> character_mismatch '\n' ci


(* Extracting tokens from the output token buffer. *)

let token_char ib = (Scanning.token ib).[0]

let token_string = Scanning.token

let token_bool ib =
  match Scanning.token ib with
  | "true" -> true
  | "false" -> false
  | s -> bad_input (Printf.sprintf "invalid boolean '%s'" s)


(* The type of integer conversions. *)
type integer_conversion =
  | B_conversion (* Unsigned binary conversion *)
  | D_conversion (* Signed decimal conversion *)
  | I_conversion (* Signed integer conversion *)
  | O_conversion (* Unsigned octal conversion *)
  | U_conversion (* Unsigned decimal conversion *)
  | X_conversion (* Unsigned hexadecimal conversion *)


let integer_conversion_of_char = function
  | 'b' -> B_conversion
  | 'd' -> D_conversion
  | 'i' -> I_conversion
  | 'o' -> O_conversion
  | 'u' -> U_conversion
  | 'x' | 'X' -> X_conversion
  | _ -> assert false


(* Extract an integer literal token.
   Since the functions Pervasives.*int*_of_string do not accept a leading +,
   we skip it if necessary. *)
let token_int_literal conv ib =
  let tok =
    match conv with
    | D_conversion | I_conversion -> Scanning.token ib
    | U_conversion -> "0u" ^ Scanning.token ib
    | O_conversion -> "0o" ^ Scanning.token ib
    | X_conversion -> "0x" ^ Scanning.token ib
    | B_conversion -> "0b" ^ Scanning.token ib in
  let l = String.length tok in
  if l = 0 || tok.[0] <> '+' then tok else String.sub tok 1 (l - 1)


(* All the functions that convert a string to a number raise the exception
   Failure when the conversion is not possible.
   This exception is then trapped in [kscanf]. *)
let token_int conv ib = int_of_string (token_int_literal conv ib)

let token_float ib = float_of_string (Scanning.token ib)

(* To scan native ints, int32 and int64 integers.
   We cannot access to conversions to/from strings for those types,
   Nativeint.of_string, Int32.of_string, and Int64.of_string,
   since those modules are not available to [Scanf].
   However, we can bind and use the corresponding primitives that are
   available in the runtime. *)
external nativeint_of_string : string -> nativeint
  = "caml_nativeint_of_string"

external int32_of_string : string -> int32
  = "caml_int32_of_string"

external int64_of_string : string -> int64
  = "caml_int64_of_string"


let token_nativeint conv ib = nativeint_of_string (token_int_literal conv ib)
let token_int32 conv ib = int32_of_string (token_int_literal conv ib)
let token_int64 conv ib = int64_of_string (token_int_literal conv ib)

(* Scanning numbers. *)

(* Digits scanning functions suppose that one character has been checked and
   is available, since they return at end of file with the currently found
   token selected.

   Put it in another way, the digits scanning functions scan for a possibly
   empty sequence of digits, (hence, a successful scanning from one of those
   functions does not imply that the token is a well-formed number: to get a
   true number, it is mandatory to check that at least one valid digit is
   available before calling one of the digit scanning functions). *)

(* The decimal case is treated especially for optimization purposes. *)
let rec scan_decimal_digit_star width ib =
  if width = 0 then width else
  let c = Scanning.peek_char ib in
  if Scanning.eof ib then width else
  match c with
  | '0' .. '9' as c ->
    let width = Scanning.store_char width ib c in
    scan_decimal_digit_star width ib
  | '_' ->
    let width = Scanning.ignore_char width ib in
    scan_decimal_digit_star width ib
  | _ -> width


let scan_decimal_digit_plus width ib =
  if width = 0 then bad_token_length "decimal digits" else
  let c = Scanning.checked_peek_char ib in
  match c with
  | '0' .. '9' ->
    let width = Scanning.store_char width ib c in
    scan_decimal_digit_star width ib
  | c ->
    bad_input (Printf.sprintf "character %C is not a decimal digit" c)


(* To scan numbers from other bases, we use a predicate argument to
   scan digits. *)
let scan_digit_star digitp width ib =
  let rec scan_digits width ib =
    if width = 0 then width else
    let c = Scanning.peek_char ib in
    if Scanning.eof ib then width else
    match c with
    | c when digitp c ->
      let width = Scanning.store_char width ib c in
      scan_digits width ib
    | '_' ->
      let width = Scanning.ignore_char width ib in
      scan_digits width ib
    | _ -> width in
  scan_digits width ib


let scan_digit_plus basis digitp width ib =
  (* Ensure we have got enough width left,
     and read at least one digit. *)
  if width = 0 then bad_token_length "digits" else
  let c = Scanning.checked_peek_char ib in
  if digitp c then
    let width = Scanning.store_char width ib c in
    scan_digit_star digitp width ib
  else
    bad_input (Printf.sprintf "character %C is not a valid %s digit" c basis)


let is_binary_digit = function
  | '0' .. '1' -> true
  | _ -> false


let scan_binary_int = scan_digit_plus "binary" is_binary_digit

let is_octal_digit = function
  | '0' .. '7' -> true
  | _ -> false


let scan_octal_int = scan_digit_plus "octal" is_octal_digit

let is_hexa_digit = function
  | '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' -> true
  | _ -> false


let scan_hexadecimal_int = scan_digit_plus "hexadecimal" is_hexa_digit

(* Scan a decimal integer. *)
let scan_unsigned_decimal_int = scan_decimal_digit_plus

let scan_sign width ib =
  let c = Scanning.checked_peek_char ib in
  match c with
  | '+' -> Scanning.store_char width ib c
  | '-' -> Scanning.store_char width ib c
  | _ -> width


let scan_optionally_signed_decimal_int width ib =
  let width = scan_sign width ib in
  scan_unsigned_decimal_int width ib


(* Scan an unsigned integer that could be given in any (common) basis.
   If digits are prefixed by one of 0x, 0X, 0o, or 0b, the number is
   assumed to be written respectively in hexadecimal, hexadecimal,
   octal, or binary. *)
let scan_unsigned_int width ib =
  match Scanning.checked_peek_char ib with
  | '0' as c ->
    let width = Scanning.store_char width ib c in
    if width = 0 then width else
    let c = Scanning.peek_char ib in
    if Scanning.eof ib then width else
    begin match c with
    | 'x' | 'X' -> scan_hexadecimal_int (Scanning.store_char width ib c) ib
    | 'o' -> scan_octal_int (Scanning.store_char width ib c) ib
    | 'b' -> scan_binary_int (Scanning.store_char width ib c) ib
    | _ -> scan_decimal_digit_star width ib end
  | _ -> scan_unsigned_decimal_int width ib


let scan_optionally_signed_int width ib =
  let width = scan_sign width ib in
  scan_unsigned_int width ib


let scan_int_conversion conv width ib =
  match conv with
  | B_conversion -> scan_binary_int width ib
  | D_conversion -> scan_optionally_signed_decimal_int width ib
  | I_conversion -> scan_optionally_signed_int width ib
  | O_conversion -> scan_octal_int width ib
  | U_conversion -> scan_unsigned_decimal_int width ib
  | X_conversion -> scan_hexadecimal_int width ib


(* Scanning floating point numbers. *)

(* Fractional part is optional and can be reduced to 0 digits. *)
let scan_fractional_part width ib =
  if width = 0 then width else
  let c = Scanning.peek_char ib in
  if Scanning.eof ib then width else
  match c with
  | '0' .. '9' as c ->
    scan_decimal_digit_star (Scanning.store_char width ib c) ib
  | _ -> width


(* Exp part is optional and can be reduced to 0 digits. *)
let scan_exponent_part width ib =
  if width = 0 then width else
  let c = Scanning.peek_char ib in
  if Scanning.eof ib then width else
  match c with
  | 'e' | 'E' as c ->
    scan_optionally_signed_decimal_int (Scanning.store_char width ib c) ib
  | _ -> width


(* Scan the integer part of a floating point number, (not using the
   OCaml lexical convention since the integer part can be empty):
   an optional sign, followed by a possibly empty sequence of decimal
   digits (e.g. -.1). *)
let scan_integer_part width ib =
  let width = scan_sign width ib in
  scan_decimal_digit_star width ib


(*
   For the time being we have (as found in scanf.mli):
   the field width is composed of an optional integer literal
   indicating the maximal width of the token to read.
   Unfortunately, the type-checker let the user write an optional precision,
   since this is valid for printf format strings.

   Thus, the next step for Scanf is to support a full width and precision
   indication, more or less similar to the one for printf, possibly extended
   to the specification of a [max, min] range for the width of the token read
   for strings. Something like the following spec for scanf.mli:

   The optional [width] is an integer indicating the maximal
   width of the token read. For instance, [%6d] reads an integer,
   having at most 6 characters.

   The optional [precision] is a dot [.] followed by an integer:

   - in the floating point number conversions ([%f], [%e], [%g], [%F], [%E],
   and [%F] conversions, the [precision] indicates the maximum number of
   digits that may follow the decimal point. For instance, [%.4f] reads a
   [float] with at most 4 fractional digits,

   - in the string conversions ([%s], [%S], [%\[ range \]]), and in the
   integer number conversions ([%i], [%d], [%u], [%x], [%o], and their
   [int32], [int64], and [native_int] correspondent), the [precision]
   indicates the required minimum width of the token read,

   - on all other conversions, the width and precision specify the [max, min]
   range for the width of the token read.
*)
let scan_float width precision ib =
  let width = scan_integer_part width ib in
  if width = 0 then width, precision else
  let c = Scanning.peek_char ib in
  if Scanning.eof ib then width, precision else
  match c with
  | '.' ->
    let width = Scanning.store_char width ib c in
    let precision = min width precision in
    let width = width - (precision - scan_fractional_part precision ib) in
    scan_exponent_part width ib, precision
  | _ ->
    scan_exponent_part width ib, precision


let check_case_insensitive_string width ib error str =
  let lowercase c =
    match c with
    | 'A' .. 'Z' ->
      char_of_int (int_of_char c - int_of_char 'A' + int_of_char 'a')
    | _ -> c in
  let len = String.length str in
  let width = ref width in
  for i = 0 to len - 1 do
    let c = Scanning.peek_char ib in
    if lowercase c <> lowercase str.[i] then error ();
    if !width = 0 then error ();
    width := Scanning.store_char !width ib c;
  done;
  !width


let scan_hex_float width precision ib =
  if width = 0 || Scanning.end_of_input ib then bad_hex_float ();
  let width = scan_sign width ib in
  if width = 0 || Scanning.end_of_input ib then bad_hex_float ();
  match Scanning.peek_char ib with
  | '0' as c -> (
    let width = Scanning.store_char width ib c in
    if width = 0 || Scanning.end_of_input ib then bad_hex_float ();
    let width = check_case_insensitive_string width ib bad_hex_float "x" in
    if width = 0 || Scanning.end_of_input ib then width else
      let width = match Scanning.peek_char ib with
        | '.' | 'p' | 'P' -> width
        | _ -> scan_hexadecimal_int width ib in
      if width = 0 || Scanning.end_of_input ib then width else
        let width = match Scanning.peek_char ib with
          | '.' as c -> (
            let width = Scanning.store_char width ib c in
            if width = 0 || Scanning.end_of_input ib then width else
              match Scanning.peek_char ib with
              | 'p' | 'P' -> width
              | _ ->
                let precision = min width precision in
                width - (precision - scan_hexadecimal_int precision ib)
          )
          | _ -> width in
        if width = 0 || Scanning.end_of_input ib then width else
          match Scanning.peek_char ib with
          | 'p' | 'P' as c ->
            let width = Scanning.store_char width ib c in
            if width = 0 || Scanning.end_of_input ib then bad_hex_float ();
            scan_optionally_signed_decimal_int width ib
          | _ -> width
  )
  | 'n' | 'N' as c ->
    let width = Scanning.store_char width ib c in
    if width = 0 || Scanning.end_of_input ib then bad_hex_float ();
    check_case_insensitive_string width ib bad_hex_float "an"
  | 'i' | 'I' as c ->
    let width = Scanning.store_char width ib c in
    if width = 0 || Scanning.end_of_input ib then bad_hex_float ();
    check_case_insensitive_string width ib bad_hex_float "nfinity"
  | _ -> bad_hex_float ()


let scan_caml_float_rest width precision ib =
  if width = 0 || Scanning.end_of_input ib then bad_float ();
  let width = scan_decimal_digit_star width ib in
  if width = 0 || Scanning.end_of_input ib then bad_float ();
  let c = Scanning.peek_char ib in
  match c with
  | '.' ->
    let width = Scanning.store_char width ib c in
    (* The effective width available for scanning the fractional part is
       the minimum of declared precision and width left. *)
    let precision = min width precision in
    (* After scanning the fractional part with [precision] provisional width,
       [width_precision] is left. *)
    let width_precision = scan_fractional_part precision ib in
    (* Hence, scanning the fractional part took exactly
       [precision - width_precision] chars. *)
    let frac_width = precision - width_precision in
    (* And new provisional width is [width - width_precision. *)
    let width = width - frac_width in
    scan_exponent_part width ib
  | 'e' | 'E' ->
    scan_exponent_part width ib
  | _ -> bad_float ()


let scan_caml_float width precision ib =
  if width = 0 || Scanning.end_of_input ib then bad_float ();
  let width = scan_sign width ib in
  if width = 0 || Scanning.end_of_input ib then bad_float ();
  match Scanning.peek_char ib with
  | '0' as c -> (
    let width = Scanning.store_char width ib c in
    if width = 0 || Scanning.end_of_input ib then bad_float ();
    match Scanning.peek_char ib with
    | 'x' | 'X' as c -> (
      let width = Scanning.store_char width ib c in
      if width = 0 || Scanning.end_of_input ib then bad_float ();
      let width = scan_hexadecimal_int width ib in
      if width = 0 || Scanning.end_of_input ib then bad_float ();
      let width = match Scanning.peek_char ib with
        | '.' as c -> (
          let width = Scanning.store_char width ib c in
          if width = 0 || Scanning.end_of_input ib then width else
            match Scanning.peek_char ib with
            | 'p' | 'P' -> width
            | _ ->
              let precision = min width precision in
              width - (precision - scan_hexadecimal_int precision ib)
        )
        | 'p' | 'P' -> width
        | _ -> bad_float () in
      if width = 0 || Scanning.end_of_input ib then width else
        match Scanning.peek_char ib with
        | 'p' | 'P' as c ->
          let width = Scanning.store_char width ib c in
          if width = 0 || Scanning.end_of_input ib then bad_hex_float ();
          scan_optionally_signed_decimal_int width ib
        | _ -> width
    )
    | _ ->
      scan_caml_float_rest width precision ib
  )
  | '1' .. '9' as c ->
    let width = Scanning.store_char width ib c in
    if width = 0 || Scanning.end_of_input ib then bad_float ();
    scan_caml_float_rest width precision ib
(* Special case of nan and infinity:
  | 'i' ->
  | 'n' ->
*)
  | _ -> bad_float ()


(* Scan a regular string:
   stops when encountering a space, if no scanning indication has been given;
   otherwise, stops when encountering the characters in the scanning
   indication [stp].
   It also stops at end of file or when the maximum number of characters has
   been read. *)
let scan_string stp width ib =
  let rec loop width =
    if width = 0 then width else
    let c = Scanning.peek_char ib in
    if Scanning.eof ib then width else
      match stp with
      | Some c' when c = c' -> Scanning.skip_char width ib
      | Some _ -> loop (Scanning.store_char width ib c)
      | None ->
        match c with
        | ' ' | '\t' | '\n' | '\r' -> width
        | _ -> loop (Scanning.store_char width ib c) in
  loop width


(* Scan a char: peek strictly one character in the input, whatsoever. *)
let scan_char width ib =
  (* The case width = 0 could not happen here, since it is tested before
     calling scan_char, in the main scanning function.
    if width = 0 then bad_token_length "a character" else *)
  Scanning.store_char width ib (Scanning.checked_peek_char ib)


let char_for_backslash = function
  | 'n' -> '\010'
  | 'r' -> '\013'
  | 'b' -> '\008'
  | 't' -> '\009'
  | c -> c


(* The integer value corresponding to the facial value of a valid
   decimal digit character. *)
let decimal_value_of_char c = int_of_char c - int_of_char '0'

let char_for_decimal_code c0 c1 c2 =
  let c =
    100 * decimal_value_of_char c0 +
     10 * decimal_value_of_char c1 +
          decimal_value_of_char c2 in
  if c < 0 || c > 255 then
    bad_input
      (Printf.sprintf
         "bad character decimal encoding \\%c%c%c" c0 c1 c2) else
  char_of_int c


(* The integer value corresponding to the facial value of a valid
   hexadecimal digit character. *)
let hexadecimal_value_of_char c =
  let d = int_of_char c in
  (* Could also be:
    if d <= int_of_char '9' then d - int_of_char '0' else
    if d <= int_of_char 'F' then 10 + d - int_of_char 'A' else
    if d <= int_of_char 'f' then 10 + d - int_of_char 'a' else assert false
  *)
  if d >= int_of_char 'a' then
    d - 87 (* 10 + int_of_char c - int_of_char 'a' *) else
  if d >= int_of_char 'A' then
    d - 55  (* 10 + int_of_char c - int_of_char 'A' *) else
    d - int_of_char '0'


let char_for_hexadecimal_code c1 c2 =
  let c =
    16 * hexadecimal_value_of_char c1 +
         hexadecimal_value_of_char c2 in
  if c < 0 || c > 255 then
    bad_input
      (Printf.sprintf "bad character hexadecimal encoding \\%c%c" c1 c2) else
  char_of_int c


(* Called in particular when encountering '\\' as starter of a char.
   Stops before the corresponding '\''. *)
let check_next_char message width ib =
  if width = 0 then bad_token_length message else
  let c = Scanning.peek_char ib in
  if Scanning.eof ib then bad_end_of_input message else
  c


let check_next_char_for_char = check_next_char "a Char"
let check_next_char_for_string = check_next_char "a String"

let scan_backslash_char width ib =
  match check_next_char_for_char width ib with
  | '\\' | '\'' | '\"' | 'n' | 't' | 'b' | 'r' as c ->
    Scanning.store_char width ib (char_for_backslash c)
  | '0' .. '9' as c ->
    let get_digit () =
      let c = Scanning.next_char ib in
      match c with
      | '0' .. '9' as c -> c
      | c -> bad_input_escape c in
    let c0 = c in
    let c1 = get_digit () in
    let c2 = get_digit () in
    Scanning.store_char (width - 2) ib (char_for_decimal_code c0 c1 c2)
  | 'x' ->
    let get_digit () =
      let c = Scanning.next_char ib in
      match c with
      | '0' .. '9' | 'A' .. 'F' | 'a' .. 'f' as c -> c
      | c -> bad_input_escape c in
    let c1 = get_digit () in
    let c2 = get_digit () in
    Scanning.store_char (width - 2) ib (char_for_hexadecimal_code c1 c2)
  | c ->
    bad_input_escape c


(* Scan a character (an OCaml token). *)
let scan_caml_char width ib =

  let rec find_start width =
    match Scanning.checked_peek_char ib with
    | '\'' -> find_char (Scanning.ignore_char width ib)
    | c -> character_mismatch '\'' c

  and find_char width =
    match check_next_char_for_char width ib with
    | '\\' ->
      find_stop (scan_backslash_char (Scanning.ignore_char width ib) ib)
    | c ->
      find_stop (Scanning.store_char width ib c)

  and find_stop width =
    match check_next_char_for_char width ib with
    | '\'' -> Scanning.ignore_char width ib
    | c -> character_mismatch '\'' c in

  find_start width


(* Scan a delimited string (an OCaml token). *)
let scan_caml_string width ib =

  let rec find_start width =
    match Scanning.checked_peek_char ib with
    | '\"' -> find_stop (Scanning.ignore_char width ib)
    | c -> character_mismatch '\"' c

  and find_stop width =
    match check_next_char_for_string width ib with
    | '\"' -> Scanning.ignore_char width ib
    | '\\' -> scan_backslash (Scanning.ignore_char width ib)
    | c -> find_stop (Scanning.store_char width ib c)

  and scan_backslash width =
    match check_next_char_for_string width ib with
    | '\r' -> skip_newline (Scanning.ignore_char width ib)
    | '\n' -> skip_spaces (Scanning.ignore_char width ib)
    | _ -> find_stop (scan_backslash_char width ib)

  and skip_newline width =
    match check_next_char_for_string width ib with
    | '\n' -> skip_spaces (Scanning.ignore_char width ib)
    | _ -> find_stop (Scanning.store_char width ib '\r')

  and skip_spaces width =
    match check_next_char_for_string width ib with
    | ' ' -> skip_spaces (Scanning.ignore_char width ib)
    | _ -> find_stop width in

  find_start width


(* Scan a boolean (an OCaml token). *)
let scan_bool ib =
  let c = Scanning.checked_peek_char ib in
  let m =
    match c with
    | 't' -> 4
    | 'f' -> 5
    | c ->
      bad_input
        (Printf.sprintf "the character %C cannot start a boolean" c) in
  scan_string None m ib


(* Scan a string containing elements in char_set and terminated by scan_indic
   if provided. *)
let scan_chars_in_char_set char_set scan_indic width ib =
  let rec scan_chars i stp =
    let c = Scanning.peek_char ib in
    if i > 0 && not (Scanning.eof ib) &&
       is_in_char_set char_set c &&
       int_of_char c <> stp then
      let _ = Scanning.store_char max_int ib c in
      scan_chars (i - 1) stp in
  match scan_indic with
  | None -> scan_chars width (-1);
  | Some c ->
    scan_chars width (int_of_char c);
    if not (Scanning.eof ib) then
      let ci = Scanning.peek_char ib in
      if c = ci
      then Scanning.invalidate_current_char ib
      else character_mismatch c ci


(* The global error report function for [Scanf]. *)
let scanf_bad_input ib = function
  | Scan_failure s | Failure s ->
    let i = Scanning.char_count ib in
    bad_input (Printf.sprintf "scanf: bad input at char number %i: %s" i s)
  | x -> raise x


(* Get the content of a counter from an input buffer. *)
let get_counter ib counter =
  match counter with
  | Line_counter -> Scanning.line_count ib
  | Char_counter -> Scanning.char_count ib
  | Token_counter -> Scanning.token_count ib


(* Compute the width of a padding option (see "%42{" and "%123("). *)
let width_of_pad_opt pad_opt = match pad_opt with
  | None -> max_int
  | Some width -> width


let stopper_of_formatting_lit fmting =
  if fmting = Escaped_percent then '%', "" else
    let str = string_of_formatting_lit fmting in
    let stp = str.[1] in
    let sub_str = String.sub str 2 (String.length str - 2) in
    stp, sub_str


(******************************************************************************)
                           (* Reader management *)

(* A call to take_format_readers on a format is evaluated into functions
   taking readers as arguments and aggregate them into an heterogeneous list *)
(* When all readers are taken, finally pass the list of the readers to the
   continuation k. *)
let rec take_format_readers : type a c d e f .
    ((d, e) heter_list -> e) -> (a, Scanning.in_channel, c, d, e, f) fmt ->
    d =
fun k fmt -> match fmt with
  | Reader fmt_rest ->
    fun reader ->
      let new_k readers_rest = k (Cons (reader, readers_rest)) in
      take_format_readers new_k fmt_rest
  | Char rest                        -> take_format_readers k rest
  | Caml_char rest                   -> take_format_readers k rest
  | String (_, rest)                 -> take_format_readers k rest
  | Caml_string (_, rest)            -> take_format_readers k rest
  | Int (_, _, _, rest)              -> take_format_readers k rest
  | Int32 (_, _, _, rest)            -> take_format_readers k rest
  | Nativeint (_, _, _, rest)        -> take_format_readers k rest
  | Int64 (_, _, _, rest)            -> take_format_readers k rest
  | Float (_, _, _, rest)            -> take_format_readers k rest
  | Bool (_, rest)                   -> take_format_readers k rest
  | Alpha rest                       -> take_format_readers k rest
  | Theta rest                       -> take_format_readers k rest
  | Flush rest                       -> take_format_readers k rest
  | String_literal (_, rest)         -> take_format_readers k rest
  | Char_literal (_, rest)           -> take_format_readers k rest
  | Custom (_, _, rest)              -> take_format_readers k rest

  | Scan_char_set (_, _, rest)       -> take_format_readers k rest
  | Scan_get_counter (_, rest)       -> take_format_readers k rest
  | Scan_next_char rest              -> take_format_readers k rest

  | Formatting_lit (_, rest)         -> take_format_readers k rest
  | Formatting_gen (Open_tag (Format (fmt, _)), rest) ->
      take_format_readers k (concat_fmt fmt rest)
  | Formatting_gen (Open_box (Format (fmt, _)), rest) ->
      take_format_readers k (concat_fmt fmt rest)

  | Format_arg (_, _, rest)          -> take_format_readers k rest
  | Format_subst (_, fmtty, rest)    ->
     take_fmtty_format_readers k (erase_rel (symm fmtty)) rest
  | Ignored_param (ign, rest)        -> take_ignored_format_readers k ign rest

  | End_of_format                    -> k Nil

(* Take readers associated to an fmtty coming from a Format_subst "%(...%)". *)
and take_fmtty_format_readers : type x y a c d e f .
    ((d, e) heter_list -> e) -> (a, Scanning.in_channel, c, d, x, y) fmtty ->
      (y, Scanning.in_channel, c, x, e, f) fmt -> d =
fun k fmtty fmt -> match fmtty with
  | Reader_ty fmt_rest ->
    fun reader ->
      let new_k readers_rest = k (Cons (reader, readers_rest)) in
      take_fmtty_format_readers new_k fmt_rest fmt
  | Ignored_reader_ty fmt_rest ->
    fun reader ->
      let new_k readers_rest = k (Cons (reader, readers_rest)) in
      take_fmtty_format_readers new_k fmt_rest fmt
  | Char_ty rest                -> take_fmtty_format_readers k rest fmt
  | String_ty rest              -> take_fmtty_format_readers k rest fmt
  | Int_ty rest                 -> take_fmtty_format_readers k rest fmt
  | Int32_ty rest               -> take_fmtty_format_readers k rest fmt
  | Nativeint_ty rest           -> take_fmtty_format_readers k rest fmt
  | Int64_ty rest               -> take_fmtty_format_readers k rest fmt
  | Float_ty rest               -> take_fmtty_format_readers k rest fmt
  | Bool_ty rest                -> take_fmtty_format_readers k rest fmt
  | Alpha_ty rest               -> take_fmtty_format_readers k rest fmt
  | Theta_ty rest               -> take_fmtty_format_readers k rest fmt
  | Any_ty rest                 -> take_fmtty_format_readers k rest fmt
  | Format_arg_ty (_, rest)     -> take_fmtty_format_readers k rest fmt
  | End_of_fmtty                -> take_format_readers k fmt
  | Format_subst_ty (ty1, ty2, rest) ->
    let ty = trans (symm ty1) ty2 in
    take_fmtty_format_readers k (concat_fmtty ty rest) fmt

(* Take readers associated to an ignored parameter. *)
and take_ignored_format_readers : type x y a c d e f .
    ((d, e) heter_list -> e) -> (a, Scanning.in_channel, c, d, x, y) ignored ->
      (y, Scanning.in_channel, c, x, e, f) fmt -> d =
fun k ign fmt -> match ign with
  | Ignored_reader ->
    fun reader ->
      let new_k readers_rest = k (Cons (reader, readers_rest)) in
      take_format_readers new_k fmt
  | Ignored_char                    -> take_format_readers k fmt
  | Ignored_caml_char               -> take_format_readers k fmt
  | Ignored_string _                -> take_format_readers k fmt
  | Ignored_caml_string _           -> take_format_readers k fmt
  | Ignored_int (_, _)              -> take_format_readers k fmt
  | Ignored_int32 (_, _)            -> take_format_readers k fmt
  | Ignored_nativeint (_, _)        -> take_format_readers k fmt
  | Ignored_int64 (_, _)            -> take_format_readers k fmt
  | Ignored_float (_, _)            -> take_format_readers k fmt
  | Ignored_bool _                  -> take_format_readers k fmt
  | Ignored_format_arg _            -> take_format_readers k fmt
  | Ignored_format_subst (_, fmtty) -> take_fmtty_format_readers k fmtty fmt
  | Ignored_scan_char_set _         -> take_format_readers k fmt
  | Ignored_scan_get_counter _      -> take_format_readers k fmt
  | Ignored_scan_next_char          -> take_format_readers k fmt

(******************************************************************************)
                          (* Generic scanning *)

(* Make a generic scanning function. *)
(* Scan a stream according to a format and readers obtained by
   take_format_readers, and aggregate scanned values into an
   heterogeneous list. *)
(* Return the heterogeneous list of scanned values. *)
let rec make_scanf : type a c d e f.
    Scanning.in_channel -> (a, Scanning.in_channel, c, d, e, f) fmt ->
      (d, e) heter_list -> (a, f) heter_list =
fun ib fmt readers -> match fmt with
  | Char rest ->
    let _ = scan_char 0 ib in
    let c = token_char ib in
    Cons (c, make_scanf ib rest readers)
  | Caml_char rest ->
    let _ = scan_caml_char 0 ib in
    let c = token_char ib in
    Cons (c, make_scanf ib rest readers)

  | String (pad, Formatting_lit (fmting_lit, rest)) ->
    let stp, str = stopper_of_formatting_lit fmting_lit in
    let scan width _ ib = scan_string (Some stp) width ib in
    let str_rest = String_literal (str, rest) in
    pad_prec_scanf ib str_rest readers pad No_precision scan token_string
  | String (pad, Formatting_gen (Open_tag (Format (fmt', _)), rest)) ->
    let scan width _ ib = scan_string (Some '{') width ib in
    pad_prec_scanf ib (concat_fmt fmt' rest) readers pad No_precision scan
                   token_string
  | String (pad, Formatting_gen (Open_box (Format (fmt', _)), rest)) ->
    let scan width _ ib = scan_string (Some '[') width ib in
    pad_prec_scanf ib (concat_fmt fmt' rest) readers pad No_precision scan
                   token_string
  | String (pad, rest) ->
    let scan width _ ib = scan_string None width ib in
    pad_prec_scanf ib rest readers pad No_precision scan token_string

  | Caml_string (pad, rest) ->
    let scan width _ ib = scan_caml_string width ib in
    pad_prec_scanf ib rest readers pad No_precision scan token_string
  | Int (iconv, pad, prec, rest) ->
    let c = integer_conversion_of_char (char_of_iconv iconv) in
    let scan width _ ib = scan_int_conversion c width ib in
    pad_prec_scanf ib rest readers pad prec scan (token_int c)
  | Int32 (iconv, pad, prec, rest) ->
    let c = integer_conversion_of_char (char_of_iconv iconv) in
    let scan width _ ib = scan_int_conversion c width ib in
    pad_prec_scanf ib rest readers pad prec scan (token_int32 c)
  | Nativeint (iconv, pad, prec, rest) ->
    let c = integer_conversion_of_char (char_of_iconv iconv) in
    let scan width _ ib = scan_int_conversion c width ib in
    pad_prec_scanf ib rest readers pad prec scan (token_nativeint c)
  | Int64 (iconv, pad, prec, rest) ->
    let c = integer_conversion_of_char (char_of_iconv iconv) in
    let scan width _ ib = scan_int_conversion c width ib in
    pad_prec_scanf ib rest readers pad prec scan (token_int64 c)
  | Float (Float_F, pad, prec, rest) ->
    pad_prec_scanf ib rest readers pad prec scan_caml_float token_float
  | Float ((Float_f | Float_pf | Float_sf | Float_e | Float_pe | Float_se
               | Float_E | Float_pE | Float_sE | Float_g | Float_pg | Float_sg
               | Float_G | Float_pG | Float_sG), pad, prec, rest) ->
    pad_prec_scanf ib rest readers pad prec scan_float token_float
  | Float ((Float_h | Float_ph | Float_sh | Float_H | Float_pH | Float_sH),
           pad, prec, rest) ->
    pad_prec_scanf ib rest readers pad prec scan_hex_float token_float
  | Bool (pad, rest) ->
    let scan _ _ ib = scan_bool ib in
    pad_prec_scanf ib rest readers pad No_precision scan token_bool
  | Alpha _ ->
    invalid_arg "scanf: bad conversion \"%a\""
  | Theta _ ->
    invalid_arg "scanf: bad conversion \"%t\""
  | Custom _ ->
    invalid_arg "scanf: bad conversion \"%?\" (custom converter)"
  | Reader fmt_rest ->
    begin match readers with
    | Cons (reader, readers_rest) ->
        let x = reader ib in
        Cons (x, make_scanf ib fmt_rest readers_rest)
    | Nil ->
        invalid_arg "scanf: missing reader"
    end
  | Flush rest ->
    if Scanning.end_of_input ib then make_scanf ib rest readers
    else bad_input "end of input not found"

  | String_literal (str, rest) ->
    String.iter (check_char ib) str;
    make_scanf ib rest readers
  | Char_literal (chr, rest) ->
    check_char ib chr;
    make_scanf ib rest readers

  | Format_arg (pad_opt, fmtty, rest) ->
    let _ = scan_caml_string (width_of_pad_opt pad_opt) ib in
    let s = token_string ib in
    let fmt =
      try format_of_string_fmtty s fmtty
      with Failure msg -> bad_input msg
    in
    Cons (fmt, make_scanf ib rest readers)
  | Format_subst (pad_opt, fmtty, rest) ->
    let _ = scan_caml_string (width_of_pad_opt pad_opt) ib in
    let s = token_string ib in
    let fmt, fmt' =
      try
        let Fmt_EBB fmt = fmt_ebb_of_string s in
        let Fmt_EBB fmt' = fmt_ebb_of_string s in
        (* TODO: find a way to avoid reparsing twice *)

        (* TODO: these type-checks below *can* fail because of type
           ambiguity in presence of ignored-readers: "%_r%d" and "%d%_r"
           are typed in the same way.

           # Scanf.sscanf "\"%_r%d\"3" "%(%d%_r%)" ignore
             (fun fmt n -> string_of_format fmt, n)
           Exception: CamlinternalFormat.Type_mismatch.

           We should properly catch this exception.
        *)
        type_format fmt (erase_rel fmtty),
        type_format fmt' (erase_rel (symm fmtty))
      with Failure msg -> bad_input msg
    in
    Cons (Format (fmt, s),
          make_scanf ib (concat_fmt fmt' rest) readers)

  | Scan_char_set (width_opt, char_set, Formatting_lit (fmting_lit, rest)) ->
    let stp, str = stopper_of_formatting_lit fmting_lit in
    let width = width_of_pad_opt width_opt in
    scan_chars_in_char_set char_set (Some stp) width ib;
    let s = token_string ib in
    let str_rest = String_literal (str, rest) in
    Cons (s, make_scanf ib str_rest readers)
  | Scan_char_set (width_opt, char_set, rest) ->
    let width = width_of_pad_opt width_opt in
    scan_chars_in_char_set char_set None width ib;
    let s = token_string ib in
    Cons (s, make_scanf ib rest readers)
  | Scan_get_counter (counter, rest) ->
    let count = get_counter ib counter in
    Cons (count, make_scanf ib rest readers)
  | Scan_next_char rest ->
    let c = Scanning.checked_peek_char ib in
    Cons (c, make_scanf ib rest readers)

  | Formatting_lit (formatting_lit, rest) ->
    String.iter (check_char ib) (string_of_formatting_lit formatting_lit);
    make_scanf ib rest readers
  | Formatting_gen (Open_tag (Format (fmt', _)), rest) ->
    check_char ib '@'; check_char ib '{';
    make_scanf ib (concat_fmt fmt' rest) readers
  | Formatting_gen (Open_box (Format (fmt', _)), rest) ->
    check_char ib '@'; check_char ib '[';
    make_scanf ib (concat_fmt fmt' rest) readers

  | Ignored_param (ign, rest) ->
    let Param_format_EBB fmt' = param_format_of_ignored_format ign rest in
    begin match make_scanf ib fmt' readers with
    | Cons (_, arg_rest) -> arg_rest
    | Nil -> assert false
    end

  | End_of_format ->
    Nil

(* Case analysis on padding and precision. *)
(* Reject formats containing "%*" or "%.*". *)
(* Pass padding and precision to the generic scanner `scan'. *)
and pad_prec_scanf : type a c d e f x y z t .
    Scanning.in_channel -> (a, Scanning.in_channel, c, d, e, f) fmt ->
      (d, e) heter_list -> (x, y) padding -> (y, z -> a) precision ->
      (int -> int -> Scanning.in_channel -> t) ->
      (Scanning.in_channel -> z) ->
      (x, f) heter_list =
fun ib fmt readers pad prec scan token -> match pad, prec with
  | No_padding, No_precision ->
    let _ = scan max_int max_int ib in
    let x = token ib in
    Cons (x, make_scanf ib fmt readers)
  | No_padding, Lit_precision p ->
    let _ = scan max_int p ib in
    let x = token ib in
    Cons (x, make_scanf ib fmt readers)
  | Lit_padding ((Right | Zeros), w), No_precision ->
    let _ = scan w max_int ib in
    let x = token ib in
    Cons (x, make_scanf ib fmt readers)
  | Lit_padding ((Right | Zeros), w), Lit_precision p ->
    let _ = scan w p ib in
    let x = token ib in
    Cons (x, make_scanf ib fmt readers)
  | Lit_padding (Left, _), _ ->
    invalid_arg "scanf: bad conversion \"%-\""
  | Lit_padding ((Right | Zeros), _), Arg_precision ->
    invalid_arg "scanf: bad conversion \"%*\""
  | Arg_padding _, _ ->
    invalid_arg "scanf: bad conversion \"%*\""
  | No_padding, Arg_precision ->
    invalid_arg "scanf: bad conversion \"%*\""

(******************************************************************************)
            (* Defining [scanf] and various flavors of [scanf] *)

type 'a kscanf_result = Args of 'a | Exc of exn

let kscanf ib ef (Format (fmt, str)) =
  let rec apply : type a b . a -> (a, b) heter_list -> b =
    fun f args -> match args with
    | Cons (x, r) -> apply (f x) r
    | Nil -> f
  in
  let k readers f =
    Scanning.reset_token ib;
    match try Args (make_scanf ib fmt readers) with
      | (Scan_failure _ | Failure _ | End_of_file) as exc -> Exc exc
      | Invalid_argument msg ->
        invalid_arg (msg ^ " in format \"" ^ String.escaped str ^ "\"")
    with
      | Args args -> apply f args
      | Exc exc -> ef ib exc
  in
  take_format_readers k fmt

(***)

let kbscanf = kscanf
let bscanf ib fmt = kbscanf ib scanf_bad_input fmt

let ksscanf s ef fmt = kbscanf (Scanning.from_string s) ef fmt
let sscanf s fmt = kbscanf (Scanning.from_string s) scanf_bad_input fmt

let scanf fmt = kscanf Scanning.stdib scanf_bad_input fmt

(***)

(* Scanning format strings. *)
let bscanf_format :
  Scanning.in_channel -> ('a, 'b, 'c, 'd, 'e, 'f) format6 ->
  (('a, 'b, 'c, 'd, 'e, 'f) format6 -> 'g) -> 'g =
  fun ib format f ->
    let _ = scan_caml_string max_int ib in
    let str = token_string ib in
    let fmt' =
      try format_of_string_format str format
      with Failure msg -> bad_input msg in
    f fmt'


let sscanf_format :
  string -> ('a, 'b, 'c, 'd, 'e, 'f) format6 ->
  (('a, 'b, 'c, 'd, 'e, 'f) format6 -> 'g) -> 'g =
  fun s format f -> bscanf_format (Scanning.from_string s) format f


let string_to_String s =
  let l = String.length s in
  let b = Buffer.create (l + 2) in
  Buffer.add_char b '\"';
  for i = 0 to l - 1 do
    let c = s.[i] in
    if c = '\"' then Buffer.add_char b '\\';
    Buffer.add_char b c;
  done;
  Buffer.add_char b '\"';
  Buffer.contents b


let format_from_string s fmt =
  sscanf_format (string_to_String s) fmt (fun x -> x)


let unescaped s =
  sscanf ("\"" ^ s ^ "\"") "%S%!" (fun x -> x)


(* Deprecated *)
let kfscanf ic ef fmt = kbscanf (Scanning.memo_from_channel ic) ef fmt
let fscanf ic fmt = kscanf (Scanning.memo_from_channel ic) scanf_bad_input fmt
end

module Format = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Pierre Weis, projet Cristal, INRIA Rocquencourt            *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* A pretty-printing facility and definition of formatters for 'parallel'
   (i.e. unrelated or independent) pretty-printing on multiple out channels. *)

(*
   The pretty-printing engine internal data structures.
*)

(* A devoted type for sizes to avoid confusion
   between sizes and mere integers. *)
type size

external size_of_int : int -> size = "%identity"

external int_of_size : size -> int = "%identity"


(* The pretty-printing boxes definition:
   a pretty-printing box is either
   - hbox: horizontal box (no line splitting)
   - vbox: vertical box (every break hint splits the line)
   - hvbox: horizontal/vertical box
     (the box behaves as an horizontal box if it fits on
      the current line, otherwise the box behaves as a vertical box)
   - hovbox: horizontal or vertical compacting box
     (the box is compacting material, printing as much material as possible
      on every lines)
   - box: horizontal or vertical compacting box with enhanced box structure
     (the box behaves as an horizontal or vertical box but break hints split
      the line if splitting would move to the left)
*)
type box_type = CamlinternalFormatBasics.block_type =
  | Pp_hbox | Pp_vbox | Pp_hvbox | Pp_hovbox | Pp_box | Pp_fits


(* The pretty-printing tokens definition:
   are either text to print or pretty printing
   elements that drive indentation and line splitting. *)
type pp_token =
  | Pp_text of string          (* normal text *)
  | Pp_break of int * int      (* complete break *)
  | Pp_tbreak of int * int     (* go to next tabulation *)
  | Pp_stab                    (* set a tabulation *)
  | Pp_begin of int * box_type (* beginning of a box *)
  | Pp_end                     (* end of a box *)
  | Pp_tbegin of tbox          (* beginning of a tabulation box *)
  | Pp_tend                    (* end of a tabulation box *)
  | Pp_newline                 (* to force a newline inside a box *)
  | Pp_if_newline              (* to do something only if this very
                                  line has been broken *)
  | Pp_open_tag of tag         (* opening a tag name *)
  | Pp_close_tag               (* closing the most recently open tag *)

and tag = string

and tbox = Pp_tbox of int list ref  (* Tabulation box *)


(* The pretty-printer queue definition:
   pretty-printing material is not written in the output as soon as emitted;
   instead, the material is simply recorded in the pretty-printer queue,
   until the enclosing box has a known computed size and proper splitting
   decisions can be made.

   To define the pretty-printer queue, we first define polymorphic queues,
   then pretty-printer queue elements.
*)

(* The pretty-printer queue: polymorphic queue definition. *)
type 'a queue_elem =
  | Nil
  | Cons of {
      head : 'a;
      mutable tail : 'a queue_elem;
    }


type 'a queue = {
  mutable insert : 'a queue_elem;
  mutable body : 'a queue_elem;
}


(* The pretty-printer queue: queue element definition.
   The pretty-printer queue contains formatting elements to be printed.
   Each formatting element is a tuple (size, token, length), where
   - length is the declared length of the token,
   - size is effective size of the token when it is printed
     (size is set when the size of the box is known, so that size of break
      hints are definitive). *)
type pp_queue_elem = {
  mutable elem_size : size;
  token : pp_token;
  length : int;
}


(* The pretty-printer queue definition. *)
type pp_queue = pp_queue_elem queue

(* The pretty-printer scanning stack. *)

(* The pretty-printer scanning stack: scanning element definition.
   Each element is (left_total, queue element) where left_total
   is the value of pp_left_total when the element has been enqueued. *)
type pp_scan_elem = Scan_elem of int * pp_queue_elem

(* The pretty-printer scanning stack definition. *)
type pp_scan_stack = pp_scan_elem list

(* The pretty-printer formatting stack:
   the formatting stack contains the description of all the currently active
   boxes; the pretty-printer formatting stack is used to split the lines
   while printing tokens. *)

(* The pretty-printer formatting stack: formatting stack element definition.
   Each stack element describes a pretty-printing box. *)
type pp_format_elem = Format_elem of box_type * int

(* The pretty-printer formatting stack definition. *)
type pp_format_stack = pp_format_elem list

(* The pretty-printer semantics tag stack definition. *)
type pp_tag_stack = tag list

(* The formatter definition.
   Each formatter value is a pretty-printer instance with all its
   machinery. *)
type formatter = {
  (* The various stacks. *)
  mutable pp_scan_stack : pp_scan_stack;
  mutable pp_format_stack : pp_format_stack;
  mutable pp_tbox_stack : tbox list;
  mutable pp_tag_stack : pp_tag_stack;
  mutable pp_mark_stack : pp_tag_stack;
  (* Value of right margin. *)
  mutable pp_margin : int;
  (* Minimal space left before margin, when opening a box. *)
  mutable pp_min_space_left : int;
  (* Maximum value of indentation:
     no box can be opened further. *)
  mutable pp_max_indent : int;
  (* Space remaining on the current line. *)
  mutable pp_space_left : int;
  (* Current value of indentation. *)
  mutable pp_current_indent : int;
  (* True when the line has been broken by the pretty-printer. *)
  mutable pp_is_new_line : bool;
  (* Total width of tokens already printed. *)
  mutable pp_left_total : int;
  (* Total width of tokens ever put in queue. *)
  mutable pp_right_total : int;
  (* Current number of open boxes. *)
  mutable pp_curr_depth : int;
  (* Maximum number of boxes which can be simultaneously open. *)
  mutable pp_max_boxes : int;
  (* Ellipsis string. *)
  mutable pp_ellipsis : string;
  (* Output function. *)
  mutable pp_out_string : string -> int -> int -> unit;
  (* Flushing function. *)
  mutable pp_out_flush : unit -> unit;
  (* Output of new lines. *)
  mutable pp_out_newline : unit -> unit;
  (* Output of break hints spaces. *)
  mutable pp_out_spaces : int -> unit;
  (* Output of indentation of new lines. *)
  mutable pp_out_indent : int -> unit;
  (* Are tags printed ? *)
  mutable pp_print_tags : bool;
  (* Are tags marked ? *)
  mutable pp_mark_tags : bool;
  (* Find opening and closing markers of tags. *)
  mutable pp_mark_open_tag : tag -> string;
  mutable pp_mark_close_tag : tag -> string;
  mutable pp_print_open_tag : tag -> unit;
  mutable pp_print_close_tag : tag -> unit;
  (* The pretty-printer queue. *)
  mutable pp_queue : pp_queue;
}


(* The formatter specific tag handling functions. *)
type formatter_tag_functions = {
  mark_open_tag : tag -> string;
  mark_close_tag : tag -> string;
  print_open_tag : tag -> unit;
  print_close_tag : tag -> unit;
}


(* The formatter functions to output material. *)
type formatter_out_functions = {
  out_string : string -> int -> int -> unit;
  out_flush : unit -> unit;
  out_newline : unit -> unit;
  out_spaces : int -> unit;
  out_indent : int -> unit;
}


(*

  Auxiliaries and basic functions.

*)

(* Queues auxiliaries. *)

let make_queue () = { insert = Nil; body = Nil; }

let clear_queue q = q.insert <- Nil; q.body <- Nil

let add_queue x q =
  let c = Cons { head = x; tail = Nil; } in
  match q with
  | { insert = Cons cell; body = _; } ->
    q.insert <- c; cell.tail <- c
  (* Invariant: when insert is Nil body should be Nil. *)
  | { insert = Nil; body = _; } ->
    q.insert <- c; q.body <- c


exception Empty_queue

let peek_queue = function
  | { body = Cons { head = x; tail = _; }; _ } -> x
  | { body = Nil; insert = _; } -> raise Empty_queue


let take_queue = function
  | { body = Cons { head = x; tail = tl; }; _ } as q ->
    q.body <- tl;
    if tl = Nil then q.insert <- Nil; (* Maintain the invariant. *)
    x
  | { body = Nil; insert = _; } -> raise Empty_queue


(* Enter a token in the pretty-printer queue. *)
let pp_enqueue state ({ length = len; _} as token) =
  state.pp_right_total <- state.pp_right_total + len;
  add_queue token state.pp_queue


let pp_clear_queue state =
  state.pp_left_total <- 1; state.pp_right_total <- 1;
  clear_queue state.pp_queue


(* Pp_infinity: large value for default tokens size.

   Pp_infinity is documented as being greater than 1e10; to avoid
   confusion about the word 'greater', we choose pp_infinity greater
   than 1e10 + 1; for correct handling of tests in the algorithm,
   pp_infinity must be even one more than 1e10 + 1; let's stand on the
   safe side by choosing 1.e10+10.

   Pp_infinity could probably be 1073741823 that is 2^30 - 1, that is
   the minimal upper bound for integers; now that max_int is defined,
   this limit could also be defined as max_int - 1.

   However, before setting pp_infinity to something around max_int, we
   must carefully double-check all the integer arithmetic operations
   that involve pp_infinity, since any overflow would wreck havoc the
   pretty-printing algorithm's invariants. Given that this arithmetic
   correctness check is difficult and error prone and given that 1e10
   + 1 is in practice large enough, there is no need to attempt to set
   pp_infinity to the theoretically maximum limit. It is not worth the
   burden ! *)
let pp_infinity = 1000000010

(* Output functions for the formatter. *)
let pp_output_string state s = state.pp_out_string s 0 (String.length s)
and pp_output_newline state = state.pp_out_newline ()
and pp_output_spaces state n = state.pp_out_spaces n
and pp_output_indent state n = state.pp_out_indent n

(* To format a break, indenting a new line. *)
let break_new_line state offset width =
  pp_output_newline state;
  state.pp_is_new_line <- true;
  let indent = state.pp_margin - width + offset in
  (* Don't indent more than pp_max_indent. *)
  let real_indent = min state.pp_max_indent indent in
  state.pp_current_indent <- real_indent;
  state.pp_space_left <- state.pp_margin - state.pp_current_indent;
  pp_output_indent state state.pp_current_indent


(* To force a line break inside a box: no offset is added. *)
let break_line state width = break_new_line state 0 width

(* To format a break that fits on the current line. *)
let break_same_line state width =
  state.pp_space_left <- state.pp_space_left - width;
  pp_output_spaces state width


(* To indent no more than pp_max_indent, if one tries to open a box
   beyond pp_max_indent, then the box is rejected on the left
   by simulating a break. *)
let pp_force_break_line state =
  match state.pp_format_stack with
  | Format_elem (bl_ty, width) :: _ ->
    if width > state.pp_space_left then
      (match bl_ty with
       | Pp_fits -> () | Pp_hbox -> ()
       | Pp_vbox | Pp_hvbox | Pp_hovbox | Pp_box ->
         break_line state width)
  | [] -> pp_output_newline state


(* To skip a token, if the previous line has been broken. *)
let pp_skip_token state =
  (* When calling pp_skip_token the queue cannot be empty. *)
  match take_queue state.pp_queue with
  | { elem_size = size; length = len; token = _; } ->
    state.pp_left_total <- state.pp_left_total - len;
    state.pp_space_left <- state.pp_space_left + int_of_size size


(*

  The main pretty printing functions.

*)

(* Formatting a token with a given size. *)
let format_pp_token state size = function

  | Pp_text s ->
    state.pp_space_left <- state.pp_space_left - size;
    pp_output_string state s;
    state.pp_is_new_line <- false

  | Pp_begin (off, ty) ->
    let insertion_point = state.pp_margin - state.pp_space_left in
    if insertion_point > state.pp_max_indent then
      (* can not open a box right there. *)
      begin pp_force_break_line state end;
    let offset = state.pp_space_left - off in
    let bl_type =
      begin match ty with
      | Pp_vbox -> Pp_vbox
      | Pp_hbox | Pp_hvbox | Pp_hovbox | Pp_box | Pp_fits ->
        if size > state.pp_space_left then ty else Pp_fits
      end in
    state.pp_format_stack <-
      Format_elem (bl_type, offset) :: state.pp_format_stack

  | Pp_end ->
    begin match state.pp_format_stack with
    | _ :: ls -> state.pp_format_stack <- ls
    | [] -> () (* No more box to close. *)
    end

  | Pp_tbegin (Pp_tbox _ as tbox) ->
    state.pp_tbox_stack <- tbox :: state.pp_tbox_stack

  | Pp_tend ->
    begin match state.pp_tbox_stack with
    | _ :: ls -> state.pp_tbox_stack <- ls
    | [] -> () (* No more tabulation box to close. *)
    end

  | Pp_stab ->
    begin match state.pp_tbox_stack with
    | Pp_tbox tabs :: _ ->
      let rec add_tab n = function
        | [] -> [n]
        | x :: l as ls -> if n < x then n :: ls else x :: add_tab n l in
      tabs := add_tab (state.pp_margin - state.pp_space_left) !tabs
    | [] -> () (* No open tabulation box. *)
    end

  | Pp_tbreak (n, off) ->
    let insertion_point = state.pp_margin - state.pp_space_left in
    begin match state.pp_tbox_stack with
    | Pp_tbox tabs :: _ ->
      let rec find n = function
        | x :: l -> if x >= n then x else find n l
        | [] -> raise Not_found in
      let tab =
        match !tabs with
        | x :: _ ->
          begin
            try find insertion_point !tabs with
            | Not_found -> x
          end
        | _ -> insertion_point in
      let offset = tab - insertion_point in
      if offset >= 0
      then break_same_line state (offset + n)
      else break_new_line state (tab + off) state.pp_margin
    | [] -> () (* No open tabulation box. *)
    end

  | Pp_newline ->
    begin match state.pp_format_stack with
    | Format_elem (_, width) :: _ -> break_line state width
    | [] -> pp_output_newline state (* No open box. *)
    end

  | Pp_if_newline ->
    if state.pp_current_indent != state.pp_margin - state.pp_space_left
    then pp_skip_token state

  | Pp_break (n, off) ->
    begin match state.pp_format_stack with
    | Format_elem (ty, width) :: _ ->
      begin match ty with
      | Pp_hovbox ->
        if size > state.pp_space_left
        then break_new_line state off width
        else break_same_line state n
      | Pp_box ->
        (* Have the line just been broken here ? *)
        if state.pp_is_new_line then break_same_line state n else
        if size > state.pp_space_left
         then break_new_line state off width else
        (* break the line here leads to new indentation ? *)
        if state.pp_current_indent > state.pp_margin - width + off
        then break_new_line state off width
        else break_same_line state n
      | Pp_hvbox -> break_new_line state off width
      | Pp_fits -> break_same_line state n
      | Pp_vbox -> break_new_line state off width
      | Pp_hbox -> break_same_line state n
      end
    | [] -> () (* No open box. *)
    end

   | Pp_open_tag tag_name ->
     let marker = state.pp_mark_open_tag tag_name in
     pp_output_string state marker;
     state.pp_mark_stack <- tag_name :: state.pp_mark_stack

   | Pp_close_tag ->
     begin match state.pp_mark_stack with
     | tag_name :: tags ->
       let marker = state.pp_mark_close_tag tag_name in
       pp_output_string state marker;
       state.pp_mark_stack <- tags
     | [] -> () (* No more tag to close. *)
     end


(* Print if token size is known else printing is delayed.
   Size is known when not negative.
   Printing is delayed when the text waiting in the queue requires
   more room to format than exists on the current line.

   Note: [advance_loop] must be tail recursive to prevent stack overflows. *)
let rec advance_loop state =
  match peek_queue state.pp_queue with
  | {elem_size = size; token = tok; length = len} ->
    let size = int_of_size size in
    if not
         (size < 0 &&
          (state.pp_right_total - state.pp_left_total < state.pp_space_left))
    then begin
      ignore (take_queue state.pp_queue);
      format_pp_token state (if size < 0 then pp_infinity else size) tok;
      state.pp_left_total <- len + state.pp_left_total;
      advance_loop state
    end


let advance_left state =
  try advance_loop state with
  | Empty_queue -> ()


(* To enqueue a token : try to advance. *)
let enqueue_advance state tok = pp_enqueue state tok; advance_left state

(* Building pretty-printer queue elements. *)
let make_queue_elem size tok len =
  { elem_size = size; token = tok; length = len; }


(* To enqueue strings. *)
let enqueue_string_as state size s =
  let len = int_of_size size in
  enqueue_advance state (make_queue_elem size (Pp_text s) len)


let enqueue_string state s =
  let len = String.length s in
  enqueue_string_as state (size_of_int len) s


(* Routines for scan stack
   determine size of boxes. *)

(* The scan_stack is never empty. *)
let scan_stack_bottom =
  let q_elem = make_queue_elem (size_of_int (-1)) (Pp_text "") 0 in
  [Scan_elem (-1, q_elem)]


(* Clearing the pretty-printer scanning stack. *)
let clear_scan_stack state = state.pp_scan_stack <- scan_stack_bottom

(* Setting the size of boxes on scan stack:
   if ty = true then size of break is set else size of box is set;
   in each case pp_scan_stack is popped.

   Note:
   Pattern matching on scan stack is exhaustive, since scan_stack is never
   empty.
   Pattern matching on token in scan stack is also exhaustive,
   since scan_push is used on breaks and opening of boxes. *)
let set_size state ty =
  match state.pp_scan_stack with
  | Scan_elem
      (left_tot,
       ({ elem_size = size; token = tok; length = _; } as queue_elem)) :: t ->
    let size = int_of_size size in
    (* test if scan stack contains any data that is not obsolete. *)
    if left_tot < state.pp_left_total then clear_scan_stack state else
      begin match tok with
      | Pp_break (_, _) | Pp_tbreak (_, _) ->
        if ty then
        begin
          queue_elem.elem_size <- size_of_int (state.pp_right_total + size);
          state.pp_scan_stack <- t
        end
      | Pp_begin (_, _) ->
        if not ty then
        begin
          queue_elem.elem_size <- size_of_int (state.pp_right_total + size);
          state.pp_scan_stack <- t
        end
      | Pp_text _ | Pp_stab | Pp_tbegin _ | Pp_tend | Pp_end
      | Pp_newline | Pp_if_newline
      | Pp_open_tag _ | Pp_close_tag ->
        () (* scan_push is only used for breaks and boxes. *)
      end
  | [] -> () (* scan_stack is never empty. *)


(* Push a token on pretty-printer scanning stack.
   If b is true set_size is called. *)
let scan_push state b tok =
  pp_enqueue state tok;
  if b then set_size state true;
  state.pp_scan_stack <-
    Scan_elem (state.pp_right_total, tok) :: state.pp_scan_stack


(* To open a new box :
   the user may set the depth bound pp_max_boxes
   any text nested deeper is printed as the ellipsis string. *)
let pp_open_box_gen state indent br_ty =
  state.pp_curr_depth <- state.pp_curr_depth + 1;
  if state.pp_curr_depth < state.pp_max_boxes then
    let elem =
      make_queue_elem
        (size_of_int (- state.pp_right_total))
        (Pp_begin (indent, br_ty))
        0 in
    scan_push state false elem else
  if state.pp_curr_depth = state.pp_max_boxes
  then enqueue_string state state.pp_ellipsis


(* The box which is always open. *)
let pp_open_sys_box state = pp_open_box_gen state 0 Pp_hovbox

(* Close a box, setting sizes of its sub boxes. *)
let pp_close_box state () =
  if state.pp_curr_depth > 1 then
  begin
    if state.pp_curr_depth < state.pp_max_boxes then
    begin
      pp_enqueue state
        { elem_size = size_of_int 0; token = Pp_end; length = 0; };
      set_size state true; set_size state false
    end;
    state.pp_curr_depth <- state.pp_curr_depth - 1;
  end


(* Open a tag, pushing it on the tag stack. *)
let pp_open_tag state tag_name =
  if state.pp_print_tags then
  begin
    state.pp_tag_stack <- tag_name :: state.pp_tag_stack;
    state.pp_print_open_tag tag_name
  end;
  if state.pp_mark_tags then
    pp_enqueue state {
      elem_size = size_of_int 0;
      token = Pp_open_tag tag_name;
      length = 0;
    }


(* Close a tag, popping it from the tag stack. *)
let pp_close_tag state () =
  if state.pp_mark_tags then
    pp_enqueue state {
      elem_size = size_of_int 0;
      token = Pp_close_tag;
      length = 0;
    };
  if state.pp_print_tags then
  begin
    match state.pp_tag_stack with
    | tag_name :: tags ->
      state.pp_print_close_tag tag_name;
      state.pp_tag_stack <- tags
    | _ -> () (* No more tag to close. *)
  end


let pp_set_print_tags state b = state.pp_print_tags <- b
let pp_set_mark_tags state b = state.pp_mark_tags <- b
let pp_get_print_tags state () = state.pp_print_tags
let pp_get_mark_tags state () = state.pp_mark_tags
let pp_set_tags state b =
  pp_set_print_tags state b; pp_set_mark_tags state b


(* Handling tag handling functions: get/set functions. *)
let pp_get_formatter_tag_functions state () = {
  mark_open_tag = state.pp_mark_open_tag;
  mark_close_tag = state.pp_mark_close_tag;
  print_open_tag = state.pp_print_open_tag;
  print_close_tag = state.pp_print_close_tag;
}


let pp_set_formatter_tag_functions state {
     mark_open_tag = mot;
     mark_close_tag = mct;
     print_open_tag = pot;
     print_close_tag = pct;
  } =
  state.pp_mark_open_tag <- mot;
  state.pp_mark_close_tag <- mct;
  state.pp_print_open_tag <- pot;
  state.pp_print_close_tag <- pct


(* Initialize pretty-printer. *)
let pp_rinit state =
  pp_clear_queue state;
  clear_scan_stack state;
  state.pp_format_stack <- [];
  state.pp_tbox_stack <- [];
  state.pp_tag_stack <- [];
  state.pp_mark_stack <- [];
  state.pp_current_indent <- 0;
  state.pp_curr_depth <- 0;
  state.pp_space_left <- state.pp_margin;
  pp_open_sys_box state

let clear_tag_stack state =
  List.iter
    (fun _ -> pp_close_tag state ())
    state.pp_tag_stack


(* Flushing pretty-printer queue. *)
let pp_flush_queue state b =
  clear_tag_stack state;
  while state.pp_curr_depth > 1 do
    pp_close_box state ()
  done;
  state.pp_right_total <- pp_infinity;
  advance_left state;
  if b then pp_output_newline state;
  pp_rinit state

(*

  Procedures to format values and use boxes.

*)

(* To format a string. *)
let pp_print_as_size state size s =
  if state.pp_curr_depth < state.pp_max_boxes
  then enqueue_string_as state size s


let pp_print_as state isize s =
  pp_print_as_size state (size_of_int isize) s


let pp_print_string state s =
  pp_print_as state (String.length s) s


(* To format an integer. *)
let pp_print_int state i = pp_print_string state (string_of_int i)

(* To format a float. *)
let pp_print_float state f = pp_print_string state (string_of_float f)

(* To format a boolean. *)
let pp_print_bool state b = pp_print_string state (string_of_bool b)

(* To format a char. *)
let pp_print_char state c =
  pp_print_as state 1 (String.make 1 c)


(* Opening boxes. *)
let pp_open_hbox state () = pp_open_box_gen state 0 Pp_hbox
and pp_open_vbox state indent = pp_open_box_gen state indent Pp_vbox

and pp_open_hvbox state indent = pp_open_box_gen state indent Pp_hvbox
and pp_open_hovbox state indent = pp_open_box_gen state indent Pp_hovbox
and pp_open_box state indent = pp_open_box_gen state indent Pp_box


(* Printing queued text.

   [pp_print_flush] prints all pending items in the pretty-printer queue and
   then flushes the low level output device of the formatter to actually
   display printing material.

   [pp_print_newline] behaves as [pp_print_flush] after printing an additional
   new line. *)
let pp_print_newline state () =
  pp_flush_queue state true; state.pp_out_flush ()
and pp_print_flush state () =
  pp_flush_queue state false; state.pp_out_flush ()


(* To get a newline when one does not want to close the current box. *)
let pp_force_newline state () =
  if state.pp_curr_depth < state.pp_max_boxes then
    enqueue_advance state (make_queue_elem (size_of_int 0) Pp_newline 0)


(* To format something, only in case the line has just been broken. *)
let pp_print_if_newline state () =
  if state.pp_curr_depth < state.pp_max_boxes then
    enqueue_advance state (make_queue_elem (size_of_int 0) Pp_if_newline 0)


(* Printing break hints:
   A break hint indicates where a box may be broken.
   If line is broken then offset is added to the indentation of the current
   box else (the value of) width blanks are printed. *)
let pp_print_break state width offset =
  if state.pp_curr_depth < state.pp_max_boxes then
    let elem =
      make_queue_elem
        (size_of_int (- state.pp_right_total))
        (Pp_break (width, offset))
        width in
    scan_push state true elem


(* Print a space :
   a space is a break hint that prints a single space if the break does not
   split the line;
   a cut is a break hint that prints nothing if the break does not split the
   line. *)
let pp_print_space state () = pp_print_break state 1 0
and pp_print_cut state () = pp_print_break state 0 0


(* Tabulation boxes. *)
let pp_open_tbox state () =
  state.pp_curr_depth <- state.pp_curr_depth + 1;
  if state.pp_curr_depth < state.pp_max_boxes then
    let elem =
      make_queue_elem (size_of_int 0) (Pp_tbegin (Pp_tbox (ref []))) 0 in
    enqueue_advance state elem


(* Close a tabulation box. *)
let pp_close_tbox state () =
  if state.pp_curr_depth > 1 then
  begin
   if state.pp_curr_depth < state.pp_max_boxes then
     let elem = make_queue_elem (size_of_int 0) Pp_tend 0 in
     enqueue_advance state elem;
     state.pp_curr_depth <- state.pp_curr_depth - 1
  end


(* Print a tabulation break. *)
let pp_print_tbreak state width offset =
  if state.pp_curr_depth < state.pp_max_boxes then
    let elem =
      make_queue_elem
        (size_of_int (- state.pp_right_total))
        (Pp_tbreak (width, offset))
        width in
    scan_push state true elem


let pp_print_tab state () = pp_print_tbreak state 0 0

let pp_set_tab state () =
  if state.pp_curr_depth < state.pp_max_boxes then
    let elem =
      make_queue_elem (size_of_int 0) Pp_stab 0 in
    enqueue_advance state elem


(*

  Procedures to control the pretty-printers

*)

(* Set_max_boxes. *)
let pp_set_max_boxes state n = if n > 1 then state.pp_max_boxes <- n

(* To know the current maximum number of boxes allowed. *)
let pp_get_max_boxes state () = state.pp_max_boxes

let pp_over_max_boxes state () = state.pp_curr_depth = state.pp_max_boxes

(* Ellipsis. *)
let pp_set_ellipsis_text state s = state.pp_ellipsis <- s
and pp_get_ellipsis_text state () = state.pp_ellipsis


(* To set the margin of pretty-printer. *)
let pp_limit n =
  if n < pp_infinity then n else pred pp_infinity


(* Internal pretty-printer functions. *)
let pp_set_min_space_left state n =
  if n >= 1 then
    let n = pp_limit n in
    state.pp_min_space_left <- n;
    state.pp_max_indent <- state.pp_margin - state.pp_min_space_left;
    pp_rinit state


(* Initially, we have :
   pp_max_indent = pp_margin - pp_min_space_left, and
   pp_space_left = pp_margin. *)
let pp_set_max_indent state n =
  pp_set_min_space_left state (state.pp_margin - n)


let pp_get_max_indent state () = state.pp_max_indent

let pp_set_margin state n =
  if n >= 1 then
    let n = pp_limit n in
    state.pp_margin <- n;
    let new_max_indent =
      (* Try to maintain max_indent to its actual value. *)
      if state.pp_max_indent <= state.pp_margin
      then state.pp_max_indent else
      (* If possible maintain pp_min_space_left to its actual value,
         if this leads to a too small max_indent, take half of the
         new margin, if it is greater than 1. *)
       max (max (state.pp_margin - state.pp_min_space_left)
                (state.pp_margin / 2)) 1 in
    (* Rebuild invariants. *)
    pp_set_max_indent state new_max_indent


let pp_get_margin state () = state.pp_margin

(* Setting a formatter basic output functions. *)
let pp_set_formatter_out_functions state {
      out_string = f;
      out_flush = g;
      out_newline = h;
      out_spaces = i;
      out_indent = j;
    } =
  state.pp_out_string <- f;
  state.pp_out_flush <- g;
  state.pp_out_newline <- h;
  state.pp_out_spaces <- i;
  state.pp_out_indent <- j

let pp_get_formatter_out_functions state () = {
  out_string = state.pp_out_string;
  out_flush = state.pp_out_flush;
  out_newline = state.pp_out_newline;
  out_spaces = state.pp_out_spaces;
  out_indent = state.pp_out_indent;
}


(* Setting a formatter basic string output and flush functions. *)
let pp_set_formatter_output_functions state f g =
  state.pp_out_string <- f; state.pp_out_flush <- g

let pp_get_formatter_output_functions state () =
  (state.pp_out_string, state.pp_out_flush)


(* The default function to output new lines. *)
let display_newline state () = state.pp_out_string "\n" 0  1

(* The default function to output spaces. *)
let blank = Char.chr(32)
let blank_line = String.make 80 blank
let rec display_blanks state n =
  if n > 0 then
  if n <= 80 then state.pp_out_string blank_line 0 n else
  begin
    state.pp_out_string blank_line 0 80;
    display_blanks state (n - 80)
  end


(* The default function to output indentation of new lines. *)
let display_indent = display_blanks

(* Setting a formatter basic output functions as printing to a given
   [Pervasive.out_channel] value. *)
let pp_set_formatter_out_channel state oc =
  state.pp_out_string <- output_substring oc;
  state.pp_out_flush <- (fun () -> flush oc);
  state.pp_out_newline <- display_newline state;
  state.pp_out_spaces <- display_blanks state;
  state.pp_out_indent <- display_indent state

(*

  Defining specific formatters

*)

let default_pp_mark_open_tag s = "<" ^ s ^ ">"
let default_pp_mark_close_tag s = "</" ^ s ^ ">"

let default_pp_print_open_tag = ignore
let default_pp_print_close_tag = ignore

(* Building a formatter given its basic output functions.
   Other fields get reasonable default values. *)
let pp_make_formatter f g h i j =
  (* The initial state of the formatter contains a dummy box. *)
  let pp_queue = make_queue () in
  let sys_tok =
    make_queue_elem (size_of_int (-1)) (Pp_begin (0, Pp_hovbox)) 0 in
  add_queue sys_tok pp_queue;
  let sys_scan_stack =
    Scan_elem (1, sys_tok) :: scan_stack_bottom in
  let pp_margin = 78
  and pp_min_space_left = 10 in
  {
    pp_scan_stack = sys_scan_stack;
    pp_format_stack = [];
    pp_tbox_stack = [];
    pp_tag_stack = [];
    pp_mark_stack = [];
    pp_margin = pp_margin;
    pp_min_space_left = pp_min_space_left;
    pp_max_indent = pp_margin - pp_min_space_left;
    pp_space_left = pp_margin;
    pp_current_indent = 0;
    pp_is_new_line = true;
    pp_left_total = 1;
    pp_right_total = 1;
    pp_curr_depth = 1;
    pp_max_boxes = max_int;
    pp_ellipsis = ".";
    pp_out_string = f;
    pp_out_flush = g;
    pp_out_newline = h;
    pp_out_spaces = i;
    pp_out_indent = j;
    pp_print_tags = false;
    pp_mark_tags = false;
    pp_mark_open_tag = default_pp_mark_open_tag;
    pp_mark_close_tag = default_pp_mark_close_tag;
    pp_print_open_tag = default_pp_print_open_tag;
    pp_print_close_tag = default_pp_print_close_tag;
    pp_queue = pp_queue;
  }


(* Build a formatter out of its out functions. *)
let formatter_of_out_functions out_funs =
  pp_make_formatter
    out_funs.out_string
    out_funs.out_flush
    out_funs.out_newline
    out_funs.out_spaces
    out_funs.out_indent


(* Make a formatter with default functions to output spaces,
  indentation, and new lines. *)
let make_formatter output flush =
  let ppf = pp_make_formatter output flush ignore ignore ignore in
  ppf.pp_out_newline <- display_newline ppf;
  ppf.pp_out_spaces <- display_blanks ppf;
  ppf.pp_out_indent <- display_indent ppf;
  ppf


(* Make a formatter writing to a given [Pervasive.out_channel] value. *)
let formatter_of_out_channel oc =
  make_formatter (output_substring oc) (fun () -> flush oc)


(* Make a formatter writing to a given [Buffer.t] value. *)
let formatter_of_buffer b =
  make_formatter (Buffer.add_substring b) ignore


(* Allocating buffer for pretty-printing purposes.
   Default buffer size is pp_buffer_size or 512.
*)
let pp_buffer_size = 512
let pp_make_buffer () = Buffer.create pp_buffer_size

(* The standard (shared) buffer. *)
let stdbuf = pp_make_buffer ()

(* Predefined formatters standard formatter to print
   to [Pervasives.stdout], [Pervasives.stderr], and {!stdbuf}. *)
let std_formatter = formatter_of_out_channel Pervasives.stdout
and err_formatter = formatter_of_out_channel Pervasives.stderr
and str_formatter = formatter_of_buffer stdbuf


(* [flush_buffer_formatter buf ppf] flushes formatter [ppf],
   then returns the contents of buffer [buf] that is reset.
   Formatter [ppf] is supposed to print to buffer [buf], otherwise this
   function is not really useful. *)
let flush_buffer_formatter buf ppf =
  pp_flush_queue ppf false;
  let s = Buffer.contents buf in
  Buffer.reset buf;
  s


(* Flush [str_formatter] and get the contents of [stdbuf]. *)
let flush_str_formatter () = flush_buffer_formatter stdbuf str_formatter

(*
  Symbolic pretty-printing
*)

(*
  Symbolic pretty-printing is pretty-printing with no low level output.

  When using a symbolic formatter, all regular pretty-printing activities
  occur but output material is symbolic and stored in a buffer of output
  items. At the end of pretty-printing, flushing the output buffer allows
  post-processing of symbolic output before low level output operations.
*)

type symbolic_output_item =
  | Output_flush
  | Output_newline
  | Output_string of string
  | Output_spaces of int
  | Output_indent of int

type symbolic_output_buffer = {
  mutable symbolic_output_contents : symbolic_output_item list;
}

let make_symbolic_output_buffer () =
  { symbolic_output_contents = [] }

let clear_symbolic_output_buffer sob =
  sob.symbolic_output_contents <- []

let get_symbolic_output_buffer sob =
  List.rev sob.symbolic_output_contents

let flush_symbolic_output_buffer sob =
  let items = get_symbolic_output_buffer sob in
  clear_symbolic_output_buffer sob;
  items

let add_symbolic_output_item sob item =
  sob.symbolic_output_contents <- item :: sob.symbolic_output_contents

let formatter_of_symbolic_output_buffer sob =
  let symbolic_flush sob () =
    add_symbolic_output_item sob Output_flush
  and symbolic_newline sob () =
    add_symbolic_output_item sob Output_newline
  and symbolic_string sob s i n =
    add_symbolic_output_item sob (Output_string (String.sub s i n))
  and symbolic_spaces sob n =
    add_symbolic_output_item sob (Output_spaces n)
  and symbolic_indent sob n =
    add_symbolic_output_item sob (Output_indent n) in

  let f = symbolic_string sob
  and g = symbolic_flush sob
  and h = symbolic_newline sob
  and i = symbolic_spaces sob
  and j = symbolic_indent sob in
  pp_make_formatter f g h i j

(*

  Basic functions on the 'standard' formatter
  (the formatter that prints to [Pervasives.stdout]).

*)

let open_hbox = pp_open_hbox std_formatter
and open_vbox = pp_open_vbox std_formatter
and open_hvbox = pp_open_hvbox std_formatter
and open_hovbox = pp_open_hovbox std_formatter
and open_box = pp_open_box std_formatter
and close_box = pp_close_box std_formatter
and open_tag = pp_open_tag std_formatter
and close_tag = pp_close_tag std_formatter
and print_as = pp_print_as std_formatter
and print_string = pp_print_string std_formatter
and print_int = pp_print_int std_formatter
and print_float = pp_print_float std_formatter
and print_char = pp_print_char std_formatter
and print_bool = pp_print_bool std_formatter
and print_break = pp_print_break std_formatter
and print_cut = pp_print_cut std_formatter
and print_space = pp_print_space std_formatter
and force_newline = pp_force_newline std_formatter
and print_flush = pp_print_flush std_formatter
and print_newline = pp_print_newline std_formatter
and print_if_newline = pp_print_if_newline std_formatter

and open_tbox = pp_open_tbox std_formatter
and close_tbox = pp_close_tbox std_formatter
and print_tbreak = pp_print_tbreak std_formatter

and set_tab = pp_set_tab std_formatter
and print_tab = pp_print_tab std_formatter

and set_margin = pp_set_margin std_formatter
and get_margin = pp_get_margin std_formatter

and set_max_indent = pp_set_max_indent std_formatter
and get_max_indent = pp_get_max_indent std_formatter

and set_max_boxes = pp_set_max_boxes std_formatter
and get_max_boxes = pp_get_max_boxes std_formatter
and over_max_boxes = pp_over_max_boxes std_formatter

and set_ellipsis_text = pp_set_ellipsis_text std_formatter
and get_ellipsis_text = pp_get_ellipsis_text std_formatter

and set_formatter_out_channel =
  pp_set_formatter_out_channel std_formatter

and set_formatter_out_functions =
  pp_set_formatter_out_functions std_formatter
and get_formatter_out_functions =
  pp_get_formatter_out_functions std_formatter

and set_formatter_output_functions =
  pp_set_formatter_output_functions std_formatter
and get_formatter_output_functions =
  pp_get_formatter_output_functions std_formatter

and set_formatter_tag_functions =
  pp_set_formatter_tag_functions std_formatter
and get_formatter_tag_functions =
  pp_get_formatter_tag_functions std_formatter
and set_print_tags =
  pp_set_print_tags std_formatter
and get_print_tags =
  pp_get_print_tags std_formatter
and set_mark_tags =
  pp_set_mark_tags std_formatter
and get_mark_tags =
  pp_get_mark_tags std_formatter
and set_tags =
  pp_set_tags std_formatter


(* Convenience functions *)

(* To format a list *)
let rec pp_print_list ?(pp_sep = pp_print_cut) pp_v ppf = function
  | [] -> ()
  | [v] -> pp_v ppf v
  | v :: vs ->
    pp_v ppf v;
    pp_sep ppf ();
    pp_print_list ~pp_sep pp_v ppf vs

(* To format free-flowing text *)
let pp_print_text ppf s =
  let len = String.length s in
  let left = ref 0 in
  let right = ref 0 in
  let flush () =
    pp_print_string ppf (String.sub s !left (!right - !left));
    incr right; left := !right;
  in
  while (!right <> len) do
    match s.[!right] with
      | '\n' ->
        flush ();
        pp_force_newline ppf ()
      | ' ' ->
        flush (); pp_print_space ppf ()
      (* there is no specific support for '\t'
         as it is unclear what a right semantics would be *)
      | _ -> incr right
  done;
  if !left <> len then flush ()

 (**************************************************************)

let compute_tag output tag_acc =
  let buf = Buffer.create 16 in
  let ppf = formatter_of_buffer buf in
  output ppf tag_acc;
  pp_print_flush ppf ();
  let len = Buffer.length buf in
  if len < 2 then Buffer.contents buf
  else Buffer.sub buf 1 (len - 2)

 (**************************************************************

  Defining continuations to be passed as arguments of
  CamlinternalFormat.make_printf.

  **************************************************************)

open CamlinternalFormatBasics
open CamlinternalFormat

(* Interpret a formatting entity on a formatter. *)
let output_formatting_lit ppf fmting_lit = match fmting_lit with
  | Close_box                 -> pp_close_box ppf ()
  | Close_tag                 -> pp_close_tag ppf ()
  | Break (_, width, offset)  -> pp_print_break ppf width offset
  | FFlush                    -> pp_print_flush ppf ()
  | Force_newline             -> pp_force_newline ppf ()
  | Flush_newline             -> pp_print_newline ppf ()
  | Magic_size (_, _)         -> ()
  | Escaped_at                -> pp_print_char ppf '@'
  | Escaped_percent           -> pp_print_char ppf '%'
  | Scan_indic c              -> pp_print_char ppf '@'; pp_print_char ppf c

(* Recursively output an "accumulator" containing a reversed list of
   printing entities (string, char, flus, ...) in an output_stream. *)
(* Differ from Printf.output_acc by the interpretation of formatting. *)
(* Used as a continuation of CamlinternalFormat.make_printf. *)
let rec output_acc ppf acc = match acc with
  | Acc_string_literal (Acc_formatting_lit (p, Magic_size (_, size)), s)
  | Acc_data_string (Acc_formatting_lit (p, Magic_size (_, size)), s) ->
    output_acc ppf p;
    pp_print_as_size ppf (size_of_int size) s;
  | Acc_char_literal (Acc_formatting_lit (p, Magic_size (_, size)), c)
  | Acc_data_char (Acc_formatting_lit (p, Magic_size (_, size)), c) ->
    output_acc ppf p;
    pp_print_as_size ppf (size_of_int size) (String.make 1 c);
  | Acc_formatting_lit (p, f) ->
    output_acc ppf p;
    output_formatting_lit ppf f;
  | Acc_formatting_gen (p, Acc_open_tag acc') ->
    output_acc ppf p;
    pp_open_tag ppf (compute_tag output_acc acc')
  | Acc_formatting_gen (p, Acc_open_box acc') ->
    output_acc ppf p;
    let (indent, bty) = open_box_of_string (compute_tag output_acc acc') in
    pp_open_box_gen ppf indent bty
  | Acc_string_literal (p, s)
  | Acc_data_string (p, s)   -> output_acc ppf p; pp_print_string ppf s;
  | Acc_char_literal (p, c)
  | Acc_data_char (p, c)     -> output_acc ppf p; pp_print_char ppf c;
  | Acc_delay (p, f)         -> output_acc ppf p; f ppf;
  | Acc_flush p              -> output_acc ppf p; pp_print_flush ppf ();
  | Acc_invalid_arg (p, msg) -> output_acc ppf p; invalid_arg msg;
  | End_of_acc               -> ()

(* Recursively output an "accumulator" containing a reversed list of
   printing entities (string, char, flus, ...) in a buffer. *)
(* Differ from Printf.bufput_acc by the interpretation of formatting. *)
(* Used as a continuation of CamlinternalFormat.make_printf. *)
let rec strput_acc ppf acc = match acc with
  | Acc_string_literal (Acc_formatting_lit (p, Magic_size (_, size)), s)
  | Acc_data_string (Acc_formatting_lit (p, Magic_size (_, size)), s) ->
    strput_acc ppf p;
    pp_print_as_size ppf (size_of_int size) s;
  | Acc_char_literal (Acc_formatting_lit (p, Magic_size (_, size)), c)
  | Acc_data_char (Acc_formatting_lit (p, Magic_size (_, size)), c) ->
    strput_acc ppf p;
    pp_print_as_size ppf (size_of_int size) (String.make 1 c);
  | Acc_delay (Acc_formatting_lit (p, Magic_size (_, size)), f) ->
    strput_acc ppf p;
    pp_print_as_size ppf (size_of_int size) (f ());
  | Acc_formatting_lit (p, f) ->
    strput_acc ppf p;
    output_formatting_lit ppf f;
  | Acc_formatting_gen (p, Acc_open_tag acc') ->
    strput_acc ppf p;
    pp_open_tag ppf (compute_tag strput_acc acc')
  | Acc_formatting_gen (p, Acc_open_box acc') ->
    strput_acc ppf p;
    let (indent, bty) = open_box_of_string (compute_tag strput_acc acc') in
    pp_open_box_gen ppf indent bty
  | Acc_string_literal (p, s)
  | Acc_data_string (p, s)   -> strput_acc ppf p; pp_print_string ppf s;
  | Acc_char_literal (p, c)
  | Acc_data_char (p, c)     -> strput_acc ppf p; pp_print_char ppf c;
  | Acc_delay (p, f)         -> strput_acc ppf p; pp_print_string ppf (f ());
  | Acc_flush p              -> strput_acc ppf p; pp_print_flush ppf ();
  | Acc_invalid_arg (p, msg) -> strput_acc ppf p; invalid_arg msg;
  | End_of_acc               -> ()

(*

  Defining [fprintf] and various flavors of [fprintf].

*)

let kfprintf k ppf (Format (fmt, _)) =
  make_printf
    (fun ppf acc -> output_acc ppf acc; k ppf)
    ppf End_of_acc fmt

and ikfprintf k ppf (Format (fmt, _)) =
  make_iprintf k ppf fmt

let fprintf ppf = kfprintf ignore ppf
let ifprintf ppf = ikfprintf ignore ppf
let printf fmt = fprintf std_formatter fmt
let eprintf fmt = fprintf err_formatter fmt

let ksprintf k (Format (fmt, _)) =
  let b = pp_make_buffer () in
  let ppf = formatter_of_buffer b in
  let k () acc =
    strput_acc ppf acc;
    k (flush_buffer_formatter b ppf) in
  make_printf k () End_of_acc fmt


let sprintf fmt = ksprintf (fun s -> s) fmt

let kasprintf k (Format (fmt, _)) =
  let b = pp_make_buffer () in
  let ppf = formatter_of_buffer b in
  let k ppf acc =
    output_acc ppf acc;
    k (flush_buffer_formatter b ppf) in
  make_printf k ppf End_of_acc fmt


let asprintf fmt = kasprintf (fun s -> s) fmt

(* Output everything left in the pretty printer queue at end of execution. *)
let () = at_exit print_flush


(*

  Deprecated stuff.

*)

(* Deprecated : subsumed by pp_set_formatter_out_functions *)
let pp_set_all_formatter_output_functions state
    ~out:f ~flush:g ~newline:h ~spaces:i =
  pp_set_formatter_output_functions state f g;
  state.pp_out_newline <- h;
  state.pp_out_spaces <- i

(* Deprecated : subsumed by pp_get_formatter_out_functions *)
let pp_get_all_formatter_output_functions state () =
  (state.pp_out_string, state.pp_out_flush,
   state.pp_out_newline, state.pp_out_spaces)


(* Deprecated : subsumed by set_formatter_out_functions *)
let set_all_formatter_output_functions =
  pp_set_all_formatter_output_functions std_formatter


(* Deprecated : subsumed by get_formatter_out_functions *)
let get_all_formatter_output_functions =
  pp_get_all_formatter_output_functions std_formatter


(* Deprecated : error prone function, do not use it.
   This function is neither compositional nor incremental, since it flushes
   the pretty-printer queue at each call.
   To get the same functionality, define a formatter of your own writing to
   the buffer argument, as in
   let ppf = formatter_of_buffer b
   then use {!fprintf ppf} as usual. *)
let bprintf b (Format (fmt, _) : ('a, formatter, unit) format) =
  let k ppf acc = output_acc ppf acc; pp_flush_queue ppf false in
  make_printf k (formatter_of_buffer b) End_of_acc fmt


(* Deprecated : alias for ksprintf. *)
let kprintf = ksprintf
end

module Obj = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Operations on internal representations of values *)

type t

external repr : 'a -> t = "%identity"
external obj : t -> 'a = "%identity"
external magic : 'a -> 'b = "%identity"
external is_int : t -> bool = "%obj_is_int"
let [@inline always] is_block a = not (is_int a)
external tag : t -> int = "caml_obj_tag"
external set_tag : t -> int -> unit = "caml_obj_set_tag"
external size : t -> int = "%obj_size"
external reachable_words : t -> int = "caml_obj_reachable_words"
external field : t -> int -> t = "%obj_field"
external set_field : t -> int -> t -> unit = "%obj_set_field"
external floatarray_get : floatarray -> int -> float = "caml_floatarray_get"
external floatarray_set :
    floatarray -> int -> float -> unit = "caml_floatarray_set"
let [@inline always] double_field x i = floatarray_get (obj x : floatarray) i
let [@inline always] set_double_field x i v =
  floatarray_set (obj x : floatarray) i v
external new_block : int -> int -> t = "caml_obj_block"
external dup : t -> t = "caml_obj_dup"
external truncate : t -> int -> unit = "caml_obj_truncate"
external add_offset : t -> Int32.t -> t = "caml_obj_add_offset"

let marshal (obj : t) =
  Marshal.to_bytes obj []
let unmarshal str pos =
  (Marshal.from_bytes str pos, pos + Marshal.total_size str pos)

let first_non_constant_constructor_tag = 0
let last_non_constant_constructor_tag = 245

let lazy_tag = 246
let closure_tag = 247
let object_tag = 248
let infix_tag = 249
let forward_tag = 250

let no_scan_tag = 251

let abstract_tag = 251
let string_tag = 252
let double_tag = 253
let double_array_tag = 254
let custom_tag = 255
let final_tag = custom_tag


let int_tag = 1000
let out_of_heap_tag = 1001
let unaligned_tag = 1002

let extension_constructor x =
  let x = repr x in
  let slot =
    if (is_block x) && (tag x) <> object_tag && (size x) >= 1 then field x 0
    else x
  in
  let name =
    if (is_block slot) && (tag slot) = object_tag then field slot 0
    else invalid_arg "Obj.extension_constructor"
  in
    if (tag name) = string_tag then (obj slot : extension_constructor)
    else invalid_arg "Obj.extension_constructor"

let [@inline always] extension_name (slot : extension_constructor) =
  (obj (field (repr slot) 0) : string)

let [@inline always] extension_id (slot : extension_constructor) =
  (obj (field (repr slot) 1) : int)

module Ephemeron = struct
  type obj_t = t

  type t (** ephemeron *)

  external create: int -> t = "caml_ephe_create"

  let length x = size(repr x) - 2

  external get_key: t -> int -> obj_t option = "caml_ephe_get_key"
  external get_key_copy: t -> int -> obj_t option = "caml_ephe_get_key_copy"
  external set_key: t -> int -> obj_t -> unit = "caml_ephe_set_key"
  external unset_key: t -> int -> unit = "caml_ephe_unset_key"
  external check_key: t -> int -> bool = "caml_ephe_check_key"
  external blit_key : t -> int -> t -> int -> int -> unit
    = "caml_ephe_blit_key"

  external get_data: t -> obj_t option = "caml_ephe_get_data"
  external get_data_copy: t -> obj_t option = "caml_ephe_get_data_copy"
  external set_data: t -> obj_t -> unit = "caml_ephe_set_data"
  external unset_data: t -> unit = "caml_ephe_unset_data"
  external check_data: t -> bool = "caml_ephe_check_data"
  external blit_data : t -> t -> unit = "caml_ephe_blit_data"


end
end

module Gc = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Damien Doligez, projet Para, INRIA Rocquencourt            *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

type stat = {
  minor_words : float;
  promoted_words : float;
  major_words : float;
  minor_collections : int;
  major_collections : int;
  heap_words : int;
  heap_chunks : int;
  live_words : int;
  live_blocks : int;
  free_words : int;
  free_blocks : int;
  largest_free : int;
  fragments : int;
  compactions : int;
  top_heap_words : int;
  stack_size : int;
}

type control = {
  mutable minor_heap_size : int;
  mutable major_heap_increment : int;
  mutable space_overhead : int;
  mutable verbose : int;
  mutable max_overhead : int;
  mutable stack_limit : int;
  mutable allocation_policy : int;
  window_size : int;
}

external stat : unit -> stat = "caml_gc_stat"
external quick_stat : unit -> stat = "caml_gc_quick_stat"
external counters : unit -> (float * float * float) = "caml_gc_counters"
external minor_words : unit -> (float [@unboxed])
  = "caml_gc_minor_words" "caml_gc_minor_words_unboxed"
external get : unit -> control = "caml_gc_get"
external set : control -> unit = "caml_gc_set"
external minor : unit -> unit = "caml_gc_minor"
external major_slice : int -> int = "caml_gc_major_slice"
external major : unit -> unit = "caml_gc_major"
external full_major : unit -> unit = "caml_gc_full_major"
external compact : unit -> unit = "caml_gc_compaction"
external get_minor_free : unit -> int = "caml_get_minor_free"
external get_bucket : int -> int = "caml_get_major_bucket" [@@noalloc]
external get_credit : unit -> int = "caml_get_major_credit" [@@noalloc]
external huge_fallback_count : unit -> int = "caml_gc_huge_fallback_count"

open Printf

let print_stat c =
  let st = stat () in
  fprintf c "minor_collections: %d\n" st.minor_collections;
  fprintf c "major_collections: %d\n" st.major_collections;
  fprintf c "compactions:       %d\n" st.compactions;
  fprintf c "\n";
  let l1 = String.length (sprintf "%.0f" st.minor_words) in
  fprintf c "minor_words:    %*.0f\n" l1 st.minor_words;
  fprintf c "promoted_words: %*.0f\n" l1 st.promoted_words;
  fprintf c "major_words:    %*.0f\n" l1 st.major_words;
  fprintf c "\n";
  let l2 = String.length (sprintf "%d" st.top_heap_words) in
  fprintf c "top_heap_words: %*d\n" l2 st.top_heap_words;
  fprintf c "heap_words:     %*d\n" l2 st.heap_words;
  fprintf c "live_words:     %*d\n" l2 st.live_words;
  fprintf c "free_words:     %*d\n" l2 st.free_words;
  fprintf c "largest_free:   %*d\n" l2 st.largest_free;
  fprintf c "fragments:      %*d\n" l2 st.fragments;
  fprintf c "\n";
  fprintf c "live_blocks: %d\n" st.live_blocks;
  fprintf c "free_blocks: %d\n" st.free_blocks;
  fprintf c "heap_chunks: %d\n" st.heap_chunks


let allocated_bytes () =
  let (mi, pro, ma) = counters () in
  (mi +. ma -. pro) *. float_of_int (Sys.word_size / 8)


external finalise : ('a -> unit) -> 'a -> unit = "caml_final_register"
external finalise_last : (unit -> unit) -> 'a -> unit =
  "caml_final_register_called_without_value"
external finalise_release : unit -> unit = "caml_final_release"


type alarm = bool ref
type alarm_rec = {active : alarm; f : unit -> unit}

let rec call_alarm arec =
  if !(arec.active) then begin
    finalise call_alarm arec;
    arec.f ();
  end


let create_alarm f =
  let arec = { active = ref true; f = f } in
  finalise call_alarm arec;
  arec.active


let delete_alarm a = a := false
end

module CamlinternalOO = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*          Jerome Vouillon, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 2002 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Obj

(**** Object representation ****)

external set_id: 'a -> 'a = "caml_set_oo_id" [@@noalloc]

(**** Object copy ****)

let copy o =
  let o = (Obj.obj (Obj.dup (Obj.repr o))) in
  set_id o

(**** Compression options ****)
(* Parameters *)
type params = {
    mutable compact_table : bool;
    mutable copy_parent : bool;
    mutable clean_when_copying : bool;
    mutable retry_count : int;
    mutable bucket_small_size : int
  }

let params = {
  compact_table = true;
  copy_parent = true;
  clean_when_copying = true;
  retry_count = 3;
  bucket_small_size = 16
}

(**** Parameters ****)

let initial_object_size = 2

(**** Items ****)

type item = DummyA | DummyB | DummyC of int
let _ = [DummyA; DummyB; DummyC 0] (* to avoid warnings *)

let dummy_item = (magic () : item)

(**** Types ****)

type tag
type label = int
type closure = item
type t = DummyA | DummyB | DummyC of int
let _ = [DummyA; DummyB; DummyC 0] (* to avoid warnings *)

type obj = t array
external ret : (obj -> 'a) -> closure = "%identity"

(**** Labels ****)

let public_method_label s : tag =
  let accu = ref 0 in
  for i = 0 to String.length s - 1 do
    accu := 223 * !accu + Char.code s.[i]
  done;
  (* reduce to 31 bits *)
  accu := !accu land (1 lsl 31 - 1);
  (* make it signed for 64 bits architectures *)
  let tag = if !accu > 0x3FFFFFFF then !accu - (1 lsl 31) else !accu in
  (* Printf.eprintf "%s = %d\n" s tag; flush stderr; *)
  magic tag

(**** Sparse array ****)

module Vars =
  Map.Make(struct type t = string let compare (x:t) y = compare x y end)
type vars = int Vars.t

module Meths =
  Map.Make(struct type t = string let compare (x:t) y = compare x y end)
type meths = label Meths.t
module Labs =
  Map.Make(struct type t = label let compare (x:t) y = compare x y end)
type labs = bool Labs.t

(* The compiler assumes that the first field of this structure is [size]. *)
type table =
 { mutable size: int;
   mutable methods: closure array;
   mutable methods_by_name: meths;
   mutable methods_by_label: labs;
   mutable previous_states:
     (meths * labs * (label * item) list * vars *
      label list * string list) list;
   mutable hidden_meths: (label * item) list;
   mutable vars: vars;
   mutable initializers: (obj -> unit) list }

let dummy_table =
  { methods = [| dummy_item |];
    methods_by_name = Meths.empty;
    methods_by_label = Labs.empty;
    previous_states = [];
    hidden_meths = [];
    vars = Vars.empty;
    initializers = [];
    size = 0 }

let table_count = ref 0

(* dummy_met should be a pointer, so use an atom *)
let dummy_met : item = obj (Obj.new_block 0 0)
(* if debugging is needed, this could be a good idea: *)
(* let dummy_met () = failwith "Undefined method" *)

let rec fit_size n =
  if n <= 2 then n else
  fit_size ((n+1)/2) * 2

let new_table pub_labels =
  incr table_count;
  let len = Array.length pub_labels in
  let methods = Array.make (len*2+2) dummy_met in
  methods.(0) <- magic len;
  methods.(1) <- magic (fit_size len * Sys.word_size / 8 - 1);
  for i = 0 to len - 1 do methods.(i*2+3) <- magic pub_labels.(i) done;
  { methods = methods;
    methods_by_name = Meths.empty;
    methods_by_label = Labs.empty;
    previous_states = [];
    hidden_meths = [];
    vars = Vars.empty;
    initializers = [];
    size = initial_object_size }

let resize array new_size =
  let old_size = Array.length array.methods in
  if new_size > old_size then begin
    let new_buck = Array.make new_size dummy_met in
    Array.blit array.methods 0 new_buck 0 old_size;
    array.methods <- new_buck
 end

let put array label element =
  resize array (label + 1);
  array.methods.(label) <- element

(**** Classes ****)

let method_count = ref 0
let inst_var_count = ref 0

(* type t *)
type meth = item

let new_method table =
  let index = Array.length table.methods in
  resize table (index + 1);
  index

let get_method_label table name =
  try
    Meths.find name table.methods_by_name
  with Not_found ->
    let label = new_method table in
    table.methods_by_name <- Meths.add name label table.methods_by_name;
    table.methods_by_label <- Labs.add label true table.methods_by_label;
    label

let get_method_labels table names =
  Array.map (get_method_label table) names

let set_method table label element =
  incr method_count;
  if Labs.find label table.methods_by_label then
    put table label element
  else
    table.hidden_meths <- (label, element) :: table.hidden_meths

let get_method table label =
  try List.assoc label table.hidden_meths
  with Not_found -> table.methods.(label)

let to_list arr =
  if arr == magic 0 then [] else Array.to_list arr

let narrow table vars virt_meths concr_meths =
  let vars = to_list vars
  and virt_meths = to_list virt_meths
  and concr_meths = to_list concr_meths in
  let virt_meth_labs = List.map (get_method_label table) virt_meths in
  let concr_meth_labs = List.map (get_method_label table) concr_meths in
  table.previous_states <-
     (table.methods_by_name, table.methods_by_label, table.hidden_meths,
      table.vars, virt_meth_labs, vars)
     :: table.previous_states;
  table.vars <-
    Vars.fold
      (fun lab info tvars ->
        if List.mem lab vars then Vars.add lab info tvars else tvars)
      table.vars Vars.empty;
  let by_name = ref Meths.empty in
  let by_label = ref Labs.empty in
  List.iter2
    (fun met label ->
       by_name := Meths.add met label !by_name;
       by_label :=
          Labs.add label
            (try Labs.find label table.methods_by_label with Not_found -> true)
            !by_label)
    concr_meths concr_meth_labs;
  List.iter2
    (fun met label ->
       by_name := Meths.add met label !by_name;
       by_label := Labs.add label false !by_label)
    virt_meths virt_meth_labs;
  table.methods_by_name <- !by_name;
  table.methods_by_label <- !by_label;
  table.hidden_meths <-
     List.fold_right
       (fun ((lab, _) as met) hm ->
          if List.mem lab virt_meth_labs then hm else met::hm)
       table.hidden_meths
       []

let widen table =
  let (by_name, by_label, saved_hidden_meths, saved_vars, virt_meths, vars) =
    List.hd table.previous_states
  in
  table.previous_states <- List.tl table.previous_states;
  table.vars <-
     List.fold_left
       (fun s v -> Vars.add v (Vars.find v table.vars) s)
       saved_vars vars;
  table.methods_by_name <- by_name;
  table.methods_by_label <- by_label;
  table.hidden_meths <-
     List.fold_right
       (fun ((lab, _) as met) hm ->
          if List.mem lab virt_meths then hm else met::hm)
       table.hidden_meths
       saved_hidden_meths

let new_slot table =
  let index = table.size in
  table.size <- index + 1;
  index

let new_variable table name =
  try Vars.find name table.vars
  with Not_found ->
    let index = new_slot table in
    if name <> "" then table.vars <- Vars.add name index table.vars;
    index

let to_array arr =
  if arr = Obj.magic 0 then [||] else arr

let new_methods_variables table meths vals =
  let meths = to_array meths in
  let nmeths = Array.length meths and nvals = Array.length vals in
  let res = Array.make (nmeths + nvals) 0 in
  for i = 0 to nmeths - 1 do
    res.(i) <- get_method_label table meths.(i)
  done;
  for i = 0 to nvals - 1 do
    res.(i+nmeths) <- new_variable table vals.(i)
  done;
  res

let get_variable table name =
  try Vars.find name table.vars with Not_found -> assert false

let get_variables table names =
  Array.map (get_variable table) names

let add_initializer table f =
  table.initializers <- f::table.initializers

(*
module Keys =
  Map.Make(struct type t = tag array let compare (x:t) y = compare x y end)
let key_map = ref Keys.empty
let get_key tags : item =
  try magic (Keys.find tags !key_map : tag array)
  with Not_found ->
    key_map := Keys.add tags tags !key_map;
    magic tags
*)

let create_table public_methods =
  if public_methods == magic 0 then new_table [||] else
  (* [public_methods] must be in ascending order for bytecode *)
  let tags = Array.map public_method_label public_methods in
  let table = new_table tags in
  Array.iteri
    (fun i met ->
      let lab = i*2+2 in
      table.methods_by_name  <- Meths.add met lab table.methods_by_name;
      table.methods_by_label <- Labs.add lab true table.methods_by_label)
    public_methods;
  table

let init_class table =
  inst_var_count := !inst_var_count + table.size - 1;
  table.initializers <- List.rev table.initializers;
  resize table (3 + magic table.methods.(1) * 16 / Sys.word_size)

let inherits cla vals virt_meths concr_meths (_, super, _, env) top =
  narrow cla vals virt_meths concr_meths;
  let init =
    if top then super cla env else Obj.repr (super cla) in
  widen cla;
  Array.concat
    [[| repr init |];
     magic (Array.map (get_variable cla) (to_array vals) : int array);
     Array.map
       (fun nm -> repr (get_method cla (get_method_label cla nm) : closure))
       (to_array concr_meths) ]

let make_class pub_meths class_init =
  let table = create_table pub_meths in
  let env_init = class_init table in
  init_class table;
  (env_init (Obj.repr 0), class_init, env_init, Obj.repr 0)

type init_table = { mutable env_init: t; mutable class_init: table -> t }

let make_class_store pub_meths class_init init_table =
  let table = create_table pub_meths in
  let env_init = class_init table in
  init_class table;
  init_table.class_init <- class_init;
  init_table.env_init <- env_init

let dummy_class loc =
  let undef = fun _ -> raise (Undefined_recursive_module loc) in
  (Obj.magic undef, undef, undef, Obj.repr 0)

(**** Objects ****)

let create_object table =
  (* XXX Appel de [obj_block] | Call to [obj_block]  *)
  let obj = Obj.new_block Obj.object_tag table.size in
  (* XXX Appel de [caml_modify] | Call to [caml_modify] *)
  Obj.set_field obj 0 (Obj.repr table.methods);
  Obj.obj (set_id obj)

let create_object_opt obj_0 table =
  if (Obj.magic obj_0 : bool) then obj_0 else begin
    (* XXX Appel de [obj_block] | Call to [obj_block]  *)
    let obj = Obj.new_block Obj.object_tag table.size in
    (* XXX Appel de [caml_modify] | Call to [caml_modify] *)
    Obj.set_field obj 0 (Obj.repr table.methods);
    Obj.obj (set_id obj)
  end

let rec iter_f obj =
  function
    []   -> ()
  | f::l -> f obj; iter_f obj l

let run_initializers obj table =
  let inits = table.initializers in
  if inits <> [] then
    iter_f obj inits

let run_initializers_opt obj_0 obj table =
  if (Obj.magic obj_0 : bool) then obj else begin
    let inits = table.initializers in
    if inits <> [] then iter_f obj inits;
    obj
  end

let create_object_and_run_initializers obj_0 table =
  if (Obj.magic obj_0 : bool) then obj_0 else begin
    let obj = create_object table in
    run_initializers obj table;
    obj
  end

(* Equivalent primitive below
let sendself obj lab =
  (magic obj : (obj -> t) array array).(0).(lab) obj
*)
external send : obj -> tag -> 'a = "%send"
external sendcache : obj -> tag -> t -> int -> 'a = "%sendcache"
external sendself : obj -> label -> 'a = "%sendself"
external get_public_method : obj -> tag -> closure
    = "caml_get_public_method" [@@noalloc]

(**** table collection access ****)

type tables =
  | Empty
  | Cons of {key : closure; mutable data: tables; mutable next: tables}

let set_data tables v = match tables with
  | Empty -> assert false
  | Cons tables -> tables.data <- v
let set_next tables v = match tables with
  | Empty -> assert false
  | Cons tables -> tables.next <- v
let get_key = function
  | Empty -> assert false
  | Cons tables -> tables.key
let get_data = function
  | Empty -> assert false
  | Cons tables -> tables.data
let get_next = function
  | Empty -> assert false
  | Cons tables -> tables.next

let build_path n keys tables =
  let res = Cons {key = Obj.magic 0; data = Empty; next = Empty} in
  let r = ref res in
  for i = 0 to n do
    r := Cons {key = keys.(i); data = !r; next = Empty}
  done;
  set_data tables !r;
  res

let rec lookup_keys i keys tables =
  if i < 0 then tables else
  let key = keys.(i) in
  let rec lookup_key (tables:tables) =
    if get_key tables == key then
      match get_data tables with
      | Empty -> assert false
      | Cons _ as tables_data ->
          lookup_keys (i-1) keys tables_data
    else
      match get_next tables with
      | Cons _ as next -> lookup_key next
      | Empty ->
          let next : tables = Cons {key; data = Empty; next = Empty} in
          set_next tables next;
          build_path (i-1) keys next
  in
  lookup_key tables

let lookup_tables root keys =
  match get_data root with
  | Cons _ as root_data ->
    lookup_keys (Array.length keys - 1) keys root_data
  | Empty ->
    build_path (Array.length keys - 1) keys root

(**** builtin methods ****)

let get_const x = ret (fun _obj -> x)
let get_var n   = ret (fun obj -> Array.unsafe_get obj n)
let get_env e n =
  ret (fun obj ->
    Array.unsafe_get (Obj.magic (Array.unsafe_get obj e) : obj) n)
let get_meth n  = ret (fun obj -> sendself obj n)
let set_var n   = ret (fun obj x -> Array.unsafe_set obj n x)
let app_const f x = ret (fun _obj -> f x)
let app_var f n   = ret (fun obj -> f (Array.unsafe_get obj n))
let app_env f e n =
  ret (fun obj ->
    f (Array.unsafe_get (Obj.magic (Array.unsafe_get obj e) : obj) n))
let app_meth f n  = ret (fun obj -> f (sendself obj n))
let app_const_const f x y = ret (fun _obj -> f x y)
let app_const_var f x n   = ret (fun obj -> f x (Array.unsafe_get obj n))
let app_const_meth f x n = ret (fun obj -> f x (sendself obj n))
let app_var_const f n x = ret (fun obj -> f (Array.unsafe_get obj n) x)
let app_meth_const f n x = ret (fun obj -> f (sendself obj n) x)
let app_const_env f x e n =
  ret (fun obj ->
    f x (Array.unsafe_get (Obj.magic (Array.unsafe_get obj e) : obj) n))
let app_env_const f e n x =
  ret (fun obj ->
    f (Array.unsafe_get (Obj.magic (Array.unsafe_get obj e) : obj) n) x)
let meth_app_const n x = ret (fun obj -> (sendself obj n : _ -> _) x)
let meth_app_var n m =
  ret (fun obj -> (sendself obj n : _ -> _) (Array.unsafe_get obj m))
let meth_app_env n e m =
  ret (fun obj -> (sendself obj n : _ -> _)
      (Array.unsafe_get (Obj.magic (Array.unsafe_get obj e) : obj) m))
let meth_app_meth n m =
  ret (fun obj -> (sendself obj n : _ -> _) (sendself obj m))
let send_const m x c =
  ret (fun obj -> sendcache x m (Array.unsafe_get obj 0) c)
let send_var m n c =
  ret (fun obj ->
    sendcache (Obj.magic (Array.unsafe_get obj n) : obj) m
      (Array.unsafe_get obj 0) c)
let send_env m e n c =
  ret (fun obj ->
    sendcache
      (Obj.magic (Array.unsafe_get
                    (Obj.magic (Array.unsafe_get obj e) : obj) n) : obj)
      m (Array.unsafe_get obj 0) c)
let send_meth m n c =
  ret (fun obj ->
    sendcache (sendself obj n) m (Array.unsafe_get obj 0) c)
let new_cache table =
  let n = new_method table in
  let n =
    if n mod 2 = 0 || n > 2 + magic table.methods.(1) * 16 / Sys.word_size
    then n else new_method table
  in
  table.methods.(n) <- Obj.magic 0;
  n

type impl =
    GetConst
  | GetVar
  | GetEnv
  | GetMeth
  | SetVar
  | AppConst
  | AppVar
  | AppEnv
  | AppMeth
  | AppConstConst
  | AppConstVar
  | AppConstEnv
  | AppConstMeth
  | AppVarConst
  | AppEnvConst
  | AppMethConst
  | MethAppConst
  | MethAppVar
  | MethAppEnv
  | MethAppMeth
  | SendConst
  | SendVar
  | SendEnv
  | SendMeth
  | Closure of closure

let method_impl table i arr =
  let next () = incr i; magic arr.(!i) in
  match next() with
    GetConst -> let x : t = next() in get_const x
  | GetVar   -> let n = next() in get_var n
  | GetEnv   -> let e = next() in let n = next() in get_env e n
  | GetMeth  -> let n = next() in get_meth n
  | SetVar   -> let n = next() in set_var n
  | AppConst -> let f = next() in let x = next() in app_const f x
  | AppVar   -> let f = next() in let n = next () in app_var f n
  | AppEnv   ->
      let f = next() in  let e = next() in let n = next() in
      app_env f e n
  | AppMeth  -> let f = next() in let n = next () in app_meth f n
  | AppConstConst ->
      let f = next() in let x = next() in let y = next() in
      app_const_const f x y
  | AppConstVar ->
      let f = next() in let x = next() in let n = next() in
      app_const_var f x n
  | AppConstEnv ->
      let f = next() in let x = next() in let e = next () in let n = next() in
      app_const_env f x e n
  | AppConstMeth ->
      let f = next() in let x = next() in let n = next() in
      app_const_meth f x n
  | AppVarConst ->
      let f = next() in let n = next() in let x = next() in
      app_var_const f n x
  | AppEnvConst ->
      let f = next() in let e = next () in let n = next() in let x = next() in
      app_env_const f e n x
  | AppMethConst ->
      let f = next() in let n = next() in let x = next() in
      app_meth_const f n x
  | MethAppConst ->
      let n = next() in let x = next() in meth_app_const n x
  | MethAppVar ->
      let n = next() in let m = next() in meth_app_var n m
  | MethAppEnv ->
      let n = next() in let e = next() in let m = next() in
      meth_app_env n e m
  | MethAppMeth ->
      let n = next() in let m = next() in meth_app_meth n m
  | SendConst ->
      let m = next() in let x = next() in send_const m x (new_cache table)
  | SendVar ->
      let m = next() in let n = next () in send_var m n (new_cache table)
  | SendEnv ->
      let m = next() in let e = next() in let n = next() in
      send_env m e n (new_cache table)
  | SendMeth ->
      let m = next() in let n = next () in send_meth m n (new_cache table)
  | Closure _ as clo -> magic clo

let set_methods table methods =
  let len = Array.length methods in let i = ref 0 in
  while !i < len do
    let label = methods.(!i) in let clo = method_impl table i methods in
    set_method table label clo;
    incr i
  done

(**** Statistics ****)

type stats =
  { classes: int; methods: int; inst_vars: int; }

let stats () =
  { classes = !table_count;
    methods = !method_count; inst_vars = !inst_var_count; }
end

module Oo = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*          Jerome Vouillon, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

let copy = CamlinternalOO.copy
external id : < .. > -> int = "%field1"
let new_method = CamlinternalOO.public_method_label
let public_method_label = CamlinternalOO.public_method_label
end

module CamlinternalLazy = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Damien Doligez, projet Para, INRIA Rocquencourt            *)
(*                                                                        *)
(*   Copyright 1997 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Internals of forcing lazy values. *)

exception Undefined

let raise_undefined = Obj.repr (fun () -> raise Undefined)

(* Assume [blk] is a block with tag lazy *)
let force_lazy_block (blk : 'arg lazy_t) =
  let closure = (Obj.obj (Obj.field (Obj.repr blk) 0) : unit -> 'arg) in
  Obj.set_field (Obj.repr blk) 0 raise_undefined;
  try
    let result = closure () in
    (* do set_field BEFORE set_tag *)
    Obj.set_field (Obj.repr blk) 0 (Obj.repr result);
    Obj.set_tag (Obj.repr blk) Obj.forward_tag;
    result
  with e ->
    Obj.set_field (Obj.repr blk) 0 (Obj.repr (fun () -> raise e));
    raise e


(* Assume [blk] is a block with tag lazy *)
let force_val_lazy_block (blk : 'arg lazy_t) =
  let closure = (Obj.obj (Obj.field (Obj.repr blk) 0) : unit -> 'arg) in
  Obj.set_field (Obj.repr blk) 0 raise_undefined;
  let result = closure () in
  (* do set_field BEFORE set_tag *)
  Obj.set_field (Obj.repr blk) 0 (Obj.repr result);
  Obj.set_tag (Obj.repr blk) (Obj.forward_tag);
  result


(* [force] is not used, since [Lazy.force] is declared as a primitive
   whose code inlines the tag tests of its argument.  This function is
   here for the sake of completeness, and for debugging purpose. *)

let force (lzv : 'arg lazy_t) =
  let x = Obj.repr lzv in
  let t = Obj.tag x in
  if t = Obj.forward_tag then (Obj.obj (Obj.field x 0) : 'arg) else
  if t <> Obj.lazy_tag then (Obj.obj x : 'arg)
  else force_lazy_block lzv


let force_val (lzv : 'arg lazy_t) =
  let x = Obj.repr lzv in
  let t = Obj.tag x in
  if t = Obj.forward_tag then (Obj.obj (Obj.field x 0) : 'arg) else
  if t <> Obj.lazy_tag then (Obj.obj x : 'arg)
  else force_val_lazy_block lzv
end

module Lazy = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Damien Doligez, projet Para, INRIA Rocquencourt            *)
(*                                                                        *)
(*   Copyright 1997 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Module [Lazy]: deferred computations *)


(*
   WARNING: some purple magic is going on here.  Do not take this file
   as an example of how to program in OCaml.
*)


(* We make use of two special tags provided by the runtime:
   [lazy_tag] and [forward_tag].

   A value of type ['a Lazy.t] can be one of three things:
   1. A block of size 1 with tag [lazy_tag].  Its field is a closure of
      type [unit -> 'a] that computes the value.
   2. A block of size 1 with tag [forward_tag].  Its field is the value
      of type ['a] that was computed.
   3. Anything else except a float.  This has type ['a] and is the value
      that was computed.
   Exceptions are stored in format (1).
   The GC will magically change things from (2) to (3) according to its
   fancy.

   If OCaml was configured with the -flat-float-array option (which is
   currently the default), the following is also true:
   We cannot use representation (3) for a [float Lazy.t] because
   [caml_make_array] assumes that only a [float] value can have tag
   [Double_tag].

   We have to use the built-in type constructor [lazy_t] to
   let the compiler implement the special typing and compilation
   rules for the [lazy] keyword.
*)

type 'a t = 'a lazy_t

exception Undefined = CamlinternalLazy.Undefined

external make_forward : 'a -> 'a lazy_t = "caml_lazy_make_forward"

external force : 'a t -> 'a = "%lazy_force"

(* let force = force *)

let force_val = CamlinternalLazy.force_val

let from_fun (f : unit -> 'arg) =
  let x = Obj.new_block Obj.lazy_tag 1 in
  Obj.set_field x 0 (Obj.repr f);
  (Obj.obj x : 'arg t)


let from_val (v : 'arg) =
  let t = Obj.tag (Obj.repr v) in
  if t = Obj.forward_tag || t = Obj.lazy_tag || t = Obj.double_tag then begin
    make_forward v
  end else begin
    (Obj.magic v : 'arg t)
  end


let is_val (l : 'arg t) = Obj.tag (Obj.repr l) <> Obj.lazy_tag

let lazy_from_fun = from_fun

let lazy_from_val = from_val

let lazy_is_val = is_val
end

module Printexc = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Printf

let printers = ref []

let locfmt = format_of_string "File \"%s\", line %d, characters %d-%d: %s"

let field x i =
  let f = Obj.field x i in
  if not (Obj.is_block f) then
    sprintf "%d" (Obj.magic f : int)           (* can also be a char *)
  else if Obj.tag f = Obj.string_tag then
    sprintf "%S" (Obj.magic f : string)
  else if Obj.tag f = Obj.double_tag then
    string_of_float (Obj.magic f : float)
  else
    "_"

let rec other_fields x i =
  if i >= Obj.size x then ""
  else sprintf ", %s%s" (field x i) (other_fields x (i+1))

let fields x =
  match Obj.size x with
  | 0 -> ""
  | 1 -> ""
  | 2 -> sprintf "(%s)" (field x 1)
  | _ -> sprintf "(%s%s)" (field x 1) (other_fields x 2)

let to_string x =
  let rec conv = function
    | hd :: tl ->
        (match try hd x with _ -> None with
        | Some s -> s
        | None -> conv tl)
    | [] ->
        match x with
        | Out_of_memory -> "Out of memory"
        | Stack_overflow -> "Stack overflow"
        | Match_failure(file, line, char) ->
            sprintf locfmt file line char (char+5) "Pattern matching failed"
        | Assert_failure(file, line, char) ->
            sprintf locfmt file line char (char+6) "Assertion failed"
        | Undefined_recursive_module(file, line, char) ->
            sprintf locfmt file line char (char+6) "Undefined recursive module"
        | _ ->
            let x = Obj.repr x in
            if Obj.tag x <> 0 then
              (Obj.magic (Obj.field x 0) : string)
            else
              let constructor =
                (Obj.magic (Obj.field (Obj.field x 0) 0) : string) in
              constructor ^ (fields x) in
  conv !printers

let print fct arg =
  try
    fct arg
  with x ->
    eprintf "Uncaught exception: %s\n" (to_string x);
    flush stderr;
    raise x

let catch fct arg =
  try
    fct arg
  with x ->
    flush stdout;
    eprintf "Uncaught exception: %s\n" (to_string x);
    exit 2

type raw_backtrace_slot
type raw_backtrace

external get_raw_backtrace:
  unit -> raw_backtrace = "caml_get_exception_raw_backtrace"

external raise_with_backtrace: exn -> raw_backtrace -> 'a
  = "%raise_with_backtrace"

type backtrace_slot =
  | Known_location of {
      is_raise    : bool;
      filename    : string;
      line_number : int;
      start_char  : int;
      end_char    : int;
      is_inline   : bool;
    }
  | Unknown_location of {
      is_raise : bool
    }

(* to avoid warning *)
let _ = [Known_location { is_raise = false; filename = "";
                          line_number = 0; start_char = 0; end_char = 0;
                          is_inline = false };
         Unknown_location { is_raise = false }]

external convert_raw_backtrace_slot:
  raw_backtrace_slot -> backtrace_slot = "caml_convert_raw_backtrace_slot"

external convert_raw_backtrace:
  raw_backtrace -> backtrace_slot array = "caml_convert_raw_backtrace"

let convert_raw_backtrace bt =
  try Some (convert_raw_backtrace bt)
  with Failure _ -> None

let format_backtrace_slot pos slot =
  let info is_raise =
    if is_raise then
      if pos = 0 then "Raised at" else "Re-raised at"
    else
      if pos = 0 then "Raised by primitive operation at" else "Called from"
  in
  match slot with
  | Unknown_location l ->
      if l.is_raise then
        (* compiler-inserted re-raise, skipped *) None
      else
        Some (sprintf "%s unknown location" (info false))
  | Known_location l ->
      Some (sprintf "%s file \"%s\"%s, line %d, characters %d-%d"
              (info l.is_raise) l.filename
              (if l.is_inline then " (inlined)" else "")
              l.line_number l.start_char l.end_char)

let print_exception_backtrace outchan backtrace =
  match backtrace with
  | None ->
      fprintf outchan
        "(Program not linked with -g, cannot print stack backtrace)\n"
  | Some a ->
      for i = 0 to Array.length a - 1 do
        match format_backtrace_slot i a.(i) with
          | None -> ()
          | Some str -> fprintf outchan "%s\n" str
      done

let print_raw_backtrace outchan raw_backtrace =
  print_exception_backtrace outchan (convert_raw_backtrace raw_backtrace)

(* confusingly named: prints the global current backtrace *)
let print_backtrace outchan =
  print_raw_backtrace outchan (get_raw_backtrace ())

let backtrace_to_string backtrace =
  match backtrace with
  | None ->
     "(Program not linked with -g, cannot print stack backtrace)\n"
  | Some a ->
      let b = Buffer.create 1024 in
      for i = 0 to Array.length a - 1 do
        match format_backtrace_slot i a.(i) with
          | None -> ()
          | Some str -> bprintf b "%s\n" str
      done;
      Buffer.contents b

let raw_backtrace_to_string raw_backtrace =
  backtrace_to_string (convert_raw_backtrace raw_backtrace)

let backtrace_slot_is_raise = function
  | Known_location l -> l.is_raise
  | Unknown_location l -> l.is_raise

let backtrace_slot_is_inline = function
  | Known_location l -> l.is_inline
  | Unknown_location _ -> false

type location = {
  filename : string;
  line_number : int;
  start_char : int;
  end_char : int;
}

let backtrace_slot_location = function
  | Unknown_location _ -> None
  | Known_location l ->
    Some {
      filename    = l.filename;
      line_number = l.line_number;
      start_char  = l.start_char;
      end_char    = l.end_char;
    }

let backtrace_slots raw_backtrace =
  (* The documentation of this function guarantees that Some is
     returned only if a part of the trace is usable. This gives us
     a bit more work than just convert_raw_backtrace, but it makes the
     API more user-friendly -- otherwise most users would have to
     reimplement the "Program not linked with -g, sorry" logic
     themselves. *)
  match convert_raw_backtrace raw_backtrace with
    | None -> None
    | Some backtrace ->
      let usable_slot = function
        | Unknown_location _ -> false
        | Known_location _ -> true in
      let rec exists_usable = function
        | (-1) -> false
        | i -> usable_slot backtrace.(i) || exists_usable (i - 1) in
      if exists_usable (Array.length backtrace - 1)
      then Some backtrace
      else None

module Slot = struct
  type t = backtrace_slot
  let format = format_backtrace_slot
  let is_raise = backtrace_slot_is_raise
  let is_inline = backtrace_slot_is_inline
  let location = backtrace_slot_location
end

external raw_backtrace_length :
  raw_backtrace -> int = "caml_raw_backtrace_length" [@@noalloc]

external get_raw_backtrace_slot :
  raw_backtrace -> int -> raw_backtrace_slot = "caml_raw_backtrace_slot"

external get_raw_backtrace_next_slot :
  raw_backtrace_slot -> raw_backtrace_slot option
  = "caml_raw_backtrace_next_slot"

(* confusingly named:
   returns the *string* corresponding to the global current backtrace *)
let get_backtrace () = raw_backtrace_to_string (get_raw_backtrace ())

external record_backtrace: bool -> unit = "caml_record_backtrace"
external backtrace_status: unit -> bool = "caml_backtrace_status"

let register_printer fn =
  printers := fn :: !printers

external get_callstack: int -> raw_backtrace = "caml_get_current_callstack"

let exn_slot x =
  let x = Obj.repr x in
  if Obj.tag x = 0 then Obj.field x 0 else x

let exn_slot_id x =
  let slot = exn_slot x in
  (Obj.obj (Obj.field slot 1) : int)

let exn_slot_name x =
  let slot = exn_slot x in
  (Obj.obj (Obj.field slot 0) : string)


let uncaught_exception_handler = ref None

let set_uncaught_exception_handler fn = uncaught_exception_handler := Some fn

let empty_backtrace : raw_backtrace = Obj.obj (Obj.new_block Obj.abstract_tag 0)

let try_get_raw_backtrace () =
  try
    get_raw_backtrace ()
  with _ (* Out_of_memory? *) ->
    empty_backtrace

let handle_uncaught_exception' exn debugger_in_use =
  try
    (* Get the backtrace now, in case one of the [at_exit] function
       destroys it. *)
    let raw_backtrace =
      if debugger_in_use (* Same test as in [byterun/printexc.c] *) then
        empty_backtrace
      else
        try_get_raw_backtrace ()
    in
    (try Pervasives.do_at_exit () with _ -> ());
    match !uncaught_exception_handler with
    | None ->
        eprintf "Fatal error: exception %s\n" (to_string exn);
        print_raw_backtrace stderr raw_backtrace;
        flush stderr
    | Some handler ->
        try
          handler exn raw_backtrace
        with exn' ->
          let raw_backtrace' = try_get_raw_backtrace () in
          eprintf "Fatal error: exception %s\n" (to_string exn);
          print_raw_backtrace stderr raw_backtrace;
          eprintf "Fatal error in uncaught exception handler: exception %s\n"
            (to_string exn');
          print_raw_backtrace stderr raw_backtrace';
          flush stderr
  with
    | Out_of_memory ->
        prerr_endline
          "Fatal error: out of memory in uncaught exception handler"

(* This function is called by [caml_fatal_uncaught_exception] in
   [byterun/printexc.c] which expects no exception is raised. *)
let handle_uncaught_exception exn debugger_in_use =
  try
    handle_uncaught_exception' exn debugger_in_use
  with _ ->
    (* There is not much we can do at this point *)
    ()

external register_named_value : string -> 'a -> unit
  = "caml_register_named_value"

let () =
  register_named_value "Printexc.handle_uncaught_exception"
    handle_uncaught_exception
end

module Array = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Array operations *)

external length : 'a array -> int = "%array_length"
external get: 'a array -> int -> 'a = "%array_safe_get"
external set: 'a array -> int -> 'a -> unit = "%array_safe_set"
external unsafe_get: 'a array -> int -> 'a = "%array_unsafe_get"
external unsafe_set: 'a array -> int -> 'a -> unit = "%array_unsafe_set"
external make: int -> 'a -> 'a array = "caml_make_vect"
external create: int -> 'a -> 'a array = "caml_make_vect"
external unsafe_sub : 'a array -> int -> int -> 'a array = "caml_array_sub"
external append_prim : 'a array -> 'a array -> 'a array = "caml_array_append"
external concat : 'a array list -> 'a array = "caml_array_concat"
external unsafe_blit :
  'a array -> int -> 'a array -> int -> int -> unit = "caml_array_blit"
external create_float: int -> float array = "caml_make_float_vect"
let make_float = create_float

module Floatarray = struct
  external create : int -> floatarray = "caml_floatarray_create"
  external length : floatarray -> int = "%floatarray_length"
  external get : floatarray -> int -> float = "%floatarray_safe_get"
  external set : floatarray -> int -> float -> unit = "%floatarray_safe_set"
  external unsafe_get : floatarray -> int -> float = "%floatarray_unsafe_get"
  external unsafe_set : floatarray -> int -> float -> unit
      = "%floatarray_unsafe_set"
end

let init l f =
  if l = 0 then [||] else
  if l < 0 then invalid_arg "Array.init"
  (* See #6575. We could also check for maximum array size, but this depends
     on whether we create a float array or a regular one... *)
  else
   let res = create l (f 0) in
   for i = 1 to pred l do
     unsafe_set res i (f i)
   done;
   res

let make_matrix sx sy init =
  let res = create sx [||] in
  for x = 0 to pred sx do
    unsafe_set res x (create sy init)
  done;
  res

let create_matrix = make_matrix

let copy a =
  let l = length a in if l = 0 then [||] else unsafe_sub a 0 l

let append a1 a2 =
  let l1 = length a1 in
  if l1 = 0 then copy a2
  else if length a2 = 0 then unsafe_sub a1 0 l1
  else append_prim a1 a2

let sub a ofs len =
  if ofs < 0 || len < 0 || ofs > length a - len
  then invalid_arg "Array.sub"
  else unsafe_sub a ofs len

let fill a ofs len v =
  if ofs < 0 || len < 0 || ofs > length a - len
  then invalid_arg "Array.fill"
  else for i = ofs to ofs + len - 1 do unsafe_set a i v done

let blit a1 ofs1 a2 ofs2 len =
  if len < 0 || ofs1 < 0 || ofs1 > length a1 - len
             || ofs2 < 0 || ofs2 > length a2 - len
  then invalid_arg "Array.blit"
  else unsafe_blit a1 ofs1 a2 ofs2 len

let iter f a =
  for i = 0 to length a - 1 do f(unsafe_get a i) done

let iter2 f a b =
  if length a <> length b then
    invalid_arg "Array.iter2: arrays must have the same length"
  else
    for i = 0 to length a - 1 do f (unsafe_get a i) (unsafe_get b i) done

let map f a =
  let l = length a in
  if l = 0 then [||] else begin
    let r = create l (f(unsafe_get a 0)) in
    for i = 1 to l - 1 do
      unsafe_set r i (f(unsafe_get a i))
    done;
    r
  end

let map2 f a b =
  let la = length a in
  let lb = length b in
  if la <> lb then
    invalid_arg "Array.map2: arrays must have the same length"
  else begin
    if la = 0 then [||] else begin
      let r = create la (f (unsafe_get a 0) (unsafe_get b 0)) in
      for i = 1 to la - 1 do
        unsafe_set r i (f (unsafe_get a i) (unsafe_get b i))
      done;
      r
    end
  end

let iteri f a =
  for i = 0 to length a - 1 do f i (unsafe_get a i) done

let mapi f a =
  let l = length a in
  if l = 0 then [||] else begin
    let r = create l (f 0 (unsafe_get a 0)) in
    for i = 1 to l - 1 do
      unsafe_set r i (f i (unsafe_get a i))
    done;
    r
  end

let to_list a =
  let rec tolist i res =
    if i < 0 then res else tolist (i - 1) (unsafe_get a i :: res) in
  tolist (length a - 1) []

(* Cannot use List.length here because the List module depends on Array. *)
let rec list_length accu = function
  | [] -> accu
  | _::t -> list_length (succ accu) t

let of_list = function
    [] -> [||]
  | hd::tl as l ->
      let a = create (list_length 0 l) hd in
      let rec fill i = function
          [] -> a
        | hd::tl -> unsafe_set a i hd; fill (i+1) tl in
      fill 1 tl

let fold_left f x a =
  let r = ref x in
  for i = 0 to length a - 1 do
    r := f !r (unsafe_get a i)
  done;
  !r

let fold_right f a x =
  let r = ref x in
  for i = length a - 1 downto 0 do
    r := f (unsafe_get a i) !r
  done;
  !r

let exists p a =
  let n = length a in
  let rec loop i =
    if i = n then false
    else if p (unsafe_get a i) then true
    else loop (succ i) in
  loop 0

let for_all p a =
  let n = length a in
  let rec loop i =
    if i = n then true
    else if p (unsafe_get a i) then loop (succ i)
    else false in
  loop 0

let mem x a =
  let n = length a in
  let rec loop i =
    if i = n then false
    else if compare (unsafe_get a i) x = 0 then true
    else loop (succ i) in
  loop 0

let memq x a =
  let n = length a in
  let rec loop i =
    if i = n then false
    else if x == (unsafe_get a i) then true
    else loop (succ i) in
  loop 0

exception Bottom of int
let sort cmp a =
  let maxson l i =
    let i31 = i+i+i+1 in
    let x = ref i31 in
    if i31+2 < l then begin
      if cmp (get a i31) (get a (i31+1)) < 0 then x := i31+1;
      if cmp (get a !x) (get a (i31+2)) < 0 then x := i31+2;
      !x
    end else
      if i31+1 < l && cmp (get a i31) (get a (i31+1)) < 0
      then i31+1
      else if i31 < l then i31 else raise (Bottom i)
  in
  let rec trickledown l i e =
    let j = maxson l i in
    if cmp (get a j) e > 0 then begin
      set a i (get a j);
      trickledown l j e;
    end else begin
      set a i e;
    end;
  in
  let trickle l i e = try trickledown l i e with Bottom i -> set a i e in
  let rec bubbledown l i =
    let j = maxson l i in
    set a i (get a j);
    bubbledown l j
  in
  let bubble l i = try bubbledown l i with Bottom i -> i in
  let rec trickleup i e =
    let father = (i - 1) / 3 in
    assert (i <> father);
    if cmp (get a father) e < 0 then begin
      set a i (get a father);
      if father > 0 then trickleup father e else set a 0 e;
    end else begin
      set a i e;
    end;
  in
  let l = length a in
  for i = (l + 1) / 3 - 1 downto 0 do trickle l i (get a i); done;
  for i = l - 1 downto 2 do
    let e = (get a i) in
    set a i (get a 0);
    trickleup (bubble i 0) e;
  done;
  if l > 1 then (let e = (get a 1) in set a 1 (get a 0); set a 0 e)


let cutoff = 5
let stable_sort cmp a =
  let merge src1ofs src1len src2 src2ofs src2len dst dstofs =
    let src1r = src1ofs + src1len and src2r = src2ofs + src2len in
    let rec loop i1 s1 i2 s2 d =
      if cmp s1 s2 <= 0 then begin
        set dst d s1;
        let i1 = i1 + 1 in
        if i1 < src1r then
          loop i1 (get a i1) i2 s2 (d + 1)
        else
          blit src2 i2 dst (d + 1) (src2r - i2)
      end else begin
        set dst d s2;
        let i2 = i2 + 1 in
        if i2 < src2r then
          loop i1 s1 i2 (get src2 i2) (d + 1)
        else
          blit a i1 dst (d + 1) (src1r - i1)
      end
    in loop src1ofs (get a src1ofs) src2ofs (get src2 src2ofs) dstofs;
  in
  let isortto srcofs dst dstofs len =
    for i = 0 to len - 1 do
      let e = (get a (srcofs + i)) in
      let j = ref (dstofs + i - 1) in
      while (!j >= dstofs && cmp (get dst !j) e > 0) do
        set dst (!j + 1) (get dst !j);
        decr j;
      done;
      set dst (!j + 1) e;
    done;
  in
  let rec sortto srcofs dst dstofs len =
    if len <= cutoff then isortto srcofs dst dstofs len else begin
      let l1 = len / 2 in
      let l2 = len - l1 in
      sortto (srcofs + l1) dst (dstofs + l1) l2;
      sortto srcofs a (srcofs + l2) l1;
      merge (srcofs + l2) l1 dst (dstofs + l1) l2 dst dstofs;
    end;
  in
  let l = length a in
  if l <= cutoff then isortto 0 a 0 l else begin
    let l1 = l / 2 in
    let l2 = l - l1 in
    let t = make l2 (get a 0) in
    sortto l1 t 0 l2;
    sortto 0 a l2 l1;
    merge l2 l1 t 0 l2 a 0;
  end


let fast_sort = stable_sort

(** {6 Iterators} *)

let to_seq a =
  let rec aux i () =
    if i < length a
    then
      let x = unsafe_get a i in
      Seq.Cons (x, aux (i+1))
    else Seq.Nil
  in
  aux 0

let to_seqi a =
  let rec aux i () =
    if i < length a
    then
      let x = unsafe_get a i in
      Seq.Cons ((i,x), aux (i+1))
    else Seq.Nil
  in
  aux 0

let of_rev_list = function
    [] -> [||]
  | hd::tl as l ->
      let len = list_length 0 l in
      let a = create len hd in
      let rec fill i = function
          [] -> a
        | hd::tl -> unsafe_set a i hd; fill (i-1) tl
      in
      fill (len-2) tl

let of_seq i =
  let l = Seq.fold_left (fun acc x -> x::acc) [] i in
  of_rev_list l
end

module ArrayLabels = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                Jacques Garrigue, Kyoto University RIMS                 *)
(*                                                                        *)
(*   Copyright 2001 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Module [ArrayLabels]: labelled Array module *)

include Array
end

module Sort = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Merging and sorting *)

open Array

let rec merge order l1 l2 =
  match l1 with
    [] -> l2
  | h1 :: t1 ->
      match l2 with
        [] -> l1
      | h2 :: t2 ->
          if order h1 h2
          then h1 :: merge order t1 l2
          else h2 :: merge order l1 t2

let list order l =
  let rec initlist = function
      [] -> []
    | [e] -> [[e]]
    | e1::e2::rest ->
        (if order e1 e2 then [e1;e2] else [e2;e1]) :: initlist rest in
  let rec merge2 = function
      l1::l2::rest -> merge order l1 l2 :: merge2 rest
    | x -> x in
  let rec mergeall = function
      [] -> []
    | [l] -> l
    | llist -> mergeall (merge2 llist) in
  mergeall(initlist l)

let swap arr i j =
  let tmp = unsafe_get arr i in
  unsafe_set arr i (unsafe_get arr j);
  unsafe_set arr j tmp

(* There is a known performance bug in the code below.  If you find
   it, don't bother reporting it.  You're not supposed to use this
   module anyway. *)
let array cmp arr =
  let rec qsort lo hi =
    if hi - lo >= 6 then begin
      let mid = (lo + hi) lsr 1 in
      (* Select median value from among LO, MID, and HI. Rearrange
         LO and HI so the three values are sorted. This lowers the
         probability of picking a pathological pivot.  It also
         avoids extra comparisons on i and j in the two tight "while"
         loops below. *)
      if cmp (unsafe_get arr mid) (unsafe_get arr lo) then swap arr mid lo;
      if cmp (unsafe_get arr hi) (unsafe_get arr mid) then begin
        swap arr mid hi;
        if cmp (unsafe_get arr mid) (unsafe_get arr lo) then swap arr mid lo
      end;
      let pivot = unsafe_get arr mid in
      let i = ref (lo + 1) and j = ref (hi - 1) in
      if not (cmp pivot (unsafe_get arr hi))
         || not (cmp (unsafe_get arr lo) pivot)
      then raise (Invalid_argument "Sort.array");
      while !i < !j do
        while not (cmp pivot (unsafe_get arr !i)) do incr i done;
        while not (cmp (unsafe_get arr !j) pivot) do decr j done;
        if !i < !j then swap arr !i !j;
        incr i; decr j
      done;
      (* Recursion on smaller half, tail-call on larger half *)
      if !j - lo <= hi - !i then begin
        qsort lo !j; qsort !i hi
      end else begin
        qsort !i hi; qsort lo !j
      end
    end in
  qsort 0 (Array.length arr - 1);
  (* Finish sorting by insertion sort *)
  for i = 1 to Array.length arr - 1 do
    let val_i = (unsafe_get arr i) in
    if not (cmp (unsafe_get arr (i - 1)) val_i) then begin
      unsafe_set arr i (unsafe_get arr (i - 1));
      let j = ref (i - 1) in
      while !j >= 1 && not (cmp (unsafe_get arr (!j - 1)) val_i) do
        unsafe_set arr !j (unsafe_get arr (!j - 1));
        decr j
      done;
      unsafe_set arr !j val_i
    end
  done
end

module Queue = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*         Francois Pottier, projet Cristal, INRIA Rocquencourt           *)
(*                  Jeremie Dimino, Jane Street Europe                    *)
(*                                                                        *)
(*   Copyright 2002 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

exception Empty

type 'a cell =
  | Nil
  | Cons of { content: 'a; mutable next: 'a cell }

type 'a t = {
  mutable length: int;
  mutable first: 'a cell;
  mutable last: 'a cell
}

let create () = {
  length = 0;
  first = Nil;
  last = Nil
}

let clear q =
  q.length <- 0;
  q.first <- Nil;
  q.last <- Nil

let add x q =
  let cell = Cons {
    content = x;
    next = Nil
  } in
  match q.last with
  | Nil ->
    q.length <- 1;
    q.first <- cell;
    q.last <- cell
  | Cons last ->
    q.length <- q.length + 1;
    last.next <- cell;
    q.last <- cell

let push =
  add

let peek q =
  match q.first with
  | Nil -> raise Empty
  | Cons { content } -> content

let top =
  peek

let take q =
  match q.first with
  | Nil -> raise Empty
  | Cons { content; next = Nil } ->
    clear q;
    content
  | Cons { content; next } ->
    q.length <- q.length - 1;
    q.first <- next;
    content

let pop =
  take

let copy =
  let rec copy q_res prev cell =
    match cell with
    | Nil -> q_res.last <- prev; q_res
    | Cons { content; next } ->
      let res = Cons { content; next = Nil } in
      begin match prev with
      | Nil -> q_res.first <- res
      | Cons p -> p.next <- res
      end;
      copy q_res res next
  in
  fun q -> copy { length = q.length; first = Nil; last = Nil } Nil q.first

let is_empty q =
  q.length = 0

let length q =
  q.length

let iter =
  let rec iter f cell =
    match cell with
    | Nil -> ()
    | Cons { content; next } ->
      f content;
      iter f next
  in
  fun f q -> iter f q.first

let fold =
  let rec fold f accu cell =
    match cell with
    | Nil -> accu
    | Cons { content; next } ->
      let accu = f accu content in
      fold f accu next
  in
  fun f accu q -> fold f accu q.first

let transfer q1 q2 =
  if q1.length > 0 then
    match q2.last with
    | Nil ->
      q2.length <- q1.length;
      q2.first <- q1.first;
      q2.last <- q1.last;
      clear q1
    | Cons last ->
      q2.length <- q2.length + q1.length;
      last.next <- q1.first;
      q2.last <- q1.last;
      clear q1

(** {6 Iterators} *)

let to_seq q =
  let rec aux c () = match c with
    | Nil -> Seq.Nil
    | Cons { content=x; next; } -> Seq.Cons (x, aux next)
  in
  aux q.first

let add_seq q i = Seq.iter (fun x -> push x q) i

let of_seq g =
  let q = create() in
  add_seq q g;
  q
end

module Int64 = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Module [Int64]: 64-bit integers *)

external neg : int64 -> int64 = "%int64_neg"
external add : int64 -> int64 -> int64 = "%int64_add"
external sub : int64 -> int64 -> int64 = "%int64_sub"
external mul : int64 -> int64 -> int64 = "%int64_mul"
external div : int64 -> int64 -> int64 = "%int64_div"
external rem : int64 -> int64 -> int64 = "%int64_mod"
external logand : int64 -> int64 -> int64 = "%int64_and"
external logor : int64 -> int64 -> int64 = "%int64_or"
external logxor : int64 -> int64 -> int64 = "%int64_xor"
external shift_left : int64 -> int -> int64 = "%int64_lsl"
external shift_right : int64 -> int -> int64 = "%int64_asr"
external shift_right_logical : int64 -> int -> int64 = "%int64_lsr"
external of_int : int -> int64 = "%int64_of_int"
external to_int : int64 -> int = "%int64_to_int"
external of_float : float -> int64
  = "caml_int64_of_float" "caml_int64_of_float_unboxed"
  [@@unboxed] [@@noalloc]
external to_float : int64 -> float
  = "caml_int64_to_float" "caml_int64_to_float_unboxed"
  [@@unboxed] [@@noalloc]
external of_int32 : int32 -> int64 = "%int64_of_int32"
external to_int32 : int64 -> int32 = "%int64_to_int32"
external of_nativeint : nativeint -> int64 = "%int64_of_nativeint"
external to_nativeint : int64 -> nativeint = "%int64_to_nativeint"

let zero = 0L
let one = 1L
let minus_one = -1L
let succ n = add n 1L
let pred n = sub n 1L
let abs n = if n >= 0L then n else neg n
let min_int = 0x8000000000000000L
let max_int = 0x7FFFFFFFFFFFFFFFL
let lognot n = logxor n (-1L)

external format : string -> int64 -> string = "caml_int64_format"
let to_string n = format "%d" n

external of_string : string -> int64 = "caml_int64_of_string"

let of_string_opt s =
  (* TODO: expose a non-raising primitive directly. *)
  try Some (of_string s)
  with Failure _ -> None



external bits_of_float : float -> int64
  = "caml_int64_bits_of_float" "caml_int64_bits_of_float_unboxed"
  [@@unboxed] [@@noalloc]
external float_of_bits : int64 -> float
  = "caml_int64_float_of_bits" "caml_int64_float_of_bits_unboxed"
  [@@unboxed] [@@noalloc]

type t = int64

let compare (x: t) (y: t) = Pervasives.compare x y
let equal (x: t) (y: t) = compare x y = 0
end

module Int32 = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Module [Int32]: 32-bit integers *)

external neg : int32 -> int32 = "%int32_neg"
external add : int32 -> int32 -> int32 = "%int32_add"
external sub : int32 -> int32 -> int32 = "%int32_sub"
external mul : int32 -> int32 -> int32 = "%int32_mul"
external div : int32 -> int32 -> int32 = "%int32_div"
external rem : int32 -> int32 -> int32 = "%int32_mod"
external logand : int32 -> int32 -> int32 = "%int32_and"
external logor : int32 -> int32 -> int32 = "%int32_or"
external logxor : int32 -> int32 -> int32 = "%int32_xor"
external shift_left : int32 -> int -> int32 = "%int32_lsl"
external shift_right : int32 -> int -> int32 = "%int32_asr"
external shift_right_logical : int32 -> int -> int32 = "%int32_lsr"
external of_int : int -> int32 = "%int32_of_int"
external to_int : int32 -> int = "%int32_to_int"
external of_float : float -> int32
  = "caml_int32_of_float" "caml_int32_of_float_unboxed"
  [@@unboxed] [@@noalloc]
external to_float : int32 -> float
  = "caml_int32_to_float" "caml_int32_to_float_unboxed"
  [@@unboxed] [@@noalloc]
external bits_of_float : float -> int32
  = "caml_int32_bits_of_float" "caml_int32_bits_of_float_unboxed"
  [@@unboxed] [@@noalloc]
external float_of_bits : int32 -> float
  = "caml_int32_float_of_bits" "caml_int32_float_of_bits_unboxed"
  [@@unboxed] [@@noalloc]

let zero = 0l
let one = 1l
let minus_one = -1l
let succ n = add n 1l
let pred n = sub n 1l
let abs n = if n >= 0l then n else neg n
let min_int = 0x80000000l
let max_int = 0x7FFFFFFFl
let lognot n = logxor n (-1l)

external format : string -> int32 -> string = "caml_int32_format"
let to_string n = format "%d" n

external of_string : string -> int32 = "caml_int32_of_string"

let of_string_opt s =
  (* TODO: expose a non-raising primitive directly. *)
  try Some (of_string s)
  with Failure _ -> None

type t = int32

let compare (x: t) (y: t) = Pervasives.compare x y
let equal (x: t) (y: t) = compare x y = 0
end

module Nativeint = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Module [Nativeint]: processor-native integers *)

external neg: nativeint -> nativeint = "%nativeint_neg"
external add: nativeint -> nativeint -> nativeint = "%nativeint_add"
external sub: nativeint -> nativeint -> nativeint = "%nativeint_sub"
external mul: nativeint -> nativeint -> nativeint = "%nativeint_mul"
external div: nativeint -> nativeint -> nativeint = "%nativeint_div"
external rem: nativeint -> nativeint -> nativeint = "%nativeint_mod"
external logand: nativeint -> nativeint -> nativeint = "%nativeint_and"
external logor: nativeint -> nativeint -> nativeint = "%nativeint_or"
external logxor: nativeint -> nativeint -> nativeint = "%nativeint_xor"
external shift_left: nativeint -> int -> nativeint = "%nativeint_lsl"
external shift_right: nativeint -> int -> nativeint = "%nativeint_asr"
external shift_right_logical: nativeint -> int -> nativeint = "%nativeint_lsr"
external of_int: int -> nativeint = "%nativeint_of_int"
external to_int: nativeint -> int = "%nativeint_to_int"
external of_float : float -> nativeint
  = "caml_nativeint_of_float" "caml_nativeint_of_float_unboxed"
  [@@unboxed] [@@noalloc]
external to_float : nativeint -> float
  = "caml_nativeint_to_float" "caml_nativeint_to_float_unboxed"
  [@@unboxed] [@@noalloc]
external of_int32: int32 -> nativeint = "%nativeint_of_int32"
external to_int32: nativeint -> int32 = "%nativeint_to_int32"

let zero = 0n
let one = 1n
let minus_one = -1n
let succ n = add n 1n
let pred n = sub n 1n
let abs n = if n >= 0n then n else neg n
let size = Sys.word_size
let min_int = shift_left 1n (size - 1)
let max_int = sub min_int 1n
let lognot n = logxor n (-1n)

external format : string -> nativeint -> string = "caml_nativeint_format"
let to_string n = format "%d" n

external of_string: string -> nativeint = "caml_nativeint_of_string"

let of_string_opt s =
  (* TODO: expose a non-raising primitive directly. *)
  try Some (of_string s)
  with Failure _ -> None

type t = nativeint

let compare (x: t) (y: t) = Pervasives.compare x y
let equal (x: t) (y: t) = compare x y = 0
end

module Digest = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Message digest (MD5) *)

type t = string

let compare = String.compare
let equal = String.equal

external unsafe_string: string -> int -> int -> t = "caml_md5_string"
external channel: in_channel -> int -> t = "caml_md5_chan"

let string str =
  unsafe_string str 0 (String.length str)

let bytes b = string (Bytes.unsafe_to_string b)

let substring str ofs len =
  if ofs < 0 || len < 0 || ofs > String.length str - len
  then invalid_arg "Digest.substring"
  else unsafe_string str ofs len

let subbytes b ofs len = substring (Bytes.unsafe_to_string b) ofs len

let file filename =
  let ic = open_in_bin filename in
  match channel ic (-1) with
    | d -> close_in ic; d
    | exception e -> close_in ic; raise e

let output chan digest =
  output_string chan digest

let input chan = really_input_string chan 16

let char_hex n =
  Char.unsafe_chr (n + if n < 10 then Char.code '0' else (Char.code 'a' - 10))

let to_hex d =
  if String.length d <> 16 then invalid_arg "Digest.to_hex";
  let result = Bytes.create 32 in
  for i = 0 to 15 do
    let x = Char.code d.[i] in
    Bytes.unsafe_set result (i*2) (char_hex (x lsr 4));
    Bytes.unsafe_set result (i*2+1) (char_hex (x land 0x0f));
  done;
  Bytes.unsafe_to_string result

let from_hex s =
  if String.length s <> 32 then invalid_arg "Digest.from_hex";
  let digit c =
    match c with
    | '0'..'9' -> Char.code c - Char.code '0'
    | 'A'..'F' -> Char.code c - Char.code 'A' + 10
    | 'a'..'f' -> Char.code c - Char.code 'a' + 10
    | _ -> raise (Invalid_argument "Digest.from_hex")
  in
  let byte i = digit s.[i] lsl 4 + digit s.[i+1] in
  let result = Bytes.create 16 in
  for i = 0 to 15 do
    Bytes.set result i (Char.chr (byte (2 * i)));
  done;
  Bytes.unsafe_to_string result
end

module Random = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*               Damien Doligez, projet Para, INRIA Rocquencourt          *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Pseudo-random number generator
   This is a lagged-Fibonacci F(55, 24, +) with a modified addition
   function to enhance the mixing of bits.
   If we use normal addition, the low-order bit fails tests 1 and 7
   of the Diehard test suite, and bits 1 and 2 also fail test 7.
   If we use multiplication as suggested by Marsaglia, it doesn't fare
   much better.
   By mixing the bits of one of the numbers before addition (XOR the
   5 high-order bits into the low-order bits), we get a generator that
   passes all the Diehard tests.
*)

external random_seed: unit -> int array = "caml_sys_random_seed"

module State = struct

  type t = { st : int array; mutable idx : int }

  let new_state () = { st = Array.make 55 0; idx = 0 }
  let assign st1 st2 =
    Array.blit st2.st 0 st1.st 0 55;
    st1.idx <- st2.idx


  let full_init s seed =
    let combine accu x = Digest.string (accu ^ string_of_int x) in
    let extract d =
      Char.code d.[0] + (Char.code d.[1] lsl 8) + (Char.code d.[2] lsl 16)
      + (Char.code d.[3] lsl 24)
    in
    let seed = if Array.length seed = 0 then [| 0 |] else seed in
    let l = Array.length seed in
    for i = 0 to 54 do
      s.st.(i) <- i;
    done;
    let accu = ref "x" in
    for i = 0 to 54 + max 55 l do
      let j = i mod 55 in
      let k = i mod l in
      accu := combine !accu seed.(k);
      s.st.(j) <- (s.st.(j) lxor extract !accu) land 0x3FFFFFFF;  (* PR#5575 *)
    done;
    s.idx <- 0


  let make seed =
    let result = new_state () in
    full_init result seed;
    result


  let make_self_init () = make (random_seed ())

  let copy s =
    let result = new_state () in
    assign result s;
    result


  (* Returns 30 random bits as an integer 0 <= x < 1073741824 *)
  let bits s =
    s.idx <- (s.idx + 1) mod 55;
    let curval = s.st.(s.idx) in
    let newval = s.st.((s.idx + 24) mod 55)
                 + (curval lxor ((curval lsr 25) land 0x1F)) in
    let newval30 = newval land 0x3FFFFFFF in  (* PR#5575 *)
    s.st.(s.idx) <- newval30;
    newval30


  let rec intaux s n =
    let r = bits s in
    let v = r mod n in
    if r - v > 0x3FFFFFFF - n + 1 then intaux s n else v

  let int s bound =
    if bound > 0x3FFFFFFF || bound <= 0
    then invalid_arg "Random.int"
    else intaux s bound


  let rec int32aux s n =
    let b1 = Int32.of_int (bits s) in
    let b2 = Int32.shift_left (Int32.of_int (bits s land 1)) 30 in
    let r = Int32.logor b1 b2 in
    let v = Int32.rem r n in
    if Int32.sub r v > Int32.add (Int32.sub Int32.max_int n) 1l
    then int32aux s n
    else v

  let int32 s bound =
    if bound <= 0l
    then invalid_arg "Random.int32"
    else int32aux s bound


  let rec int64aux s n =
    let b1 = Int64.of_int (bits s) in
    let b2 = Int64.shift_left (Int64.of_int (bits s)) 30 in
    let b3 = Int64.shift_left (Int64.of_int (bits s land 7)) 60 in
    let r = Int64.logor b1 (Int64.logor b2 b3) in
    let v = Int64.rem r n in
    if Int64.sub r v > Int64.add (Int64.sub Int64.max_int n) 1L
    then int64aux s n
    else v

  let int64 s bound =
    if bound <= 0L
    then invalid_arg "Random.int64"
    else int64aux s bound


  let nativeint =
    if Nativeint.size = 32
    then fun s bound -> Nativeint.of_int32 (int32 s (Nativeint.to_int32 bound))
    else fun s bound -> Int64.to_nativeint (int64 s (Int64.of_nativeint bound))


  (* Returns a float 0 <= x <= 1 with at most 60 bits of precision. *)
  let rawfloat s =
    let scale = 1073741824.0  (* 2^30 *)
    and r1 = Pervasives.float (bits s)
    and r2 = Pervasives.float (bits s)
    in (r1 /. scale +. r2) /. scale


  let float s bound = rawfloat s *. bound

  let bool s = (bits s land 1 = 0)

end

(* This is the state you get with [init 27182818] and then applying
   the "land 0x3FFFFFFF" filter to them.  See #5575, #5793, #5977. *)
let default = {
  State.st = [|
      0x3ae2522b; 0x1d8d4634; 0x15b4fad0; 0x18b14ace; 0x12f8a3c4; 0x3b086c47;
      0x16d467d6; 0x101d91c7; 0x321df177; 0x0176c193; 0x1ff72bf1; 0x1e889109;
      0x0b464b18; 0x2b86b97c; 0x0891da48; 0x03137463; 0x085ac5a1; 0x15d61f2f;
      0x3bced359; 0x29c1c132; 0x3a86766e; 0x366d8c86; 0x1f5b6222; 0x3ce1b59f;
      0x2ebf78e1; 0x27cd1b86; 0x258f3dc3; 0x389a8194; 0x02e4c44c; 0x18c43f7d;
      0x0f6e534f; 0x1e7df359; 0x055d0b7e; 0x10e84e7e; 0x126198e4; 0x0e7722cb;
      0x1cbede28; 0x3391b964; 0x3d40e92a; 0x0c59933d; 0x0b8cd0b7; 0x24efff1c;
      0x2803fdaa; 0x08ebc72e; 0x0f522e32; 0x05398edc; 0x2144a04c; 0x0aef3cbd;
      0x01ad4719; 0x35b93cd6; 0x2a559d4f; 0x1e6fd768; 0x26e27f36; 0x186f18c3;
      0x2fbf967a;
    |];
  State.idx = 0;
}

let bits () = State.bits default
let int bound = State.int default bound
let int32 bound = State.int32 default bound
let nativeint bound = State.nativeint default bound
let int64 bound = State.int64 default bound
let float scale = State.float default scale
let bool () = State.bool default

let full_init seed = State.full_init default seed
let init seed = State.full_init default [| seed |]
let self_init () = full_init (random_seed())

(* Manipulating the current state. *)

let get_state () = State.copy default
let set_state s = State.assign default s

(********************

(* Test functions.  Not included in the library.
   The [chisquare] function should be called with n > 10r.
   It returns a triple (low, actual, high).
   If low <= actual <= high, the [g] function passed the test,
   otherwise it failed.

  Some results:

init 27182818; chisquare int 100000 1000
init 27182818; chisquare int 100000 100
init 27182818; chisquare int 100000 5000
init 27182818; chisquare int 1000000 1000
init 27182818; chisquare int 100000 1024
init 299792643; chisquare int 100000 1024
init 14142136; chisquare int 100000 1024
init 27182818; init_diff 1024; chisquare diff 100000 1024
init 27182818; init_diff 100; chisquare diff 100000 100
init 27182818; init_diff2 1024; chisquare diff2 100000 1024
init 27182818; init_diff2 100; chisquare diff2 100000 100
init 14142136; init_diff2 100; chisquare diff2 100000 100
init 299792643; init_diff2 100; chisquare diff2 100000 100
- : float * float * float = (936.754446796632465, 997.5, 1063.24555320336754)
# - : float * float * float = (80., 89.7400000000052387, 120.)
# - : float * float * float = (4858.57864376269, 5045.5, 5141.42135623731)
# - : float * float * float =
(936.754446796632465, 944.805999999982305, 1063.24555320336754)
# - : float * float * float = (960., 1019.19744000000355, 1088.)
# - : float * float * float = (960., 1059.31776000000536, 1088.)
# - : float * float * float = (960., 1039.98463999999512, 1088.)
# - : float * float * float = (960., 1054.38207999999577, 1088.)
# - : float * float * float = (80., 90.096000000005, 120.)
# - : float * float * float = (960., 1076.78720000000612, 1088.)
# - : float * float * float = (80., 85.1760000000067521, 120.)
# - : float * float * float = (80., 85.2160000000003492, 120.)
# - : float * float * float = (80., 80.6220000000030268, 120.)

*)

(* Return the sum of the squares of v[i0,i1[ *)
let rec sumsq v i0 i1 =
  if i0 >= i1 then 0.0
  else if i1 = i0 + 1 then Pervasives.float v.(i0) *. Pervasives.float v.(i0)
  else sumsq v i0 ((i0+i1)/2) +. sumsq v ((i0+i1)/2) i1


let chisquare g n r =
  if n <= 10 * r then invalid_arg "chisquare";
  let f = Array.make r 0 in
  for i = 1 to n do
    let t = g r in
    f.(t) <- f.(t) + 1
  done;
  let t = sumsq f 0 r
  and r = Pervasives.float r
  and n = Pervasives.float n in
  let sr = 2.0 *. sqrt r in
  (r -. sr,   (r *. t /. n) -. n,   r +. sr)


(* This is to test for linear dependencies between successive random numbers.
*)
let st = ref 0
let init_diff r = st := int r
let diff r =
  let x1 = !st
  and x2 = int r
  in
  st := x2;
  if x1 >= x2 then
    x1 - x2
  else
    r + x1 - x2


let st1 = ref 0
and st2 = ref 0


(* This is to test for quadratic dependencies between successive random
   numbers.
*)
let init_diff2 r = st1 := int r; st2 := int r
let diff2 r =
  let x1 = !st1
  and x2 = !st2
  and x3 = int r
  in
  st1 := x2;
  st2 := x3;
  (x3 - x2 - x2 + x1 + 2*r) mod r


********************)
end

(* module Hashtbl = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Hash tables *)

external seeded_hash_param :
  int -> int -> int -> 'a -> int = "caml_hash" [@@noalloc]
external old_hash_param :
  int -> int -> 'a -> int = "caml_hash_univ_param" [@@noalloc]

let hash x = seeded_hash_param 10 100 0 x
let hash_param n1 n2 x = seeded_hash_param n1 n2 0 x
let seeded_hash seed x = seeded_hash_param 10 100 seed x

(* We do dynamic hashing, and resize the table and rehash the elements
   when buckets become too long. *)

type ('a, 'b) t =
  { mutable size: int;                        (* number of entries *)
    mutable data: ('a, 'b) bucketlist array;  (* the buckets *)
    mutable seed: int;                        (* for randomization *)
    mutable initial_size: int;                (* initial array size *)
  }

and ('a, 'b) bucketlist =
    Empty
  | Cons of { mutable key: 'a;
              mutable data: 'b;
              mutable next: ('a, 'b) bucketlist }

(* The sign of initial_size encodes the fact that a traversal is
   ongoing or not.

   This disables the efficient in place implementation of resizing.
*)

let ongoing_traversal h =
  Obj.size (Obj.repr h) < 4 (* compatibility with old hash tables *)
  || h.initial_size < 0

let flip_ongoing_traversal h =
  h.initial_size <- - h.initial_size

(* To pick random seeds if requested *)

let randomized_default =
  let params = "" in
    (* try Sys.getenv "OCAMLRUNPARAM" with Not_found ->
    try Sys.getenv "CAMLRUNPARAM" with Not_found -> "" in *)
  String.contains params 'R'

let randomized = ref randomized_default

let randomize () = randomized := true
let is_randomized () = !randomized

let prng = lazy (Random.State.make_self_init())

(* Creating a fresh, empty table *)

let rec power_2_above x n =
  if x >= n then x
  else if x * 2 > Sys.max_array_length then x
  else power_2_above (x * 2) n

let create ?(random = !randomized) initial_size =
  let s = power_2_above 16 initial_size in
  let seed = if random then Random.State.bits (Lazy.force prng) else 0 in
  { initial_size = s; size = 0; seed = seed; data = Array.make s Empty }

let clear h =
  h.size <- 0;
  let len = Array.length h.data in
  for i = 0 to len - 1 do
    h.data.(i) <- Empty
  done

let reset h =
  let len = Array.length h.data in
  if Obj.size (Obj.repr h) < 4 (* compatibility with old hash tables *)
    || len = abs h.initial_size then
    clear h
  else begin
    h.size <- 0;
    h.data <- Array.make (abs h.initial_size) Empty
  end

let copy_bucketlist = function
  | Empty -> Empty
  | Cons {key; data; next} ->
      let rec loop prec = function
        | Empty -> ()
        | Cons {key; data; next} ->
            let r = Cons {key; data; next} in
            begin match prec with
            | Empty -> assert false
            | Cons prec ->  prec.next <- r
            end;
            loop r next
      in
      let r = Cons {key; data; next} in
      loop r next;
      r

let copy h = { h with data = Array.map copy_bucketlist h.data }

let length h = h.size

let resize indexfun h =
  let odata = h.data in
  let osize = Array.length odata in
  let nsize = osize * 2 in
  if nsize < Sys.max_array_length then begin
    let ndata = Array.make nsize Empty in
    let ndata_tail = Array.make nsize Empty in
    let inplace = not (ongoing_traversal h) in
    h.data <- ndata;          (* so that indexfun sees the new bucket count *)
    let rec insert_bucket = function
      | Empty -> ()
      | Cons {key; data; next} as cell ->
          let cell =
            if inplace then cell
            else Cons {key; data; next = Empty}
          in
          let nidx = indexfun h key in
          begin match ndata_tail.(nidx) with
          | Empty -> ndata.(nidx) <- cell;
          | Cons tail -> tail.next <- cell;
          end;
          ndata_tail.(nidx) <- cell;
          insert_bucket next
    in
    for i = 0 to osize - 1 do
      insert_bucket odata.(i)
    done;
    if inplace then
      for i = 0 to nsize - 1 do
        match ndata_tail.(i) with
        | Empty -> ()
        | Cons tail -> tail.next <- Empty
      done;
  end

let key_index h key =
  (* compatibility with old hash tables *)
  if Obj.size (Obj.repr h) >= 3
  then (seeded_hash_param 10 100 h.seed key) land (Array.length h.data - 1)
  else (old_hash_param 10 100 key) mod (Array.length h.data)

let add h key data =
  let i = key_index h key in
  let bucket = Cons{key; data; next=h.data.(i)} in
  h.data.(i) <- bucket;
  h.size <- h.size + 1;
  if h.size > Array.length h.data lsl 1 then resize key_index h

let rec remove_bucket h i key prec = function
  | Empty ->
      ()
  | (Cons {key=k; next}) as c ->
      if compare k key = 0
      then begin
        h.size <- h.size - 1;
        match prec with
        | Empty -> h.data.(i) <- next
        | Cons c -> c.next <- next
      end
      else remove_bucket h i key c next

let remove h key =
  let i = key_index h key in
  remove_bucket h i key Empty h.data.(i)

let rec find_rec key = function
  | Empty ->
      raise Not_found
  | Cons{key=k; data; next} ->
      if compare key k = 0 then data else find_rec key next

let find h key =
  match h.data.(key_index h key) with
  | Empty -> raise Not_found
  | Cons{key=k1; data=d1; next=next1} ->
      if compare key k1 = 0 then d1 else
      match next1 with
      | Empty -> raise Not_found
      | Cons{key=k2; data=d2; next=next2} ->
          if compare key k2 = 0 then d2 else
          match next2 with
          | Empty -> raise Not_found
          | Cons{key=k3; data=d3; next=next3} ->
              if compare key k3 = 0 then d3 else find_rec key next3

let rec find_rec_opt key = function
  | Empty ->
      None
  | Cons{key=k; data; next} ->
      if compare key k = 0 then Some data else find_rec_opt key next

let find_opt h key =
  match h.data.(key_index h key) with
  | Empty -> None
  | Cons{key=k1; data=d1; next=next1} ->
      if compare key k1 = 0 then Some d1 else
      match next1 with
      | Empty -> None
      | Cons{key=k2; data=d2; next=next2} ->
          if compare key k2 = 0 then Some d2 else
          match next2 with
          | Empty -> None
          | Cons{key=k3; data=d3; next=next3} ->
              if compare key k3 = 0 then Some d3 else find_rec_opt key next3

let find_all h key =
  let rec find_in_bucket = function
  | Empty ->
      []
  | Cons{key=k; data; next} ->
      if compare k key = 0
      then data :: find_in_bucket next
      else find_in_bucket next in
  find_in_bucket h.data.(key_index h key)

let rec replace_bucket key data = function
  | Empty ->
      true
  | Cons ({key=k; next} as slot) ->
      if compare k key = 0
      then (slot.key <- key; slot.data <- data; false)
      else replace_bucket key data next

let replace h key data =
  let i = key_index h key in
  let l = h.data.(i) in
  if replace_bucket key data l then begin
    h.data.(i) <- Cons{key; data; next=l};
    h.size <- h.size + 1;
    if h.size > Array.length h.data lsl 1 then resize key_index h
  end

let mem h key =
  let rec mem_in_bucket = function
  | Empty ->
      false
  | Cons{key=k; next} ->
      compare k key = 0 || mem_in_bucket next in
  mem_in_bucket h.data.(key_index h key)

let iter f h =
  let rec do_bucket = function
    | Empty ->
        ()
    | Cons{key; data; next} ->
        f key data; do_bucket next in
  let old_trav = ongoing_traversal h in
  if not old_trav then flip_ongoing_traversal h;
  try
    let d = h.data in
    for i = 0 to Array.length d - 1 do
      do_bucket d.(i)
    done;
    if not old_trav then flip_ongoing_traversal h;
  with exn when not old_trav ->
    flip_ongoing_traversal h;
    raise exn

let rec filter_map_inplace_bucket f h i prec = function
  | Empty ->
      begin match prec with
      | Empty -> h.data.(i) <- Empty
      | Cons c -> c.next <- Empty
      end
  | (Cons ({key; data; next} as c)) as slot ->
      begin match f key data with
      | None ->
          h.size <- h.size - 1;
          filter_map_inplace_bucket f h i prec next
      | Some data ->
          begin match prec with
          | Empty -> h.data.(i) <- slot
          | Cons c -> c.next <- slot
          end;
          c.data <- data;
          filter_map_inplace_bucket f h i slot next
      end

let filter_map_inplace f h =
  let d = h.data in
  let old_trav = ongoing_traversal h in
  if not old_trav then flip_ongoing_traversal h;
  try
    for i = 0 to Array.length d - 1 do
      filter_map_inplace_bucket f h i Empty h.data.(i)
    done
  with exn when not old_trav ->
    flip_ongoing_traversal h;
    raise exn

let fold f h init =
  let rec do_bucket b accu =
    match b with
      Empty ->
        accu
    | Cons{key; data; next} ->
        do_bucket next (f key data accu) in
  let old_trav = ongoing_traversal h in
  if not old_trav then flip_ongoing_traversal h;
  try
    let d = h.data in
    let accu = ref init in
    for i = 0 to Array.length d - 1 do
      accu := do_bucket d.(i) !accu
    done;
    if not old_trav then flip_ongoing_traversal h;
    !accu
  with exn when not old_trav ->
    flip_ongoing_traversal h;
    raise exn

type statistics = {
  num_bindings: int;
  num_buckets: int;
  max_bucket_length: int;
  bucket_histogram: int array
}

let rec bucket_length accu = function
  | Empty -> accu
  | Cons{next} -> bucket_length (accu + 1) next

let stats h =
  let mbl =
    Array.fold_left (fun m b -> max m (bucket_length 0 b)) 0 h.data in
  let histo = Array.make (mbl + 1) 0 in
  Array.iter
    (fun b ->
      let l = bucket_length 0 b in
      histo.(l) <- histo.(l) + 1)
    h.data;
  { num_bindings = h.size;
    num_buckets = Array.length h.data;
    max_bucket_length = mbl;
    bucket_histogram = histo }

(** {6 Iterators} *)

let to_seq tbl =
  (* capture current array, so that even if the table is resized we
     keep iterating on the same array *)
  let tbl_data = tbl.data in
  (* state: index * next bucket to traverse *)
  let rec aux i buck () = match buck with
    | Empty ->
        if i = Array.length tbl_data
        then Seq.Nil
        else aux(i+1) tbl_data.(i) ()
    | Cons {key; data; next} ->
        Seq.Cons ((key, data), aux i next)
  in
  aux 0 Empty

let to_seq_keys m = Seq.map fst (to_seq m)

let to_seq_values m = Seq.map snd (to_seq m)

let add_seq tbl i =
  Seq.iter (fun (k,v) -> add tbl k v) i

let replace_seq tbl i =
  Seq.iter (fun (k,v) -> replace tbl k v) i

let of_seq i =
  let tbl = create 16 in
  replace_seq tbl i;
  tbl

(* Functorial interface *)

module type HashedType =
  sig
    type t
    val equal: t -> t -> bool
    val hash: t -> int
  end

module type SeededHashedType =
  sig
    type t
    val equal: t -> t -> bool
    val hash: int -> t -> int
  end

module type S =
  sig
    type key
    type 'a t
    val create: int -> 'a t
    val clear : 'a t -> unit
    val reset : 'a t -> unit
    val copy: 'a t -> 'a t
    val add: 'a t -> key -> 'a -> unit
    val remove: 'a t -> key -> unit
    val find: 'a t -> key -> 'a
    val find_opt: 'a t -> key -> 'a option
    val find_all: 'a t -> key -> 'a list
    val replace : 'a t -> key -> 'a -> unit
    val mem : 'a t -> key -> bool
    val iter: (key -> 'a -> unit) -> 'a t -> unit
    val filter_map_inplace: (key -> 'a -> 'a option) -> 'a t -> unit
    val fold: (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
    val length: 'a t -> int
    val stats: 'a t -> statistics
    val to_seq : 'a t -> (key * 'a) Seq.t
    val to_seq_keys : _ t -> key Seq.t
    val to_seq_values : 'a t -> 'a Seq.t
    val add_seq : 'a t -> (key * 'a) Seq.t -> unit
    val replace_seq : 'a t -> (key * 'a) Seq.t -> unit
    val of_seq : (key * 'a) Seq.t -> 'a t
  end

module type SeededS =
  sig
    type key
    type 'a t
    val create : ?random:bool -> int -> 'a t
    val clear : 'a t -> unit
    val reset : 'a t -> unit
    val copy : 'a t -> 'a t
    val add : 'a t -> key -> 'a -> unit
    val remove : 'a t -> key -> unit
    val find : 'a t -> key -> 'a
    val find_opt: 'a t -> key -> 'a option
    val find_all : 'a t -> key -> 'a list
    val replace : 'a t -> key -> 'a -> unit
    val mem : 'a t -> key -> bool
    val iter : (key -> 'a -> unit) -> 'a t -> unit
    val filter_map_inplace: (key -> 'a -> 'a option) -> 'a t -> unit
    val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
    val length : 'a t -> int
    val stats: 'a t -> statistics
    val to_seq : 'a t -> (key * 'a) Seq.t
    val to_seq_keys : _ t -> key Seq.t
    val to_seq_values : 'a t -> 'a Seq.t
    val add_seq : 'a t -> (key * 'a) Seq.t -> unit
    val replace_seq : 'a t -> (key * 'a) Seq.t -> unit
    val of_seq : (key * 'a) Seq.t -> 'a t
  end

module MakeSeeded(H: SeededHashedType): (SeededS with type key = H.t) =
  struct
    type key = H.t
    type 'a hashtbl = (key, 'a) t
    type 'a t = 'a hashtbl
    let create = create
    let clear = clear
    let reset = reset
    let copy = copy

    let key_index h key =
      (H.hash h.seed key) land (Array.length h.data - 1)

    let add h key data =
      let i = key_index h key in
      let bucket = Cons{key; data; next=h.data.(i)} in
      h.data.(i) <- bucket;
      h.size <- h.size + 1;
      if h.size > Array.length h.data lsl 1 then resize key_index h

    let rec remove_bucket h i key prec = function
      | Empty ->
          ()
      | (Cons {key=k; next}) as c ->
          if H.equal k key
          then begin
            h.size <- h.size - 1;
            match prec with
            | Empty -> h.data.(i) <- next
            | Cons c -> c.next <- next
          end
          else remove_bucket h i key c next

    let remove h key =
      let i = key_index h key in
      remove_bucket h i key Empty h.data.(i)

    let rec find_rec key = function
      | Empty ->
          raise Not_found
      | Cons{key=k; data; next} ->
          if H.equal key k then data else find_rec key next

    let find h key =
      match h.data.(key_index h key) with
      | Empty -> raise Not_found
      | Cons{key=k1; data=d1; next=next1} ->
          if H.equal key k1 then d1 else
          match next1 with
          | Empty -> raise Not_found
          | Cons{key=k2; data=d2; next=next2} ->
              if H.equal key k2 then d2 else
              match next2 with
              | Empty -> raise Not_found
              | Cons{key=k3; data=d3; next=next3} ->
                  if H.equal key k3 then d3 else find_rec key next3

    let rec find_rec_opt key = function
      | Empty ->
          None
      | Cons{key=k; data; next} ->
          if H.equal key k then Some data else find_rec_opt key next

    let find_opt h key =
      match h.data.(key_index h key) with
      | Empty -> None
      | Cons{key=k1; data=d1; next=next1} ->
          if H.equal key k1 then Some d1 else
          match next1 with
          | Empty -> None
          | Cons{key=k2; data=d2; next=next2} ->
              if H.equal key k2 then Some d2 else
              match next2 with
              | Empty -> None
              | Cons{key=k3; data=d3; next=next3} ->
                  if H.equal key k3 then Some d3 else find_rec_opt key next3

    let find_all h key =
      let rec find_in_bucket = function
      | Empty ->
          []
      | Cons{key=k; data=d; next} ->
          if H.equal k key
          then d :: find_in_bucket next
          else find_in_bucket next in
      find_in_bucket h.data.(key_index h key)

    let rec replace_bucket key data = function
      | Empty ->
          true
      | Cons ({key=k; next} as slot) ->
          if H.equal k key
          then (slot.key <- key; slot.data <- data; false)
          else replace_bucket key data next

    let replace h key data =
      let i = key_index h key in
      let l = h.data.(i) in
      if replace_bucket key data l then begin
        h.data.(i) <- Cons{key; data; next=l};
        h.size <- h.size + 1;
        if h.size > Array.length h.data lsl 1 then resize key_index h
      end

    let mem h key =
      let rec mem_in_bucket = function
      | Empty ->
          false
      | Cons{key=k; next} ->
          H.equal k key || mem_in_bucket next in
      mem_in_bucket h.data.(key_index h key)

    let iter = iter
    let filter_map_inplace = filter_map_inplace
    let fold = fold
    let length = length
    let stats = stats
    let to_seq = to_seq
    let to_seq_keys = to_seq_keys
    let to_seq_values = to_seq_values
    let add_seq = add_seq
    let replace_seq = replace_seq
    let of_seq = of_seq
  end

module Make(H: HashedType): (S with type key = H.t) =
  struct
    include MakeSeeded(struct
        type t = H.t
        let equal = H.equal
        let hash (_seed: int) x = H.hash x
      end)
    let create sz = create ~random:false sz
  end
end *)

module Lexing = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* The run-time library for lexers generated by camllex *)

type position = {
  pos_fname : string;
  pos_lnum : int;
  pos_bol : int;
  pos_cnum : int;
}

let dummy_pos = {
  pos_fname = "";
  pos_lnum = 0;
  pos_bol = 0;
  pos_cnum = -1;
}

type lexbuf =
  { refill_buff : lexbuf -> unit;
    mutable lex_buffer : bytes;
    mutable lex_buffer_len : int;
    mutable lex_abs_pos : int;
    mutable lex_start_pos : int;
    mutable lex_curr_pos : int;
    mutable lex_last_pos : int;
    mutable lex_last_action : int;
    mutable lex_eof_reached : bool;
    mutable lex_mem : int array;
    mutable lex_start_p : position;
    mutable lex_curr_p : position;
  }

type lex_tables =
  { lex_base: string;
    lex_backtrk: string;
    lex_default: string;
    lex_trans: string;
    lex_check: string;
    lex_base_code : string;
    lex_backtrk_code : string;
    lex_default_code : string;
    lex_trans_code : string;
    lex_check_code : string;
    lex_code: string;}

external c_engine : lex_tables -> int -> lexbuf -> int = "caml_lex_engine"
external c_new_engine : lex_tables -> int -> lexbuf -> int
                      = "caml_new_lex_engine"

let engine tbl state buf =
  let result = c_engine tbl state buf in
  if result >= 0 then begin
    buf.lex_start_p <- buf.lex_curr_p;
    buf.lex_curr_p <- {buf.lex_curr_p
                       with pos_cnum = buf.lex_abs_pos + buf.lex_curr_pos};
  end;
  result


let new_engine tbl state buf =
  let result = c_new_engine tbl state buf in
  if result >= 0 then begin
    buf.lex_start_p <- buf.lex_curr_p;
    buf.lex_curr_p <- {buf.lex_curr_p
                       with pos_cnum = buf.lex_abs_pos + buf.lex_curr_pos};
  end;
  result


let lex_refill read_fun aux_buffer lexbuf =
  let read =
    read_fun aux_buffer (Bytes.length aux_buffer) in
  let n =
    if read > 0
    then read
    else (lexbuf.lex_eof_reached <- true; 0) in
  (* Current state of the buffer:
        <-------|---------------------|----------->
        |  junk |      valid data     |   junk    |
        ^       ^                     ^           ^
        0    start_pos             buffer_end    Bytes.length buffer
  *)
  if lexbuf.lex_buffer_len + n > Bytes.length lexbuf.lex_buffer then begin
    (* There is not enough space at the end of the buffer *)
    if lexbuf.lex_buffer_len - lexbuf.lex_start_pos + n
       <= Bytes.length lexbuf.lex_buffer
    then begin
      (* But there is enough space if we reclaim the junk at the beginning
         of the buffer *)
      Bytes.blit lexbuf.lex_buffer lexbuf.lex_start_pos
                  lexbuf.lex_buffer 0
                  (lexbuf.lex_buffer_len - lexbuf.lex_start_pos)
    end else begin
      (* We must grow the buffer.  Doubling its size will provide enough
         space since n <= String.length aux_buffer <= String.length buffer.
         Watch out for string length overflow, though. *)
      let newlen =
        min (2 * Bytes.length lexbuf.lex_buffer) Sys.max_string_length in
      if lexbuf.lex_buffer_len - lexbuf.lex_start_pos + n > newlen
      then failwith "Lexing.lex_refill: cannot grow buffer";
      let newbuf = Bytes.create newlen in
      (* Copy the valid data to the beginning of the new buffer *)
      Bytes.blit lexbuf.lex_buffer lexbuf.lex_start_pos
                  newbuf 0
                  (lexbuf.lex_buffer_len - lexbuf.lex_start_pos);
      lexbuf.lex_buffer <- newbuf
    end;
    (* Reallocation or not, we have shifted the data left by
       start_pos characters; update the positions *)
    let s = lexbuf.lex_start_pos in
    lexbuf.lex_abs_pos <- lexbuf.lex_abs_pos + s;
    lexbuf.lex_curr_pos <- lexbuf.lex_curr_pos - s;
    lexbuf.lex_start_pos <- 0;
    lexbuf.lex_last_pos <- lexbuf.lex_last_pos - s;
    lexbuf.lex_buffer_len <- lexbuf.lex_buffer_len - s ;
    let t = lexbuf.lex_mem in
    for i = 0 to Array.length t-1 do
      let v = t.(i) in
      if v >= 0 then
        t.(i) <- v-s
    done
  end;
  (* There is now enough space at the end of the buffer *)
  Bytes.blit aux_buffer 0 lexbuf.lex_buffer lexbuf.lex_buffer_len n;
  lexbuf.lex_buffer_len <- lexbuf.lex_buffer_len + n

let zero_pos = {
  pos_fname = "";
  pos_lnum = 1;
  pos_bol = 0;
  pos_cnum = 0;
}

let from_function f =
  { refill_buff = lex_refill f (Bytes.create 512);
    lex_buffer = Bytes.create 1024;
    lex_buffer_len = 0;
    lex_abs_pos = 0;
    lex_start_pos = 0;
    lex_curr_pos = 0;
    lex_last_pos = 0;
    lex_last_action = 0;
    lex_mem = [||];
    lex_eof_reached = false;
    lex_start_p = zero_pos;
    lex_curr_p = zero_pos;
  }

let from_channel ic =
  from_function (fun buf n -> input ic buf 0 n)

let from_string s =
  { refill_buff = (fun lexbuf -> lexbuf.lex_eof_reached <- true);
    lex_buffer = Bytes.of_string s; (* have to make a copy for compatibility
                                       with unsafe-string mode *)
    lex_buffer_len = String.length s;
    lex_abs_pos = 0;
    lex_start_pos = 0;
    lex_curr_pos = 0;
    lex_last_pos = 0;
    lex_last_action = 0;
    lex_mem = [||];
    lex_eof_reached = true;
    lex_start_p = zero_pos;
    lex_curr_p = zero_pos;
  }

let lexeme lexbuf =
  let len = lexbuf.lex_curr_pos - lexbuf.lex_start_pos in
  Bytes.sub_string lexbuf.lex_buffer lexbuf.lex_start_pos len

let sub_lexeme lexbuf i1 i2 =
  let len = i2-i1 in
  Bytes.sub_string lexbuf.lex_buffer i1 len

let sub_lexeme_opt lexbuf i1 i2 =
  if i1 >= 0 then begin
    let len = i2-i1 in
    Some (Bytes.sub_string lexbuf.lex_buffer i1 len)
  end else begin
    None
  end

let sub_lexeme_char lexbuf i = Bytes.get lexbuf.lex_buffer i

let sub_lexeme_char_opt lexbuf i =
  if i >= 0 then
    Some (Bytes.get lexbuf.lex_buffer i)
  else
    None


let lexeme_char lexbuf i =
  Bytes.get lexbuf.lex_buffer (lexbuf.lex_start_pos + i)

let lexeme_start lexbuf = lexbuf.lex_start_p.pos_cnum
let lexeme_end lexbuf = lexbuf.lex_curr_p.pos_cnum

let lexeme_start_p lexbuf = lexbuf.lex_start_p
let lexeme_end_p lexbuf = lexbuf.lex_curr_p

let new_line lexbuf =
  let lcp = lexbuf.lex_curr_p in
  lexbuf.lex_curr_p <- { lcp with
    pos_lnum = lcp.pos_lnum + 1;
    pos_bol = lcp.pos_cnum;
  }



(* Discard data left in lexer buffer. *)

let flush_input lb =
  lb.lex_curr_pos <- 0;
  lb.lex_abs_pos <- 0;
  lb.lex_curr_p <- {lb.lex_curr_p with pos_cnum = 0};
  lb.lex_buffer_len <- 0;
end

module Parsing = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* The parsing engine *)

open Lexing

(* Internal interface to the parsing engine *)

type parser_env =
  { mutable s_stack : int array;        (* States *)
    mutable v_stack : Obj.t array;      (* Semantic attributes *)
    mutable symb_start_stack : position array; (* Start positions *)
    mutable symb_end_stack : position array;   (* End positions *)
    mutable stacksize : int;            (* Size of the stacks *)
    mutable stackbase : int;            (* Base sp for current parse *)
    mutable curr_char : int;            (* Last token read *)
    mutable lval : Obj.t;               (* Its semantic attribute *)
    mutable symb_start : position;      (* Start pos. of the current symbol*)
    mutable symb_end : position;        (* End pos. of the current symbol *)
    mutable asp : int;                  (* The stack pointer for attributes *)
    mutable rule_len : int;             (* Number of rhs items in the rule *)
    mutable rule_number : int;          (* Rule number to reduce by *)
    mutable sp : int;                   (* Saved sp for parse_engine *)
    mutable state : int;                (* Saved state for parse_engine *)
    mutable errflag : int }             (* Saved error flag for parse_engine *)

type parse_tables =
  { actions : (parser_env -> Obj.t) array;
    transl_const : int array;
    transl_block : int array;
    lhs : string;
    len : string;
    defred : string;
    dgoto : string;
    sindex : string;
    rindex : string;
    gindex : string;
    tablesize : int;
    table : string;
    check : string;
    error_function : string -> unit;
    names_const : string;
    names_block : string }

exception YYexit of Obj.t
exception Parse_error

type parser_input =
    Start
  | Token_read
  | Stacks_grown_1
  | Stacks_grown_2
  | Semantic_action_computed
  | Error_detected

type parser_output =
    Read_token
  | Raise_parse_error
  | Grow_stacks_1
  | Grow_stacks_2
  | Compute_semantic_action
  | Call_error_function

(* to avoid warnings *)
let _ = [Read_token; Raise_parse_error; Grow_stacks_1; Grow_stacks_2;
         Compute_semantic_action; Call_error_function]

external parse_engine :
    parse_tables -> parser_env -> parser_input -> Obj.t -> parser_output
    = "caml_parse_engine"

external set_trace: bool -> bool
    = "caml_set_parser_trace"

let env =
  { s_stack = Array.make 100 0;
    v_stack = Array.make 100 (Obj.repr ());
    symb_start_stack = Array.make 100 dummy_pos;
    symb_end_stack = Array.make 100 dummy_pos;
    stacksize = 100;
    stackbase = 0;
    curr_char = 0;
    lval = Obj.repr ();
    symb_start = dummy_pos;
    symb_end = dummy_pos;
    asp = 0;
    rule_len = 0;
    rule_number = 0;
    sp = 0;
    state = 0;
    errflag = 0 }

let grow_stacks() =
  let oldsize = env.stacksize in
  let newsize = oldsize * 2 in
  let new_s = Array.make newsize 0
  and new_v = Array.make newsize (Obj.repr ())
  and new_start = Array.make newsize dummy_pos
  and new_end = Array.make newsize dummy_pos in
    Array.blit env.s_stack 0 new_s 0 oldsize;
    env.s_stack <- new_s;
    Array.blit env.v_stack 0 new_v 0 oldsize;
    env.v_stack <- new_v;
    Array.blit env.symb_start_stack 0 new_start 0 oldsize;
    env.symb_start_stack <- new_start;
    Array.blit env.symb_end_stack 0 new_end 0 oldsize;
    env.symb_end_stack <- new_end;
    env.stacksize <- newsize

let clear_parser() =
  Array.fill env.v_stack 0 env.stacksize (Obj.repr ());
  env.lval <- Obj.repr ()

let current_lookahead_fun = ref (fun (_ : Obj.t) -> false)

let yyparse tables start lexer lexbuf =
  let rec loop cmd arg =
    match parse_engine tables env cmd arg with
      Read_token ->
        let t = Obj.repr(lexer lexbuf) in
        env.symb_start <- lexbuf.lex_start_p;
        env.symb_end <- lexbuf.lex_curr_p;
        loop Token_read t
    | Raise_parse_error ->
        raise Parse_error
    | Compute_semantic_action ->
        let (action, value) =
          try
            (Semantic_action_computed, tables.actions.(env.rule_number) env)
          with Parse_error ->
            (Error_detected, Obj.repr ()) in
        loop action value
    | Grow_stacks_1 ->
        grow_stacks(); loop Stacks_grown_1 (Obj.repr ())
    | Grow_stacks_2 ->
        grow_stacks(); loop Stacks_grown_2 (Obj.repr ())
    | Call_error_function ->
        tables.error_function "syntax error";
        loop Error_detected (Obj.repr ()) in
  let init_asp = env.asp
  and init_sp = env.sp
  and init_stackbase = env.stackbase
  and init_state = env.state
  and init_curr_char = env.curr_char
  and init_lval = env.lval
  and init_errflag = env.errflag in
  env.stackbase <- env.sp + 1;
  env.curr_char <- start;
  env.symb_end <- lexbuf.lex_curr_p;
  try
    loop Start (Obj.repr ())
  with exn ->
    let curr_char = env.curr_char in
    env.asp <- init_asp;
    env.sp <- init_sp;
    env.stackbase <- init_stackbase;
    env.state <- init_state;
    env.curr_char <- init_curr_char;
    env.lval <- init_lval;
    env.errflag <- init_errflag;
    match exn with
      YYexit v ->
        Obj.magic v
    | _ ->
        current_lookahead_fun :=
          (fun tok ->
            if Obj.is_block tok
            then tables.transl_block.(Obj.tag tok) = curr_char
            else tables.transl_const.(Obj.magic tok) = curr_char);
        raise exn

let peek_val env n =
  Obj.magic env.v_stack.(env.asp - n)

let symbol_start_pos () =
  let rec loop i =
    if i <= 0 then env.symb_end_stack.(env.asp)
    else begin
      let st = env.symb_start_stack.(env.asp - i + 1) in
      let en = env.symb_end_stack.(env.asp - i + 1) in
      if st <> en then st else loop (i - 1)
    end
  in
  loop env.rule_len

let symbol_end_pos () = env.symb_end_stack.(env.asp)
let rhs_start_pos n = env.symb_start_stack.(env.asp - (env.rule_len - n))
let rhs_end_pos n = env.symb_end_stack.(env.asp - (env.rule_len - n))

let symbol_start () = (symbol_start_pos ()).pos_cnum
let symbol_end () = (symbol_end_pos ()).pos_cnum
let rhs_start n = (rhs_start_pos n).pos_cnum
let rhs_end n = (rhs_end_pos n).pos_cnum

let is_current_lookahead tok =
  (!current_lookahead_fun)(Obj.repr tok)

let parse_error (_ : string) = ()
end

module Weak = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Damien Doligez, projet Para, INRIA Rocquencourt            *)
(*                                                                        *)
(*   Copyright 1997 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(** Weak array operations *)

type 'a t

external create : int -> 'a t = "caml_weak_create"

(** number of additional values in a weak pointer *)
let additional_values = 2

let length x = Obj.size(Obj.repr x) - additional_values

external set : 'a t -> int -> 'a option -> unit = "caml_weak_set"
external get : 'a t -> int -> 'a option = "caml_weak_get"
external get_copy : 'a t -> int -> 'a option = "caml_weak_get_copy"
external check : 'a t -> int -> bool = "caml_weak_check"
external blit : 'a t -> int -> 'a t -> int -> int -> unit = "caml_weak_blit"
(* blit: src srcoff dst dstoff len *)

let fill ar ofs len x =
  if ofs < 0 || len < 0 || ofs + len > length ar
  then raise (Invalid_argument "Weak.fill")
  else begin
    for i = ofs to (ofs + len - 1) do
      set ar i x
    done
  end


(** Weak hash tables *)

module type S = sig
  type data
  type t
  val create : int -> t
  val clear : t -> unit
  val merge : t -> data -> data
  val add : t -> data -> unit
  val remove : t -> data -> unit
  val find : t -> data -> data
  val find_opt : t -> data -> data option
  val find_all : t -> data -> data list
  val mem : t -> data -> bool
  val iter : (data -> unit) -> t -> unit
  val fold : (data -> 'a -> 'a) -> t -> 'a -> 'a
  val count : t -> int
  val stats : t -> int * int * int * int * int * int
end

module Make (H : Hashtbl.HashedType) : (S with type data = H.t) = struct

  type 'a weak_t = 'a t
  let weak_create = create
  let emptybucket = weak_create 0

  type data = H.t

  type t = {
    mutable table : data weak_t array;
    mutable hashes : int array array;
    mutable limit : int;               (* bucket size limit *)
    mutable oversize : int;            (* number of oversize buckets *)
    mutable rover : int;               (* for internal bookkeeping *)
  }

  let get_index t h = (h land max_int) mod (Array.length t.table)

  let limit = 7
  let over_limit = 2

  let create sz =
    let sz = if sz < 7 then 7 else sz in
    let sz = if sz > Sys.max_array_length then Sys.max_array_length else sz in
    {
      table = Array.make sz emptybucket;
      hashes = Array.make sz [| |];
      limit = limit;
      oversize = 0;
      rover = 0;
    }

  let clear t =
    for i = 0 to Array.length t.table - 1 do
      t.table.(i) <- emptybucket;
      t.hashes.(i) <- [| |];
    done;
    t.limit <- limit;
    t.oversize <- 0


  let fold f t init =
    let rec fold_bucket i b accu =
      if i >= length b then accu else
      match get b i with
      | Some v -> fold_bucket (i+1) b (f v accu)
      | None -> fold_bucket (i+1) b accu
    in
    Array.fold_right (fold_bucket 0) t.table init


  let iter f t =
    let rec iter_bucket i b =
      if i >= length b then () else
      match get b i with
      | Some v -> f v; iter_bucket (i+1) b
      | None -> iter_bucket (i+1) b
    in
    Array.iter (iter_bucket 0) t.table


  let iter_weak f t =
    let rec iter_bucket i j b =
      if i >= length b then () else
      match check b i with
      | true -> f b t.hashes.(j) i; iter_bucket (i+1) j b
      | false -> iter_bucket (i+1) j b
    in
    Array.iteri (iter_bucket 0) t.table


  let rec count_bucket i b accu =
    if i >= length b then accu else
    count_bucket (i+1) b (accu + (if check b i then 1 else 0))


  let count t =
    Array.fold_right (count_bucket 0) t.table 0


  let next_sz n = min (3 * n / 2 + 3) Sys.max_array_length
  let prev_sz n = ((n - 3) * 2 + 2) / 3

  let test_shrink_bucket t =
    let bucket = t.table.(t.rover) in
    let hbucket = t.hashes.(t.rover) in
    let len = length bucket in
    let prev_len = prev_sz len in
    let live = count_bucket 0 bucket 0 in
    if live <= prev_len then begin
      let rec loop i j =
        if j >= prev_len then begin
          if check bucket i then loop (i + 1) j
          else if check bucket j then begin
            blit bucket j bucket i 1;
            hbucket.(i) <- hbucket.(j);
            loop (i + 1) (j - 1);
          end else loop i (j - 1);
        end;
      in
      loop 0 (length bucket - 1);
      if prev_len = 0 then begin
        t.table.(t.rover) <- emptybucket;
        t.hashes.(t.rover) <- [| |];
      end else begin
        Obj.truncate (Obj.repr bucket) (prev_len + additional_values);
        Obj.truncate (Obj.repr hbucket) prev_len;
      end;
      if len > t.limit && prev_len <= t.limit then t.oversize <- t.oversize - 1;
    end;
    t.rover <- (t.rover + 1) mod (Array.length t.table)


  let rec resize t =
    let oldlen = Array.length t.table in
    let newlen = next_sz oldlen in
    if newlen > oldlen then begin
      let newt = create newlen in
      let add_weak ob oh oi =
        let setter nb ni _ = blit ob oi nb ni 1 in
        let h = oh.(oi) in
        add_aux newt setter None h (get_index newt h);
      in
      iter_weak add_weak t;
      t.table <- newt.table;
      t.hashes <- newt.hashes;
      t.limit <- newt.limit;
      t.oversize <- newt.oversize;
      t.rover <- t.rover mod Array.length newt.table;
    end else begin
      t.limit <- max_int;             (* maximum size already reached *)
      t.oversize <- 0;
    end

  and add_aux t setter d h index =
    let bucket = t.table.(index) in
    let hashes = t.hashes.(index) in
    let sz = length bucket in
    let rec loop i =
      if i >= sz then begin
        let newsz =
          min (3 * sz / 2 + 3) (Sys.max_array_length - additional_values)
        in
        if newsz <= sz then failwith "Weak.Make: hash bucket cannot grow more";
        let newbucket = weak_create newsz in
        let newhashes = Array.make newsz 0 in
        blit bucket 0 newbucket 0 sz;
        Array.blit hashes 0 newhashes 0 sz;
        setter newbucket sz d;
        newhashes.(sz) <- h;
        t.table.(index) <- newbucket;
        t.hashes.(index) <- newhashes;
        if sz <= t.limit && newsz > t.limit then begin
          t.oversize <- t.oversize + 1;
          for _i = 0 to over_limit do test_shrink_bucket t done;
        end;
        if t.oversize > Array.length t.table / over_limit then resize t;
      end else if check bucket i then begin
        loop (i + 1)
      end else begin
        setter bucket i d;
        hashes.(i) <- h;
      end;
    in
    loop 0


  let add t d =
    let h = H.hash d in
    add_aux t set (Some d) h (get_index t h)


  let find_or t d ifnotfound =
    let h = H.hash d in
    let index = get_index t h in
    let bucket = t.table.(index) in
    let hashes = t.hashes.(index) in
    let sz = length bucket in
    let rec loop i =
      if i >= sz then ifnotfound h index
      else if h = hashes.(i) then begin
        match get_copy bucket i with
        | Some v when H.equal v d
           -> begin match get bucket i with
              | Some v -> v
              | None -> loop (i + 1)
              end
        | _ -> loop (i + 1)
      end else loop (i + 1)
    in
    loop 0


  let merge t d =
    find_or t d (fun h index -> add_aux t set (Some d) h index; d)


  let find t d = find_or t d (fun _h _index -> raise Not_found)

  let find_opt t d =
    let h = H.hash d in
    let index = get_index t h in
    let bucket = t.table.(index) in
    let hashes = t.hashes.(index) in
    let sz = length bucket in
    let rec loop i =
      if i >= sz then None
      else if h = hashes.(i) then begin
        match get_copy bucket i with
        | Some v when H.equal v d
           -> begin match get bucket i with
              | Some _ as v -> v
              | None -> loop (i + 1)
              end
        | _ -> loop (i + 1)
      end else loop (i + 1)
    in
    loop 0


  let find_shadow t d iffound ifnotfound =
    let h = H.hash d in
    let index = get_index t h in
    let bucket = t.table.(index) in
    let hashes = t.hashes.(index) in
    let sz = length bucket in
    let rec loop i =
      if i >= sz then ifnotfound
      else if h = hashes.(i) then begin
        match get_copy bucket i with
        | Some v when H.equal v d -> iffound bucket i
        | _ -> loop (i + 1)
      end else loop (i + 1)
    in
    loop 0


  let remove t d = find_shadow t d (fun w i -> set w i None) ()


  let mem t d = find_shadow t d (fun _w _i -> true) false


  let find_all t d =
    let h = H.hash d in
    let index = get_index t h in
    let bucket = t.table.(index) in
    let hashes = t.hashes.(index) in
    let sz = length bucket in
    let rec loop i accu =
      if i >= sz then accu
      else if h = hashes.(i) then begin
        match get_copy bucket i with
        | Some v when H.equal v d
           -> begin match get bucket i with
              | Some v -> loop (i + 1) (v :: accu)
              | None -> loop (i + 1) accu
              end
        | _ -> loop (i + 1) accu
      end else loop (i + 1) accu
    in
    loop 0 []


  let stats t =
    let len = Array.length t.table in
    let lens = Array.map length t.table in
    Array.sort compare lens;
    let totlen = Array.fold_left ( + ) 0 lens in
    (len, count t, totlen, lens.(0), lens.(len/2), lens.(len-1))


end
end

module Ephemeron = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Damien Doligez, projet Para, INRIA Rocquencourt            *)
(*                                                                        *)
(*   Copyright 1997 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

module type SeededS = sig
  include Hashtbl.SeededS
  val clean: 'a t -> unit
  val stats_alive: 'a t -> Hashtbl.statistics
    (** same as {!stats} but only count the alive bindings *)
end

module type S = sig
  include Hashtbl.S
  val clean: 'a t -> unit
  val stats_alive: 'a t -> Hashtbl.statistics
    (** same as {!stats} but only count the alive bindings *)
end

module GenHashTable = struct

  type equal =
  | ETrue | EFalse
  | EDead (** the garbage collector reclaimed the data *)

  module MakeSeeded(H: sig
    type t
    type 'a container
    val create: t -> 'a -> 'a container
    val hash: int -> t -> int
    val equal: 'a container -> t -> equal
    val get_data: 'a container -> 'a option
    val get_key: 'a container -> t option
    val set_key_data: 'a container -> t -> 'a -> unit
    val check_key: 'a container -> bool
  end) : SeededS with type key = H.t
  = struct

    type 'a t =
      { mutable size: int;                  (* number of entries *)
        mutable data: 'a bucketlist array;  (* the buckets *)
        mutable seed: int;                  (* for randomization *)
        initial_size: int;                  (* initial array size *)
      }

    and 'a bucketlist =
    | Empty
    | Cons of int (* hash of the key *) * 'a H.container * 'a bucketlist

    (** the hash of the key is kept in order to test the equality of the hash
      before the key. Same reason as for Weak.Make *)

    type key = H.t

    let rec power_2_above x n =
      if x >= n then x
      else if x * 2 > Sys.max_array_length then x
      else power_2_above (x * 2) n

    let prng = lazy (Random.State.make_self_init())

    let create ?(random = (Hashtbl.is_randomized ())) initial_size =
      let s = power_2_above 16 initial_size in
      let seed = if random then Random.State.bits (Lazy.force prng) else 0 in
      { initial_size = s; size = 0; seed = seed; data = Array.make s Empty }

    let clear h =
      h.size <- 0;
      let len = Array.length h.data in
      for i = 0 to len - 1 do
        h.data.(i) <- Empty
      done

    let reset h =
      let len = Array.length h.data in
      if len = h.initial_size then
        clear h
      else begin
        h.size <- 0;
        h.data <- Array.make h.initial_size Empty
      end

    let copy h = { h with data = Array.copy h.data }

    let key_index h hkey =
      hkey land (Array.length h.data - 1)

    let clean h =
      let rec do_bucket = function
        | Empty ->
            Empty
        | Cons(_, c, rest) when not (H.check_key c) ->
            h.size <- h.size - 1;
            do_bucket rest
        | Cons(hkey, c, rest) ->
            Cons(hkey, c, do_bucket rest)
      in
      let d = h.data in
      for i = 0 to Array.length d - 1 do
        d.(i) <- do_bucket d.(i)
      done

    (** resize is the only function to do the actual cleaning of dead keys
        (remove does it just because it could).

        The goal is to:

        - not resize infinitely when the actual number of alive keys is
        bounded but keys are continuously added. That would happen if
        this function always resize.
        - not call this function after each addition, that would happen if this
        function don't resize even when only one key is dead.

        So the algorithm:
        - clean the keys before resizing
        - if the number of remaining keys is less than half the size of the
        array, don't resize.
        - if it is more, resize.

        The second problem remains if the table reaches {!Sys.max_array_length}.

    *)
    let resize h =
      let odata = h.data in
      let osize = Array.length odata in
      let nsize = osize * 2 in
      clean h;
      if nsize < Sys.max_array_length && h.size >= osize lsr 1 then begin
        let ndata = Array.make nsize Empty in
        h.data <- ndata;       (* so that key_index sees the new bucket count *)
        let rec insert_bucket = function
            Empty -> ()
          | Cons(hkey, data, rest) ->
              insert_bucket rest; (* preserve original order of elements *)
              let nidx = key_index h hkey in
              ndata.(nidx) <- Cons(hkey, data, ndata.(nidx)) in
        for i = 0 to osize - 1 do
          insert_bucket odata.(i)
        done
      end

    let add h key info =
      let hkey = H.hash h.seed key in
      let i = key_index h hkey in
      let container = H.create key info in
      let bucket = Cons(hkey, container, h.data.(i)) in
      h.data.(i) <- bucket;
      h.size <- h.size + 1;
      if h.size > Array.length h.data lsl 1 then resize h

    let remove h key =
      let hkey = H.hash h.seed key in
      let rec remove_bucket = function
        | Empty -> Empty
        | Cons(hk, c, next) when hkey = hk ->
            begin match H.equal c key with
            | ETrue -> h.size <- h.size - 1; next
            | EFalse -> Cons(hk, c, remove_bucket next)
            | EDead ->
                (* The dead key is automatically removed. It is acceptable
                    for this function since it already removes a binding *)
                h.size <- h.size - 1;
                remove_bucket next
            end
        | Cons(hk,c,next) -> Cons(hk, c, remove_bucket next) in
      let i = key_index h hkey in
      h.data.(i) <- remove_bucket h.data.(i)

    (** {!find} don't remove dead keys because it would be surprising for
        the user that a read-only function mutates the state (eg. concurrent
        access). Same for {!iter}, {!fold}, {!mem}.
    *)
    let rec find_rec key hkey = function
      | Empty ->
          raise Not_found
      | Cons(hk, c, rest) when hkey = hk  ->
          begin match H.equal c key with
          | ETrue ->
              begin match H.get_data c with
              | None ->
                  (* This case is not impossible because the gc can run between
                      H.equal and H.get_data *)
                  find_rec key hkey rest
              | Some d -> d
              end
          | EFalse -> find_rec key hkey rest
          | EDead ->
              find_rec key hkey rest
          end
      | Cons(_, _, rest) ->
          find_rec key hkey rest

    let find h key =
      let hkey = H.hash h.seed key in
      (* TODO inline 3 iterations *)
      find_rec key hkey (h.data.(key_index h hkey))

    let rec find_rec_opt key hkey = function
      | Empty ->
          None
      | Cons(hk, c, rest) when hkey = hk  ->
          begin match H.equal c key with
          | ETrue ->
              begin match H.get_data c with
              | None ->
                  (* This case is not impossible because the gc can run between
                      H.equal and H.get_data *)
                  find_rec_opt key hkey rest
              | Some _ as d -> d
              end
          | EFalse -> find_rec_opt key hkey rest
          | EDead ->
              find_rec_opt key hkey rest
          end
      | Cons(_, _, rest) ->
          find_rec_opt key hkey rest

    let find_opt h key =
      let hkey = H.hash h.seed key in
      (* TODO inline 3 iterations *)
      find_rec_opt key hkey (h.data.(key_index h hkey))

    let find_all h key =
      let hkey = H.hash h.seed key in
      let rec find_in_bucket = function
      | Empty -> []
      | Cons(hk, c, rest) when hkey = hk  ->
          begin match H.equal c key with
          | ETrue -> begin match H.get_data c with
              | None ->
                  find_in_bucket rest
              | Some d -> d::find_in_bucket rest
            end
          | EFalse -> find_in_bucket rest
          | EDead ->
              find_in_bucket rest
          end
      | Cons(_, _, rest) ->
          find_in_bucket rest in
      find_in_bucket h.data.(key_index h hkey)


    let replace h key info =
      let hkey = H.hash h.seed key in
      let rec replace_bucket = function
        | Empty -> raise Not_found
        | Cons(hk, c, next) when hkey = hk ->
            begin match H.equal c key with
            | ETrue -> H.set_key_data c key info
            | EFalse | EDead -> replace_bucket next
            end
        | Cons(_,_,next) -> replace_bucket next
      in
      let i = key_index h hkey in
      let l = h.data.(i) in
      try
        replace_bucket l
      with Not_found ->
        let container = H.create key info in
        h.data.(i) <- Cons(hkey, container, l);
        h.size <- h.size + 1;
        if h.size > Array.length h.data lsl 1 then resize h

    let mem h key =
      let hkey = H.hash h.seed key in
      let rec mem_in_bucket = function
      | Empty ->
          false
      | Cons(hk, c, rest) when hk = hkey ->
          begin match H.equal c key with
          | ETrue -> true
          | EFalse | EDead -> mem_in_bucket rest
          end
      | Cons(_hk, _c, rest) -> mem_in_bucket rest in
      mem_in_bucket h.data.(key_index h hkey)

    let iter f h =
      let rec do_bucket = function
        | Empty ->
            ()
        | Cons(_, c, rest) ->
            begin match H.get_key c, H.get_data c with
            | None, _ | _, None -> ()
            | Some k, Some d -> f k d
            end; do_bucket rest in
      let d = h.data in
      for i = 0 to Array.length d - 1 do
        do_bucket d.(i)
      done

    let fold f h init =
      let rec do_bucket b accu =
        match b with
          Empty ->
            accu
        | Cons(_, c, rest) ->
            let accu = begin match H.get_key c, H.get_data c with
              | None, _ | _, None -> accu
              | Some k, Some d -> f k d accu
            end in
            do_bucket rest accu  in
      let d = h.data in
      let accu = ref init in
      for i = 0 to Array.length d - 1 do
        accu := do_bucket d.(i) !accu
      done;
      !accu

    let filter_map_inplace f h =
      let rec do_bucket = function
        | Empty ->
            Empty
        | Cons(hk, c, rest) ->
            match H.get_key c, H.get_data c with
            | None, _ | _, None ->
                do_bucket rest
            | Some k, Some d ->
                match f k d with
                | None ->
                    do_bucket rest
                | Some new_d ->
                    H.set_key_data c k new_d;
                    Cons(hk, c, do_bucket rest)
      in
      let d = h.data in
      for i = 0 to Array.length d - 1 do
        d.(i) <- do_bucket d.(i)
      done

    let length h = h.size

    let rec bucket_length accu = function
      | Empty -> accu
      | Cons(_, _, rest) -> bucket_length (accu + 1) rest

    let stats h =
      let mbl =
        Array.fold_left (fun m b -> max m (bucket_length 0 b)) 0 h.data in
      let histo = Array.make (mbl + 1) 0 in
      Array.iter
        (fun b ->
           let l = bucket_length 0 b in
           histo.(l) <- histo.(l) + 1)
        h.data;
      { Hashtbl.num_bindings = h.size;
        num_buckets = Array.length h.data;
        max_bucket_length = mbl;
        bucket_histogram = histo }

    let rec bucket_length_alive accu = function
      | Empty -> accu
      | Cons(_, c, rest) when H.check_key c ->
          bucket_length_alive (accu + 1) rest
      | Cons(_, _, rest) -> bucket_length_alive accu rest

    let stats_alive h =
      let size = ref 0 in
      let mbl =
        Array.fold_left (fun m b -> max m (bucket_length_alive 0 b)) 0 h.data in
      let histo = Array.make (mbl + 1) 0 in
      Array.iter
        (fun b ->
           let l = bucket_length_alive 0 b in
           size := !size + l;
           histo.(l) <- histo.(l) + 1)
        h.data;
      { Hashtbl.num_bindings = !size;
        num_buckets = Array.length h.data;
        max_bucket_length = mbl;
        bucket_histogram = histo }

    let to_seq tbl =
      (* capture current array, so that even if the table is resized we
         keep iterating on the same array *)
      let tbl_data = tbl.data in
      (* state: index * next bucket to traverse *)
      let rec aux i buck () = match buck with
        | Empty ->
            if i = Array.length tbl_data
            then Seq.Nil
            else aux(i+1) tbl_data.(i) ()
        | Cons (_, c, next) ->
            begin match H.get_key c, H.get_data c with
              | None, _ | _, None -> aux i next ()
              | Some key, Some data ->
                  Seq.Cons ((key, data), aux i next)
            end
      in
      aux 0 Empty

    let to_seq_keys m = Seq.map fst (to_seq m)

    let to_seq_values m = Seq.map snd (to_seq m)

    let add_seq tbl i =
      Seq.iter (fun (k,v) -> add tbl k v) i

    let replace_seq tbl i =
      Seq.iter (fun (k,v) -> replace tbl k v) i

    let of_seq i =
      let tbl = create 16 in
      replace_seq tbl i;
      tbl

  end
end

module ObjEph = Obj.Ephemeron

let _obj_opt : Obj.t option -> 'a option = fun x ->
  match x with
  | None -> x
  | Some v -> Some (Obj.obj v)

(** The previous function is typed so this one is also correct *)
let obj_opt : Obj.t option -> 'a option = fun x -> Obj.magic x


module K1 = struct
  type ('k,'d) t = ObjEph.t

  let create () : ('k,'d) t = ObjEph.create 1

  let get_key (t:('k,'d) t) : 'k option = obj_opt (ObjEph.get_key t 0)
  let get_key_copy (t:('k,'d) t) : 'k option = obj_opt (ObjEph.get_key_copy t 0)
  let set_key (t:('k,'d) t) (k:'k) : unit = ObjEph.set_key t 0 (Obj.repr k)
  let unset_key (t:('k,'d) t) : unit = ObjEph.unset_key t 0
  let check_key (t:('k,'d) t) : bool = ObjEph.check_key t 0

  let blit_key (t1:('k,'d) t) (t2:('k,'d) t): unit =
    ObjEph.blit_key t1 0 t2 0 1

  let get_data (t:('k,'d) t) : 'd option = obj_opt (ObjEph.get_data t)
  let get_data_copy (t:('k,'d) t) : 'd option = obj_opt (ObjEph.get_data_copy t)
  let set_data (t:('k,'d) t) (d:'d) : unit = ObjEph.set_data t (Obj.repr d)
  let unset_data (t:('k,'d) t) : unit = ObjEph.unset_data t
  let check_data (t:('k,'d) t) : bool = ObjEph.check_data t
  let blit_data (t1:(_,'d) t) (t2:(_,'d) t) : unit = ObjEph.blit_data t1 t2

  module MakeSeeded (H:Hashtbl.SeededHashedType) =
    GenHashTable.MakeSeeded(struct
      type 'a container = (H.t,'a) t
      type t = H.t
      let create k d =
        let c = create () in
        set_data c d;
        set_key c k;
        c
      let hash = H.hash
      let equal c k =
        (* {!get_key_copy} is not used because the equality of the user can be
            the physical equality *)
        match get_key c with
        | None -> GenHashTable.EDead
        | Some k' ->
            if H.equal k k' then GenHashTable.ETrue else GenHashTable.EFalse
      let get_data = get_data
      let get_key = get_key
      let set_key_data c k d =
        unset_data c;
        set_key c k;
        set_data c d
      let check_key = check_key
    end)

  module Make(H: Hashtbl.HashedType): (S with type key = H.t) =
  struct
    include MakeSeeded(struct
        type t = H.t
        let equal = H.equal
        let hash (_seed: int) x = H.hash x
      end)
    let create sz = create ~random:false sz
  end

end

module K2 = struct
  type ('k1, 'k2, 'd) t = ObjEph.t

  let create () : ('k1,'k2,'d) t = ObjEph.create 2

  let get_key1 (t:('k1,'k2,'d) t) : 'k1 option = obj_opt (ObjEph.get_key t 0)
  let get_key1_copy (t:('k1,'k2,'d) t) : 'k1 option =
    obj_opt (ObjEph.get_key_copy t 0)
  let set_key1 (t:('k1,'k2,'d) t) (k:'k1) : unit =
    ObjEph.set_key t 0 (Obj.repr k)
  let unset_key1 (t:('k1,'k2,'d) t) : unit = ObjEph.unset_key t 0
  let check_key1 (t:('k1,'k2,'d) t) : bool = ObjEph.check_key t 0

  let get_key2 (t:('k1,'k2,'d) t) : 'k2 option = obj_opt (ObjEph.get_key t 1)
  let get_key2_copy (t:('k1,'k2,'d) t) : 'k2 option =
    obj_opt (ObjEph.get_key_copy t 1)
  let set_key2 (t:('k1,'k2,'d) t) (k:'k2) : unit =
    ObjEph.set_key t 1 (Obj.repr k)
  let unset_key2 (t:('k1,'k2,'d) t) : unit = ObjEph.unset_key t 1
  let check_key2 (t:('k1,'k2,'d) t) : bool = ObjEph.check_key t 1


  let blit_key1 (t1:('k1,_,_) t) (t2:('k1,_,_) t) : unit =
    ObjEph.blit_key t1 0 t2 0 1
  let blit_key2 (t1:(_,'k2,_) t) (t2:(_,'k2,_) t) : unit =
    ObjEph.blit_key t1 1 t2 1 1
  let blit_key12 (t1:('k1,'k2,_) t) (t2:('k1,'k2,_) t) : unit =
    ObjEph.blit_key t1 0 t2 0 2

  let get_data (t:('k1,'k2,'d) t) : 'd option = obj_opt (ObjEph.get_data t)
  let get_data_copy (t:('k1,'k2,'d) t) : 'd option =
    obj_opt (ObjEph.get_data_copy t)
  let set_data (t:('k1,'k2,'d) t) (d:'d) : unit =
    ObjEph.set_data t (Obj.repr d)
  let unset_data (t:('k1,'k2,'d) t) : unit = ObjEph.unset_data t
  let check_data (t:('k1,'k2,'d) t) : bool = ObjEph.check_data t
  let blit_data (t1:(_,_,'d) t) (t2:(_,_,'d) t) : unit = ObjEph.blit_data t1 t2

  module MakeSeeded
      (H1:Hashtbl.SeededHashedType)
      (H2:Hashtbl.SeededHashedType) =
    GenHashTable.MakeSeeded(struct
      type 'a container = (H1.t,H2.t,'a) t
      type t = H1.t * H2.t
      let create (k1,k2) d =
        let c = create () in
        set_data c d;
        set_key1 c k1; set_key2 c k2;
        c
      let hash seed (k1,k2) =
        H1.hash seed k1 + H2.hash seed k2 * 65599
      let equal c (k1,k2) =
        match get_key1 c, get_key2 c with
        | None, _ | _ , None -> GenHashTable.EDead
        | Some k1', Some k2' ->
            if H1.equal k1 k1' && H2.equal k2 k2'
            then GenHashTable.ETrue else GenHashTable.EFalse
      let get_data = get_data
      let get_key c =
        match get_key1 c, get_key2 c with
        | None, _ | _ , None -> None
        | Some k1', Some k2' -> Some (k1', k2')
      let set_key_data c (k1,k2) d =
        unset_data c;
        set_key1 c k1; set_key2 c k2;
        set_data c d
      let check_key c = check_key1 c && check_key2 c
    end)

  module Make(H1: Hashtbl.HashedType)(H2: Hashtbl.HashedType):
    (S with type key = H1.t * H2.t) =
  struct
    include MakeSeeded
        (struct
          type t = H1.t
          let equal = H1.equal
          let hash (_seed: int) x = H1.hash x
        end)
        (struct
          type t = H2.t
          let equal = H2.equal
          let hash (_seed: int) x = H2.hash x
        end)
    let create sz = create ~random:false sz
  end

end

module Kn = struct
  type ('k,'d) t = ObjEph.t

  let create n : ('k,'d) t = ObjEph.create n
  let length (k:('k,'d) t) : int = ObjEph.length k

  let get_key (t:('k,'d) t) (n:int) : 'k option = obj_opt (ObjEph.get_key t n)
  let get_key_copy (t:('k,'d) t) (n:int) : 'k option =
    obj_opt (ObjEph.get_key_copy t n)
  let set_key (t:('k,'d) t) (n:int) (k:'k) : unit =
    ObjEph.set_key t n (Obj.repr k)
  let unset_key (t:('k,'d) t) (n:int) : unit = ObjEph.unset_key t n
  let check_key (t:('k,'d) t) (n:int) : bool = ObjEph.check_key t n

  let blit_key (t1:('k,'d) t) (o1:int) (t2:('k,'d) t) (o2:int) (l:int) : unit =
    ObjEph.blit_key t1 o1 t2 o2 l

  let get_data (t:('k,'d) t) : 'd option = obj_opt (ObjEph.get_data t)
  let get_data_copy (t:('k,'d) t) : 'd option = obj_opt (ObjEph.get_data_copy t)
  let set_data (t:('k,'d) t) (d:'d) : unit = ObjEph.set_data t (Obj.repr d)
  let unset_data (t:('k,'d) t) : unit = ObjEph.unset_data t
  let check_data (t:('k,'d) t) : bool = ObjEph.check_data t
  let blit_data (t1:(_,'d) t) (t2:(_,'d) t) : unit = ObjEph.blit_data t1 t2

  module MakeSeeded (H:Hashtbl.SeededHashedType) =
    GenHashTable.MakeSeeded(struct
      type 'a container = (H.t,'a) t
      type t = H.t array
      let create k d =
        let c = create (Array.length k) in
        set_data c d;
        for i=0 to Array.length k -1 do
          set_key c i k.(i);
        done;
        c
      let hash seed k =
        let h = ref 0 in
        for i=0 to Array.length k -1 do
          h := H.hash seed k.(i) * 65599 + !h;
        done;
        !h
      let equal c k =
        let len  = Array.length k in
        let len' = length c in
        if len != len' then GenHashTable.EFalse
        else
          let rec equal_array k c i =
            if i < 0 then GenHashTable.ETrue
            else
              match get_key c i with
              | None -> GenHashTable.EDead
              | Some ki ->
                  if H.equal k.(i) ki
                  then equal_array k c (i-1)
                  else GenHashTable.EFalse
          in
          equal_array k c (len-1)
      let get_data = get_data
      let get_key c =
        let len = length c in
        if len = 0 then Some [||]
        else
          match get_key c 0 with
          | None -> None
          | Some k0 ->
              let rec fill a i =
                if i < 1 then Some a
                else
                  match get_key c i with
                  | None -> None
                  | Some ki ->
                      a.(i) <- ki;
                      fill a (i-1)
              in
              let a = Array.make len k0 in
              fill a (len-1)
      let set_key_data c k d =
        unset_data c;
        for i=0 to Array.length k -1 do
          set_key c i k.(i);
        done;
        set_data c d
      let check_key c =
        let rec check c i =
          i < 0 || (check_key c i && check c (i-1)) in
        check c (length c - 1)
    end)

  module Make(H: Hashtbl.HashedType): (S with type key = H.t array) =
  struct
    include MakeSeeded(struct
        type t = H.t
        let equal = H.equal
        let hash (_seed: int) x = H.hash x
      end)
    let create sz = create ~random:false sz
  end
end
end

(* module Spacetime = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*           Mark Shinwell and Leo White, Jane Street Europe              *)
(*                                                                        *)
(*   Copyright 2015--2016 Jane Street Group LLC                           *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

external spacetime_enabled : unit -> bool
  = "caml_spacetime_enabled" [@@noalloc]

let enabled = spacetime_enabled ()

let if_spacetime_enabled f =
  if enabled then f () else ()

module Series = struct
  type t = {
    channel : out_channel;
    mutable closed : bool;
  }

  external write_magic_number : out_channel -> unit
    = "caml_spacetime_only_works_for_native_code"
      "caml_spacetime_write_magic_number"

  external register_channel_for_spacetime : out_channel -> unit
    = "caml_register_channel_for_spacetime"

  let create ~path =
    if spacetime_enabled () then begin
      let channel = open_out path in
      register_channel_for_spacetime channel;
      let t =
        { channel = channel;
          closed = false;
        }
      in
      write_magic_number t.channel;
      t
    end else begin
      { channel = stdout;  (* arbitrary value *)
        closed = true;
      }
    end

  external save_event : ?time:float -> out_channel -> event_name:string -> unit
    = "caml_spacetime_only_works_for_native_code"
      "caml_spacetime_save_event"

  let save_event ?time t ~event_name =
    if_spacetime_enabled (fun () ->
      save_event ?time t.channel ~event_name)

  external save_trie : ?time:float -> out_channel -> unit
    = "caml_spacetime_only_works_for_native_code"
      "caml_spacetime_save_trie"

  let save_and_close ?time t =
    if_spacetime_enabled (fun () ->
      if t.closed then failwith "Series is closed";
      save_trie ?time t.channel;
      close_out t.channel;
      t.closed <- true)
end

module Snapshot = struct
  external take : ?time:float -> out_channel -> unit
    = "caml_spacetime_only_works_for_native_code"
      "caml_spacetime_take_snapshot"

  let take ?time { Series.closed; channel } =
    if_spacetime_enabled (fun () ->
      if closed then failwith "Series is closed";
      Gc.minor ();
      take ?time channel)
end

external save_event_for_automatic_snapshots : event_name:string -> unit
  = "caml_spacetime_only_works_for_native_code"
    "caml_spacetime_save_event_for_automatic_snapshots"

let save_event_for_automatic_snapshots ~event_name =
  if_spacetime_enabled (fun () ->
    save_event_for_automatic_snapshots ~event_name)
end *)

module Stack = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

type 'a t = { mutable c : 'a list; mutable len : int; }

exception Empty

let create () = { c = []; len = 0; }

let clear s = s.c <- []; s.len <- 0

let copy s = { c = s.c; len = s.len; }

let push x s = s.c <- x :: s.c; s.len <- s.len + 1

let pop s =
  match s.c with
  | hd::tl -> s.c <- tl; s.len <- s.len - 1; hd
  | []     -> raise Empty

let top s =
  match s.c with
  | hd::_ -> hd
  | []     -> raise Empty

let is_empty s = (s.c = [])

let length s = s.len

let iter f s = List.iter f s.c

let fold f acc s = List.fold_left f acc s.c

(** {6 Iterators} *)

let to_seq s = List.to_seq s.c

let add_seq q i = Seq.iter (fun x -> push x q) i

let of_seq g =
  let s = create() in
  add_seq s g;
  s
end

module Arg = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*              Damien Doligez, projet Para, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

type key = string
type doc = string
type usage_msg = string
type anon_fun = (string -> unit)

type spec =
  | Unit of (unit -> unit)     (* Call the function with unit argument *)
  | Bool of (bool -> unit)     (* Call the function with a bool argument *)
  | Set of bool ref            (* Set the reference to true *)
  | Clear of bool ref          (* Set the reference to false *)
  | String of (string -> unit) (* Call the function with a string argument *)
  | Set_string of string ref   (* Set the reference to the string argument *)
  | Int of (int -> unit)       (* Call the function with an int argument *)
  | Set_int of int ref         (* Set the reference to the int argument *)
  | Float of (float -> unit)   (* Call the function with a float argument *)
  | Set_float of float ref     (* Set the reference to the float argument *)
  | Tuple of spec list         (* Take several arguments according to the
                                  spec list *)
  | Symbol of string list * (string -> unit)
                               (* Take one of the symbols as argument and
                                  call the function with the symbol. *)
  | Rest of (string -> unit)   (* Stop interpreting keywords and call the
                                  function with each remaining argument *)
  | Expand of (string -> string array) (* If the remaining arguments to process
                                          are of the form
                                          [["-foo"; "arg"] @ rest] where "foo" is
                                          registered as [Expand f], then the
                                          arguments [f "arg" @ rest] are
                                          processed. Only allowed in
                                          [parse_and_expand_argv_dynamic]. *)

exception Bad of string
exception Help of string

type error =
  | Unknown of string
  | Wrong of string * string * string  (* option, actual, expected *)
  | Missing of string
  | Message of string

exception Stop of error (* used internally *)

open Printf

let rec assoc3 x l =
  match l with
  | [] -> raise Not_found
  | (y1, y2, _) :: _ when y1 = x -> y2
  | _ :: t -> assoc3 x t


let split s =
  let i = String.index s '=' in
  let len = String.length s in
  String.sub s 0 i, String.sub s (i+1) (len-(i+1))


let make_symlist prefix sep suffix l =
  match l with
  | [] -> "<none>"
  | h::t -> (List.fold_left (fun x y -> x ^ sep ^ y) (prefix ^ h) t) ^ suffix


let print_spec buf (key, spec, doc) =
  if String.length doc > 0 then
    match spec with
    | Symbol (l, _) ->
        bprintf buf "  %s %s%s\n" key (make_symlist "{" "|" "}" l) doc
    | _ ->
        bprintf buf "  %s %s\n" key doc


let help_action () = raise (Stop (Unknown "-help"))

let add_help speclist =
  let add1 =
    try ignore (assoc3 "-help" speclist); []
    with Not_found ->
            ["-help", Unit help_action, " Display this list of options"]
  and add2 =
    try ignore (assoc3 "--help" speclist); []
    with Not_found ->
            ["--help", Unit help_action, " Display this list of options"]
  in
  speclist @ (add1 @ add2)


let usage_b buf speclist errmsg =
  bprintf buf "%s\n" errmsg;
  List.iter (print_spec buf) (add_help speclist)


let usage_string speclist errmsg =
  let b = Buffer.create 200 in
  usage_b b speclist errmsg;
  Buffer.contents b


let usage speclist errmsg =
  eprintf "%s" (usage_string speclist errmsg)


let current = ref 0

let bool_of_string_opt x =
  try Some (bool_of_string x)
  with Invalid_argument _ -> None

let int_of_string_opt x =
  try Some (int_of_string x)
  with Failure _ -> None

let float_of_string_opt x =
  try Some (float_of_string x)
  with Failure _ -> None

let parse_and_expand_argv_dynamic_aux allow_expand current argv speclist anonfun errmsg =
  let initpos = !current in
  let convert_error error =
    (* convert an internal error to a Bad/Help exception
       *or* add the program name as a prefix and the usage message as a suffix
       to an user-raised Bad exception.
    *)
    let b = Buffer.create 200 in
    let progname = if initpos < (Array.length !argv) then !argv.(initpos) else "(?)" in
    begin match error with
      | Unknown "-help" -> ()
      | Unknown "--help" -> ()
      | Unknown s ->
          bprintf b "%s: unknown option '%s'.\n" progname s
      | Missing s ->
          bprintf b "%s: option '%s' needs an argument.\n" progname s
      | Wrong (opt, arg, expected) ->
          bprintf b "%s: wrong argument '%s'; option '%s' expects %s.\n"
                  progname arg opt expected
      | Message s -> (* user error message *)
          bprintf b "%s: %s.\n" progname s
    end;
    usage_b b !speclist errmsg;
    if error = Unknown "-help" || error = Unknown "--help"
    then Help (Buffer.contents b)
    else Bad (Buffer.contents b)
  in
  incr current;
  while !current < (Array.length !argv) do
    begin try
      let s = !argv.(!current) in
      if String.length s >= 1 && s.[0] = '-' then begin
        let action, follow =
          try assoc3 s !speclist, None
          with Not_found ->
          try
            let keyword, arg = split s in
            assoc3 keyword !speclist, Some arg
          with Not_found -> raise (Stop (Unknown s))
        in
        let no_arg () =
          match follow with
          | None -> ()
          | Some arg -> raise (Stop (Wrong (s, arg, "no argument"))) in
        let get_arg () =
          match follow with
          | None ->
              if !current + 1 < (Array.length !argv) then !argv.(!current + 1)
              else raise (Stop (Missing s))
          | Some arg -> arg
        in
        let consume_arg () =
          match follow with
          | None -> incr current
          | Some _ -> ()
        in
        let rec treat_action = function
        | Unit f -> f ();
        | Bool f ->
            let arg = get_arg () in
            begin match bool_of_string_opt arg with
            | None -> raise (Stop (Wrong (s, arg, "a boolean")))
            | Some s -> f s
            end;
            consume_arg ();
        | Set r -> no_arg (); r := true;
        | Clear r -> no_arg (); r := false;
        | String f ->
            let arg = get_arg () in
            f arg;
            consume_arg ();
        | Symbol (symb, f) ->
            let arg = get_arg () in
            if List.mem arg symb then begin
              f arg;
              consume_arg ();
            end else begin
              raise (Stop (Wrong (s, arg, "one of: "
                                          ^ (make_symlist "" " " "" symb))))
            end
        | Set_string r ->
            r := get_arg ();
            consume_arg ();
        | Int f ->
            let arg = get_arg () in
            begin match int_of_string_opt arg with
            | None -> raise (Stop (Wrong (s, arg, "an integer")))
            | Some x -> f x
            end;
            consume_arg ();
        | Set_int r ->
            let arg = get_arg () in
            begin match int_of_string_opt arg with
            | None -> raise (Stop (Wrong (s, arg, "an integer")))
            | Some x -> r := x
            end;
            consume_arg ();
        | Float f ->
            let arg = get_arg () in
            begin match float_of_string_opt arg with
            | None -> raise (Stop (Wrong (s, arg, "a float")))
            | Some x -> f x
            end;
            consume_arg ();
        | Set_float r ->
            let arg = get_arg () in
            begin match float_of_string_opt arg with
            | None -> raise (Stop (Wrong (s, arg, "a float")))
            | Some x -> r := x
            end;
            consume_arg ();
        | Tuple specs ->
            List.iter treat_action specs;
        | Rest f ->
            while !current < (Array.length !argv) - 1 do
              f !argv.(!current + 1);
              consume_arg ();
            done;
        | Expand f ->
            if not allow_expand then
              raise (Invalid_argument "Arg.Expand is is only allowed with Arg.parse_and_expand_argv_dynamic");
            let arg = get_arg () in
            let newarg = f arg in
            consume_arg ();
            let before = Array.sub !argv 0 (!current + 1)
            and after = Array.sub !argv (!current + 1) ((Array.length !argv) - !current - 1) in
            argv:= Array.concat [before;newarg;after];
        in
        treat_action action end
      else anonfun s
    with | Bad m -> raise (convert_error (Message m));
         | Stop e -> raise (convert_error e);
    end;
    incr current
  done

let parse_and_expand_argv_dynamic current argv speclist anonfun errmsg =
  parse_and_expand_argv_dynamic_aux true current argv speclist anonfun errmsg

let parse_argv_dynamic ?(current=current) argv speclist anonfun errmsg =
  parse_and_expand_argv_dynamic_aux false current (ref argv) speclist anonfun errmsg


let parse_argv ?(current=current) argv speclist anonfun errmsg =
  parse_argv_dynamic ~current:current argv (ref speclist) anonfun errmsg


let parse l f msg =
  try
    parse_argv Sys.argv l f msg
  with
  | Bad msg -> eprintf "%s" msg; exit 2
  | Help msg -> printf "%s" msg; exit 0


let parse_dynamic l f msg =
  try
    parse_argv_dynamic Sys.argv l f msg
  with
  | Bad msg -> eprintf "%s" msg; exit 2
  | Help msg -> printf "%s" msg; exit 0

let parse_expand l f msg =
  try
    let argv = ref Sys.argv in
    let spec = ref l in
    let current = ref (!current) in
    parse_and_expand_argv_dynamic current argv spec f msg
  with
  | Bad msg -> eprintf "%s" msg; exit 2
  | Help msg -> printf "%s" msg; exit 0


let second_word s =
  let len = String.length s in
  let rec loop n =
    if n >= len then len
    else if s.[n] = ' ' then loop (n+1)
    else n
  in
  match String.index s '\t' with
  | n -> loop (n+1)
  | exception Not_found ->
      begin match String.index s ' ' with
      | n -> loop (n+1)
      | exception Not_found -> len
      end


let max_arg_len cur (kwd, spec, doc) =
  match spec with
  | Symbol _ -> max cur (String.length kwd)
  | _ -> max cur (String.length kwd + second_word doc)


let replace_leading_tab s =
  let seen = ref false in
  String.map (function '\t' when not !seen -> seen := true; ' ' | c -> c) s

let add_padding len ksd =
  match ksd with
  | (_, _, "") ->
      (* Do not pad undocumented options, so that they still don't show up when
       * run through [usage] or [parse]. *)
      ksd
  | (kwd, (Symbol _ as spec), msg) ->
      let cutcol = second_word msg in
      let spaces = String.make ((max 0 (len - cutcol)) + 3) ' ' in
      (kwd, spec, "\n" ^ spaces ^ replace_leading_tab msg)
  | (kwd, spec, msg) ->
      let cutcol = second_word msg in
      let kwd_len = String.length kwd in
      let diff = len - kwd_len - cutcol in
      if diff <= 0 then
        (kwd, spec, replace_leading_tab msg)
      else
        let spaces = String.make diff ' ' in
        let prefix = String.sub (replace_leading_tab msg) 0 cutcol in
        let suffix = String.sub msg cutcol (String.length msg - cutcol) in
        (kwd, spec, prefix ^ spaces ^ suffix)


let align ?(limit=max_int) speclist =
  let completed = add_help speclist in
  let len = List.fold_left max_arg_len 0 completed in
  let len = min len limit in
  List.map (add_padding len) completed

let trim_cr s =
  let len = String.length s in
  if len > 0 && String.get s (len - 1) = '\r' then
    String.sub s 0 (len - 1)
  else
    s

let read_aux trim sep file =
  let ic = open_in_bin file in
  let buf = Buffer.create 200 in
  let words = ref [] in
  let stash () =
    let word = Buffer.contents buf in
    let word = if trim then trim_cr word else word in
    words := word :: !words;
    Buffer.clear buf
  in
  begin
    try while true do
        let c = input_char ic in
        if c = sep then stash () else Buffer.add_char buf c
      done
    with End_of_file -> ()
  end;
  if Buffer.length buf > 0 then stash ();
  close_in ic;
  Array.of_list (List.rev !words)

let read_arg = read_aux true '\n'

let read_arg0 = read_aux false '\x00'

let write_aux sep file args =
  let oc = open_out_bin file in
  Array.iter (fun s -> fprintf oc "%s%c" s sep) args;
  close_out oc

let write_arg = write_aux '\n'

let write_arg0 = write_aux '\x00'
end

module Filename = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*           Xavier Leroy and Damien Doligez, INRIA Rocquencourt          *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

let generic_quote quotequote s =
  let l = String.length s in
  let b = Buffer.create (l + 20) in
  Buffer.add_char b '\'';
  for i = 0 to l - 1 do
    if s.[i] = '\''
    then Buffer.add_string b quotequote
    else Buffer.add_char b  s.[i]
  done;
  Buffer.add_char b '\'';
  Buffer.contents b

(* This function implements the Open Group specification found here:
  [[1]] http://pubs.opengroup.org/onlinepubs/9699919799/utilities/basename.html
  In step 1 of [[1]], we choose to return "." for empty input.
    (for compatibility with previous versions of OCaml)
  In step 2, we choose to process "//" normally.
  Step 6 is not implemented: we consider that the [suffix] operand is
    always absent.  Suffixes are handled by [chop_suffix] and [chop_extension].
*)
let generic_basename is_dir_sep current_dir_name name =
  let rec find_end n =
    if n < 0 then String.sub name 0 1
    else if is_dir_sep name n then find_end (n - 1)
    else find_beg n (n + 1)
  and find_beg n p =
    if n < 0 then String.sub name 0 p
    else if is_dir_sep name n then String.sub name (n + 1) (p - n - 1)
    else find_beg (n - 1) p
  in
  if name = ""
  then current_dir_name
  else find_end (String.length name - 1)

(* This function implements the Open Group specification found here:
  [[2]] http://pubs.opengroup.org/onlinepubs/9699919799/utilities/dirname.html
  In step 6 of [[2]], we choose to process "//" normally.
*)
let generic_dirname is_dir_sep current_dir_name name =
  let rec trailing_sep n =
    if n < 0 then String.sub name 0 1
    else if is_dir_sep name n then trailing_sep (n - 1)
    else base n
  and base n =
    if n < 0 then current_dir_name
    else if is_dir_sep name n then intermediate_sep n
    else base (n - 1)
  and intermediate_sep n =
    if n < 0 then String.sub name 0 1
    else if is_dir_sep name n then intermediate_sep (n - 1)
    else String.sub name 0 (n + 1)
  in
  if name = ""
  then current_dir_name
  else trailing_sep (String.length name - 1)

module Unix = struct
  let current_dir_name = "."
  let parent_dir_name = ".."
  let dir_sep = "/"
  let is_dir_sep s i = s.[i] = '/'
  let is_relative n = String.length n < 1 || n.[0] <> '/'
  let is_implicit n =
    is_relative n
    && (String.length n < 2 || String.sub n 0 2 <> "./")
    && (String.length n < 3 || String.sub n 0 3 <> "../")
  let check_suffix name suff =
    String.length name >= String.length suff &&
    String.sub name (String.length name - String.length suff)
                    (String.length suff) = suff
  let temp_dir_name = "tmp"
    (* try Sys.getenv "TMPDIR" with _ -> "/tmp" *)
  let quote = generic_quote "'\\''"
  let basename = generic_basename is_dir_sep current_dir_name
  let dirname = generic_dirname is_dir_sep current_dir_name
end

module Win32 = struct
  let current_dir_name = "."
  let parent_dir_name = ".."
  let dir_sep = "\\"
  let is_dir_sep s i = let c = s.[i] in c = '/' || c = '\\' || c = ':'
  let is_relative n =
    (String.length n < 1 || n.[0] <> '/')
    && (String.length n < 1 || n.[0] <> '\\')
    && (String.length n < 2 || n.[1] <> ':')
  let is_implicit n =
    is_relative n
    && (String.length n < 2 || String.sub n 0 2 <> "./")
    && (String.length n < 2 || String.sub n 0 2 <> ".\\")
    && (String.length n < 3 || String.sub n 0 3 <> "../")
    && (String.length n < 3 || String.sub n 0 3 <> "..\\")
  let check_suffix name suff =
   String.length name >= String.length suff &&
   (let s = String.sub name (String.length name - String.length suff)
                            (String.length suff) in
    String.lowercase_ascii s = String.lowercase_ascii suff)
  (* let temp_dir_name =
    try Sys.getenv "TEMP" with _ -> "." *)
  let quote s =
    let l = String.length s in
    let b = Buffer.create (l + 20) in
    Buffer.add_char b '\"';
    let rec loop i =
      if i = l then Buffer.add_char b '\"' else
      match s.[i] with
      | '\"' -> loop_bs 0 i;
      | '\\' -> loop_bs 0 i;
      | c    -> Buffer.add_char b c; loop (i+1);
    and loop_bs n i =
      if i = l then begin
        Buffer.add_char b '\"';
        add_bs n;
      end else begin
        match s.[i] with
        | '\"' -> add_bs (2*n+1); Buffer.add_char b '\"'; loop (i+1);
        | '\\' -> loop_bs (n+1) (i+1);
        | _    -> add_bs n; loop i
      end
    and add_bs n = for _j = 1 to n do Buffer.add_char b '\\'; done
    in
    loop 0;
    Buffer.contents b
  let has_drive s =
    let is_letter = function
      | 'A' .. 'Z' | 'a' .. 'z' -> true
      | _ -> false
    in
    String.length s >= 2 && is_letter s.[0] && s.[1] = ':'
  let drive_and_path s =
    if has_drive s
    then (String.sub s 0 2, String.sub s 2 (String.length s - 2))
    else ("", s)
  let dirname s =
    let (drive, path) = drive_and_path s in
    let dir = generic_dirname is_dir_sep current_dir_name path in
    drive ^ dir
  let basename s =
    let (_drive, path) = drive_and_path s in
    generic_basename is_dir_sep current_dir_name path
end

module Cygwin = struct
  let current_dir_name = "."
  let parent_dir_name = ".."
  let dir_sep = "/"
  let is_dir_sep = Win32.is_dir_sep
  let is_relative = Win32.is_relative
  let is_implicit = Win32.is_implicit
  let check_suffix = Win32.check_suffix
  let temp_dir_name = Unix.temp_dir_name
  let quote = Unix.quote
  let basename = generic_basename is_dir_sep current_dir_name
  let dirname = generic_dirname is_dir_sep current_dir_name
end

 let (current_dir_name, parent_dir_name, dir_sep, is_dir_sep,
     is_relative, is_implicit, check_suffix, temp_dir_name, quote, basename,
     dirname) =
  match Sys.os_type with
  | "Win32" ->
      (Win32.current_dir_name, Win32.parent_dir_name, Win32.dir_sep,
       Win32.is_dir_sep,
       Win32.is_relative, Win32.is_implicit, Win32.check_suffix,
       Win32.temp_dir_name, Win32.quote, Win32.basename, Win32.dirname)
  | "Cygwin" ->
      (Cygwin.current_dir_name, Cygwin.parent_dir_name, Cygwin.dir_sep,
       Cygwin.is_dir_sep,
       Cygwin.is_relative, Cygwin.is_implicit, Cygwin.check_suffix,
       Cygwin.temp_dir_name, Cygwin.quote, Cygwin.basename, Cygwin.dirname)
  | _ -> (* normally "Unix" *)
      (Unix.current_dir_name, Unix.parent_dir_name, Unix.dir_sep,
       Unix.is_dir_sep,
       Unix.is_relative, Unix.is_implicit, Unix.check_suffix,
       Unix.temp_dir_name, Unix.quote, Unix.basename, Unix.dirname)

let concat dirname filename =
  let l = String.length dirname in
  if l = 0 || is_dir_sep dirname (l-1)
  then dirname ^ filename
  else dirname ^ dir_sep ^ filename

let chop_suffix name suff =
  let n = String.length name - String.length suff in
  if n < 0 then invalid_arg "Filename.chop_suffix" else String.sub name 0 n

let extension_len name =
  let rec check i0 i =
    if i < 0 || is_dir_sep name i then 0
    else if name.[i] = '.' then check i0 (i - 1)
    else String.length name - i0
  in
  let rec search_dot i =
    if i < 0 || is_dir_sep name i then 0
    else if name.[i] = '.' then check i (i - 1)
    else search_dot (i - 1)
  in
  search_dot (String.length name - 1)

let extension name =
  let l = extension_len name in
  if l = 0 then "" else String.sub name (String.length name - l) l

let chop_extension name =
  let l = extension_len name in
  if l = 0 then invalid_arg "Filename.chop_extension"
  else String.sub name 0 (String.length name - l)

let remove_extension name =
  let l = extension_len name in
  if l = 0 then name else String.sub name 0 (String.length name - l)

external open_desc: string -> open_flag list -> int -> int = "caml_sys_open"
external close_desc: int -> unit = "caml_sys_close"

let prng = lazy(Random.State.make_self_init ())

let temp_file_name temp_dir prefix suffix =
  let rnd = (Random.State.bits (Lazy.force prng)) land 0xFFFFFF in
  concat temp_dir (Printf.sprintf "%s%06x%s" prefix rnd suffix)


let current_temp_dir_name = ref temp_dir_name

let set_temp_dir_name s = current_temp_dir_name := s
let get_temp_dir_name () = !current_temp_dir_name

let temp_file ?(temp_dir = !current_temp_dir_name) prefix suffix =
  let rec try_name counter =
    let name = temp_file_name temp_dir prefix suffix in
    try
      close_desc(open_desc name [Open_wronly; Open_creat; Open_excl] 0o600);
      name
    with Sys_error _ as e ->
      if counter >= 1000 then raise e else try_name (counter + 1)
  in try_name 0

let open_temp_file ?(mode = [Open_text]) ?(perms = 0o600)
                   ?(temp_dir = !current_temp_dir_name) prefix suffix =
  let rec try_name counter =
    let name = temp_file_name temp_dir prefix suffix in
    try
      (name,
       open_out_gen (Open_wronly::Open_creat::Open_excl::mode) perms name)
    with Sys_error _ as e ->
      if counter >= 1000 then raise e else try_name (counter + 1)
  in try_name 0
end

module Marshal = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1997 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

type extern_flags =
    No_sharing
  | Closures
  | Compat_32
(* note: this type definition is used in 'byterun/debugger.c' *)

external to_channel: out_channel -> 'a -> extern_flags list -> unit
    = "caml_output_value"
external to_bytes: 'a -> extern_flags list -> bytes
    = "caml_output_value_to_bytes"
external to_string: 'a -> extern_flags list -> string
    = "caml_output_value_to_string"
external to_buffer_unsafe:
      bytes -> int -> int -> 'a -> extern_flags list -> int
    = "caml_output_value_to_buffer"

let to_buffer buff ofs len v flags =
  if ofs < 0 || len < 0 || ofs > Bytes.length buff - len
  then invalid_arg "Marshal.to_buffer: substring out of bounds"
  else to_buffer_unsafe buff ofs len v flags

(* The functions below use byte sequences as input, never using any
   mutation. It makes sense to use non-mutated [bytes] rather than
   [string], because we really work with sequences of bytes, not
   a text representation.
*)

external from_channel: in_channel -> 'a = "caml_input_value"
external from_bytes_unsafe: bytes -> int -> 'a = "caml_input_value_from_bytes"
external data_size_unsafe: bytes -> int -> int = "caml_marshal_data_size"

let header_size = 20
let data_size buff ofs =
  if ofs < 0 || ofs > Bytes.length buff - header_size
  then invalid_arg "Marshal.data_size"
  else data_size_unsafe buff ofs
let total_size buff ofs = header_size + data_size buff ofs

let from_bytes buff ofs =
  if ofs < 0 || ofs > Bytes.length buff - header_size
  then invalid_arg "Marshal.from_bytes"
  else begin
    let len = data_size_unsafe buff ofs in
    if ofs > Bytes.length buff - (header_size + len)
    then invalid_arg "Marshal.from_bytes"
    else from_bytes_unsafe buff ofs
  end

let from_string buff ofs =
  (* Bytes.unsafe_of_string is safe here, as the produced byte
     sequence is never mutated *)
  from_bytes (Bytes.unsafe_of_string buff) ofs
end

module Bigarray = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*           Manuel Serrano et Xavier Leroy, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 2000 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Module [Bigarray]: large, multi-dimensional, numerical arrays *)

(* These types in must be kept in sync with the tables in
   ../typing/typeopt.ml *)

type float32_elt = Float32_elt
type float64_elt = Float64_elt
type int8_signed_elt = Int8_signed_elt
type int8_unsigned_elt = Int8_unsigned_elt
type int16_signed_elt = Int16_signed_elt
type int16_unsigned_elt = Int16_unsigned_elt
type int32_elt = Int32_elt
type int64_elt = Int64_elt
type int_elt = Int_elt
type nativeint_elt = Nativeint_elt
type complex32_elt = Complex32_elt
type complex64_elt = Complex64_elt

type ('a, 'b) kind =
    Float32 : (float, float32_elt) kind
  | Float64 : (float, float64_elt) kind
  | Int8_signed : (int, int8_signed_elt) kind
  | Int8_unsigned : (int, int8_unsigned_elt) kind
  | Int16_signed : (int, int16_signed_elt) kind
  | Int16_unsigned : (int, int16_unsigned_elt) kind
  | Int32 : (int32, int32_elt) kind
  | Int64 : (int64, int64_elt) kind
  | Int : (int, int_elt) kind
  | Nativeint : (nativeint, nativeint_elt) kind
  | Complex32 : (Complex.t, complex32_elt) kind
  | Complex64 : (Complex.t, complex64_elt) kind
  | Char : (char, int8_unsigned_elt) kind

type c_layout = C_layout_typ
type fortran_layout = Fortran_layout_typ (**)

type 'a layout =
    C_layout: c_layout layout
  | Fortran_layout: fortran_layout layout

(* Keep those constants in sync with the caml_ba_kind enumeration
   in bigarray.h *)

let float32 = Float32
let float64 = Float64
let int8_signed = Int8_signed
let int8_unsigned = Int8_unsigned
let int16_signed = Int16_signed
let int16_unsigned = Int16_unsigned
let int32 = Int32
let int64 = Int64
let int = Int
let nativeint = Nativeint
let complex32 = Complex32
let complex64 = Complex64
let char = Char

let kind_size_in_bytes : type a b. (a, b) kind -> int = function
  | Float32 -> 4
  | Float64 -> 8
  | Int8_signed -> 1
  | Int8_unsigned -> 1
  | Int16_signed -> 2
  | Int16_unsigned -> 2
  | Int32 -> 4
  | Int64 -> 8
  | Int -> Sys.word_size / 8
  | Nativeint -> Sys.word_size / 8
  | Complex32 -> 8
  | Complex64 -> 16
  | Char -> 1

(* Keep those constants in sync with the caml_ba_layout enumeration
   in bigarray.h *)

let c_layout = C_layout
let fortran_layout = Fortran_layout

module Genarray = struct
  type ('a, 'b, 'c) t
  external create: ('a, 'b) kind -> 'c layout -> int array -> ('a, 'b, 'c) t
     = "caml_ba_create"
  external get: ('a, 'b, 'c) t -> int array -> 'a
     = "caml_ba_get_generic"
  external set: ('a, 'b, 'c) t -> int array -> 'a -> unit
     = "caml_ba_set_generic"
  external num_dims: ('a, 'b, 'c) t -> int = "caml_ba_num_dims"
  external nth_dim: ('a, 'b, 'c) t -> int -> int = "caml_ba_dim"
  let dims a =
    let n = num_dims a in
    let d = Array.make n 0 in
    for i = 0 to n-1 do d.(i) <- nth_dim a i done;
    d

  external kind: ('a, 'b, 'c) t -> ('a, 'b) kind = "caml_ba_kind"
  external layout: ('a, 'b, 'c) t -> 'c layout = "caml_ba_layout"
  external change_layout: ('a, 'b, 'c) t -> 'd layout -> ('a, 'b, 'd) t
     = "caml_ba_change_layout"

  let size_in_bytes arr =
    (kind_size_in_bytes (kind arr)) * (Array.fold_left ( * ) 1 (dims arr))

  external sub_left: ('a, 'b, c_layout) t -> int -> int -> ('a, 'b, c_layout) t
     = "caml_ba_sub"
  external sub_right: ('a, 'b, fortran_layout) t -> int -> int ->
                          ('a, 'b, fortran_layout) t
     = "caml_ba_sub"
  external slice_left: ('a, 'b, c_layout) t -> int array ->
                          ('a, 'b, c_layout) t
     = "caml_ba_slice"
  external slice_right: ('a, 'b, fortran_layout) t -> int array ->
                          ('a, 'b, fortran_layout) t
     = "caml_ba_slice"
  external blit: ('a, 'b, 'c) t -> ('a, 'b, 'c) t -> unit
     = "caml_ba_blit"
  external fill: ('a, 'b, 'c) t -> 'a -> unit = "caml_ba_fill"
end

module Array0 = struct
  type ('a, 'b, 'c) t = ('a, 'b, 'c) Genarray.t
  let create kind layout =
    Genarray.create kind layout [||]
  let get arr = Genarray.get arr [||]
  let set arr = Genarray.set arr [||]
  external kind: ('a, 'b, 'c) t -> ('a, 'b) kind = "caml_ba_kind"
  external layout: ('a, 'b, 'c) t -> 'c layout = "caml_ba_layout"

  external change_layout: ('a, 'b, 'c) t -> 'd layout -> ('a, 'b, 'd) t
    = "caml_ba_change_layout"

  let size_in_bytes arr = kind_size_in_bytes (kind arr)

  external blit: ('a, 'b, 'c) t -> ('a, 'b, 'c) t -> unit = "caml_ba_blit"
  external fill: ('a, 'b, 'c) t -> 'a -> unit = "caml_ba_fill"

  let of_value kind layout v =
    let a = create kind layout in
    set a v;
    a
end

module Array1 = struct
  type ('a, 'b, 'c) t = ('a, 'b, 'c) Genarray.t
  let create kind layout dim =
    Genarray.create kind layout [|dim|]
  external get: ('a, 'b, 'c) t -> int -> 'a = "%caml_ba_ref_1"
  external set: ('a, 'b, 'c) t -> int -> 'a -> unit = "%caml_ba_set_1"
  external unsafe_get: ('a, 'b, 'c) t -> int -> 'a = "%caml_ba_unsafe_ref_1"
  external unsafe_set: ('a, 'b, 'c) t -> int -> 'a -> unit
     = "%caml_ba_unsafe_set_1"
  external dim: ('a, 'b, 'c) t -> int = "%caml_ba_dim_1"
  external kind: ('a, 'b, 'c) t -> ('a, 'b) kind = "caml_ba_kind"
  external layout: ('a, 'b, 'c) t -> 'c layout = "caml_ba_layout"

  external change_layout: ('a, 'b, 'c) t -> 'd layout -> ('a, 'b, 'd) t
    = "caml_ba_change_layout"

  let size_in_bytes arr =
    (kind_size_in_bytes (kind arr)) * (dim arr)

  external sub: ('a, 'b, 'c) t -> int -> int -> ('a, 'b, 'c) t = "caml_ba_sub"
  let slice (type t) (a : (_, _, t) Genarray.t) n =
    match layout a with
    | C_layout -> (Genarray.slice_left a [|n|] : (_, _, t) Genarray.t)
    | Fortran_layout -> (Genarray.slice_right a [|n|]: (_, _, t) Genarray.t)
  external blit: ('a, 'b, 'c) t -> ('a, 'b, 'c) t -> unit = "caml_ba_blit"
  external fill: ('a, 'b, 'c) t -> 'a -> unit = "caml_ba_fill"
  let of_array (type t) kind (layout: t layout) data =
    let ba = create kind layout (Array.length data) in
    let ofs =
      match layout with
        C_layout -> 0
      | Fortran_layout -> 1
    in
    for i = 0 to Array.length data - 1 do unsafe_set ba (i + ofs) data.(i) done;
    ba
end

module Array2 = struct
  type ('a, 'b, 'c) t = ('a, 'b, 'c) Genarray.t
  let create kind layout dim1 dim2 =
    Genarray.create kind layout [|dim1; dim2|]
  external get: ('a, 'b, 'c) t -> int -> int -> 'a = "%caml_ba_ref_2"
  external set: ('a, 'b, 'c) t -> int -> int -> 'a -> unit = "%caml_ba_set_2"
  external unsafe_get: ('a, 'b, 'c) t -> int -> int -> 'a
     = "%caml_ba_unsafe_ref_2"
  external unsafe_set: ('a, 'b, 'c) t -> int -> int -> 'a -> unit
     = "%caml_ba_unsafe_set_2"
  external dim1: ('a, 'b, 'c) t -> int = "%caml_ba_dim_1"
  external dim2: ('a, 'b, 'c) t -> int = "%caml_ba_dim_2"
  external kind: ('a, 'b, 'c) t -> ('a, 'b) kind = "caml_ba_kind"
  external layout: ('a, 'b, 'c) t -> 'c layout = "caml_ba_layout"

  external change_layout: ('a, 'b, 'c) t -> 'd layout -> ('a, 'b, 'd) t
    = "caml_ba_change_layout"

  let size_in_bytes arr =
    (kind_size_in_bytes (kind arr)) * (dim1 arr) * (dim2 arr)

  external sub_left: ('a, 'b, c_layout) t -> int -> int -> ('a, 'b, c_layout) t
     = "caml_ba_sub"
  external sub_right:
    ('a, 'b, fortran_layout) t -> int -> int -> ('a, 'b, fortran_layout) t
     = "caml_ba_sub"
  let slice_left a n = Genarray.slice_left a [|n|]
  let slice_right a n = Genarray.slice_right a [|n|]
  external blit: ('a, 'b, 'c) t -> ('a, 'b, 'c) t -> unit = "caml_ba_blit"
  external fill: ('a, 'b, 'c) t -> 'a -> unit = "caml_ba_fill"
  let of_array (type t) kind (layout: t layout) data =
    let dim1 = Array.length data in
    let dim2 = if dim1 = 0 then 0 else Array.length data.(0) in
    let ba = create kind layout dim1 dim2 in
    let ofs =
      match layout with
        C_layout -> 0
      | Fortran_layout -> 1
    in
    for i = 0 to dim1 - 1 do
      let row = data.(i) in
      if Array.length row <> dim2 then
        invalid_arg("Bigarray.Array2.of_array: non-rectangular data");
      for j = 0 to dim2 - 1 do
        unsafe_set ba (i + ofs) (j + ofs) row.(j)
      done
    done;
    ba
end

module Array3 = struct
  type ('a, 'b, 'c) t = ('a, 'b, 'c) Genarray.t
  let create kind layout dim1 dim2 dim3 =
    Genarray.create kind layout [|dim1; dim2; dim3|]
  external get: ('a, 'b, 'c) t -> int -> int -> int -> 'a = "%caml_ba_ref_3"
  external set: ('a, 'b, 'c) t -> int -> int -> int -> 'a -> unit
     = "%caml_ba_set_3"
  external unsafe_get: ('a, 'b, 'c) t -> int -> int -> int -> 'a
     = "%caml_ba_unsafe_ref_3"
  external unsafe_set: ('a, 'b, 'c) t -> int -> int -> int -> 'a -> unit
     = "%caml_ba_unsafe_set_3"
  external dim1: ('a, 'b, 'c) t -> int = "%caml_ba_dim_1"
  external dim2: ('a, 'b, 'c) t -> int = "%caml_ba_dim_2"
  external dim3: ('a, 'b, 'c) t -> int = "%caml_ba_dim_3"
  external kind: ('a, 'b, 'c) t -> ('a, 'b) kind = "caml_ba_kind"
  external layout: ('a, 'b, 'c) t -> 'c layout = "caml_ba_layout"

  external change_layout: ('a, 'b, 'c) t -> 'd layout -> ('a, 'b, 'd) t
    = "caml_ba_change_layout"

  let size_in_bytes arr =
    (kind_size_in_bytes (kind arr)) * (dim1 arr) * (dim2 arr) * (dim3 arr)

  external sub_left: ('a, 'b, c_layout) t -> int -> int -> ('a, 'b, c_layout) t
     = "caml_ba_sub"
  external sub_right:
     ('a, 'b, fortran_layout) t -> int -> int -> ('a, 'b, fortran_layout) t
     = "caml_ba_sub"
  let slice_left_1 a n m = Genarray.slice_left a [|n; m|]
  let slice_right_1 a n m = Genarray.slice_right a [|n; m|]
  let slice_left_2 a n = Genarray.slice_left a [|n|]
  let slice_right_2 a n = Genarray.slice_right a [|n|]
  external blit: ('a, 'b, 'c) t -> ('a, 'b, 'c) t -> unit = "caml_ba_blit"
  external fill: ('a, 'b, 'c) t -> 'a -> unit = "caml_ba_fill"
  let of_array (type t) kind (layout: t layout) data =
    let dim1 = Array.length data in
    let dim2 = if dim1 = 0 then 0 else Array.length data.(0) in
    let dim3 = if dim2 = 0 then 0 else Array.length data.(0).(0) in
    let ba = create kind layout dim1 dim2 dim3 in
    let ofs =
      match layout with
        C_layout -> 0
      | Fortran_layout -> 1
    in
    for i = 0 to dim1 - 1 do
      let row = data.(i) in
      if Array.length row <> dim2 then
        invalid_arg("Bigarray.Array3.of_array: non-cubic data");
      for j = 0 to dim2 - 1 do
        let col = row.(j) in
        if Array.length col <> dim3 then
          invalid_arg("Bigarray.Array3.of_array: non-cubic data");
        for k = 0 to dim3 - 1 do
          unsafe_set ba (i + ofs) (j + ofs) (k + ofs) col.(k)
        done
      done
    done;
    ba
end

external genarray_of_array0: ('a, 'b, 'c) Array0.t -> ('a, 'b, 'c) Genarray.t
   = "%identity"
external genarray_of_array1: ('a, 'b, 'c) Array1.t -> ('a, 'b, 'c) Genarray.t
   = "%identity"
external genarray_of_array2: ('a, 'b, 'c) Array2.t -> ('a, 'b, 'c) Genarray.t
   = "%identity"
external genarray_of_array3: ('a, 'b, 'c) Array3.t -> ('a, 'b, 'c) Genarray.t
   = "%identity"
let array0_of_genarray a =
  if Genarray.num_dims a = 0 then a
  else invalid_arg "Bigarray.array0_of_genarray"
let array1_of_genarray a =
  if Genarray.num_dims a = 1 then a
  else invalid_arg "Bigarray.array1_of_genarray"
let array2_of_genarray a =
  if Genarray.num_dims a = 2 then a
  else invalid_arg "Bigarray.array2_of_genarray"
let array3_of_genarray a =
  if Genarray.num_dims a = 3 then a
  else invalid_arg "Bigarray.array3_of_genarray"

external reshape:
   ('a, 'b, 'c) Genarray.t -> int array -> ('a, 'b, 'c) Genarray.t
   = "caml_ba_reshape"
let reshape_0 a = reshape a [||]
let reshape_1 a dim1 = reshape a [|dim1|]
let reshape_2 a dim1 dim2 = reshape a [|dim1;dim2|]
let reshape_3 a dim1 dim2 dim3 = reshape a [|dim1;dim2;dim3|]

(* Force caml_ba_get_{1,2,3,N} to be linked in, since we don't refer
   to those primitives directly in this file *)

let _ =
  let _ = Genarray.get in
  let _ = Array1.get in
  let _ = Array2.get in
  let _ = Array3.get in
  ()

[@@@ocaml.warning "-32"]
external get1: unit -> unit = "caml_ba_get_1"
external get2: unit -> unit = "caml_ba_get_2"
external get3: unit -> unit = "caml_ba_get_3"
external set1: unit -> unit = "caml_ba_set_1"
external set2: unit -> unit = "caml_ba_set_2"
external set3: unit -> unit = "caml_ba_set_3"
end

(* module MoreLabels = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                Jacques Garrigue, Kyoto University RIMS                 *)
(*                                                                        *)
(*   Copyright 2001 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Module [MoreLabels]: meta-module for compatibility labelled libraries *)

module Hashtbl = Hashtbl

module Map = Map

module Set = Set
end *)

module StdLabels = struct
(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                Jacques Garrigue, Kyoto University RIMS                 *)
(*                                                                        *)
(*   Copyright 2001 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Module [StdLabels]: meta-module for labelled libraries *)

module Array = ArrayLabels

module List = ListLabels

module String = StringLabels

module Bytes = BytesLabels
end