let fibo x =
  let rec fib x acc1 acc2 =
    match x with
    | 0 -> acc2
    | x -> fib (x-1) acc2 (acc1+acc2)
in fib x 1 0;;

let a = fibo 2;;
let b = fibo 3;;
let c = fibo 4;;

let result = (a, b, c);;