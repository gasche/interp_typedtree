let f x =
  match x with
  | 0 -> 0
  | _ -> 1

let a = f 1;;

let g x =
  if x = 0 then x else 1;;

(* let c = h 1;; *)

let h x =
  let y = ref 1 in
  while (!y != 0)
  do y := 0 done;
  1;;

(* let d = w 1;; *)

let f1 x =
  let y = ref x in
  for i = 0 to 10 do
    y := !y + 1 done;
  y;;

(* let e = f1 1;; *)