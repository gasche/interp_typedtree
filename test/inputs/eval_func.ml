let f x = x;;

let y = f 1;;

let g = function
  | None -> 0
  | Some n -> f y

let rec h = function
  | 0 -> 0
  | n -> h 0

let z = h y;;

assert(z=0);;