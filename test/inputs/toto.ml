let f x = 1;;
let g x = (fun _ -> 1) x;;
let h x =
  let y = 1
  in y;;

let a = f 0 
let b = g (f 0)
let c = g (f 1)