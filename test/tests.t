  $ interp_typedtree -dsource inputs/empty.ml
  
  

  $ interp_typedtree -dsource inputs/constant.ml
  ;;1
  ;;1l
  ;;1L
  ;;1n
  ;;'a'
  ;;"a"
  ;;1.0
  

  $ interp_typedtree inputs/var.ml
  val a = a
  val b = "a"
  val c = 1.000000
  val t = (a, "a")
  val x = 1n
  val y = 1l
  val z = 1L
  

  $ interp_typedtree -dsource inputs/lib.ml
  external caml_ml_open_descriptor_out :
    int -> out_channel = "caml_ml_open_descriptor_out"
  external caml_ml_output :
    out_channel -> string -> int -> int -> unit = "caml_ml_output"
  external caml_ml_flush : out_channel -> unit = "caml_ml_flush"
  external caml_ml_bytes_length : string -> int = "%bytes_length"
  external format_int : string -> int -> string = "caml_format_int"
  external (~-) : int -> int = "%negint"
  external (+) : int -> int -> int = "%addint"
  external (-) : int -> int -> int = "%subint"
  external ( * ) : int -> int -> int = "%mulint"
  external (/) : int -> int -> int = "%divint"
  external (mod) : int -> int -> int = "%modint"
  external (land) : int -> int -> int = "%andint"
  external (lor) : int -> int -> int = "%orint"
  external (lxor) : int -> int -> int = "%xorint"
  external (lsl) : int -> int -> int = "%lslint"
  external (lsr) : int -> int -> int = "%lsrint"
  external (asr) : int -> int -> int = "%asrint"
  external (=) : 'a -> 'a -> bool = "%equal"
  external (<>) : 'a -> 'a -> bool = "%notequal"
  external (>) : 'a -> 'a -> bool = "%greaterthan"
  external (>=) : 'a -> 'a -> bool = "%greaterequal"
  external (<) : 'a -> 'a -> bool = "%lessthan"
  external (<=) : 'a -> 'a -> bool = "%lessequal"
  external raise : exn -> 'a = "%raise"
  let failwith s = raise (Failure s)
  let invalid_arg s = raise (Invalid_argument s)
  let assert_ b = if b then () else raise (Assert_failure ("", 0, 0))
  let fst (a, b) = a
  let snd (a, b) = b
  let stdout = caml_ml_open_descriptor_out 1
  let flush () = caml_ml_flush stdout
  let print_string s = caml_ml_output stdout s 0 (caml_ml_bytes_length s)
  let print_int n = print_string (format_int "%d" n)
  let show_int n = print_string " "; print_int n
  let print_newline () = print_string "\n"; flush ()
  let print_endline s = print_string s; print_string "\n"; flush ()
  type bool =
    | false 
    | true 
  type 'a list =
    | [] 
    | (::) of 'a * 'a list 
  type 'a option =
    | None 
    | Some of 'a 
  type 'a t = ('a * int)
  type 'a ref = {
    mutable contents: 'a }
  let ref x = { contents = x }
  let (!) x = x.contents
  let (:=) x v = x.contents <- v
  let (@@) f x = f x
  let (|>) x f = f x
  let __atexit () = flush ()
  val ! = <function>
  val * = <primitive>
  val + = <primitive>
  val - = <primitive>
  val / = <primitive>
  val := = <function>
  val < = <primitive>
  val <= = <primitive>
  val <> = <primitive>
  val = = <primitive>
  val > = <primitive>
  val >= = <primitive>
  val @@ = <function>
  val __atexit = <function>
  val asr = <primitive>
  val assert_ = <function>
  val caml_ml_bytes_length = <primitive>
  val caml_ml_flush = <primitive>
  val caml_ml_open_descriptor_out = <primitive>
  val caml_ml_output = <primitive>
  val failwith = <function>
  val flush = <function>
  val format_int = <primitive>
  val fst = <function>
  val invalid_arg = <function>
  val land = <primitive>
  val lor = <primitive>
  val lsl = <primitive>
  val lsr = <primitive>
  val lxor = <primitive>
  val mod = <primitive>
  val print_endline = <function>
  val print_int = <function>
  val print_newline = <function>
  val print_string = <function>
  val raise = <primitive>
  val ref = <function>
  val show_int = <function>
  val snd = <function>
  val stdout = <out_channel>
  val |> = <function>
  val ~- = <primitive>
  

  $ interp_typedtree inputs/lib.ml inputs/arith.ml
  Arithmetic:
   42 29 2 6 1 1 1 7 6 14 3 3 4611686018427387903 4611686018427387903 -1
   1 2 3
   1 2 3
   255 255 255 255 255 42 42 42 42
  9223372036854775807
  2147483647
  9223372036854775807
  
  module Arith = <module>
  module Lib = <module>

  $ interp_typedtree inputs/lib.ml inputs/functions.ml
  Functions:
  simple:  3 3 3
  currified:  42
  higher-order:  42 65536 42 17 42
  local:  42
  recursive:  45 20 25 0
  let-binding tests:  17 42 17 42 42
  more recursion:  10 9 8 7 6 5 4 3 2 1
  general function application:  42
  
  module Functions = <module>
  module Lib = <module>



  $ interp_typedtree inputs/lib.ml inputs/functors.ml
  Functors:
   only once 42 24 ok
  High-order functors:
   4
  
  module Functors = <module>
  module Lib = <module>

  $ interp_typedtree inputs/lib.ml inputs/lists.ml
  Lists:
  [ 1 2 3 4 5 6 7 8 9]
  [ 1; 2; 3; 4; 5; 6; 7; 8; 9]
  [ 2; 3; 4; 5; 6; 7; 8; 9; 10][ 2; 4; 6]
  
  module Lib = <module>
  module Lists = <module>



  $ interp_typedtree inputs/lib.ml inputs/records.ml
  Records:
  simple:  5 7 7 5
  with:  5 7 42 7 5 16
  record field punning:  1 2
  inline records:  1 2 3
  
  module Lib = <module>
  module Records = <module>


  $ interp_typedtree inputs/lib.ml inputs/exceptions.ml
  Exceptions:
   ok ok ok ok ok ok ok ok ok E1 (E2 7) <unknown> <unknown> 8
  
  module Exceptions = <module>
  module Lib = <module>


  $ interp_typedtree inputs/lib.ml inputs/infix_sugar.ml
  Infix operators treated as sugar:
   2 3
  
  module Infix_sugar = <module>
  module Lib = <module>


  $ interp_typedtree inputs/lib.ml inputs/loops.ml
  nonempty while
   42 21 10 5 2 1
  empty while
  
  nonempty for (up)
   0 1 2 3 4 5 6 7 8 9 10
  nonempty for (down)
   10 9 8 7 6 5 4 3 2 1 0
  one-iteration for
   42 42
  empty for
  
  
  module Lib = <module>
  module Loops = <module>

  $ interp_typedtree inputs/lib.ml inputs/patterns.ml


$ OCAMLINTERP_STDLIB_PATH=inputs/stdlib-4.07 OCAMLINTERP_DEBUG=1 interp_typedtree -ocamlc -nostdlib -nopervasives
