open Interp_typedtree

module C = Conf
module D = Data
module Envir = Envir
module E = Eval
module I = Interp
module P = Primitives
module T = Typing

let check = ref true
let printer = ref false
let ocaml_stdlib = ref false
let ocaml_compiler = ref false

let usage_msg =
  Printf.sprintf
"Expected usage:\n\
$ interp_typedtree <flags> foo/bar/toto.ml\n\
(we expect that the activated opam switch contains a compatible stdlib)
"

let () =
  (* disable production of .cmi files during
     type-checking *)
  Ocaml_common.Clflags.dont_write_files := true;

  (* default printing choices, overridden by flags below *)
  Ocaml_common.Clflags.locations := false;
  Ocaml_common.Clflags.dump_source := false;
  Ocaml_common.Clflags.dump_typedtree := false;
  Ocaml_common.Clflags.dump_parsetree := false;

  ()

let interp files =
  if !ocaml_stdlib then
    Interp.ocaml_stdlib ~print:!printer ~check:!check
  else if !ocaml_compiler then
    Interp.ocaml_compiler ~print:!printer ~check:!check
  else
    let rec pathing paths files =
      if not (Queue.is_empty files) then
        let path = Queue.pop files in
        pathing (([], path)::paths) files
      else
        paths
    in
    let paths = List.rev (pathing [] files) in
    Interp.interp ~print:!printer ~check:!check paths

let args_spec = [
  "-nostdlib", Arg.Set Ocaml_common.Clflags.no_std_include,
    " do not implicitly load the standard library modules";
  "-nopervasives", Arg.Set Ocaml_common.Clflags.nopervasives,
    " do not implicitly load stdlib.ml";
  "-dsource", Arg.Set Ocaml_common.Clflags.dump_source,
    " pretty-print the program source code";
  "-dparsetree", Arg.Set Ocaml_common.Clflags.dump_parsetree,
    " print the untyped AST";
  "-dtypedtree", Arg.Set printer,
    " print the typed AST";
  "-typechecker", Arg.Clear check,
    " use the typed-checking version of Ocaml";
  "-ocaml_stdlib", Arg.Set ocaml_stdlib,
    " compile stdlib to bytecode";
  "-ocaml_compiler", Arg.Set ocaml_compiler,
    " compile compiler to bytecode";
]

let () =
  let paths = Queue.create () in
  Arg.parse
    args_spec
    (fun path -> Queue.push path paths)
    usage_msg;
  if Queue.is_empty paths && not !ocaml_stdlib && not !ocaml_compiler then
    Arg.usage args_spec usage_msg
  else
    interp paths

